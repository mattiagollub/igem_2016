# ETH Zurich iGEM 2016 Model#

## [Pavlov's Coli](http://2016.igem.org/Team:ETH_Zurich) ##

This repository contains the MATLAB files used by the ETH Zurich team for the modeling work during the iGEM 2016 competition. Although most of the scripts is specific to our project and undocumented, we wrote some generic code for simulating biochemical networks that we believe can be reused easily by other teams. These methods are listed and explained in the following sections.

The standard unit is *nM*, stochastic simulation internally use molecule numbers but store results in *nM*. Conversion between *nM* and molecule numbers are performed using `n2nM()` and `nM2n()`, which assume the approximate cell volume of *E.Coli* (1μm).

Please refer to our [modeling](http://2016.igem.org/Team:ETH_Zurich/Model) sections for the results. In case you have problems trying to use our code we would be glad to help you. (especially if you are an iGEM team!)

### Model definition ###

We used a common format for defining our models, this allowed us to define methods for simulating and analyzing several models. Examples of this format can be seen in `createSwitchV1MAmodel.m` and `createAndGateAHLpartMAmodel.m`.

All fields shown in the examples above are mandatory, here are the most important:

* `model.c0`: Initial concentration of the species.
* `model.inputs`: Name of the input species for the model.
* `model.specieNames`: Names of the species. These must be symbolic variables in order to allow sensitivity analysis and faster simulation.
* `model.paramNames`: Names of the parameters. These must be symbolic variables in order to allow sensitivity analysis and faster simulation.
* `model.N`: The stoichiometric matrix of the system. Row `i` corresponds to the entries for specie `model.specieNames(i)`.
* `model.rates`: The reaction rates used for ODE simulations.
* `model.a`: The reaction propensities used for stochastic simulations.
* `model.paramsSet`: Path to a file containing the parameters of the model. See `parameters/switch_literature.xlsx` for an example of the format.

Models can be linked and simulated together using `linkModels.m`, an example can be found in `linkedSSAplot.m`. This is particularly useful for reusing models that are used in different constructs (e.g. a protein production model can be linked to different promoter models).

### Simulation methods ###

Two kinds of simulation are supported. Please refer to the documentation of the methods for more precise instructions about how to use them.

* **Deterministic** (ODE): Defined in `deterministicSimulation.m`, simulates a model deterministically over a time interval. If requested, this method also performs a sensitivity analysis.  
+ **Stochastic** (SDE): We implemented three different methods. The code of the simulation is compiled to C thanks to the MATLAB MEX compiler, which is much faster than plain MATLAB code.
    * `stochasticSimulationFRM.m` implements the *First Reaction Method* variant of the Gillespie algorithm.
    * `stochasticSimulationNRM.m` implements the *Next Reaction Method* variant of the Gillespie algorithm and is considerably faster of the FRM.
    * `stochasticSimulationTDI.m` implements a variant of the *Next Reaction Method* which supports reactions with input parameters varying over time. The method works well for our case, but due to limitations of the MATLAB language it cannot be extended to support an arbitrary number of such reactions. In this implementation a maximum on three reactions with time-dependent parameters is supported.

### Plotting ###

After a simulation, the results are stored in the model:

* `model.speciesMap`: Map containing the concentrations at each time point for every specie.
* `model.timesMap`: Map containing the times at which the concentrations are measured for every specie.
* `model.histogramsMap`: Map containing the distribution of the number of molecules at each time point for every specie. 

For example you can plot the time evolution of the concentration of specie *GFP* with:
`plot(model.timesMap('GFP'), model.speciesMap('GFP'));`

Results of the sensitivity analysis for specie *specie* towards parameter *param* are stored in `model.speciesMap('S_specie_param')`.