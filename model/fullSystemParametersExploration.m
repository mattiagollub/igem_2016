% clear;
% addpath('HYPERSPACE/Files');
% 


        %%
clear;
addpath('HYPERSPACE');
addpath('HYPERSPACE/Files');
addpath('mexFunctions');

%% 
globalizeModelODE()
 %%
%cost = parameterExploration([3, 3, 4, 0.1, 1.6]);
% OutM = MCexp('parameterExplorationV2', 0.01, [3 3 4 0.1 1.6],...
%               [10 10 10 1 10], [0 0 0 0 0], 100000)

cost=zeros(11,11,11);
step=0:1:5;
for i=1:1:length(step)
    for j=1:1:length(step)
        for k=1:1:length(step)
            param=[step(i),step(j),step(k)];
            cost(i,j,k)=parameterExplorationV2(param);
        end
    end 
end

%% plot
    % 3D graphics - Function of three variables
%     format compact
%     set(gcf,'Menubar','none','Name','Three Variables', ...
%         'NumberTitle','off','Position',[10,350,700,300], ...
%         'Color',[1 1 1]);
%     figure(7)
%         x = step;
%         y = step;
%         z = step;
% 
%         [X Y Z] = meshgrid(x,y,z);
%         scatter3(X(:),Y(:),Z(:),10,cost(:));
%         colorbar;
step1=0:1:10;        
for i=1:6
    figure(i)
    surf(step1,step1, cost(:,:,i));
    %meanMap = mesh([-2,-1,0,1,2],[-2,-1,0,1,2],cost);
    view(2);
    axis equal;
%     addTitle(meanMap, 'GFPnormal/GFPill as a function of the KnorProd and kesarProd tunble parameters');
%     addXLabel(meanMap, 'KnorProd');
%     addYLabel(meanMap, 'kesarProd');
    xlabel knor
    ylabel kesar
    zlabel cost
    title ('cost heatmap for dBcb1 =+i')

end