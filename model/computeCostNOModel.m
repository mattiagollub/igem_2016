function cost=computeCostNOModel(paramToExplore)

 global modelNO  TNO SNO NOparamMap 
 
kdnor=paramToExplore(1);
k_dnor=paramToExplore(2);
kl=paramToExplore(3);
knorProd=paramToExplore(4);
VectData=loadDatafromPlot();
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;
nP = 15;            % Number of plasmids
Ptot = n2nM(nP);% Avogadro constant

% Time range for the experiment
t0   = 0;
tf   = 60*6;
tspan = [t0, tf];

    points = 100;
    NO_stimulus = noinput(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.2/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
    
TNO('NO') = t_NO;
SNO('NO') = NO;

NOparamMap('kdnor')=paramToExplore(1);
NOparamMap('k_dnor')=paramToExplore(2);
NOparamMap('kl')=paramToExplore(3);
NOparamMap('knorProd')=paramToExplore(4);

deterministicSimulation([t0, tf], modelLac);
cost = norm(VectData-interp1(SNO('GFP')));
