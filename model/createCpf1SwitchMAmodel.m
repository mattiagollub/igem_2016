function model = createCpf1SwitchMAmodel()
%CREATECPF1SWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   Cpf1: The switch is based on the Cpf1 system
%
%       model = createCpf1SwitchMAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    S_tot = n2nM(nP);   % nM, Total concentration of plasmids
    
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0, 0, 0, 0, 0, n2nM(1), 0, 0, 0, 0, 0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'Phyb_ON'};
    
    %% Define symbolic variables for species and parameters
    syms Phyb_ON mRNAcpf1 Cpf1 Cpf1_I Cpf1_II C C_I C_II C_b C_a dead
    syms k_mRNAcpf1 d_mRNAcpf1 k_Cpf1 k_Cpf1S k_Cpf1S_ d_Cpf1 k_cut ...
         k_death k_join
    
    model.specieNames = [Phyb_ON mRNAcpf1 Cpf1 Cpf1_I Cpf1_II ...
                         C C_I C_II C_b C_a dead];
    model.paramNames = [k_mRNAcpf1 d_mRNAcpf1 k_Cpf1 k_Cpf1S k_Cpf1S_ ...
                        d_Cpf1 k_cut k_death k_join]; 
    
    %% Stoichiometric and rate matrices
    % Phyb_ON | mRNAcpf1 | Cpf1 | Cpf1_I | Cpf1_II | C | C_I | C_II | C_b | C_a | dead
    model.N = [
        0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0; % Phyb_ON -> Phyb_ON + mRNAcpf1
        0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0; % mRNAcpf1 ->
        0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0; % mRNAcpf1 -> mRNAcpf1 + Cpf1
        0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0; % Cpf1 + sgRNA_I -> Cpf1_I
        0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0; % Cpf1_I -> Cpf1 + sgRNA_I
        0,  0, -1,  0,  1,  0,  0,  0,  0,  0,  0; % Cpf1 + sgRNA_II -> Cpf1_II
        0,  0,  1,  0, -1,  0,  0,  0,  0,  0,  0; % Cpf1_II -> Cpf1 + sgRNA_II
        0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0; % Cpf1 ->
        0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0; % Cpf1_I ->
        0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0; % Cpf1_II ->
        0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0; % Cpf1_I + C -> Cpf1_I + C_I
        0,  0,  0,  0,  0, -1,  0,  1,  0,  0,  0; % Cpf1_II + C -> Cpf1_II + C_II
        0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0; % Cpf1_I + C_II -> Cpf1_I + C_b
        0,  0,  0,  0,  0,  0, -1,  0,  1,  0,  0; % Cpf1_II + C_I -> Cpf1_II + C_b
        0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1; % C_I -> dead
        0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  1; % C_II -> dead
        0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  1; % C_b -> dead
        0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0; % C_b + ligase -> C_a + ligase
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAcpf1 * Phyb_ON * S_tot;   % Phyb_ON -> Phyb_ON + mRNAcpf1
        d_mRNAcpf1 * mRNAcpf1;          % mRNAcpf1 ->
        k_Cpf1 * mRNAcpf1;              % mRNAcpf1 -> mRNAcpf1 + Cpf1
        k_Cpf1S * Cpf1;                 % Cpf1 + sgRNA_I -> Cpf1_I
        k_Cpf1S_ * Cpf1_I;              % Cpf1_I -> Cpf1 + sgRNA_I
        k_Cpf1S * Cpf1;                 % Cpf1 + sgRNA_II -> Cpf1_II
        k_Cpf1S_ * Cpf1_II;             % Cpf1_II -> Cpf1 + sgRNA_II
        d_Cpf1 * Cpf1;                  % Cpf1 ->
        d_Cpf1 * Cpf1_I;                % Cpf1_I ->
        d_Cpf1 * Cpf1_II;               % Cpf1_II ->
        k_cut * Cpf1_I*C;               % Cpf1_I + C -> Cpf1_I + C_I
        k_cut * Cpf1_II*C;              % Cpf1_II + C -> Cpf1_II + C_II
        k_cut * Cpf1_I*C_II;            % Cpf1_I + C_II -> Cpf1_I + C_b
        k_cut * Cpf1_II*C_I;            % Cpf1_II + C_I -> Cpf1_II + C_b
        k_death * C_I;                  % C_I -> dead
        k_death * C_II;                 % C_II -> dead
        k_death * C_b;                  % C_b -> dead
        k_join * C_b;                   % C_b + ligase -> C_a + ligase
    ];

    %% Reaction propensities
    model.a = [
        k_mRNAcpf1 * n2nM(Phyb_ON) * nP;    % Phyb_ON -> Phyb_ON + mRNAcpf1
        d_mRNAcpf1 * mRNAcpf1;              % mRNAcpf1 ->
        k_Cpf1 * mRNAcpf1;                  % mRNAcpf1 -> mRNAcpf1 + Cpf1
        k_Cpf1S * Cpf1;                     % Cpf1 + sgRNA_I -> Cpf1_I
        k_Cpf1S_ * Cpf1_I;                  % Cpf1_I -> Cpf1 + sgRNA_I
        k_Cpf1S * Cpf1;                     % Cpf1 + sgRNA_II -> Cpf1_II
        k_Cpf1S_ * Cpf1_II;                 % Cpf1_II -> Cpf1 + sgRNA_II
        d_Cpf1 * Cpf1;                      % Cpf1 ->
        d_Cpf1 * Cpf1_I;                    % Cpf1_I ->
        d_Cpf1 * Cpf1_II;                   % Cpf1_II ->
        n2nM(k_cut) * Cpf1_I * C;           % Cpf1_I + C -> Cpf1_I + C_I
        n2nM(k_cut) * Cpf1_II * C;          % Cpf1_II + C -> Cpf1_II + C_II
        n2nM(k_cut) * Cpf1_I * C_II;        % Cpf1_I + C_II -> Cpf1_I + C_b
        n2nM(k_cut) * Cpf1_II * C_I;        % Cpf1_II + C_I -> Cpf1_II + C_b
        k_death * C_I;                      % C_I -> dead
        k_death * C_II;                     % C_II -> dead
        k_death * C_b;                      % C_b -> dead
        k_join * C_b;                       % C_b + ligase -> C_a + ligase
	];
end