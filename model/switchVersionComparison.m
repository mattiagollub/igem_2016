% SWITCHVERSIONCOMPARISON Comparison between two different switch versions.
%   
%% Simulation settings
clear;

% Parameter set
params_set = 'switch_bonnet';

% Time range for the experiment
t0   = 0;
tf   = 6*60;

% Input: Phyb activity
stimulus_1 = 0.06;   % Assume leakiness
stimulus_2 = 1;   % Assume all promoters active
[t_Phyb_ON_1, v_Phyb_ON_1] = makeStimulusProfile(0.0, 1.0, stimulus_1, 100);
[t_Phyb_ON_2, v_Phyb_ON_2] = makeStimulusProfile(0.0, 1.0, stimulus_2, 100);
t_Phyb_ON_1 = t_Phyb_ON_1 * (tf - t0) + t0;
t_Phyb_ON_2 = t_Phyb_ON_2 * (tf - t0) + t0;

%% Setup models
modelV1 = createSwitchV1MAmodel();
modelV2 = createSwitchV2MAmodel();

TV1 = modelV1.timesMap;
SV1 = modelV1.speciesMap;
TV2 = modelV2.timesMap;
SV2 = modelV2.speciesMap;

modelV1.paramsSet = 'switch_bonnet';
modelV2.paramsSet = 'switch_bonnet';

%% Set inputs for leaky behavior
TV1('Phyb_ON') = t_Phyb_ON_1;
SV1('Phyb_ON') = v_Phyb_ON_1;
TV2('Phyb_ON') = t_Phyb_ON_1;
SV2('Phyb_ON') = v_Phyb_ON_1;

%% Run simulation on both models
deterministicSimulation([t0, tf], modelV1);
deterministicSimulation([t0, tf], modelV2);

%% Plot results
figure(1);
clf;

% Plot Phyb_ON input profile
subplot(2, 2, 1);
plot(TV1('Phyb_ON'), SV1('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Fraction of Phyb activity')
xlabel('Time [min]');
ylabel('Phyb_{ON} fraction');
legend('Phyb^{ON}');
ylim([0, 1.3]);

% Pout flipping
subplot(2, 2, 2);
hold on;
plot(TV1('Pout_flipped'), nM2n(SV1('Pout_flipped')), 'linewidth', 2);
plot(TV2('Pout_flipped'), nM2n(SV2('Pout_flipped')), 'linewidth', 2);
grid on;
title('Pout flipping (leakiness)');
xlabel('Time [min]');
ylabel('Number of promoters');
legend('Pout^{flipped} (V1)', 'Pout^{flipped} (V2)');
ylim([0, 1]);

%% Set inputs for active behavior
TV1('Phyb_ON') = t_Phyb_ON_2;
SV1('Phyb_ON') = v_Phyb_ON_2;
TV2('Phyb_ON') = t_Phyb_ON_2;
SV2('Phyb_ON') = v_Phyb_ON_2;

%% Run simulation on both models
deterministicSimulation([t0, tf], modelV1);
deterministicSimulation([t0, tf], modelV2);

%% Plot results
% Plot Phyb_ON input profile
subplot(2, 2, 3);
plot(TV1('Phyb_ON'), SV1('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Fraction of Phyb activity')
xlabel('Time [min]');
ylabel('Phyb_{ON} fraction');
legend('Phyb^{ON}');
ylim([0, 1.3]);

% Integrase behavior
subplot(2, 2, 4);
hold on;
plot(TV1('Pout_flipped'), nM2n(SV1('Pout_flipped')), 'linewidth', 2);
plot(TV2('Pout_flipped'), nM2n(SV2('Pout_flipped')), 'linewidth', 2);
grid on;
title('Pout flipping (signal)');
xlabel('Time [min]');
ylabel('Number of promoters');
l = legend('Pout^{flipped} (V1)', 'Pout^{flipped} (V2)');
l.Location = 'southeast';
ylim([0, 18]);