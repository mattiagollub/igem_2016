V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23; 
Mno=30.01;%g�mol?1
Mdeta=103.2;%g�mol?1
Mo2= 16;%g�mol?1
% Avogadro constant
t0    = 0;    
x0    = [1e-6,0,(40e-3)/Mo2,0];
tend  = 1000;
tstep = 0.1;
tspan = t0:tstep:tend;
% integrator options
opt = odeset('AbsTol', 1e-15, 'RelTol', 1e-15);
%yp0=zeros(3,1);
% 
%% equations
[t, x] = ode15s(@NOdiffusionplate, tspan, x0);
%plotTimeCourse(t1, x3, 1, '\bf simpleAndGate', t_sti, Sx_sti,Sy_sti, t0, tend, 0);
%[t1, x1] = ode45(@plotspecies, tspan, x0)%, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF);
figure(1)
clf;
plot(t,x(:,1));
hold on
plot(t,x(:,2));
hold on
grid on;
title('stimulus Nitric Oxyde')
xlabel('t (min)');
ylabel('concentration (M)');
legend DETA NO 

figure(2)
clf;
plot(t,x(:,3));
hold on
plot(t,x(:,4));
grid on;
title('stimulus Nitric Oxyde')
xlabel('t (min)');
ylabel('concentration (M)');
legend  O2 NO2