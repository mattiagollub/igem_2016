function [J, g, R] = parameterEstimationProblem( ...
                            x, options, experiments, model)
% PARAMETERESTIMATIONPROBLEM Error function of the parameter estimation. 
%   Computes an error measure of a set of parameters. The error is defined
%   as the sum of squared errors of the simulated datapoints from the
%   experimental data. This function is intended to be used with the MEIGO
%   toolbox.
%
    %% Change the IntegrationTolNotMet warning into an error 
    warnstate = warning;
    warnId_ode45 = 'MATLAB:ode45:IntegrationTolNotMet';
    warnId_ode15s = 'MATLAB:ode15s:IntegrationTolNotMet';
    warning('error', warnId_ode45);
    warning('error', warnId_ode15s);
    
    %% Insert parameters in the model
    c0 = model.c0;
    
    % Insert default parameters
    if ~isfield(model, 'paramsSet')
        P = containers.Map();
    elseif (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    
    % Insert extimated parameters
    for p=1:length(x)
        P(options.targetParams{p}) = x(p);
    end
    
    % Perform substitutions if necessary
    P = options.subsFunc(P);
    model.paramsSet = P;
    for i=1:length(model.paramNames)
        paramName = char(model.paramNames(i));
        model.c0 = subs(model.c0, model.paramNames(i), P(paramName));
    end
    model.c0 = double(model.c0);
    
    %% Iterate over all the experiments
    nExp = size(experiments.inputTimes, 2);
    J = 0;
    R = [];
    g = 0;
    
    for i=1:nExp
        
        EIT = experiments.inputTimes{i};
        EIS = experiments.inputSpecies{i};
        EOT = experiments.outputTimes{i};
        EOS = experiments.outputSpecies{i};
        
        model.timesMap = containers.Map();
        model.speciesMap = containers.Map();
        MT = model.timesMap;
        MS = model.speciesMap;
        
        %% Set inputs for the model
        inSpecies = keys(EIS);
        for s=1:length(inSpecies)
            specie = inSpecies(s);
            specie = specie{1};
            MT(specie) = EIT(specie);
            MS(specie) = EIS(specie);
        end
        
        %% Determine all required datapoints that the simulation
        reqTExp = 0;
        outSpecies = keys(EOS);
        for s=1:length(outSpecies)
            specie = outSpecies(s);
            specie = specie{1};
            reqTExp = [reqTExp, EOT(specie)];
        end
        reqTExp = unique(reqTExp);
        
        %% Run the simulation with the given parameters
        failed = false;
        try
            deterministicSimulation(reqTExp, model);
        catch ERR
            if strcmp(ERR.identifier, warnId_ode45) || ...
                strcmp(ERR.identifier, warnId_ode15s)
                failed = true;
            else
                throw(ERR);
            end
        end
        
        %% Compute least squares error w.r.t. the experimental results
        if (failed)
            % If the integration failed then we probably got very bad
            % parameters
            yExp = EOS(specie);
            J = 100000000000000;
            R = yExp * J;
            R = reshape(R, numel(R), 1);
            break;
        else
            species = keys(EOS);
            for s=1:length(species)
                specie = species(s);
                specie = specie{1};
                tExp = EOT(specie);
                yExp = EOS(specie);
                yOut = interp1(MT(specie), MS(specie), tExp);
                nPoints = size(yExp, 2);
                
                Rt = (yOut - yExp);
                
                J = J + sum(Rt.^2) / nPoints;
                if ~isreal(J)
                    J = 500000000000000;
                end
            end
        end
    end
    
    model.c0 = c0;
    display(J);
    display(x);
    
    warning(warnstate);
end