function model = createAndGateNOpartMAmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0,0,pnortot,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-1);
    model.inputs = {'NO'};
    
    %% Define symbolic variables for species and parameters
    syms NO NoR NoR_NO DNoR DNoR_NO1 DNoR_NO Pnor0 Pnor1 Pnor2 NoR_NO PnorV0 PnorV1 ...
         PnorV2 PnorV3 %mRNAinv
    syms kdnor k_dnor knor1 k_nor1 knor2 k_nor2 k_nor3 knor3 ...
         knor_no k_nor_no knorV1 k_norV1 knorV2 k_norV2...
         knorV3 k_norV3 kmrna kl knorProd dnor dnor_no dmrna
    
    model.specieNames = [NO NoR NoR_NO DNoR DNoR_NO1 DNoR_NO PnorV0 PnorV1 ...
                         PnorV2 PnorV3 ];%mRNAinv];
    model.paramNames = [kdnor k_dnor knor_no k_nor_no knorV1 k_norV1 knorV2 k_norV2...
                        knorV3 k_norV3 kmrna kl knorProd dnor dnor_no dmrna]; 
    
    %% Stoichiometric and rate matrices
    % NO | NoR | NoR_NO | DNoR | DNoR_NO1 | DNoR_NO | PnorV0 | PnorV1 | PnorV2 | PnorV3 | mRNAinv | 
    model.N = [ 
     0,  -1,   1,    0,  0,  0,  0,  0,  0,   0;  % 0;    % NO +NoR  -> NoR_NO 
     0,   1,  -1,    0,  0,  0,  0,  0,  0,   0;  % 0;    % NO +NoR  <- NoR_NO 
     0,   0,  -2,    0,  0,  1,  0,  0,  0,   0;  % 0;    % 2NoR_NO  -> DNoR_NO 
     0,   0,   2,    0,  0, -1,  0,  0,  0,   0;  % 0;    % 2NoR_NO  <- DNoR_NO 
     0,  -2,   0,    1,  0,  0,  0,  0,  0,   0;  % 0;    % 2NoR  -> DNoR 
     0,   2,   0,   -1,  0,  0,  0,  0,  0,   0;  % 0;    % 2NoR  <- DNoR 
     0,   0,   0,   -1,  1,  0,  0,  0,  0,   0;  % 0;    % DNoR + NO -> DNoR_NO1
     0,   0,   0,    1, -1,  0,  0,  0,  0,   0;  % 0;    % DNoR + NO <- DNoR_NO1
     0,   0,   0,    0, -1,  1,  0,  0,  0,   0;  % 0;    % DNoR_NO1 + NO -> DNoR_NO
     0,   0,   0,    0,  1, -1,  0,  0,  0,   0;  % 0;    % DNoR_NO1 + NO <- DNoR_NO
     0,   0,   0,    0,  0, -1, -1,  1,  0,   0;  % 0;    % DNoR_NO + PnorV0 -> PnorV1
     0,   0,   0,    0,  0,  1,  1, -1,  0,   0;  % 0;    % DNoR_NO + PnorV0 <- PnorV1
     0,   0,   0,    0,  0, -1,  0, -1,  1,   0;  % 0;    % DNoR_NO + PnorV1-> PnorV2
     0,   0,   0,    0,  0,  1,  0,  1, -1,   0;  % 0;    % DNoR_NO + PnorV1<- PnorV2
     0,   0,   0,    0,  0, -1,  0,  0, -1,   1;  % 0;    % DNoR_NO + PnorV2 -> PnorV3
     0,   0,   0,    0,  0,  1,  0,  0,  1,  -1;  % 0;    % DNoR_NO + PnorV2 <- PnorV3
%    0,   0,   0,    0,  0,  0,  0,  0,  0,   0,  % 1;    % PnorV3-> mRNAinv      
     0,   1,   0,    0,  0,  0,  0,  0,  0,   0;  % 0;    % -> NoR  
     0,  -1,   0,    0,  0,  0,  0,  0,  0,   0;  % 0;    % NoR ->  
     0,   0,   0,   -1,  0,  0,  0,  0,  0,   0;  % 0;    % DNOR ->
     0,   0,   0,    0,  0, -1,  0,  0,  0,   0;  % 0;    % DNoR_NO -> 
     0,   0,  -1,    0,  0,  0,  0,  0,  0,   0;  % 0;    % NoR_NO -> 
%     0,   0,   0,    0,  0,  0,  0,  0,  0,   0, % -1;    % mRNAinv ->      
     ]';
    
    %% Reaction rates
    model.rates = [
            knor_no*NO*NoR;               % NO +NoR  -> NoR_NO
            k_nor_no*NoR_NO;              % NO +NoR  <- NoR_NO
            kdnor*(NoR_NO^2);             % 2NoR_NO  -> DNoR_NO
            k_dnor*DNoR_NO;               % 2NoR_NO <- DNoR_NO
            kdnor*(NoR^2);                 % 2NoR  -> DNoR 
            k_dnor*DNoR;                   % 2NoR  <- DNoR 
            knor_no*DNoR*NO;                % DNoR + NO -> DNoR_NO1
            k_nor_no*DNoR_NO1;              % DNoR + NO <- DNoR_NO1
            knor_no*DNoR_NO1*NO;            % DNoR + NO -> DNoR_NO1
            k_nor_no*DNoR_NO;               % DNoR + NO <- DNoR_NO1
            knorV1*DNoR_NO*PnorV0;          % NoR_NO + PnorV0 -> PnorV1  (NoR_NO)
            k_norV1*PnorV1;                % NoR_NO + PnorV0 <- PnorV1
            knorV2*DNoR_NO*PnorV1;          % NoR_NO + PnorV1-> PnorV2 (NoR_NO)
            k_norV2*PnorV2;                % NoR_NO + PnorV1<- PnorV2
            knorV3*DNoR_NO*PnorV2;          % NoR_NO + PnorV2 -> PnorV3  (NoR_NO)
            k_norV3*PnorV3;                % NoR_NO + PnorV2 <- PnorV3
%          (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; %PnorV3 -> mrna inv
           (knorProd*kl)+(knorProd*(1-kl)*Ptot) ;  % -> NoR
            dnor*NoR;                      % NOR ->
            dnor*DNoR;                      % DNOR ->
            dnor_no*DNoR_NO;                 % DNoR_NO ->
            dnor_no*NoR_NO  ;               % NoR_NO ->
%            dmrna*mRNAinv;                 % mRNAinv ->
    ];

    %% Reaction propensities
    model.a = [
            n2nM(knor_no)*NO*NoR;               % NO +NoR  -> NoR_NO
            k_nor_no*NoR_NO;              % NO +NoR  <- NoR_NO
            n2nM(kdnor)/2*(NoR_NO)*(NoR_NO-1);             % 2NoR_NO  -> DNoR_NO
            k_dnor*DNoR_NO;               % 2NoR_NO <- DNoR_NO
            n2nM(kdnor)/2*(NoR)*(NoR-1);                 % 2NoR  -> DNoR 
            k_dnor*DNoR;                   % 2NoR  <- DNoR 
            n2nM(knor_no)*DNoR*NO;                % DNoR + NO -> DNoR_NO1
            k_nor_no*DNoR_NO1;              % DNoR + NO <- DNoR_NO1
            n2nM(knor_no)*DNoR_NO1*NO;            % DNoR + NO -> DNoR_NO1
            k_nor_no*DNoR_NO;               % DNoR + NO <- DNoR_NO1
            n2nM(knorV1)*DNoR_NO*PnorV0;          % NoR_NO + PnorV0 -> PnorV1  (NoR_NO)
            k_norV1*PnorV1;                % NoR_NO + PnorV0 <- PnorV1
            n2nM(knorV2)*DNoR_NO*PnorV1;          % NoR_NO + PnorV1-> PnorV2 (NoR_NO)
            k_norV2*PnorV2;                % NoR_NO + PnorV1<- PnorV2
            n2nM(knorV3)*DNoR_NO*PnorV2;          % NoR_NO + PnorV2 -> PnorV3  (NoR_NO)
            k_norV3*PnorV3;                % NoR_NO + PnorV2 <- PnorV3
%           (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; %PnorV3 -> mrna inv
           (knorProd*kl)+(knorProd*(1-kl)*n2nM(Ptot)) ;  % -> NoR
            dnor*NoR;                      % NOR ->
            dnor*DNoR;                      % DNOR ->
            dnor_no*DNoR_NO;                 % DNoR_NO ->
            dnor_no*NoR_NO;                 % NoR_NO ->
%            dmrna*mRNAinv;                 % mRNAinv ->
	];
end