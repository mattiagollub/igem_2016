function [x, y] = parameterSet(model, paramName,paramRange)
lineWidth = 1.5;
N=zeros(1,length(paramRange));

% modelNO = createAndGateNOpartMAmodel();
% modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(model.paramsSet);

for j=1:length(paramRange)
    j
    paramRange(j)
  
    ParamMap(paramName)=paramRange(j);
    %ParamMap('dnor')=0.9*paramRange(j);
    model.paramsSet = ParamMap;
    %NO=[0,0.1,1,10,100, 2000, 200000];
    Lac=horzcat( 10,1000, 10000, 100000, 1000000,10000000,100000000, 1000000000);
    %AHL=horzcat(0.01, [0.1:0.1:1], [5, 10, 20, 50, 70, 100]);%,[200:200:1000], 5000, 10000);
    [~, threshold]=saturationCurve(model,'Lac','G_on',Lac);
    N(j)=threshold
end

 x=[paramRange(1):1:paramRange(end)];
 y=interp1(paramRange,N,x,'pchip');
% x=[paramRange(1):0.1:paramRange(end)];
% y=interp1(log(paramRange),N,log(x),'pchip');
% x=paramRange;
% y=N;



