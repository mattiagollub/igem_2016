function stochasticSimulationTDI(tspan, model, nRuns, drawFuncs, compile)
% STOCHASTICSIMULATIONTDI Simulate a system using the Next Reaction Method
%   Perform a stochastic simulation of the given model. The results of the
%   simulation (specie concentrations) are saved in model.speciesMap and
%   model.timesMap. Histograms will be saved in model.histogramsMap.
%   This version of the algorithm supports up to three reactions with
%   time-dependent inputs.
%
%       stochasticSimulationTDI(tspan, model, nRuns, drawFuncs, compile)
%
%   Required:
%       tspan:          Timestamps at which the simulation will compute the
%                       concentrations and histograms. The first and the
%                       last elements define the start and end timestamp
%                       of the simulation.
%       model:          MATLAB structure containing the definition of the
%                       model.
%       nRuns:          Number of trajectories to simulate.
%
%	Optional:
%       drawFuncs:      MATLAB Map containing draw functions for the
%                       species. This is useful if for plotting
%                       intermediate results like trajectories.
%       compile:        Specifies if the code of the simualtion must be
%                       recompiled. If not, the last compiled code will be
%                       used. Default value is true.
%
    %% Setup directories
    if ~exist('mexFunctions/tmp', 'dir')
        mkdir('mexFunctions/tmp');
    end
    addpath('mexFunctions');
    addpath('mexFunctions/tmp');
       
    %% Set default inputs
    if ~exist('drawFuncs', 'var')
        drawFuncs = containers.Map();
    end
    if ~exist('compile', 'var')
        compile = true;
    end
    
    %% Identify reactions that depends on the inputs and reorder them
    timeDepReactions = zeros(size(model.a));
    for i=1:length(model.inputs)
        inputSymbol = sym(model.inputs{i});
        timeDepReactions = timeDepReactions | has(model.a, inputSymbol);
    end
    aTimeDep = model.a(timeDepReactions);
    reactionInputs = zeros(1, sum(timeDepReactions));
    for i=1:length(reactionInputs)
        if has(aTimeDep(i), sym(model.inputs{1}))
            reactionInputs(i) = 1;
        else
            reactionInputs(i) = 2;
        end
    end
    N = [model.N(:,timeDepReactions), model.N(:,~timeDepReactions)];
    
    %% Get model sizes
    nInputs = length(model.inputs);
    nSpecies = length(model.specieNames);
    
    %% Create empty histograms
    histograms = zeros(length(tspan), nRuns, nSpecies);
    
    %% Initial conditions
    tInput = cell(size(reactionInputs));
    xInput = cell(size(reactionInputs));
    for i=1:length(reactionInputs)
        tInput{i} = model.timesMap( ...
            model.inputs{reactionInputs(i)});
        xInput{i} = nM2n(model.speciesMap( ...
            model.inputs{reactionInputs(i)}));
    end
    n0 = [nM2n(interpInputs(model, 0))', round(nM2n(model.c0(nInputs+1:end)))];
    
    %% Substitute parameters in the propensity functions
    nParams = length(model.paramNames);
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    if compile
        %% Create propensities function
        mexCreatePropensitiesFuncsTDI(model, timeDepReactions, reactionInputs);
    
        %% Compile mex files
        if sum(timeDepReactions) == 1
            codegen -d mexFunctions/codegen ...
                    -o mexFunctions/tmp/nextReactionMethodTDI_mex ...
                    mexFunctions/mexNextReactionMethod1TDI ...
                    -args { N', tspan, n0, tInput, xInput, parameters }
        elseif sum(timeDepReactions) == 2
            codegen -d mexFunctions/codegen ...
                    -o mexFunctions/tmp/nextReactionMethodTDI_mex ...
                    mexFunctions/mexNextReactionMethod2TDI ...
                    -args { N', tspan, n0, tInput, xInput, parameters }
        elseif sum(timeDepReactions) == 3
            codegen -d mexFunctions/codegen ...
                    -o mexFunctions/tmp/nextReactionMethodTDI_mex ...
                    mexFunctions/mexNextReactionMethod3TDI ...
                    -args { N', tspan, n0, tInput, xInput, parameters }
        else
            error('mexNextReactionMethodTDI only supports one, two or three time-dependent reactions');
        end
    end
            
    %% Run simulation
    simStart = tic;
    if isempty(keys(drawFuncs))
        %% Simulate the required trajectories
        parfor r=1:nRuns
            trajStart = tic;
            histograms(:, r, :) = nextReactionMethodTDI_mex(...
                N', tspan, n0, tInput, xInput, parameters);
            fprintf('Trajectory %d. ', r);
            toc(trajStart);
        end
    else
        for r=1:nRuns

            %% Simulate trajectory
            trajStart = tic;
            histograms(:, r, :) = nextReactionMethodTDI_mex(...
                N', tspan, n0, tInput, xInput, parameters);
            fprintf('Trajectory %d. ', r);
            toc(trajStart);

            %% Write output
            for i=nInputs+1:nSpecies
                specieName = char(model.specieNames(i));

                % Call draw function if necessary
                if isKey(drawFuncs, specieName)
                    fn = drawFuncs(specieName);
                    fn(tspan, n2nM(histograms(:, r, i)));
                end
            end
            drawnow;
        end
    end
    toc(simStart)
    
    %% Compute and write average trajectory
    for i=nInputs+1:nSpecies
        specieName = char(model.specieNames(i));
        model.histogramsMap(specieName) = histograms(:, :, i);
        meanTraj = n2nM(mean(histograms(:, :, i), 2));
        model.timesMap(specieName) = tspan;
        model.speciesMap(specieName) = meanTraj;
    end
end 

function inputs = interpInputs(model, t)
    
    % Get number of inputs
    nInputs = size(model.inputs, 2);
    inputs = zeros(nInputs, 1);
    
    % Interpolate inputs
    for i=1:nInputs
        inputName = model.inputs{i};
        inputs(i) = interp1( ...
            model.timesMap(inputName), ...
            model.speciesMap(inputName), t);
    end
end
    