%% AND_GATE_DOSE_RESPONSE    Example simulation of the and gate. 
%   Simulates the concentration of the and gate species using an example
%   input.

%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 100;

NO_max=20;
AHL_max=6;

NO_range=0:5:NO_max;
AHL_range=0:22:AHL_max;
output_maxMrnaBxb1=zeros(length(AHL_range),length(NO_range));
output_mrnaBxb1=zeros(length(AHL_range),length(NO_range));

time=1:100:tf;

[NO,AHL,time] = meshgrid(0:50:NO_max,0:20:AHL_max,1:100:tf);

for i=1:length(AHL_range)
    for j=1:length(NO_range)
        % Input: NO and AHL profile
        points = 100;
        NO_stimulus = NO_range(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        AHL_stimulus = AHL_range(i) * 1e9 / (Na * V);
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.5/tf*points);
        t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        t_AHL = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        AHL = zeros(size(t_AHL));
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Run simulation
        timesMap = containers.Map();
        timesMap('NO') = t_NO;
        timesMap('AHL') = t_AHL;

        speciesMap = containers.Map();
        speciesMap('NO') = NO;
        speciesMap('AHL') = AHL;

        andGateSSA([t0, tf], timesMap, speciesMap, params_set);

        output_maxMrnaBxb1(i,j)=max(speciesMap('mRNAinv'));
        for t=time
            output_mrnaBxb1(i,j,t)=interp1(timesMap('mRNAinv'),speciesMap('mRNAinv'),t);
        end
    end
end
%% Plot results

% mRNAinv input profile
figure(1);
clf;
surf(NO_range, AHL_range,output_maxMrnaBxb1);


bool=true;
while(bool) 
    display(time);
    prompt = 'for which time do you want to monitor the species(type n to stop)? ';
    t1 = input(prompt);
    figure(2);
    clf;
    if t1=='n'
       bool=false;
    else
        surf(NO_range, AHL_range, output_mrnaBxb1(:,:,t1));
    end
end