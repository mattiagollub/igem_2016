function sfGFPParameterEstimation(newParams)
    % SFGFPPARAMSESTIMATION Deterministic estimation of the Ptet_sfGFP
    % parameters
    %    
    
    p25 = [
        0.0247846
        1.51881
        2.57922
        0.168804
        0.182427
        25.9686
        0.00995047
        2.25771
        5.82923
        561.645];
    p4 = [
        0.00204625
        1.6312
        10.1515
        2.26198
        0.0124721
        3.9226
        0.0283264
        7.91305
        20.0706
        821.245];
    targetParams = { 'l_pTet', 'n_TetR', 'Km_TetR', 'k_mRNAxfp', ...
        'd_mRNAxfp', 'k_XFP', 'd_XFP', 'k_ON', 'k_OFF', 'TetR_tot' };
    newParams = containers.Map(targetParams, p4);
    growthTime = 60*4;
    
    if ~exist('newParams', 'var')
        estimateDeterministicParamsMT('setupsfGFPparamEstimation');
    end
    
    %% Data from second sfGFP FACS
    times_in = [0, 45, 100, 160, 220];
    dose_0 = [0, 0, 0, 0, 0, 0, 0];
    dose_2 = [0, 0, 4.32, 4.32, 4.32, 4.32, 4.32];
    dose_20 = [0, 0, 43.2, 43.2, 43.2, 43.2, 43.2];
    dose_2000 = [0, 0, 4320, 4320, 4320, 4320, 4320];

    times_out = [0, 45, 100, 160, 220];
    xfp_0 = [0, 598, 708, 765, 796, 1030];
    xfp_2 = [0, 690, 758, 840, 1045, 1052];
    xfp_20 = [0, 646, 1005, 1164, 1121, 1203];
    xfp_2000 = [0, 685, 2765, 5590, 7262, 8062];

    doses = [dose_0; dose_2; dose_20; dose_2000];
    xfps = [xfp_0; xfp_2; xfp_20; xfp_2000];

    %% Adjust growth time
    times_in = [0, growthTime-0.01, times_in+growthTime];
    times_out = [0, times_out+growthTime];

    %% Simulation settings

    % Time range for the experiment
    t0   = 0;
    tf   = times_in(end);

    %% Setup model
    model = createPtetXFPSuperDetailedModel();

    %% Run simulation with new parameters
    model.paramsSet = newParams;
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    Tn = model.timesMap;
    Sn = model.speciesMap;
    Tn('aTc') = times_in;

    model.c0 = double(subs(model.c0, sym('TetR_tot'), newParams('TetR_tot')));

    %% Plot results
    figure(1);
    clf;
    set(gca,'FontSize',14);

    for i=1:4
        Sn('aTc') = doses(i,:);
        deterministicSimulation([t0, tf], model);

        hold on;
        plot(Tn('XFP'), Sn('XFP'), 'linewidth', 2);
        plot(times_out, xfps(i,:), 'k+', 'linewidth', 2);
        grid on;
        title('Ptet-sfGFP simulation');
        xlabel('Time [min]');
        ylabel('Fluorescence');
    end
    legend('0ng/uL simulated', '0ng/uL data', ...
           '2ng/uL simulated', '2ng/uL data', ...
           '20ng/uL simulated', '20ng/uL data', ...
           '2000ng/uL simulated', '2000ng/uL data');
end
