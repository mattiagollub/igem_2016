function model = createCollaborationKillerSwitchModel()
%   createCollaborationKillerSwitchModel Defines a mass action model of the killer switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createCollaborationKillerSwitchModel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);    % nM, Total concentration of plasmids
    pnortot= 24.9081;   %concentration of plasmid
    %% Function
    clamp=@(x)feval(symengine,'max',x,0);
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,10,60,0,0,0,0,0,1];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'sigECF'};
    
    %% Define symbolic variables for species and parameters
    syms sigECF antiMaz mRNAmazE mRNAmazF Pconst MazE MazF DMazF DMazF_E...
         DMazF_E2 Pblock cfu
    syms kProd dmazE dmazF dantiMaz kdmazf k_dmazf kdmazfe ...
         k_dmazfe kdmazfe2 k_dmazfe2 kpbloc k_pbloc cfucoeff...
         Kanti nanti Kinhib ninhib kl ClpAP kantimaz kdeg ka
    
    model.specieNames = [sigECF antiMaz mRNAmazE mRNAmazF Pconst MazE MazF DMazF DMazF_E...
                         DMazF_E2 Pblock cfu];
    model.paramNames = [kProd dmazE dmazF dantiMaz kdmazf k_dmazf kdmazfe ...
                        k_dmazfe kdmazfe2 k_dmazfe2 kpbloc k_pbloc cfucoeff...
                        Kanti nanti Kinhib ninhib kl ClpAP kantimaz kdeg ka]; 
    
%% Stoichiometric and rate matrices
% sigECF | antiMaz| mRNAmazE | mRNAmazF | Pconst | MazE | MazF | DMazF | DMazF_E | DMazF_E2
%  Pblock | cfu             
    model.N = [
     0,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0; %sigECF->antiMaz 
     0,  -1,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0; %antiMaz->
     0,   0,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0; %Pconst ->mrnaMazE
     0,   0,  -1,   0,   0,   0,   0,   0,   0,   0,   0,   0; %mrnaMaze + antiMaz -> antiMaz
     0,   0,   0,   1,   0,   0,   0,   0,   0,   0,   0,   0; %Pconst-> mrnaMazF
     0,   0,  -1,   0,   0,   1,   0,   0,   0,   0,   0,   0; %mRNAmazE->MazE
     0,   0,   0,   0,   0,  -1,   0,   0,   0,   0,   0,   0; %MazE->
     0,   0,   0,  -1,   0,   0,   1,   0,   0,   0,   0,   0; %mRNAmazF->MazF
     0,   0,   0,   0,   0,   0,  -1,   0,   0,   0,   0,   0; %MazF->  
     0,   0,   0,   0,   0,   0,  -2,   1,   0,   0,   0,   0; %2*DMazF->DMazF
     0,   0,   0,   0,   0,   0,   2,  -1,   0,   0,   0,   0; %2*DMazF<-DMazF
     0,   0,   0,   0,   0,  -1,   0,  -1,   1,   0,   0,   0; %DMazF+MazE->DMazF_E
     0,   0,   0,   0,   0,   1,   0,   1,  -1,   0,   0,   0; %DMazF+MazE<-DMazF_E
     0,   0,   0,   0,   0,  -1,   0,   0,  -1,   1,   0,   0; %DMazF_E+MazE->DMazF_E2
     0,   0,   0,   0,   0,   1,   0,   0,   1,  -1,   0,   0; %DMazF_E+MazE<-DMazF_E2
     0,   0,   0,   0,  -1,   0,   0,   0,   0,  -3,   1,   0; %3*DMazF_E2+Pconst->Pblock
     0,   0,   0,   0,   1,   0,   0,   0,   0,   3,  -1,   0; %3*DMazF_E2+Pconst<-Pblock
     %0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,  -1; %MazF-> cfu
     0,   0,   0,   0,   0,   0,   0,   0,   0,   -1,  0,   0; %DMazF_E2-->
     ]';
    
    %% Reaction rates
    model.rates = [
     (((sigECF/Kanti)^nanti)./(1+(sigECF/Kanti)^nanti))-dantiMaz*antiMaz; % sigECF->antiMaz
      dantiMaz*antiMaz;                 % antiMaz ->
      kProd*Pconst;                     % Pconst-> mRNAMazE 
      kdeg*mRNAmazE*antiMaz;            % mrnaMaze + antiMaz -> antiMaz
      kProd*Pconst;                     % Pconst-> mrnaMazF
      ka*mRNAmazE;                      % mRNAmazE->MazE
      dmazE*ClpAP*MazE                  % MazE ->
      ka*mRNAmazF;                      % mRNAmazF->MazF
      10*dmazF*DMazF;                      % DMazF ->   
      MazF^2*kdmazf;                    % 2*MazF->DMazF
      DMazF*k_dmazf;                    % 2*MazF<-DMazF
      DMazF*MazE*kdmazfe;               % 2*DMazF+MazE->DMazF_E
      DMazF_E*k_dmazfe;                 % 2*DMazF+MazE<-DMazF_E
      DMazF_E*MazE*kdmazfe2;            % 2*DMazF_E+MazE->DMazF_E2
      DMazF_E2*k_dmazfe2;               % 2*DMazF_E+MazE<-DMazF_E2
      DMazF_E2^3*Pconst*kpbloc;         % 3*DMazF_E2+Pconst->Pblock
      Pblock*k_pbloc;                   % 3*DMazF_E2+Pconst<-Pblock
     %clamp(MazF-1e-7)*cfu/(0.1*cfucoeff);      %((-MazF-log(2))/cfucoeff)*cfu;   % MazF->cfu
      dmazF*DMazF_E2;
    ];
   % sigECF | antiMaz| mRNAmazE | mRNAmazF | Pconst | MazE | MazF | DMazF | DMazF_E | DMazF_E2
%  Pblock | cfu  
%     model.specieFunctions = [
%         
%         sigECF;
%         
%         mRNAmazE;
%         mRNAmazF;
%         Pconst;
%         MazE;
%         MazF;
%         DMazF;
%         DMazF_E;
%         DMazF_E2;
%         Pblock;
%        (MazF-((log(2)/0.001)))
%        ];  
    %% Reaction propensities
    model.a = [
     0.1*kantimaz+(((sigECF/Kanti)^nanti)./(1+(sigECF/Kanti)^nanti))-dantiMaz*antiMaz;% sigECF->antiMaz
    %  dantiMaz*antiMaz;                 % -> antiMaz
     (kProd*kl)+(kProd*(1-kl)*Pconst);  % Pconst-> mRNAMazE 
      kdeg*mRNAmazE*antiMaz;            % mrnaMaze + antiMaz -> antiMaz
      (kProd*kl)+(kProd*(1-kl)*Pconst); % Pconst-> mrnaMazF
      ka*mRNAmazE;                      % mRNAmazE->MazE
      dmazE*MazE                        % MazE ->
      ka*mRNAmazF;                      % mRNAmazF->MazF
      dmazF*MazF;                       % MazF ->   
      MazF^2*kdmazf;                    % 2*MazF->DMazF
      DMazF*k_dmazf;                    % 2*MazF<-DMazF
      DMazF*MazE*kdmazfe;               % 2*DMazF+MazE->DMazF_E
      DMazF_E*k_dmazfe;                 % 2*DMazF+MazE<-DMazF_E
      DMazF_E*MazE*kdmazfe2;            % 2*DMazF_E+MazE->DMazF_E2
      DMazF_E2*k_dmazfe2;               % 2*DMazF_E+MazE<-DMazF_E2
      DMazF_E2^3*Pconst*kpbloc;         % 3*DMazF_E2+Pconst->Pblock
      Pblock*k_pbloc;                   % 3*DMazF_E2+Pconst<-Pblock
     ((-MazF-log(2))/cfucoeff)*cfu;     % MazF->cfu
      dmazF*DMazF_E2;
    ];
  

end