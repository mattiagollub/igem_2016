function andGateSSA(tspan, timesMap, speciesMap, params_set)
% ANDGATESSA    SSA simulation of the AND GATE. 
%   Simulates the concentration of the AND GATE species on the given time
%   interval. Input and outputs are placed in the timesMap and speciesMap
%   structures respectively. Simulation is performed using the specified
%   parameter set.

    %% Common constants
    V = 1e-15;              % Cell volume in �m
    nP = 15;                % Number of plasmids
    Na = 6.022140857e23;    % Avogadro constant
    NAV = 1e9 / (Na * V);     % Conversion from nM to molecule counts

    %% Simulation settings

    % Input data
   
    t_NO = timesMap('NO');
    t_AHL = timesMap('AHL');
    NO = speciesMap('NO');
    AHL = speciesMap('AHL');
    
    %% Parameters
    S_tot = nP * 1e9 / (Na * V); % nM, Total concentration of plasmids
    nortot= 200;
    pnortot= 24;
    esartot= 200;
    stot= nP * 1e9 / (Na * V);
    pdesar= 1000 * 1e9 / (Na * V);
    
    P = loadParameters(params_set);
    
    p.knor_no   =P('knor_no');
    p.k_nor_no  =P('k_nor_no');
    p.knorV1   =P('knorV1');
    p.k_norV1  =P('k_norV1');
    p.knorV2   =P('knorV2');
    p.k_norV2  =P('k_norV2');
    p.knorV3   =P('knorV3');
    p.k_norV3  =P('k_norV3');
    p.knor1    =P('knor1');
    p.k_nor1   =P('k_nor1');
    p.knor2    =P('knor2');
    p.k_nor2   =P('k_nor2');
    p.knor3    =P('knor3');
    p.k_nor3   =P('k_nor3');
    p.k5       =P('k5');
    p.k_5      =P('k_5');
    p.k6       =P('k6');
    p.k_6      =P('k_6');
    p.k7        =P('k7');
    p.k_7       =P('k_7');
    p.pnor      =P('pnor');
    p.dnor      =P('dnor');
    p.dnor_no   =P('dnor_no');
    p.pesar     =P('pesar');
    p.Ptot      =P('Ptot');
    p.kmrna     =P('kmrna');
    p.dmrna     =P('dmrna');
    p.kl        =P('kl');
    p.desar     =P('desar');
   

    %% Initial conditions and integrator options
       x0 = [pnortot,0,0,nortot,0,0,0,0,0,esartot,0,0,0,pnortot,pnortot,0,0];

    %% Stoichiometric matrix
N = [
    -1,  1, 0, -1,  0,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % NoR + Pnor0 -> Pnor1
     1, -1, 0,  1,  0,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % NoR + Pnor0 <- Pnor1
     0, -1, 1, -1,  0,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;  % NoR + Pnor1-> Pnor2
     0,  1,-1,  1,  0,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;  % NoR + Pnor1<- Pnor2
     0,  0,-1, -1,  0,   1,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;  % NoR + Pnor2 -> Pnor3
     0,  0, 1,  1,  0,  -1,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;  % NoR + Pnor2 <- PnorV0        
     0,  0, 0, -1,  1,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % NoR + NO -> NoR_NO
     0,  0, 0,  1, -1,   0,  0,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % NoR + NO <- NoR_NO
     0,  0, 0,  0,  0,  -1,  1,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % No + PnorV0 -> PnorV1
     0,  0, 0,  0,  0,  1,  -1,  0,  0,  0,  0, 0,  0, 0, 0, 0, 0;   % No + PnorV0 <- PnorV1
     0,  0, 0,  0,  0,  0,  -1,  1,  0,  0,  0, 0,  0, 0, 0, 0, 0;  % No + PnorV1-> PnorV2
     0,  0, 0,  0,  0,   0,   1, -1,  0,  0, 0, 0,  0, 0, 0, 0, 0;  % No + PnorV1<- PnorV2
     0,  0, 0,  0,  0,   0,   0, -1,  1,  0, 0, 0,  0, 0, 0, 0, 0;  % No + PnorV2 -> PnorV3
     0,  0, 0,  0,  0,   0,   0,  1, -1,  0, 0, 0,  0, 0, 0, 0, 0;  % No + PnorV2 <- PnorV3
     0,  0, 0,  0,  0,   0,   0,  0,  0, -2, 1, 0,  0, 0, 0, 0, 0;  % EsaR + Esar -> DEsar
     0,  0, 0,  0,  0,   0,   0,  0,  0,  2,-1, 0,  0, 0, 0, 0, 0;  % EsaR + Esar <- DEsar
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0,-1, 1,  0, 0, 0, 0, 0;  % 2*AHL + DEsar -> DEsar_AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 1,-1,  0, 0, 0, 0, 0;  % 2*AHL + DEsar <- DEsar_AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0,-1, -1, 1, 0, 0, 0;  % DEsar_AHL + Pfree -> Pesar1 + 2*AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0, 1,  1,-1, 0, 0, 0;    % DEsar_AHL + Pfree <- Pesar1 + 2*AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0,-1,  0, 0, 1, -1, 0; % DEsar_AHL + Pout -> Pesar2 + 2*AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0, 1,  0, 0, -1, 1, 0; % DEsar_AHL + Pout <- Pesar2 + 2*AHL
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0, 0,  0, 0,  0, 0, 1; % -> mRNAinv 
     0,  0, 0,  1,  0,   0,   0,  0,  0,  0, 0, 0,  0, 0,  0, 0, 0; % -> NoR
     0,  0, 0,  0,  0,   0,   0,  0,  0,  1, 0, 0,  0, 0,  0, 0, 0; % -> EsaR
     0,  0, 0, -1,  0,   0,   0,  0,  0,  0, 0, 0,  0, 0,  0, 0, 0; % NOR ->
     0,  0, 0,  0, -1,   0,   0,  0,  0,  0, 0, 0,  0, 0,  0, 0, 0; % NoR_NO ->
     0,  0, 0,  0,  0,   0,   0,  0,  0, -1, 0, 0,  0, 0,  0, 0, 0; % EsaR ->
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0,-1, 0,  0, 0,  0, 0, 0; % DEsaR ->
     0,  0, 0,  0,  0,   0,   0,  0,  0,  0, 0, 0,  0, 0,  0, 0, -1; % mRNAinv ->
    ];
    %0,   0,   0,   0,  0,  0,  0, 0,-1,  0, 0; % DEsar_AHL ->
    %% Run simulation
    [t, x] = firstReactionMethod(N, @propensities_at_state, tspan, x0, p, timesMap, speciesMap);

   %% Write output
    timesMap('Pnor0')       = t;
    timesMap('Pnor1')       = t;
    timesMap('Pnor2')       = t;
    timesMap('NoR')         = t;
    timesMap('NoR_NO')      = t;
    timesMap('PnorV0')      = t;
    timesMap('PnorV1')      = t;
    timesMap('PnorV2')      = t;
    timesMap('PnorV3')      = t;
    timesMap('EsaR')        = t;
    timesMap('DesaR')       = t;
    timesMap('DesaR_AHL')   = t;
    timesMap('Pfree')       = t;
    timesMap('Pesar1')      = t;
    timesMap('Pesar2')      = t;
    timesMap('Pout')        = t;
    timesMap('mRNAinv')     = t;
    
    speciesMap('Pnor0')     = x(:,1);
    speciesMap('Pnor1')     = x(:,2);
    speciesMap('Pnor2')     = x(:,3);
    speciesMap('NoR')       = x(:,4);
    speciesMap('NoR_NO')    = x(:,5);
    speciesMap('PnorV0')    = x(:,6);
    speciesMap('PnorV1')    = x(:,7);
    speciesMap('PnorV2')    = x(:,8);
    speciesMap('PnorV3')    = x(:,9);
    speciesMap('EsaR')      = x(:,10);
    speciesMap('DesaR')     = x(:,11);
    speciesMap('DesaR_AHL') = x(:,12);
    speciesMap('Pfree')     = x(:,13);
    speciesMap('Pesar1')    = x(:,14);
    speciesMap('Pesar2')    = x(:,15);
    speciesMap('Pout')      = x(:,16);
    speciesMap('mRNAinv')   = x(:,17);

end

function a = propensities_at_state(c, p, t, timesMap, speciesMap)
    t
    
    t_NO = timesMap('NO');
    t_AHL = timesMap('AHL');
    NO = speciesMap('NO');
    AHL = speciesMap('AHL');
 % variables
    NO            = round(interp1(t_NO, NO, t));
    AHL           = round(interp1(t_AHL, AHL, t));
    Pnor0         = c(1);
    Pnor1         = c(2);
    Pnor2         = c(3);
    NoR           = c(4);
    NoR_NO        = c(5);
    PnorV0        = c(6);
    PnorV1        = c(7);
    PnorV2        = c(8);
    PnorV3        = c(9);
    EsaR          = c(10);
    DesaR         = c(11);
    DesaR_AHL     = c(12);
    Pfree         = c(13);
    Pesar1        = c(14);
    Pesar2        = c(15);
    Pout          = c(16);
    mRNAinv       = c(17);
 

    a = [   p.knor1*NoR*Pnor0;          % NoR + Pnor0 -> Pnor1  (NoR_NO)
            p.k_nor1*Pnor1;                % NoR + Pnor0 <- Pnor1
            p.knor2*NoR*Pnor1;          % NoR + Pnor1-> Pnor2 (NoR_NO)
            p.k_nor2*Pnor2;                % NoR + Pnor1<- Pnor2
            p.knor3*NoR*Pnor2;          % NoR+ Pnor2 -> Pnor3  (NoR_NO)
            p.k_nor3*PnorV0;                % NoR + Pnor2 <- PnorV0
            p.knor_no*NoR*NO;                % NoR + NO -> NoR_NO
            p.k_nor_no*NoR_NO;               % NoR + NO <- NoR_NO
            p.knorV1*NO*PnorV0;          % NO + PnorV0 -> PnorV1  (NoR_NO)
            p.k_norV1*PnorV1;                % NO + PnorV0 <- PnorV1
            p.knorV2*NO*PnorV1;          % NO + PnorV1-> PnorV2 (NoR_NO)
            p.k_norV2*PnorV2;                % NO + PnorV1<- PnorV2
            p.knorV3*NO*PnorV2;          % NO + PnorV2 -> PnorV3  (NoR_NO)
            p.k_norV3*PnorV3;                % NO + PnorV2 <- PnorV3
            p.k5*EsaR*(EsaR-1);             % EsaR + Esar -> DEsar
            p.k_5*DesaR;                % EsaR + Esar <- DEsar
            p.k6*AHL*(AHL-1)*DesaR;         % 2*AHL + DEsar -> DEsar_AHL
            p.k_6*DesaR_AHL;            % 2*AHL + DEsar <- DEsar_AHL
            p.k7*DesaR_AHL*Pfree;       % DEsar_AHL + Pfree -> Pesar1 + 2*AHL 
            p.k_7*Pesar1*AHL*(AHL-1);        % DEsar_AHL + Pfree <- Pesar1 + 2*AHL
            p.k7*DesaR_AHL*Pout;       % DEsar_AHL + Pfree -> Pesar2 + 2*AHL 
            p.k_7*Pesar2*AHL*(AHL-1);        % DEsar_AHL + Pfree <- Pesar2 + 2*AHL
           (p.kmrna*p.kl)+(p.kmrna*(1-p.kl)*((PnorV3*Pfree)/p.Ptot)) ; %Phyb -> mrna inv
            p.pnor;                     % -> NoR
            p.pesar;                    % -> EsaR
            p.dnor*NoR;                 % NOR ->
            p.dnor_no*NoR_NO               % NoR_NO ->
            p.desar*abs(EsaR);               % EsaR ->
            p.desar*DesaR;              % DEsaR ->
            p.dmrna*mRNAinv;          % mRNAinv ->
        ];
    
    if min(a)<0
        blah = 0
    end
end
