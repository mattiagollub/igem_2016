function dxdt = andGateMAstep(t, c, t_NO, NO, t_AHL, AHL, params)
% SWITCHMASTEP    Mass action ODE model of the switch. 
%   Computes the derivatives of the specie concentrations at a timestamp t.
    NO            = interp1(t_NO, NO, t);
    AHL           = interp1(t_AHL, AHL, t);
    Pnor0         = c(1);
    Pnor1         = c(2);
    Pnor2         = c(3);
    NoR           = c(4);
    NoR_NO        = c(5);
    PnorV0        = c(6);
    PnorV1        = c(7);
    PnorV2        = c(8);
    PnorV3        = c(9);
    EsaR          = c(10);
    DesaR         = c(11);
    DesaR_AHL     = c(12);
    Pfree         = c(13);
    Pesar1        = c(14);
    Pesar2        = c(15);
    Pout          = c(16);
    mRNAinv       = c(17);
    %% Switch species
 

    %% Switch parameters
    knor_no  = params(1);
    k_nor_no = params(2);
    knorV1   = params(3);
    k_norV1  = params(4);
    knorV2   = params(5);
    k_norV2  = params(6);
    knorV3   = params(7);
    k_norV3  = params(8);
    knor1    = params(9);
    k_nor1   = params(10);
    knor2    = params(11);
    k_nor2   = params(12);
    knor3    = params(13);
    k_nor3   = params(14);
    k5       = params(15);
    k_5      = params(16);
    k6       = params(17);
    k_6      = params(18);
    k7       = params(19);
    k_7      = params(20);
    pnor     = params(21);
    dnor         = params(22);
    dnor_no      = params(23);
    pesar        = params(24);
    Ptot         = params(25);
    kmrna        = params(26);
    dmrna        = params(27);        
    kl           = params(28);
    knorProd     = params(29);
    %% Compute derivatives for the current state of the system
    dxdt = zeros(17,1);    
    
    
    %Pnor0 (Plasmids with both DBxb1 binding sites free)
    dxdt(1)= -knor1*NoR*Pnor0+k_nor1*Pnor1;%Pnor0 (NO instead of NoR_NO in all the system)

    
    %Pnor1 (Plasmids with one DBxb1 binding site occupied)
    dxdt(2)= knor1*NoR*Pnor0-k_nor1*Pnor1...
             +k_nor2*Pnor2-knor2*Pnor1*NoR;

    
    %Pnor2 (Plasmids with both DBxb1 binding sites occupied)
    dxdt(3)= -k_nor2*Pnor2+knor2*Pnor1*NoR...
             -knor3*NoR*Pnor2+k_nor3*PnorV0;
    %NoR (Integrase)
    dxdt(4)= -knor1*NoR*Pnor0+k_nor1*Pnor1...
             +k_nor2*Pnor2-knor2*Pnor1*NoR...
             -knor3*NoR*Pnor2+k_nor3*PnorV0...
             -knor_no*NoR*NO+k_nor_no*NoR_NO...
             -dnor*NoR+knorProd*(kl+(1-kl)*Ptot);
    %dxdt(1)=0;
    
    %NoR_NO 
    dxdt(5)= +knor_no*NO*NoR-k_nor_no*NoR_NO...
             -dnor_no*NoR_NO;
             

    %PnorV0 (Plasmids with both DBxb1 binding sites free)
    dxdt(6)=  knor3*NoR*Pnor2-k_nor3*PnorV0...
             -knorV1*NO*PnorV0 + k_norV1*PnorV1;%Pnor0 (NO instead of NoR_NO in all the system)

    
    %PnorV1 (Plasmids with one DBxb1 binding site occupied)
    dxdt(7)= knorV1*NO*PnorV0-k_norV1*PnorV1...
             +k_norV2*PnorV2-knorV2*PnorV1*NO;

    
    %Pnor2 (Plasmids with both DBxb1 binding sites occupied)
    dxdt(8)= -k_norV2*PnorV2+knorV2*PnorV1*NO...
             -knorV3*NO*PnorV2+k_norV3*PnorV3;
    
    %Pnor3 (Promoter enabling the transcription of the output)
    dxdt(9)= knorV3*NO*PnorV2-k_norV3*PnorV3;%
    
    %EsaR
    dxdt(10)= 1/2*(-k5*(EsaR^2) +k_5*DesaR)...
            -0.1*EsaR...
            +pesar;
    %dxdt(7)=0;
    
    %DesaR
    dxdt(11)= -k_5*DesaR+k5*(EsaR^2) ...
            -k6*(AHL^2)*DesaR+k_6*DesaR_AHL;
            
     
    %DesaR_AHL    
    dxdt(12)= k6*(AHL^2)*DesaR-k_6*DesaR_AHL...
             -k7*DesaR_AHL*Pfree+k_7*Pesar1*(AHL^2)...
             -k7*DesaR_AHL*Pout+k_7*Pesar2*(AHL^2);

    %Pfree
    dxdt(13)= -k7*DesaR_AHL*Pfree+k_7*Pesar1*(AHL^2);
    
    %Pesar1
    dxdt(14)= k7*DesaR_AHL*Pfree-k_7*Pesar1*(AHL^2);
    
    %Pesar2
    dxdt(15)= k7*DesaR_AHL*Pout-k_7*Pesar2*(AHL^2);

    %Pout
    dxdt(16)= -k7*DesaR_AHL*Pout+k_7*Pesar2*(AHL^2);
    
    %Phyb
    Phyb=(Pfree/Ptot)*(PnorV3/Ptot)*Ptot ;
    
    %mran (bxb)
    dxdt(17)= kmrna*(kl+(1-kl)*Phyb)...%*Ptot
              -dmrna*mRNAinv;
