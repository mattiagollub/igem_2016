function andGateMA(tspan, timesMap, speciesMap, params_set)
% SWITCHMA    Mass action simulation of the switch. 
%   Simulates the concentration of the switch species on the given time
%   interval. Input and outputs are placed in the timesMap and speciesMap
%   structures respectively.

    %% Common constants
    V = 1e-15;              % Cell volume in �m
    nP = 15;                % Number of plasmids
    Na = 6.022140857e23;    % Avogadro constant

    %% Simulation settings

    % Input data
    t_NO = timesMap('NO');
    t_AHL = timesMap('AHL');
    NO = speciesMap('NO');
    AHL = speciesMap('AHL');
    
    %% Parameters
    S_tot = nP * 1e9 / (Na * V); % nM, Total concentration of plasmids
    nortot= 150*1e9 / (Na * V);
    pnortot= nP * 1e9 / (Na * V);
    esartot= 60*1e9 / (Na * V);;
    stot= nP * 1e9 / (Na * V);
    pdesar= 1000 * 1e9 / (Na * V);
    
    P = loadParameters(params_set);
    
    knor_no   =P('knor_no');
    k_nor_no  =P('k_nor_no');
    knorV1   =P('knorV1');
    k_norV1  =P('k_norV1');
    knorV2   =P('knorV2');
    k_norV2  =P('k_norV2');
    knorV3   =P('knorV3');
    k_norV3  =P('k_norV3');
    knor1    =P('knor1');
    k_nor1   =P('k_nor1');
    knor2    =P('knor2');
    k_nor2   =P('k_nor2');
    knor3    =P('knor3');
    k_nor3   =P('k_nor3');
    k5       =P('k5');
    k_5      =P('k_5');
    k6       =P('k6');
    k_6      =P('k_6');
    k7        =P('k7');
    k_7       =P('k_7');
    pnor      =P('pnor');
    dnor      =P('dnor');
    dnor_no   =P('dnor_no');
    pesar     =P('pesar');
    Ptot      =P('Ptot');
    kmrna     =P('kmrna');
    dmrna     =P('dmrna');
    kl        =P('kl');
    knorProd  =P('knorProd');
    parameters = [knor_no,k_nor_no,knorV1, k_norV1 knorV2, k_norV2, knorV3,...
                  k_norV3,knor1, k_nor1 knor2, k_nor2, knor3,  k_nor3 ...
                  k5, k_5, k6, k_6, k7, k_7,...
                  pnor dnor, dnor_no,...
                  pesar, Ptot, kmrna, dmrna, kl, knorProd];

    %% Initial conditions and integrator options
    
    %Pnor0,Pnor1, Pnor2, PnorV0, NorR, norR_NO, PnorV1, pnorV2,
    %pnorV3,Esar, desar desar_ahl, pfree, pesar1, pesar2, pout, mrna
    x0 = [pnortot,0,0,0,0,0,0,0,0,esartot,0,0,0,pnortot,pnortot,0,0];
    options = odeset('AbsTol', 1e-1, 'RelTol', 1e-1);

    %% Run simulation
    [t, x] = ode45( ...
        @(t, y) andGateMAstep(t, y, t_NO, NO, t_AHL, AHL, parameters), ...
        tspan, x0, options);

    %% Write output
    timesMap('Pnor0')       = t;
    timesMap('Pnor1')       = t;
    timesMap('Pnor2')       = t;
    timesMap('NoR')         = t;
    timesMap('NoR_NO')      = t;
    timesMap('PnorV0')      = t;
    timesMap('PnorV1')      = t;
    timesMap('PnorV2')      = t;
    timesMap('PnorV3')      = t;
    timesMap('EsaR')        = t;
    timesMap('DesaR')       = t;
    timesMap('DesaR_AHL')   = t;
    timesMap('Pfree')       = t;
    timesMap('Pesar1')      = t;
    timesMap('Pesar2')      = t;
    timesMap('Pout')        = t;
    timesMap('mRNAinv')     = t;
    
    speciesMap('Pnor0')     = x(:,1);
    speciesMap('Pnor1')     = x(:,2);
    speciesMap('Pnor2')     = x(:,3);
    speciesMap('NoR')       = x(:,4);
    speciesMap('NoR_NO')    = x(:,5);
    speciesMap('PnorV0')    = x(:,6);
    speciesMap('PnorV1')    = x(:,7);
    speciesMap('PnorV2')    = x(:,8);
    speciesMap('PnorV3')    = x(:,9);
    speciesMap('EsaR')      = x(:,10);
    speciesMap('DesaR')     = x(:,11);
    speciesMap('DesaR_AHL') = x(:,12);
    speciesMap('Pfree')     = x(:,13);
    speciesMap('Pesar1')    = x(:,14);
    speciesMap('Pesar2')    = x(:,15);
    speciesMap('Pout')      = x(:,16);
    speciesMap('mRNAinv')   = x(:,17);

