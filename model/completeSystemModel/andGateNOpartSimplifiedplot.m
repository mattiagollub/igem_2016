%% ANDGATE NO part SIMPLIFIED PLOT    Example simulation of the switch. 
%   Simulates the concentration of the switch species using an example
%   input.
%   default NO and AHL values are 60 (range between 0 and 60)
%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
 
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 100;
NO_max=30;
NO_range=0:3:NO_max;
knorProdValue = (0:1:10);
mrna_max=zeros(length(knorProdValue),length(NO_range));
% Input: NO 
for j=1:length(NO_range)
    points = 100;
    NO_stimulus = NO_range(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.5/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;


%% Run simulation
    timesMap = containers.Map();
    timesMap('NO') = t_NO;


    speciesMap = containers.Map();
    speciesMap('NO') = NO;

    

    for i=1:length(knorProdValue)
        j
        i
        mrna_max(i,j)=andGateNOpartSimplified([t0, tf], timesMap, speciesMap, params_set, knorProdValue(i));
    end
end
%% Plot results

% mRNAinv input profile
figure(1);
surf( NO_range, knorProdValue,mrna_max);
xlabel('NO input value');
ylabel('NorR promoter strength');
zlabel('GFP ontensity');

