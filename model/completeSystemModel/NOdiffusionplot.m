V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
x0    = [500* 1e9 / (Na * V),0,1e-4* 1e9];
tend  = 10000;
tstep = 1;
tspan = t0:tstep:tend;
% integrator options
opt = odeset('AbsTol', 1e-10, 'RelTol', 1e-10);
%yp0=zeros(3,1);
% 
%% equations
[t, x] = ode45(@NOdiffusionplate, tspan, x0);
%plotTimeCourse(t1, x3, 1, '\bf simpleAndGate', t_sti, Sx_sti,Sy_sti, t0, tend, 0);
%[t1, x1] = ode45(@plotspecies, tspan, x0)%, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF);
figure(1)
clf;
plot(t,x(:,1));
hold on
plot(t,x(:,2));
grid on;
title('stimulus Nitric Oxyde')
xlabel('t (min)');
ylabel('concentration (nM)');
legend DETA NO