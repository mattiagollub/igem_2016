function dxdt = andGateSimplifiedStep(t, c, t_NO, NO, t_AHL, AHL, params)
% SWITCHMASTEP    Mass action ODE model of the switch. 
%   Computes the derivatives of the specie concentrations at a timestamp t.
   
    %% Switch species
    NO            = interp1(t_NO, NO, t);
    AHL           = interp1(t_AHL, AHL, t);
    NorR          = c(1);
    mRNAinv       = c(2);
    

    %% Switch parameters
    Ptot         = params(1);
    Kahl         = params(2);
    Kno          = params(3);
    nno          = params(4);
    nahl         = params(5);
    Kmahl        = params(6);
    Kmno         = params(7);
    k1           = params(8);
    k_1          = params(9);
    kmrna        = params(10);
    nortot       = params(11);
    pswitchtot   = params(12);
    K9           = params(13);
    Ptot         = params(14);
    kmrna        = params(15);      
    kl           = params(16);
    knorProd     = params(17);
    Knor         = params(18);
    Kmnor        = params(19);
    nnor         = params(20);
    dnor         = params(21);
    %% Compute derivatives for the current state of the system
    dxdt = zeros(2,1); 
    
    
    Pnoravailable = (Ptot*Knor*(NorR)^nnor)/(Kmnor + (NorR)^nnor);
    %NOrR production an dactivity
    dxdt(1)= knorProd*(kl+(1-kl)*Ptot)-dnor*NorR-3*Pnoravailable;
    
    %%intermediate equations
    pfree=(Ptot*Kahl*(AHL)^nahl)/(Kmahl + (AHL)^nahl)
    pnorV=(Pnoravailable*Kno*max((NO-NorR),0)^nno)/(Kmno + max((NO-NorR),0)^nno)
    Phyb=(pfree/Ptot)*(pnorV/Ptot)*Ptot
    

    %mRNAinv
    dxdt(2)= kmrna*(kl+(1-kl)*Phyb)-0.857*mRNAinv;
    %Pout
    
