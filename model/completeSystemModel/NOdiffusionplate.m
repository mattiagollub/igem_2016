function dxdt = NOdiffusionplate(t, c)    
    k1=0.000577; %(min^-1)calculated from the half-life of the species according to Lucas's paper
    k2=0.00100; %cell absorption (neglectible at first approximation
    kacq=5.4e-11; % Lucas ? (nM^-1 min^-1)
    r=8.9e-5;%(m s^-1) calculated from the diffusion coeffiscient of NO in water
    l=115e-12;%mean free path for NO in water (m)
    L=1e-2;%height of a petri dish (1cm=1e-2m)
    DETA  = c(1);
    NO    = c(2);
    O2    = c(3);
    %NO diffusion
    dxdt = zeros(3,1);    
    
    %DETA degradation/transformation into NO
    dxdt(1)= -k1*DETA;
    %dxdt(1)=0;
    
    %NO production and degradation
    dxdt(2)= k1*DETA...
             -k2*NO...
             -NO*r*25e-4*pi ...
             -(NO^2)*O2*kacq;
    %O2 degradation     
    dxdt(3) = -(NO^2)*O2*kacq;