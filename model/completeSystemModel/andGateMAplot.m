%% SWITCHMAPLOT    Example simulation of the switch. 
%   Simulates the concentration of the switch species using an example
%   input.

%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 100;
lineWidth = 1.5;
% Input: NO and AHL profile
points = 100;
NO_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
AHL_stimulus = 30 * 1e9 / (Na * V);
t_NO_ON = ceil((tf-t0)*0.1/tf*points);
t_NO_OFF = floor((tf-t0)*0.5/tf*points);
t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
t_NO = (0:points)/points*(tf-t0) + t0;
t_AHL = (0:points)/points*(tf-t0) + t0;
NO = zeros(size(t_NO));
AHL = zeros(size(t_AHL));
NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

%% Run simulation
timesMap = containers.Map();
timesMap('NO') = t_NO;
timesMap('AHL') = t_AHL;

speciesMap = containers.Map();
speciesMap('NO') = NO;
speciesMap('AHL') = AHL;

andGateMA([t0, tf], timesMap, speciesMap, params_set);

%% Plot results

% mRNAinv input profile
figure(1);
clf;
subplot(3, 2, 1);
stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
hold on
stairs(t_AHL, AHL, 'r', 'linewidth', 2);% Stairstep graph of the input
grid on;
title('Input: NO')
xlabel('Time [min]');
ylabel('Concentration [nMol]');
legend('NO');

 %% Plot NOR system
    ax = subplot(3, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(timesMap('NoR'), speciesMap('NoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(timesMap('NoR_NO'), speciesMap('NoR_NO'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('NOR system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NoR', 'NoR_NO');

    %% Plot PnorV behavior
    ax = subplot(3, 2, 3);
    ax.ColorOrderIndex = 1;
    p = plot(timesMap('mRNAinv'), speciesMap('mRNAinv'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(timesMap('Pout'), speciesMap('Pout'), 'linewidth', lineWidth);
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('mRNAinv', 'Pout');
    
    %% Plot DEsaR behavior
    ax = subplot(3, 2, 4);
    hold on;
    ax.ColorOrderIndex = 2;
    p = plot(timesMap('PnorV0'), speciesMap('PnorV0'), 'linewidth', lineWidth);
    hold on
    p = plot(timesMap('PnorV1'), speciesMap('PnorV1'), 'linewidth', lineWidth);
    hold on
    p = plot(timesMap('PnorV2'), speciesMap('PnorV2'), 'linewidth', lineWidth);
    hold on
    p = plot(timesMap('PnorV3'), speciesMap('PnorV3'), 'linewidth', lineWidth);
    hold on
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('DesaR behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0','PnorV1', 'PnorV2','PnorV3');
    
    %% Plot DEsaR_AHL behavior
    ax = subplot(3, 2, 5);
    hold on;
    ax.ColorOrderIndex = 3;
    p = plot(timesMap('Pnor0'), speciesMap('Pnor0'), 'linewidth', lineWidth);
    hold on
    p = plot(timesMap('Pnor1'), speciesMap('Pnor1'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(timesMap('Pnor2'), speciesMap('Pnor2'), 'linewidth', lineWidth);
    hold on
    p = plot(timesMap('PnorV0'), speciesMap('PnorV0'), 'linewidth', lineWidth);
    grid on;
    title('DesaR_AHL behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('Pnor0', 'Pnor1', 'Pnor2', 'PnorV0');
    
    %% Plot Pfree behavior
    ax = subplot(3, 2, 6);
    hold on;
    ax.ColorOrderIndex = 4;
    p = plot(timesMap('Pfree'), speciesMap('Pfree'), 'linewidth', 2);
    %p.Color(4) = 0 / (nRuns / 10);
    hold on
    p = plot(timesMap('PnorV3'), speciesMap('PnorV3'), 'linewidth', lineWidth);
    grid on;
    title('Pfree behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('Pfree', 'Pnor3');
    
    drawnow;
