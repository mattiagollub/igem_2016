function andGateSimplified(tspan, timesMap, speciesMap, params_set)
% SWITCHMA    Mass action simulation of the switch. 
%   Simulates the concentration of the switch species on the given time
%   interval. Input and outputs are placed in the timesMap and speciesMap
%   structures respectively.

    %% Common constants
   
    V = 1e-15;              % Cell volume in �m
    nP = 15;                % Number of plasmids
    Na = 6.022140857e23;    % Avogadro constant

    %% Simulation settings

    % Input data
    t_NO = timesMap('NO');
    t_AHL = timesMap('AHL');
    NO = speciesMap('NO');
    AHL = speciesMap('AHL');
    
    %% Parameters
    S_tot = nP * 1e9 / (Na * V); % nM, Total concentration of plasmids
    nortot=60*1e9 / (Na * V);
    pnortot= nP * 1e9 / (Na * V);
    esartot= 60*1e9 / (Na * V);;
    Stot= nP * 1e9 / (Na * V);
    pdesar= 1000 * 1e9 / (Na * V);
    
    P = loadParameters(params_set);
    
    Ptot =P('Ptot');
    Kahl = P('Kahl');
    Kno = P('Kno');
    nno = P('nno');
    nahl      =P('nahl');
    Kmahl     =P('Kmahl');
    Kmno      =P('Kmno');
    k1        =P('knor_no');
    k_1       =P('knor_no');
    kmrna     =P('kmrna');
    nortot    = P('nortot');
    pswitchtot= P('pswitchtot');
    K9        = P('K9');
    Ptot      = P('Ptot');
    kmrna     = P('kmrna'); 
    kl        = P('kl');
    Knor      = P('Knor');
    Kmnor     = P('Kmnor');
    nnor      = P('nnor');
    dnor      = P('dnor');
    knorProd  = P('knorProd');
    
    parameters = [Ptot Kahl Kno nno nahl Kmahl Kmno... 
                  k1 k_1 kmrna nortot pswitchtot K9 ...
                  Ptot kmrna kl knorProd Knor Kmnor nnor dnor];
    %% Initial conditions and integrator options
    x0 = [0,0];
    options = odeset('AbsTol', 1e-10, 'RelTol', 1e-10);

    %% Run simulation
    [t, x] = ode45( ...
        @(t, y) andGateSimplifiedStep(t, y, t_NO, NO, t_AHL, AHL, parameters), ...
        tspan, x0, options);
    %% output
    Pout = zeros(size(t_AHL));
    Pout=(Ptot*Kahl*(AHL).^nahl)./(Kmahl + (AHL).^nahl);
   

    %% Write output
    timesMap('NorR')        = t;
    timesMap('mRNAinv')     = t;
    timesMap('Pout')        = t_AHL;
    
    speciesMap('NorR')      = x(:,1);
    speciesMap('mRNAinv')   = x(:,2);
    speciesMap('Pout')      = Pout;

    
    %% KnorProd optimization loop
%     knorProdValue = (0:0.1:10);
%     mrna_max=zeros(length(knorProdValue));
%     for i=1:length(knorProdValue)
%         parameters(17)=knorProdValue(i);
%     [t, x] = ode45( ...
%         @(t, y) andGateSimplifiedStep(t, y, t_NO, NO, t_AHL, AHL, parameters), ...
%         tspan, x0, options);
%     mrna_max(i)=max(x(:,2));
%     end
%     figure(2)
%     plot(knorProdValue, mrna_max, '+');
%     xlabel('NorR promoter strength');
%     ylabel('mrna max reached concentration');
%     title('NorR promoter optimizatuion based on BxB1 mRNA production');
