function [t, x] = makeStimulusProfile(starts, ends, values, nPoints)
% MAKESTIMULUSPROFILE    Creates a stimulus profile. 
%   Creates a stimulus profile on the time interval [0, 1] where the
%   intervals [starts(i), ends(i)] are set to values(i). The remaining
%   intervals are set to 0. The profile is built using nPoints points.
    
    n = size(starts, 1);
    t = (0:nPoints) ./ nPoints;
    x = zeros(size(t));

    % Set stimuli on the intervals
    for i=1:n
        x(floor(starts(i)*nPoints+1):ceil(ends(i)*nPoints)+1) ...
            = values(i);
    end
end