%% AND_GATE_DATA FITTING    Example fitting parameters between the deterministic model
%   and the simplified one the and gate. 
%   Simulates the concentration of the and gate species using an example
%   input.

%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 100;

NO_value=100;
AHL_value=100;

NO_max=1;
AHL_max=2;


AHL_range=0:0.1:AHL_max;
NO_range=0:0.1:NO_max;
output_maxPfree=zeros(1,length(AHL_range));
output_maxPnorv3=zeros(1,length(NO_range));

%time=1:100:tf;



for i=1:length(AHL_range)
        % Input: NO and AHL profile
        points = 100;
        NO_stimulus = NO_value * 1e9 / (Na * V);    % Assume ~500 mRNAs
        AHL_stimulus = AHL_range(i) * 1e9 / (Na * V);
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.5/tf*points);
        t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        t_AHL = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        AHL = zeros(size(t_AHL));
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Run simulation
        %creation of the times map
        timesMap = containers.Map();
        timesMap('NO') = t_NO;
        timesMap('AHL') = t_AHL;

        %creation od the species map
        speciesMap = containers.Map();
        speciesMap('NO') = NO;
        speciesMap('AHL') = AHL;

        %run simulation
        andGateMA([t0, tf], timesMap, speciesMap, params_set);
        output_maxPfree(i)=max(speciesMap('Pfree'));
       
  
end
%% Plot results

% mRNAinv input profile
figure(1);
clf;
xdata = AHL_range;
ydata = output_maxPfree;
fun = @(x,xdata)(24.95.*x(1).*xdata.^x(2))./(x(3)+xdata.^x(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.5 2 0.1];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x = lsqcurvefit(fun,x0,xdata,ydata,lb,ub,options)
times = linspace(xdata(1),xdata(end));
%plotting
semilogx(xdata,ydata,'ko',times,fun(x,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pfree')


for j=1:length(NO_range)
        % Input: NO and AHL profile
        points = 100;
        NO_stimulus = NO_range(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        AHL_stimulus = AHL_value * 1e9 / (Na * V);
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.5/tf*points);
        t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        t_AHL = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        AHL = zeros(size(t_AHL));
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Run simulation
        %creation of the times map
        timesMap = containers.Map();
        timesMap('NO') = t_NO;
        timesMap('AHL') = t_AHL;

        %creation od the species map
        speciesMap = containers.Map();
        speciesMap('NO') = NO;
        speciesMap('AHL') = AHL;

         %run simulation
        andGateMA([t0, tf], timesMap, speciesMap, params_set);
        output_maxPnorv3(j)=max(speciesMap('PnorV3'));
  
end
figure(2);
clf;
xdata1 = NO_range;
ydata = output_maxPnorv3;
fun = @(x1,xdata1)(24.95.*x1(1).*xdata1.^x1(2))./(x1(3)+xdata1.^x1(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.1 2 0.001];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x1 = lsqcurvefit(fun,x0,xdata1,ydata,lb,ub,options)
times = linspace(xdata(1),xdata1(end));
%plotting
semilogx(xdata1,ydata,'ko',times,fun(x1,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pnor3')
