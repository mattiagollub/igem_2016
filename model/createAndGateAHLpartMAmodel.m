function model = createAndGateAHLpartMAmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);    % nM, Total concentration of plasmids
    pnortot= 24.9081;        %concentration of plasmid
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0,pnortot,0,0,pnortot,0,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'AHL'};
    
    %% Define symbolic variables for species and parameters
    
    
    syms AHL Esar DEsar DEsar_AHL1 DEsar_AHL Pdesar1 Pdesar1_AHL Pfree Pdesar2 Pdesar2_AHL Pout mRNAinv Bxb1
    syms k5 k_5 k_6 k6 k7 k_7 r...
         kmrna kl kesarProd desar dmrna k_Bxb1 d_Bxb1 desarahl 
    
    model.specieNames = [AHL  Esar DEsar DEsar_AHL1 DEsar_AHL Pdesar1 Pdesar1_AHL Pfree Pdesar2 Pdesar2_AHL Pout mRNAinv Bxb1];
    model.paramNames = [k5 k_5 k_6 k6 k7 k_7 ...
                        kmrna kl kesarProd desar dmrna k_Bxb1 d_Bxb1 desarahl]; 
    
    %% Stoichiometric and rate matrices
    % AHL | Esar | DEsar | DEsar_AHL1 | DEsar_AHL | Pdesar1 | Pdesar1_AHL |Pfree| Pdesar2 | Pdesar2_AHL | Pout |
    % mRNAinv | Bxb1
    model.N = [ 
     0,     1,  0,  0,   0,  0,  0,  0,  0,   0,   0,  0,  0;    % ->mRNA ESar
  %   0,   -1,  0,  0,  0,   0,  0,  0,  0,  0,   0,   0,  0,  0;    % mRNA ESar->
     0,     -2,  1,  0,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % EsaR + Esar -> DEsar
     0,      2, -1,  0,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % EsaR + Esar <- DEsar
     0,      0, -1,  1,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % AHL + DEsar -> DEsar_AHL1
     0,      0,  1, -1,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % AHL + DEsar <- DEsar_AHL1
     0,      0,  0, -1,  1,   0,  0,  0,  0,   0,   0,   0,  0;   % AHL + DEsar_AHL1 -> DEsar_AHL
     0,      0,  0,  1, -1,   0,  0,  0,  0,   0,   0,   0,  0;   % AHL + DEsar_AHL1 <- DEsar_AHL
     0,      0,  0,  0, -1,   0,  1, -1,  0,   0,   0,   0,  0;   % DEsar_AHL + Pfree -> Pesar1_AHL + AHL
     0,      0,  0,  0,  1,   0, -1,  1,  0,   0,   0,   0,  0;   % DEsar_AHL + Pfree <- Pesar1_AHL + AHL
     0,      0,  0,  0,  0,   1, -1,  0,  0,   0,   0,   0,  0;   % Pdesar1_AHL -> Pesar1 + AHL
     0,      0,  0,  0,  0,  -1,  1,  0,  0,   0,   0,   0,  0;   % Pdesar1_AHL <- Pesar1 + AHL
     0,      0,  0,  0, -1,   0,  0,  0,  0,   1,  -1,   0,  0;   % DEsar_AHL + Pout -> Pesar2_AHL + AHL
     0,      0,  0,  0,  1,   0,  0,  0,  0,  -1,   1,   0,  0;   % DEsar_AHL + Pout <- Pesar2_AHL + AHL
     0,      0,  0,  0,  0,   0,  0,  0,  1,  -1,   0,   0,  0;   % Pdesar2_AHL -> Pesar2 + AHL
     0,      0,  0,  0,  0,   0,  0,  0, -1,   1,   0,   0,  0;   % Pdesar2_AHL <- Pesar2 + AHL
     0,      0,  0,  0,  0,   0,  0,  0,  0,   0,   0,   1,  0;   % Pfree-> mRNAinv      
    % 0,      1,  0,  0,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % mRNA -> mRNA + EsaR     
     0,     -1,  0,  0,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % EsaR ->
     0,      0, -1,  0,  0,   0,  0,  0,  0,   0,   0,   0,  0;   % DEsar -> 
     0,      0,  0,  0,  0,   0,  0,  0,  0,   0,   0,  -1,  0;   % mRNAinv -> 
     0,      0,  0,  0,  0,   0,  0,  0,  0,   0,   0,   0,  1;   % mRNA1 -> mRNA1 + Bxb1
     0,      0,  0,  0,  0,   0,  0,  0,  0,   0,   0,   0, -1;   % Bxb1->
     0,      0,  0,  0, -1,   0,  0,  0,  0,   0,   0,   0,  0;   % DEsar_AHL->
     ]';
    
    %% Reaction rates
    model.rates = [
            kesarProd*Ptot; % -> EsaR
 %           dmrna*mRNA_Esar;  
            k5*Esar*(Esar-1);             % EsaR + Esar -> DEsar
            k_5*DEsar;                % EsaR + Esar <- DEsar
            k6*AHL*DEsar;            % AHL + DEsar -> DEsar_AHL1
            k_6*DEsar_AHL1;            % AHL + DEsar <- DEsar_AHL1
            k6*AHL*DEsar_AHL1;         % AHL + DEsar_AHL1 -> DEsar_AHL
            k_6*DEsar_AHL;            % AHL + DEsar_AHL1 <- DEsar_AHL
            k_7*DEsar_AHL*Pfree;       % DEsar_AHL + Pfree -> Pesar1_AHL + AHL 
            k7*Pdesar1_AHL*AHL;        % DEsar_AHL + Pfree <- Pesar1_AHL + AHL
            k_7*Pdesar1_AHL;       % Pdesar1_AHL -> Pesar1 + AHL
            k7*Pdesar1*AHL;        % Pdesar1_AHL <- Pesar1 + AHL
            k_7*DEsar_AHL*Pout;       % DEsar_AHL + Pout -> Pesar2_AHL + AHL 
            k7*Pdesar2_AHL*AHL;        % DEsar_AHL + Pout <- Pesar2_AHL + AHL
            k_7*Pdesar2_AHL;       % Pdesar2_AHL -> Pesar2 + AHL
            k7*Pdesar2*AHL;        % Pdesar2_AHL <- Pesar2 + AHL
           (kl*(Ptot-Pfree))+(kmrna*Pfree); %Pfree -> mrna inv
         %   rbs_Esar*mRNA_Esar ;  % -> Esar
            desar*Esar;                 % EsaR ->
            desar*DEsar;              % DEsaR ->
            dmrna*mRNAinv;          % mRNAinv ->
            k_Bxb1*mRNAinv;         % mRNAinv -> mRNAinv + Bxb1
            d_Bxb1*Bxb1;                    % Bxb1 ->
            desarahl*DEsar_AHL;              % DEsaR ->
    ];

    %% Reaction propensities
    model.a = [
           (kesarProd*kl)+(kesarProd*(1-kl)*Ptot); % -> mrNA
       %     dmrna*mRNA_Esar; 
            n2nM(k5)/2*(Esar)*(Esar-1);             % EsaR + Esar -> DEsar
            k_5*DEsar;                % EsaR + Esar <- DEsar
            n2nM(k6)*AHL*DEsar;            % AHL + DEsar -> DEsar_AHL1
            k_6*DEsar_AHL1;            % AHL + DEsar <- DEsar_AHL1
            n2nM(k6)*AHL*DEsar_AHL1;         % AHL + DEsar_AHL1 -> DEsar_AHL
            k_6*DEsar_AHL;            % AHL + DEsar_AHL1 <- DEsar_AHL
            n2nM(k_7)*DEsar_AHL*Pfree;       % DEsar_AHL + Pfree -> Pesar1_AHL + AHL 
            n2nM(k7)*Pdesar1_AHL*AHL;        % DEsar_AHL + Pfree <- Pesar1_AHL + AHL
            k_7*Pdesar1_AHL;       % Pdesar1_AHL -> Pesar1 + AHL
            n2nM(k7)*Pdesar1*AHL;        % Pdesar1_AHL <- Pesar1 + AHL
            n2nM(k_7)*DEsar_AHL*Pout;       % DEsar_AHL + Pout -> Pesar2_AHL + AHL 
            n2nM(k7)*Pdesar2_AHL*AHL;        % DEsar_AHL + Pout <- Pesar2_AHL + AHL
            k_7*Pdesar2_AHL;       % Pdesar2_AHL -> Pesar2 + AHL
            n2nM(k7)*Pdesar2*AHL;        % Pdesar2_AHL <- Pesar2 + AHL
           (kmrna*kl)+(kmrna*(1-kl)*Pfree) ; %Pfree -> mrna inv
        %    rbs_Esar*mRNA_Esar ;  % -> Esar  % -> Esar
            desar*Esar;                 % EsaR ->
            desar*DEsar;              % DEsaR ->
            dmrna*mRNAinv;          % mRNAinv ->
            k_Bxb1*mRNAinv;         % mRNAinv -> mRNAinv + Bxb1
            d_Bxb1*Bxb1;                    % Bxb1 ->
            desar*DEsar_AHL;              % DEsaR ->
    ];
end