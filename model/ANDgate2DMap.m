function M = ANDgate2DMap(model, paramName1, paramName2,paramRange1, paramRange2)

t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;

 [N1,N2]=meshgrid(paramRange1, paramRange2)

 [I1,I2]=meshgrid(logspace(log10(paramRange1(1)),log10(paramRange1(end)),800),...
                   logspace(log10(paramRange2(1)),log10(paramRange2(end)),800));

M=zeros(length(paramRange1), length(paramRange2));
% modelNO = createAndGateNOpartMAmodel();
% modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(model.paramsSet);

ParamMap('kesarProd')=0.05;
ParamMap('desar')=8*0.2;
ParamMap('knorProd')=3;
ParamMap('dnor')=0.1;

figure;
    colormap('jet');
    yticklabels=paramRange1;
    xticklabels=paramRange2;
    
for j=1:length(paramRange1)
    for i=1:length(paramRange2)
        
    j
    i
    paramRange1(j)
    paramRange2(i)
    
    ParamMap(paramName1)=paramRange1(j);
    ParamMap(paramName2)=paramRange2(i);
    
    model.paramsSet = ParamMap;
    
    [t_NO, NO] = makeStimulusProfile(0.2, 1.0, paramRange1(j), 100);
    t_NO = t_NO * (tf - t0) + t0;
    
    [t_AHL, AHL] = makeStimulusProfile(0.2, 1.0, paramRange2(i), 100);
    t_AHL = t_AHL * (tf - t0) + t0;
    
    
    T = model.timesMap;
    S = model.speciesMap;
    T('NO') = t_NO;
    S('NO') = NO;
    
    T('AHL') = t_AHL;
    S('AHL') = AHL;
    
    deterministicSimulation([t0, tf], model);
    R1=nM2n(S('PnorV3')/15)
    R2=nM2n(S('Pfree')/15)
    M(j,i)=R1(end)*R2(end)
%     R1=nM2n(S('PnorV3_Pfree')/15)
%     M(j,i)=R1(end)

    
    N=interp2(N1, N2, M, I1, I2);
    imagesc(flipud(N));
    ytick = linspace(1, size(I1, 1), 6);
    xtick = linspace(1, size(I1, 2), 6);
    set(gca, 'XTick', xtick, 'XTickLabel', xticklabels);
    set(gca, 'YTick', ytick, 'YTickLabel',flipud(yticklabels(:)));
    grid on
    title('number of active hybrid promoter for range of input values');
    
    xlabel('AHL input concentration rate [nM]');
    ylabel('NO input concentration rate [nM]');
    colorbar;
    drawnow;
    end
     
end


