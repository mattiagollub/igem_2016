function model = createAndGateMAfullmodel()
%CREATEANDGATEFULLMAMODEL Defines a mass action model of the AHL PART OF THE GATE.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0,0,0,0,0,0,0,pnortot,0,0,0,0,0,0,0,0,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-1, 'AbsTol',1e-1);
    model.inputs = {'AHL', 'NO'};
    
    %% Define symbolic variables for species and parameters
    syms AHL  NO  NoR  NoR_NO  DNoR  DNoR_NO1 DNoR_NO...
         EsaR  DEsaR DEsaR_AHL1 DEsar_AHL...
         PnorV0_Pfree  PnorV1_Pfree  PnorV2_Pfree  PnorV3_Pfree ...
         PnorV0_Pesar  PnorV1_Pesar  PnorV2_Pesar  PnorV3_Pesar ...
         PnorV0_Pesar1  PnorV1_Pesar1  PnorV2_Pesar1  PnorV3_Pesar1 ... 
        
                        
    syms kdnor k_dnor knor_no k_nor_no knorV1 k_norV1 knorV2 k_norV2...
         knorV3 k_norV3 kmrna kl knorProd dnor dnor_no kesar k_esar...
         k5 k_5 k_6 k6 k7 k_7 kmrna kl kesarProd desar...
         k7 k_7 knorV1 k_norV1 knorV2 k_norV2 knorV3 k_norV3
    
    model.specieNames = [AHL  NO  NoR  NoR_NO  DNoR  DNoR_NO1 DNoR_NO...
                         EsaR  DEsaR DEsaR_AHL1 DEsar_AHL...
                         PnorV0_Pfree  PnorV1_Pfree  PnorV2_Pfree  PnorV3_Pfree ...
                         PnorV0_Pesar  PnorV1_Pesar  PnorV2_Pesar  PnorV3_Pesar ...
                         PnorV0_Pesar1  PnorV1_Pesar1  PnorV2_Pesar1  PnorV3_Pesar1 ... 
                         ];
                     
    model.paramNames =  [kdnor k_dnor knor_no k_nor_no knorV1 k_norV1 knorV2 k_norV2...
                         knorV3 k_norV3 kmrna kl knorProd dnor dnor_no...
                         k5 k_5 k_6 k6 k7 k_7 kmrna kl kesarProd desar kesar k_esar...
                         k7 k_7 knorV1 k_norV1 knorV2 k_norV2 knorV3 k_norV3]; 
    
    %% Stoichiometric and rate matrices
    % AHL | NO | NoR | NoR_NO | DNoR | DNoR_NO1 | DNoR_NO
    % EsaR | DEsaR |DEsaR_AHL1 |DEsar_AHL
    % PnorV0_Pfree | PorV1_Pfree | PnorV2_Pfree | PnorV3_Pfree |
    % PnorV0_Pesar | PnorV1_Pesar | PnorV2_Pesar | PnorV3_Pesar |
    % PnorV0_Pesar1 | PnorV1_Pesar1 | PnorV2_Pesar1 | PnorV3_Pesar | 
    

 matrixNOSyst = [       
    0,   0,  -1,   1,    0,  0,  0;   % 0;    % NO +NoR  -> NoR_NO 
    0,   0,   1,  -1,    0,  0,  0;    % 0;    % NO +NoR  <- NoR_NO 
    0,   0,   0,  -2,    0,  0,  1;    % 0;    % 2NoR_NO  -> DNoR_NO 
    0,   0,   0,   2,    0,  0, -1;   % 0;    % 2NoR_NO  <- DNoR_NO 
    0,   0,  -2,   0,    1,  0,  0;   % 0;    % 2NoR  -> DNoR 
    0,   0,   2,   0,   -1,  0,  0;    % 0;    % 2NoR  <- DNoR 
    0,   0,   0,   0,   -1,  1,  0;   % 0;    % DNoR + NO -> DNoR_NO1
    0,   0,   0,   0,    1, -1,  0;   % 0;    % DNoR + NO <- DNoR_NO1
    0,   0,   0,   0,    0, -1,  1;    % 0;    % DNoR_NO1 + NO -> DNoR_NO
    0,   0,   0,   0,    0,  1, -1;    % 0;    % DNoR_NO1 + NO <- DNoR_NO
    0,   0,   1,   0,    0,  0,  0;  % 0;    % -> NoR  
    0,   0,  -1,   0,    0,  0,  0; % 0;    % NoR ->  
    0,   0,   0,   0,   -1,  0,  0;  % 0;    % DNOR ->
    0,   0,   0,   0,    0,  0, -1;  % 0;    % DNoR_NO -> 
    0,   0,   0,  -1,    0,  0,  0;  % 0;    % NoR_NO -> 
 ];    
  
matrixAHLSyst = [    
   -2,  1,  0,  0;    % EsaR + Esar -> DEsar
    2, -1,  0,  0;    % EsaR + Esar <- DEsar
    0, -1,  1,  0;   % AHL + DEsar -> DEsar_AHL1
    0,  1, -1,  0;   % AHL + DEsar <- DEsar_AHL1
    0,  0, -1,  1;   % AHL + DEsar_AHL1 -> DEsar_AHL
    0,  0,  1, -1;   % AHL + DEsar_AHL1 <- DEsar_AHL
    1,  0,  0,  0;   % -> EsaR     
   -1,  0,  0,  0;    % EsaR ->
    0, -1,  0,  0;   % DEsar -> 
    0,  0,  0, -1;
];

matrixPhybsyst = [
%1  %2  %3  %4  %5  %6  %7  %8  %9  %10 %11 %12 %13  %14 %15 %16
-1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0; % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar
 1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0;

-1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1
 1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0;

 0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
 0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0;

 %%
-1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0; % PnorV0_Pfree + DNOR_NO <-> PnorV1_Pfree
 1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0; 

 0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0;   % PnorV0_Pesar + DNOR_NO <-> PnorV1_Pesar
 0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0;

 0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0;    % PnorV0_Pesar1 + DNOR_NO <-> PnorV1_Pesar1
 0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0;   

 %%
 0, -1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0;   % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
 0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0;  

 0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0;    % PnorV1_Pesar + AHL <-> PnorV1_Pesar1
 0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0;   

 0,  1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0;    % PnorV1_Pesar1  <-> PnorV1_Pfree + DEsar_AHL
 0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0;   

 %%
 0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0;   % PnorV1_Pfree + NO <-> PnorV2_Pfree
 0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0;   

 0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0;    % PnorV1_PseaR + NO <-> PnorV2_Pesar
 0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0;   

 0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0;    % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
 0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0;   

 %%
 0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0,  0;   % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
 0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0;   

 0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0;   % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
 0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0;   

 0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1,  0;    % PnorV2_Pesar1  <-> PnorV2_Pfree + DEsar_AHL
 0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0;   

 %%
 0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0;   % PnorV2_Pfree + NO <-> PnorV3_Pfree
 0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0;   

 0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0;   % PnorV2_Pesar + NO <-> PnorV3_Pesar
 0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0;  

 0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1;   % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1;   

 %%
 0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0;  % PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
 0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0;   

 0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
 0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1;

 0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1; % PnorV3_Pesar1 + AHL  <-> PnorV3_Pfree + DEsar_AHL
 0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1;

];

%DEsaR |DEsaR_AHL| DNoR_NO
matrixHelpSpecies = [
    
   -1,  0,  0;
    1,  0,  0;
    
    0, -1,  0;
    0,  1,  0;
    
    0,  0,  0;
    0,  0,  0;
    
    0,  0, -1;
    0,  0,  1;
    
    0,  0, -1;
    0,  0,  1;
    
    0,  0, -1;
    0,  0,  1;
    
   -1,  0,  0;
    1,  0,  0;
    
    0,  0,  0;
    0,  0,  0;
    
    0,  1,  0;
    0, -1,  0;
    
    0,  0, -1;
    0,  0,  1;
    
    0,  0,  -1;
    0,  0,  1;
    
    0,  0, -1;
    0,  0,  1;
    
   -1,  0,  0;
    1,  0,  0;
    
    0,  0,  0;
    0,  0,  0;
    
    0,  1,  0;
    0, -1,  0;
    
    0,  0, -1;
    0,  0,  1;
    
    0,  0, -1;
    0,  0,  1;
    
    0,  0, -1;
    0,  0,  1;
    
   -1,  0,  0;
    1,  0,  0;
    
    0,  0,  0;
    0,  0,  0;
    
    0,  1,  0;
    0, -1,  0;
 ];
matrixFullSyst = zeros(67, 23);
matrixFullSyst(1:15, 1:7)=matrixNOSyst;
matrixFullSyst(16:25, 8:11)=matrixAHLSyst;
matrixFullSyst(26:67, 12:23)=matrixPhybsyst;
matrixFullSyst(26:67, 7)=matrixHelpSpecies(:,3);
matrixFullSyst(26:67, 9)=matrixHelpSpecies(:,1);
matrixFullSyst(26:67, 11)=matrixHelpSpecies(:,2);

   model.N = matrixFullSyst';
    
    %% Reaction rates
    model.rates = [
        
          knor_no*NO*NoR;               % NO +NoR  -> NoR_NO
          k_nor_no*NoR_NO;              % NO +NoR  <- NoR_NO
          kdnor*(NoR_NO^2);             % 2NoR_NO  -> DNoR_NO
          k_dnor*DNoR_NO;               % 2NoR_NO <- DNoR_NO
          kdnor*(NoR^2);                 % 2NoR  -> DNoR 
          k_dnor*DNoR;                   % 2NoR  <- DNoR 
          knor_no*DNoR*NO;                % DNoR + NO -> DNoR_NO1
          k_nor_no*DNoR_NO1;              % DNoR + NO <- DNoR_NO1
          knor_no*DNoR_NO1*NO;            % DNoR_NO1 + NO -> DNoR_NO
          k_nor_no*DNoR_NO;               % DNoR_NO1 + NO <- DNoR_NO
          (knorProd*kl)+(knorProd*(1-kl)*Ptot) ;  % -> NoR
          dnor*NoR;                      % NOR ->
          dnor*DNoR;                      % DNOR ->
          dnor_no*DNoR_NO;                 % DNoR_NO ->
          dnor_no*NoR_NO  ;               % NoR_NO ->
          
          k5*EsaR*(EsaR-1);             % EsaR + Esar -> DEsar
          k_5*DEsaR;                % EsaR + Esar <- DEsar
          k6*AHL*DEsaR;            % AHL + DEsar -> DEsar_AHL1
          k_6*DEsaR_AHL1;            % AHL + DEsar <- DEsar_AHL1
          k6*AHL*DEsaR_AHL1;         % AHL + DEsar_AHL1 -> DEsar_AHL
          k_6*DEsar_AHL;            % AHL + DEsar_AHL1 <- DEsar_AHL
          (kesarProd*kl)+(kesarProd*(1-kl)*Ptot) ;  % -> Esar
          desar*abs(EsaR);                 % EsaR ->
          desar*DEsaR;              % DEsaR ->
          desar*DEsar_AHL;           % DEsar_AHL ->
          
          
          k_7*PnorV0_Pfree*DEsaR; % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar 
          k7*PnorV0_Pesar;
          
          k_7*PnorV0_Pfree*DEsar_AHL; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1 +AHL
          k7*PnorV0_Pesar1*AHL;
          
          k7*PnorV0_Pesar*AHL; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
          k_7*PnorV0_Pesar1;
          
          knorV1*PnorV0_Pfree*DNoR_NO; % PnorV0_Pfree + NO <-> PnorV1_Pfree
          k_norV1*PnorV1_Pfree;
          
          knorV1*PnorV0_Pesar*DNoR_NO;% PnorV0_Pesar + NO <-> PnorV1_Pesar
          k_norV1*PnorV1_Pesar;
          
          knorV1*PnorV0_Pesar1*DNoR_NO;% PnorV0_Pesar1 + NO <-> PnorV1_Pesar1
          k_norV1*PnorV1_Pesar1;
          
          k7*PnorV1_Pfree*DEsaR; % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
          k_7*PnorV1_Pesar;
          
          k7*PnorV1_Pesar*AHL;% PnorV1_Pesar + AHL <-> PnorV1_Pesar1
          k_7*PnorV1_Pesar1;
          
          k7*PnorV1_Pesar1*AHL;% PnorV1_Pesar1 +AHL <-> PnorV1_Pfree + DEsar_AHL
          k_7*PnorV1_Pfree*DEsar_AHL;
          
          knorV2*PnorV1_Pfree*DNoR_NO; % PnorV1_Pfree + NO <-> PnorV2_Pfree
          k_norV2*PnorV2_Pfree;
          
          knorV2*PnorV1_Pesar*DNoR_NO % PnorV1_PseaR + NO <-> PnorV2_Pesar
          k_norV2*PnorV2_Pesar;
          
          knorV2*PnorV1_Pesar1*DNoR_NO; % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
          k_norV2*PnorV2_Pesar1;
          
          k7*PnorV2_Pfree*DEsaR; % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
          k_7*PnorV2_Pesar;
          
          k7*PnorV2_Pesar*AHL; % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
          k_7*PnorV2_Pesar1;
          
          k7*PnorV2_Pesar1*AHL ; % PnorV2_Pesar1 + AHL  <-> PnorV2_Pfree + DEsar_AHL
          k_7*PnorV2_Pfree*DEsar_AHL;
          
          knorV3*PnorV2_Pfree*DNoR_NO; % PnorV2_Pfree + NO <-> PnorV3_Pfree
          k_norV3*PnorV3_Pfree;
          
          knorV3*PnorV2_Pesar*DNoR_NO; % PnorV2_Pesar + NO <-> PnorV3_Pesar
          k_norV3*PnorV3_Pesar;
          
          knorV3*PnorV2_Pesar1*DNoR_NO; % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
          k_norV3*PnorV3_Pesar1;
          
          k7*PnorV3_Pfree*DEsaR% PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
          k_7*PnorV3_Pesar;
          
          k7*PnorV3_Pesar*AHL; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
          k_7*PnorV3_Pesar1;
          
          k7*PnorV3_Pesar1*AHL; % PnorV3_Pesar1 + AHL <-> PnorV3_Pfree + DEsar_AHL
          k_7*PnorV3_Pfree*DEsar_AHL;
    ];

    %% Reaction propensities
    model.a = [
          n2nM(knor_no)*NO*NoR;               % NO +NoR  -> NoR_NO
          k_nor_no*NoR_NO;              % NO +NoR  <- NoR_NO
          n2nM(kdnor)/2*(NoR_NO^2);             % 2NoR_NO  -> DNoR_NO
          k_dnor*DNoR_NO;               % 2NoR_NO <- DNoR_NO
          n2nM(kdnor)/2*(NoR^2);                 % 2NoR  -> DNoR 
          k_dnor*DNoR;                   % 2NoR  <- DNoR 
          n2nM(knor_no)*DNoR*NO;                % DNoR + NO -> DNoR_NO1
          k_nor_no*DNoR_NO1;              % DNoR + NO <- DNoR_NO1
          n2nM(knor_no)*DNoR_NO1*NO;            % DNoR_NO1 + NO -> DNoR_NO
          k_nor_no*DNoR_NO;               % DNoR_NO1 + NO <- DNoR_NO
          (knorProd*kl)+(knorProd*(1-kl)*Ptot) ;  % -> NoR
          dnor*NoR;                      % NOR ->
          dnor*DNoR;                      % DNOR ->
          dnor_no*DNoR_NO;                 % DNoR_NO ->
          dnor_no*NoR_NO  ;               % NoR_NO ->
          
          n2nM(k5)/2*EsaR*(EsaR-1);             % EsaR + Esar -> DEsar
          k_5*DEsaR;                % EsaR + Esar <- DEsar
          n2nM(k6)*AHL*DEsaR;            % AHL + DEsar -> DEsar_AHL1
          k_6*DEsaR_AHL1;            % AHL + DEsar <- DEsar_AHL1
          n2nM(k6)*AHL*DEsaR_AHL1;         % AHL + DEsar_AHL1 -> DEsar_AHL
          k_6*DEsar_AHL;            % AHL + DEsar_AHL1 <- DEsar_AHL
          (kesarProd*kl)+(kesarProd*(1-kl)*Ptot) ;  % -> Esar
          desar*abs(EsaR);                 % EsaR ->
          desar*DEsaR;              % DEsaR ->
          desar*DEsar_AHL;           % DEsar_AHL ->
          
          n2nM(k_7)*PnorV0_Pfree*DEsaR; % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar 
          k7*PnorV0_Pesar;
          
          n2nM(k_7)*PnorV0_Pfree*DEsar_AHL; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1 +AHL
          n2nM(k7)*PnorV0_Pesar1*AHL;
          
          n2nM(k7)*PnorV0_Pesar*AHL; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
          k_7*PnorV0_Pesar1;
          
          n2nM(knorV1)*PnorV0_Pfree*DNoR_NO; % PnorV0_Pfree + NO <-> PnorV1_Pfree
          k_norV1*PnorV1_Pfree;
          
          n2nM(knorV1)*PnorV0_Pesar*DNoR_NO;% PnorV0_Pesar + NO <-> PnorV1_Pesar
          k_norV1*PnorV1_Pesar;
          
          n2nM(knorV1)*PnorV0_Pesar1*DNoR_NO;% PnorV0_Pesar1 + NO <-> PnorV1_Pesar1
          k_norV1*PnorV1_Pesar1;
          
          n2nM(k7)*PnorV1_Pfree*DEsaR; % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
          k_7*PnorV1_Pesar;
          
          n2nM(k7)*PnorV1_Pesar*AHL;% PnorV1_Pesar + AHL <-> PnorV1_Pesar1
          k_7*PnorV1_Pesar1;
          
          n2nM(k7)*PnorV1_Pesar1*AHL;% PnorV1_Pesar1 +AHL <-> PnorV1_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV1_Pfree*DEsar_AHL;
          
          n2nM(knorV2)*PnorV1_Pfree*DNoR_NO; % PnorV1_Pfree + NO <-> PnorV2_Pfree
          k_norV2*PnorV2_Pfree;
          
          n2nM(knorV2)*PnorV1_Pesar*DNoR_NO % PnorV1_PseaR + NO <-> PnorV2_Pesar
          k_norV2*PnorV2_Pesar;
          
          n2nM(knorV2)*PnorV1_Pesar1*DNoR_NO; % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
          k_norV2*PnorV2_Pesar1;
          
          n2nM(k7)*PnorV2_Pfree*DEsaR; % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
          k_7*PnorV2_Pesar;
          
          n2nM(k7)*PnorV2_Pesar*AHL; % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
          k_7*PnorV2_Pesar1;
          
          n2nM(k7)*PnorV2_Pesar1*AHL ; % PnorV2_Pesar1 + AHL  <-> PnorV2_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV2_Pfree*DEsar_AHL;
          
          n2nM(knorV3)*PnorV2_Pfree*DNoR_NO; % PnorV2_Pfree + NO <-> PnorV3_Pfree
          k_norV3*PnorV3_Pfree;
          
          n2nM(knorV3)*PnorV2_Pesar*DNoR_NO; % PnorV2_Pesar + NO <-> PnorV3_Pesar
          k_norV3*PnorV3_Pesar;
          
          n2nM(knorV3)*PnorV2_Pesar1*DNoR_NO; % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
          k_norV3*PnorV3_Pesar1;
          
          n2nM(k7)*PnorV3_Pfree*DEsaR% PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
          k_7*PnorV3_Pesar;
          
          n2nM(k7)*PnorV3_Pesar*AHL; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
          k_7*PnorV3_Pesar1;
          
          n2nM(k7)*PnorV3_Pesar1*AHL; % PnorV3_Pesar1 + AHL <-> PnorV3_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV3_Pfree*DEsar_AHL;
    ];
end