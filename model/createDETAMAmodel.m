function model = createDETAMAmodel()
%CREATEREPORTERMAMODEL Defines a mass action model of the reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%       model = createReporterMAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
% 
    
%% Some variables

    Mno=30.01;%g�mol?1
    Mdeta=103.2;%g�mol?1
    Mo2= 16;%g�mol?1

%% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 = [0, 1e-3, 0];
    model.odeOptions = odeset('RelTol', 1e-10);
    model.inputs = {'AHL'};
    
    %% Define symbolic variables for species and parameters
    syms AHL DETA NO 
    syms kox kdeta
    model.specieNames = [AHL DETA NO];
    model.paramNames = [kdeta kox]; 
    
    %% Stoichiometric and rate matrices
    % AHL | DETA | NO  
    model.N = [
     0,  -1, 2;% DETA -> NO
     ]';  
    
    %% Reaction rates
    model.rates = [
        kdeta*DETA;      % DETA -> NO
    ];

  
    %% Reaction propensities
    model.a = [
        kdeta*DETA*60;      % DETA -> NO

	];
    
end