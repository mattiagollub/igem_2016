function [] = globalizeFunctionODE(model)
    clear;
addpath('HYPERSPACE');
addpath('HYPERSPACE/Files');
addpath('mexFunctions');

%%
switch model
    case 'modelNO'
        global modelNO  TNO SNO NOparamMap 
            tic;
         modelNO = createAndGateNOpartMAmodel();
         TNO = modelNO.timesMap;
         SNO = modelNO.speciesMap;
         NOparamMap=loadParameters('andgate_eth_igem_2014');  
         dxdt = modelNO.N * modelNO.rates;
         matlabFunction(dxdt, 'File', 'HYPERSPACE/NOode', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
         if isfield(modelNO, 'specieFunctions')
            sf = modelNO.specieFunctions;
         else
            sf = modelNO.specieNames';
         end
         matlabFunction(sf, 'File', 'HYPERSPACE/NOsf', ...
            'Vars', { modelNO.specieNames, modelNO.paramNames });
            toc 
 

    case 'modelAHL'
        global modelAHL TAHL SAHL AHLparamMap 
          tic;
         modelAHL = createAndGateAHLpartMAmodel();
         TAHL = modelAHL.timesMap;
         SAHL = modelAHL.speciesMap;
         AHLparamMap=loadParameters('andgate_eth_igem_2014');
         dxdt = modelAHL.N * modelAHL.rates;
         matlabFunction(dxdt, 'File', 'HYPERSPACE/c', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
        if isfield(modelAHL, 'specieFunctions')
        sf = modelAHL.specieFunctions;
        else
        sf = modelAHL.specieNames';
        end
         matlabFunction(sf, 'File', 'HYPERSPACE/AHLsf', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
           toc 
 
 
 
    case 'modelLact'
      
         global modelLact TLact SLact LactparamMap
               tic;
         modelLact = createSwitchV1MAmodel();
         TLact = modelSwitch.timesMap;
         SLact = modelSwitch.speciesMap;
         LactparamMap = loadParameters('andgate_eth_igem_2014');
           toc
 
    otherwise
        display('unknown model');
end
end