function dxdt = NOdiffusionplate(t, c)   
 
    k1=0.000577; %(min^-1)calculated from the half-life of the species according to Lucas's paper
    k2=0.00000; %cell absorption (neglectible at first approximation
    kacq=6.3e6*60; % Lucas ? (M^-1 min^-1)
    r=8.9e-5*60;%(m min^-1) calculated from the diffusion coeffiscient of NO in water
    l=115e-12;%mean free path for NO in water (m)
    L=1e-2;%height of a petri dish (1cm=1e-2m)
    v=25e-4*2e-2;%volume of the petri dishes
    %r=0;
    DETA  = c(1);
    NO    = c(2);
    O2    = c(3);
    NO2   = c(4);
    %NO diffusion
    dxdt = zeros(3,1);    
    
    %DETA degradation/transformation into NO
    dxdt(1)= -k1*DETA;
    %dxdt(1)=0;
    
    %NO production and degradation
    dxdt(2)= 2*k1*DETA...
             -0.5*(NO^2)*O2*kacq;
    %O2 degradation     
    dxdt(3) = -(NO^2)*O2*kacq;
    
    dxdt(4) = 0.5*kacq*NO^2*O2;
    
    
    % %-(NO*r*25e-4*pi)/v ...