function dcdtp = createSensitivityFunc(model)
%CRAETESENSITIVITYFUNCTION Creates a sensitivity function handle.
%   Builds an ODE system from the stoichiometric and rate matrices
%   contained in the specified model. Inserts the parameters in the system
%   and returns a function handle that computes the sensitivities of the
%   system in a given state.
%
%       h = createSensitivityFunc(model)
%
%   Returns:
%       h:              function handle in the form h = @(c, s) dcdtp(c, s)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Compute expressions for the ODEs
    dxdt = model.N * model.rates;
    nParams = size(model.paramNames, 2);
    
    %% Insert parameters
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    subsParameters = @(symExpr) subs( ...
        symExpr, model.paramNames, parameters);
    
    %% Compute Jacobians w.r.t. concentrations and parameters
    Jc = jacobian(dxdt, model.specieNames);
    Jp = jacobian(dxdt, model.paramNames);
                     
    %% Insert parameters and compute d*dc/(dt*dp)
    Jc = subsParameters(Jc);
    Jp = subsParameters(Jp);
    Jc_fn = matlabFunction(Jc, 'Vars', {model.specieNames});
    Jp_fn = matlabFunction(Jp, 'Vars', {model.specieNames});
    
    dcdtp = @(c, s) Jc_fn(c) * s + Jp_fn(c);
end