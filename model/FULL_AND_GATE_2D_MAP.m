%% Simulation settings
clear;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
nRuns = 500;
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 1.5;


%% Setup model
model = createAndGateMAfullmodel();


%% Run simulation
model.paramsSet = 'andgate_eth_igem_2014';



AHLrange=[0.1, 1,10,100, 1000, 10000];
NOrange=[1, 10, 100,1000, 10000, 100000];
M=ANDgate2DMap(model, 'NO', 'AHL', NOrange, AHLrange);