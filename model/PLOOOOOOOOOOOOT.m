function []=PLOOOOOOOOOOOOT()

% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range focr the experiment
t0   = 0;
ts   = 0.1;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;
nRuns=100;
% Input : AHL Activity
stimulus = 1000;   % Assume all promoters active

%% Setup model
modelAHL = createAndGateAHLpartMAmodel();
modelAHL.paramsSet = 'andgate_eth_igem_2014';
modelNO = createAndGateNOpartMAmodel();
modelNO.paramsSet = 'andgate_eth_igem_2014';
model = linkModels({'NO', 'AHL'}, [], ...
        modelNO, modelAHL);
T = model.timesMap;
S = model.speciesMap;
% T('NO') = t_NO;
% S('NO') = NO;
model.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(model.paramsSet);

ParamMap('kesarProd')=0.05;
ParamMap('desar')=8*0.2;
ParamMap('knorProd')=3;
ParamMap('dnor')=0.1;

   [t_NO, NO] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_NO = t_NO * (tf - t0) + t0;
    
    [t_AHL, AHL] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_AHL = t_AHL * (tf - t0) + t0;
    
    
    T = model.timesMap;
    S = model.speciesMap;
    T('NO') = t_NO;
    S('NO') = NO;
    
    T('AHL') = t_AHL;
    S('AHL') = AHL;
    
    deterministicSimulation([t0, tf], model);
    
    
    figure(500);
    clf;
    subplot(4,1,1);
    stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
    hold on
    stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: NO, AHL')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NO','AHL');
    
    subplot(4,1,2);
    hold on
    plot(T('Pout'),nM2n(S('Pout')), 'linewidth',2);
    title('Pout')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
   % legend('NO','AHL');
   
    subplot(4,1,3);
    hold on
    plot(T('Pfree'),nM2n(S('Pfree')), 'linewidth',2);
    title('Pfree')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    
    subplot(4,1,4);
    hold on
    plot(T('PnorV3'),nM2n(S('PnorV3'))*1.3, 'linewidth',2);
    title('PnorV3')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    
    stochasticSimulationNRM(tspan, model, 100);
    
    histPfree= model.histogramsMap('Pfree');
    histPout= model.histogramsMap('Pout');
    histPnorV3= model.histogramsMap('PnorV3');
   
    
    figure;

%     drawFunctions = containers.Map( ...
%         {'Pout', 'Pfree','PnorV3'}, ...
%         { ...
%             @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
%             @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
%             @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
%             @(t, x) drawInSubplot(t, x, 2, 4, lineWidth, 0.3), ...           
%         });
  
    subplot(4,1,1);
    stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
    hold on
    stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: NO, AHL')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NO','AHL');
    
    subplot(4,1,2);
    hold on
    plot(T('Pout'),histPout(:,1), 'linewidth',0.5);
    %plot(T('Pout'),nM2n(S('Pout')), 'black','linewidth',2);
    %plot(T('Pout'),pout, 'black','linewidth',2);
    title('Pout')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
   % legend('NO','AHL');
   
   subplot(4,1,3);
    hold on
    plot(T('Pout'),histPfree(:,10), 'linewidth',0.5);
    %plot(T('Pfree'),nM2n(S('Pfree')), 'black','linewidth',2);
    %plot(T('Pout'),pfre, 'black','linewidth',2);
    title('Pfree')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    
    subplot(4,1,4);
    hold on
    plot(T('Pout'),histPnorV3(:,10), 'linewidth',0.5);
    %plot(T('PnorV3'),nM2n(S('PnorV3')), 'black', 'linewidth',2);
    %plot(T('Pout'),pnor3, 'black','linewidth',2);
    title('PnorV3')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    
    
    
    
    end    
    
    
    function drawWithErrorBarInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(2, 2, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = errorbar(t, x, 'linewidth', lineWidth);
    p.Color(4) = alpha;
    end

    function drawInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(2, 2, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, x, 'linewidth', lineWidth);
    p.Color(4) = alpha;
    end