
% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input.
% 
%% Simulation settings
clear;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
nRuns = 500;
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 1.5;
% Input: NO activity
% points = 100;
%     NO_stimulus = 100 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_NO_ON = ceil((tf-t0)*0.1/tf*points);
%     t_NO_OFF = floor((tf-t0)*0.5/tf*points);
%     t_NO = (0:points)/points*(tf-t0) + t0;
%     NO = zeros(size(t_NO));
%     NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;


% Input : AHL Activity
% points = 100;
%     AHL_stimulus = 100 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
%     t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
%     t_AHL = (0:points)/points*(tf-t0) + t0;
%     AHL = zeros(size(t_AHL));
%     AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;
    stimulus = 100;   
    [t_AHL, AHL] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_AHL = t_AHL * (tf - t0) + t0;
% Input : DEsaR Activity
    stimulus = 100;   
    [t_NO, NO] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_NO = t_NO * (tf - t0) + t0;    


    % Input : DEsaR Activity
    stimulus = 100;   
    [t_DEsaR, DEsaR] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_DEsaR = t_DEsaR * (tf - t0) + t0;
    
    % Input : AHL Activity
    stimulus = 100;   
    [t_DEsar_AHL, DEsar_AHL] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_DEsar_AHL = t_DEsar_AHL * (tf - t0) + t0;
%% Setup model
model = createAndGateAHLpartMAfullmodel();
model.paramsSet = 'andgate_eth_igem_2014';





T = model.timesMap;
S = model.speciesMap;

T('DNoR_NO') = t_NO;
S('DNoR_NO') = NO;

T('AHL') = t_AHL;
S('AHL') = AHL;

T('DEsaR') = t_DEsaR;
S('DEsaR') = DEsaR;

T('DEsar_AHL') = t_DEsar_AHL;
S('DEsar_AHL') = DEsar_AHL;

%% Run simulation
deterministicSimulation([t0, tf], model);
stochasticSimulationNRM(tspan, model, nRuns);
%% Plot results
% mRNAinv input profile
    figure;
    clf;
    
    subplot(2, 2, 1);
    p = plot(T('DEsaR'), S('DEsaR'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DEsar_AHL'), S('DEsar_AHL'), 'linewidth', lineWidth);
    stairs(t_NO, NO, 'b','linewidth',2); % Stairstep graph of the input
    hold on
    stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input:DEsaR,DEsar_AHL, DNoR_NO, AHL')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('DEsaR','DEsar_AHL','DNoR_NO','AHL');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    p = plot(T('PnorV0_Pfree'), S('PnorV0_Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV0_Pesar'), S('PnorV0_Pesar'), 'linewidth', lineWidth);
    p = plot(T('PnorV0_Pesar1'), S('PnorV0_Pesar1'), 'linewidth', lineWidth);

    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0_Pfree', 'PnorV0_Pesar','PnorV0_Pesar1');
    
    %% Plot DEsaR behavior
    ax = subplot(2, 2, 4);
    hold on;
    ax.ColorOrderIndex = 2;
    p = plot(T('PnorV0_Pfree'), S('PnorV0_Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV1_Pfree'), S('PnorV1_Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV2_Pfree'), S('PnorV2_Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV3_Pfree'), S('PnorV3_Pfree'), 'linewidth', lineWidth);
    grid on;
    title('no effect behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0_Pfree','PnorV1_Pfree','PnorV2_Pfree','PnorV3_Pfree');
    
    ax = subplot(2, 2, 2);
    p = plot(T('PnorV0_Pesar'), S('PnorV0_Pesar'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV1_Pesar'), S('PnorV1_Pesar'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV2_Pesar'), S('PnorV2_Pesar'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV3_Pesar'), S('PnorV3_Pesar'), 'linewidth', lineWidth);
    hold on
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('esar binding behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0_Pesar','PnorV1_Pesar','PnorV2_Pesar','PnorV3_Pesar');
    
