function [M,L] = System2DMap(paramName1, paramName2,paramRange1, paramRange2)

t0   = 0;
ts   = 1;
tf   = 60*20;
tspan = t0:ts:tf;
lineWidth = 1.5;

nP = 15;            % Number of plasmids
Ptot = n2nM(nP);
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
kl=0.1;             %Estimated leakiness of the hybrid promoter

nRuns=10000;        %nomber of trajectories for the stochastic simulation
           %Estimated leakiness of the hybrid promoter

 [N1,N2]=meshgrid(paramRange1, paramRange2)

 [I1,I2]=meshgrid(logspace(log10(paramRange1(1)),log10(paramRange1(end)),800),...
                   logspace(log10(paramRange2(1)),log10(paramRange2(end)),800));

M=zeros(length(paramRange1), length(paramRange2));
L=zeros(length(paramRange1), length(paramRange2));
% modelNO = createAndGateNOpartMAmodel();
% modelNO.paramsSet = 'andgate_eth_igem_2014';



    
for j=1:length(paramRange1)
    for i=1:length(paramRange2)
        
    j
    i
    paramRange1(j)
    paramRange2(i)
    
%%
% Input: NO activity
stimulus = 10000;   % Assume all promoters active
[t_NO, NO] = makeStimulusProfile(0.2, 0.5, paramRange2(i), 100);
t_NO = t_NO * (tf - t0) + t0;

% Input: AHL activity
stimulus = 10000;   % Assume all promoters active
[t_AHL, AHL] = makeStimulusProfile([0.1, 0.8], [0.3, 1.0], [paramRange1(j),paramRange1(j)], 100);
t_AHL = t_AHL * (tf - t0) + t0;


%% AHL Deterministic part AND gate
modelAHL = createAndGateAHLpartMAmodel();
TAHL = modelAHL.timesMap;
SAHL = modelAHL.speciesMap;

TAHL('AHL') = t_AHL;
SAHL('AHL') = AHL;
modelAHL.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(modelAHL.paramsSet);
ParamMap('kesarProd')=0.05;
ParamMap('desar')=8*0.2;

%% NO Deterministic part AND gate
modelNO = createAndGateNOpartMAmodel();
TNO = modelNO.timesMap;
SNO = modelNO.speciesMap;

TNO('NO') = t_NO;
SNO('NO') = NO;
modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(modelNO.paramsSet);
ParamMap('knorProd')=3;
ParamMap('dnor')=0.1;

%% Run simulation
deterministicSimulation([t0, tf], modelNO);
deterministicSimulation([t0, tf], modelAHL);

%Compute Phyb_ON
a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
c=abs((kl+(1-kl)*(a.*b)/Ptot^2));

%% Switch
switchModel = createSwitchV2MAmodel();
switchModel.paramsSet = 'switch_bonnet';

%% Reporter
reporterModel = createReporterMAmodel();
reporterModel.paramsSet = 'reporter_literature';

%% LinksModel
model = linkModels({'Phyb_ON', 'Pout_free'}, [], switchModel, reporterModel);
T = model.timesMap;
S = model.speciesMap;

T('Phyb_ON') = TNO('NO');
S('Phyb_ON') = c;
T('Pout_free') = TAHL('Pout');
S('Pout_free') = SAHL('Pout')/Ptot;

toc;
    
    deterministicSimulation([t0, tf], model);

    R2=nM2n(S('GFP'))
    M(j,i)=R2(end)
    L(j,i)=max(R2);
%     R1=nM2n(S('PnorV3_Pfree')/15)
%     M(j,i)=R1(end)

    figure(1003);
    colormap('jet');
    yticklabels=paramRange1;
    xticklabels=paramRange2;
    Nm=interp2(N1, N2, M, I1, I2);
    imagesc(flipud(Nm));
    ytick = linspace(1, size(I1, 1), 4);
    xtick = linspace(1, size(I1, 2), 4);
    set(gca, 'XTick', xtick, 'XTickLabel', xticklabels);
    set(gca, 'YTick', ytick, 'YTickLabel',flipud(yticklabels(:)));
    grid on
    title('GFP cocentration in reporing phase');
    
    xlabel('AHL input concentration rate [nM]');
    ylabel('NO input concentration rate [nM]');
    colorbar;
    drawnow;
    
    
    figure(1002);
    colormap('jet');
    yticklabels=paramRange1;
    xticklabels=paramRange2;
    Nl=interp2(N1, N2, M, I1, I2);
    imagesc(flipud(Nl));
    ytick = linspace(1, size(I1, 1), 6);
    xtick = linspace(1, size(I1, 2), 6);
    set(gca, 'XTick', xtick, 'XTickLabel', xticklabels);
    set(gca, 'YTick', ytick, 'YTickLabel',flipud(yticklabels(:)));
    grid on
    title('GFP cocentration in learning phase');
    
    xlabel('AHL input concentration rate [nM]');
    ylabel('NO input concentration rate [nM]');
    colorbar;
    drawnow;
    end
     
end


