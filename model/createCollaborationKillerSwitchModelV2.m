function model = createCollaborationKillerSwitchModelV2()
%   createCollaborationKillerSwitchModel Defines a mass action model of the killer switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createCollaborationKillerSwitchModel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);    % nM, Total concentration of plasmids
    pnortot= 24.9081;   %concentration of plasmid
    %% Function
    
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,60,0,0,0]%,1];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'sigECF'};
    
    %% Define symbolic variables for species and parameters
    syms sigECF antiMaz MazE MazF DMazF ...
         DMazF_E2 cfu
    syms kProd dmazE dmazF dantiMaz kdmazf k_dmazf  ...
         cfucoeff Kd2 Kd1 Ktox ntox...
         Kanti nanti Kinhib ninhib kantimaz
    
    model.specieNames = [sigECF antiMaz MazE MazF DMazF ...
                         DMazF_E2 cfu];
    model.paramNames = [kProd dmazE dmazF dantiMaz kdmazf k_dmazf  ...
                        cfucoeff Kd2 Kd1 Ktox ntox...
                        Kanti nanti Kinhib ninhib kantimaz]; 
    
%% Stoichiometric and rate matrices
% sigECF | antiMaz| MazE | MazF | DMazF| DMazF_E2 | cfu             
    model.N = [
     0,   1,   0,   0,   0,   0,   0;%antimaz
     0,   0,   1,   0,   0,   0,   0;%mazE
     0,   0,   0,   1,   0,   0,   0;%mazF
     0,   0,   0,   0,   1,   0,   0;%DmazF
     0,   0,   0,   0,   0,   1,   0;%DMazF_E2
     %0,   0,   0,   0,   0,   0,   1;%cfu
     ]';
    
    %% Reaction rates
    model.rates = [
     ((kantimaz*(sigECF/Kanti)^nanti)./(1+(sigECF/Kanti)^nanti))-dantiMaz*antiMaz; % sigECF->antiMaz
     ((kProd)/((1+(antiMaz/Ktox)^ntox)*(1+(DMazF_E2/0.1*Kinhib)^ninhib)))- dmazE*MazE;%MazE
     ((kProd)/(1+(DMazF_E2/0.1*Kinhib)^ninhib))-dmazF*MazF;%MazF
     2*kdmazf*(MazF^2)-k_dmazf*DMazF;%DMazF
     1;
     ((-MazF-log(2))/cfucoeff)*cfu;%cfu
    ];
   % sigECF | antiMaz| Pconst | MazE | MazF | DMazF | DMazF_E | DMazF_E2
   %  Pblock
     model.specieFunctions = [
        sigECF;
        antiMaz;
        MazE;
        MazF;
        2*kdmazf*(MazF^2)/k_dmazf;
        (1/(Kd1*Kd2))*DMazF*(MazE^2);
        1-((-MazF-log(2))/cfucoeff)*cfu;
        ];  
    %% Reaction propensities
    model.a = [
     0.1*kantimaz+(((sigECF/Kanti)^nanti)./(1+(sigECF/Kanti)^nanti))-dantiMaz*antiMaz; % sigECF->antiMaz
     0.1*kProd+((kProd)/(1+(antiMaz/Kinhib)^nanti)*(1+(DMazF_E2/Kinhib)^ninhib))- dmazE*MazE;%MazE
     0.1*kProd+((kProd)/(1+(DMazF_E2/Kinhib)^ninhib))-dmazE*MazE;%MazF
     2*kdmazf*MazF-k_dmazf*DMazF;%DMazF
     1;
     ((-MazF-log(2))/cfucoeff)*cfu;%cfu
    ];
  

end