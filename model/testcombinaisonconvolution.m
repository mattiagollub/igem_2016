x_values=[1:15]
% dist1=500*normpdf(x_values, 12, 1.8);
% dist2=500*normpdf(x_values, 12, 1.44);
load('andgateNOpartStochastic.mat')
histsPnorV3 = model.histogramsMap('PnorV3');
PnorV3Dist = fitdist(histsPnorV3(end, :)','Lognormal');
dist1=pdf(PnorV3Dist,x_values);

load('AHLmodularPart.mat');
histsPfree = model.histogramsMap('Pfree');
PfreeDist = fitdist(histsPfree(end, :)','Lognormal');
dist2=pdf(PfreeDist,x_values);

x_values=[1:15]

Phybnorm = 500*convol(dist1,dist2);
f = fit(x_values.',Phybnorm,'gauss1')
mu=f.b1;
sigma=f.c1;
%dist = sigma*randn(500,1)+ abs(mu);
dist=zeros(500,1);
%Phyb=zeros(249);
% for t=1:1:250
%     Phyb(t)=sigma*randn(1,1)+ abs(mu);
% end
for t=1:1:500
    dist(t)=16;
    while dist(t)>15
    dist(t)=sigma*randn(1,1)+ abs(mu);
    end
end

figure;
hold on
histogram(dist);
plot(f,x_values,Phybnorm)
% plot(dist1);
% plot(dist2);
legend Phyb Phyb_{theorique}


figure;
load('andgatefullstatemodel.mat');
histsPnorV3_Pfree = model.histogramsMap('PnorV3_Pfree');
stdDev=zeros(500,1);
for i=1:nRuns
    stdDev(i)=nM2n(std(histsPnorV3_Pfree(:,i)));
end
%histogram(stdDev,100);
stdDec_Dist = fitdist(stdDev,'Lognormal');
sig=stdDec_Dist.sigma*randn(1,1)+ abs(stdDec_Dist.mu);
moy=sigma*randn(1,1)+ abs(mu);
ExTraj=zeros(359,1);
for t=1:1:360
    ExTraj(t)=sig*randn(1,1)+ abs(moy);
end
figure;
plot(n2nM(ExTraj));
hold on
plot(n2nM(histsPnorV3_Pfree(:, 150)));
ylabel(' concentration (nM)');
xlabel ('time (min)');
title ('Exemple de trajectoire');
legend('example trajectory', 'real trajectory');