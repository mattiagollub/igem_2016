 figure;
   clf;
    subplot(2, 1, 1);
    histsPfree = model.histogramsMap('Pfree');
    PfreeDist = fitdist(histsPfree(end, :)','Normal');
    histogram(histsPfree(end, :));
    hold on
    x_values = 0:1:30;
    y1 = pdf(PfreeDist,x_values);
    f = fit(x_values.',500*y1','gauss1')
    plot(f,x_values,500*y1)
    title(sprintf('Pfree distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');
    
    subplot(2, 1, 2);
    histsPnorV3 = model.histogramsMap('PnorV3');
    PnorV3Dist = fitdist(histsPnorV3(end, :)','Normal');
    histogram(histsPnorV3(end, :));
    hold on
    x_values = 0:1:30;
    y = pdf(PnorV3Dist,x_values);
    f = fit(x_values.',500*y','gauss1');
    plot(f,x_values,500*y)
    title(sprintf('PnorV3 distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');