% SWITCHMAPLOT Example simulation of the switch. 
%   Deterministically simulates the concentration of the switch species
%   using an example input.
% 
%% Simulation settings
clear;

% Time range for the experiment
t0   = 0;
tf   = 1*60;

% Input: Phyb activity
leakiness = 0.05;
stimulus = 1;       % Assume all promoters active
[t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile( ...
    [0.0, 0.3], [0.3, 1.0], [leakiness, stimulus], 100);
t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

%% Setup model
model = createSwitchV1MAmodel();
T = model.timesMap;
S = model.speciesMap;

T('Phyb_ON') = t_Phyb_ON;
S('Phyb_ON') = v_Phyb_ON;
model.paramsSet = 'switch_bonnet';

%% Run simulation
deterministicSimulation([t0, tf], model);

%% Plot results
figure(1);
clf;

% Plot Phyb_ON input profile
subplot(2, 2, 1);
plot(T('Phyb_ON'), S('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Fraction of Phyb activity')
xlabel('Time [min]');
ylabel('Phyb_{ON} fraction');
legend('Phyb^{ON}');
ylim([0, stimulus * 1.3]);

% Invertase mRNA production
subplot(2, 2, 2);
hold on;
plot(T('mRNAinv'), S('mRNAinv'), 'linewidth', 2);
grid on;
title('Invertase mRNA production');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('mRNAinv');

% Bxb1 dimerization
subplot(2, 2, 3);
hold on;
plot(T('Bxb1'), S('Bxb1'), 'linewidth', 2);
plot(T('DBxb1'), S('DBxb1'), 'linewidth', 2);
grid on;
title('Bxb1 dimerization');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Bxb1', 'DBxb1'); 

% Pout flipping
subplot(2, 2, 4);
hold on;
plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'linewidth', 2);
plot(T('S_0'), nM2n(S('S_0')), 'linewidth', 2);
plot(T('S_1'), nM2n(S('S_1')), 'linewidth', 2);
plot(T('S_2'), nM2n(S('S_2')), 'linewidth', 2);
grid on;
title('Pout flipping');
xlabel('Time [min]');
ylabel('Number of promoters');
l = legend('Pout^{flipped}', 'S_0', 'S_1', 'S_2'); 
l.Location = 'east';