function switchSSAplot()
% SWITCHSSAPLOT Example simulation of the switch. 
%   Simulates the concentration of the switch species using an example
%   input and a stochastic method.
%
    %% Simulation settings
    clear;
    nRuns = 50;
    lineWidth = 0.5;
    
    % Time range for the experiment
    t0   = 0;
    ts   = 0.2;
    tf   = 120;
    tspan = t0:ts:tf;
    
    % Histogram plotting
    histogramsX = 2;
    histogramsY = 2;
    nHistograms = histogramsX * histogramsY;

    % Input: Phyb activity
    stimulus = 1;   % Assume all promoters active
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

    %% Setup model
    model = createSwitchV2MAmodel();
    T = model.timesMap;
    S = model.speciesMap;

    T('Phyb_ON') = t_Phyb_ON;
    S('Phyb_ON') = v_Phyb_ON;
    model.paramsSet = 'switch_bonnet';

    %% Plot Phyb_ON input profile
    figure(1);
    clf;
    subplot(3, 2, 1);
    plot(T('Phyb_ON'), S('Phyb_ON'), 'linewidth', 2);
    grid on;
    title('Input: Fraction of Phyb activity');
    xlabel('Time [min]');
    ylabel('Phyb_{ON} fraction');
    legend('Phyb^{ON}');
    ylim([0, stimulus * 1.3]);

    drawFunctions = containers.Map( ...
        {'Bxb1', 'DBxb1', 'S_0', 'S_1', 'S_2', 'Pout_flipped'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 3, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 4, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 5, 5, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 6, 6, 1, 0.4), ...
        });

    %% Simulate system
    stochasticSimulationTDI(tspan, model, nRuns, drawFunctions);

    %% Plot Bxb1 dimerization
    subplot(3, 2, 2);
    hold on;
    plot(T('Bxb1'), nM2n(S('Bxb1')), 'k', 'linewidth', 2);
    plot(T('DBxb1'), nM2n(S('DBxb1')), 'k', 'linewidth', 2);
    grid on;
    title('Bxb1 dimerization');
    xlabel('Time [min]');
    ylabel('Number of molecules');
    legend('Bxb1', 'DBxb1');

    %% Plot S_0 behavior
    subplot(3, 2, 3);
    plot(T('S_0'), nM2n(S('S_0')), 'k', 'linewidth', 2);
    grid on;
    title('S_0 evolution');
    xlabel('Time [min]');
    ylabel('Number of promoters');

    %% Plot S_1 behavior
    subplot(3, 2, 4);
    plot(T('S_1'), nM2n(S('S_1')), 'k', 'linewidth', 2);
    grid on;
    title('S_1 evolution');
    xlabel('Time [min]');
    ylabel('Number of promoters');

    %% Plot S_2 behavior
    subplot(3, 2, 5);
    plot(T('S_2'), nM2n(S('S_2')), 'k', 'linewidth', 2);
    grid on;
    title('S_2 evolution');
    xlabel('Time [min]');
    ylabel('Number of promoters');

    %% Plot Pout_flipped behavior
    subplot(3, 2, 6);
    plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'k', 'linewidth', 2);
    grid on;
    title('Pout flipping');
    xlabel('Time [min]');
    ylabel('Number of promoters');
    
    %% Plot histograms
    figure(2);
    tH = round((1:nHistograms) / nHistograms * length(tspan));
    hist = model.histogramsMap('Pout_flipped');
    hist = hist(tH, :);
    
    for i=1:nHistograms
        subplot(histogramsY, histogramsX, i);
        histogram(hist(i, :));
        title(sprintf('Pout^{flipped} distribution at t=%d min', ...
            tspan(tH(i))));
        xlabel('Number of molecules');
        ylabel('Cell count');
    end
end

function drawInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(3, 2, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, nM2n(x), 'linewidth', lineWidth);
    p.Color(4) = alpha;
end