function distributionComparison()
% SWITCHACTIVITYRESPONSE Response of the switch to inputs. 
%   Simulates the concentration of the reporters with different activity
%   levels of the hybrid promoter.
%   
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 2000;

    % Dose inputs
    Phyb_ON_levels = n2nM([0.9, 0.4]);
    Phyb_ON_durations = [20, 80];
    
    % Template input 1: Phyb activity
    Phyb_states = [
        0.0, 0.0, 1.0, 1.0, 0.0,  0.0;
        0,   9.8, 9.9, 10,  10.1, 160.0];

    % Template input 2: Pout activity
    Pout_states = [
        0.0, 0.0,   1.0,    1.0;
        0,   99.9, 100.0,  160.0];
    
    %% Setup model
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], ...
        switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;
    
    figure(1);
    clf;
        
    %% Run simulation with different activity levels
    for i=1:length(Phyb_ON_levels)
            
        %% Setup inputs
        v_Phyb_ON = Phyb_states(1,:) * Phyb_ON_levels(i);
        t_Phyb_ON = [Phyb_states(2,1:3), ...
            Phyb_states(2,4:end) + Phyb_ON_durations(i)];
        v_Pout_free = Pout_states(1,:) * n2nM(1);
        t_Pout_free = [Phyb_states(2,1), ...
            Pout_states(2,2:end) + Phyb_ON_durations(i)];

        T('Phyb_ON') = t_Phyb_ON;
        S('Phyb_ON') = v_Phyb_ON;
        T('Pout_free') = t_Pout_free;
        S('Pout_free') = v_Pout_free;

        %% Simulate system
        tspan = (0:0.01:1) * t_Pout_free(end);
        stochasticSimulationTDI(tspan, model, nRuns, containers.Map(), i <= 1);

        %% Extract statistics from the simulation
        histGFP = model.histogramsMap('GFP');
        histmNect = model.histogramsMap('mNect');
       
        %% Draw histograms
        subplot(3, 1, 1);
        hold on;
        h = histogram(histGFP(end, :));
        h.BinWidth = 20;
        title('GFP distributions');
        xlabel('Number of molecules');
        ylabel('Number of cells');
        grid on;

        subplot(3, 1, 2);
        hold on;
        h = histogram(histmNect(end, :));
        h.BinWidth = 20;
        title('mNectarine distributions');
        xlabel('Number of molecules');
        ylabel('Number of cells');
        grid on;
        
        subplot(3, 1, 3);
        hold on;
        h = histogram(min(histmNect(end, :) ./ histGFP(end, :), 2));
        h.BinWidth = 0.04;
        title(sprintf('Distribution of reporter ratios at t=%d min', tspan(end)));
        xlabel('mNectarine/GFP ratio');
        ylabel('Number of cells');
        grid on;
        drawnow;
    end
    
    %% Add legends
    subplot(3, 1, 1);
    legend( ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(1)), ...
            Phyb_ON_durations(1)), ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(2)), ...
            Phyb_ON_durations(2)));

    subplot(3, 1, 2);
    legend( ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(1)), ...
            Phyb_ON_durations(1)), ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(2)), ...
            Phyb_ON_durations(2)));
        
    subplot(3, 1, 3);
    legend( ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(1)), ...
            Phyb_ON_durations(1)), ...
        sprintf('Phyb_{ON}=%.1f, t=%d min', ...
            nM2n(Phyb_ON_levels(2)), ...
            Phyb_ON_durations(2)));
end