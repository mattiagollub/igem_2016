function sf = Switchsf(in1,in2)
%SWITCHSF
%    SF = SWITCHSF(IN1,IN2)

%    This function was generated by the Symbolic Math Toolbox version 6.3.
%    12-Aug-2016 12:35:36

Bxb1 = in1(:,3);
DBxb1 = in1(:,4);
Phyb_ON = in1(:,1);
Pout_flipped = in1(:,8);
S_0 = in1(:,5);
S_1 = in1(:,6);
S_2 = in1(:,7);
Sf_0 = in1(:,9);
mRNAinv = in1(:,2);
sf = [conj(Phyb_ON);conj(mRNAinv);conj(Bxb1);conj(DBxb1);conj(S_0);conj(S_1);conj(S_2);conj(Pout_flipped);conj(Sf_0)];
