%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%sexpini.m
%
%Given a set of viable points, it samples uniformly in an ellipsoid with
%the same direction as the minimum volume ellipoid of the initial set but larger axes.  
%(The growth rate is constant)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [B EjesA c]=sexpini(V,dim,n,gini)
%
%Input: 
%       V=set of viable points
%       dim=dimension of the parameter space
%       n=number points sampled       
%       gini = initial scaling parameter for the first ellipsoid expanision
%
%Output: 
%        B = set of sampled parameter points
%        EjesA = matrix of the minimum volume ellipsoid that encloses V    
%        c = center of the minimum volume ellipsoid that encloses V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [auxs1 auxs2 auxs3]=sexpini(V,dim,n,gini)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D=zeros(dim);
E=ones(n,dim);
g=gini; %Axes growth rate


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Definición del conjunto. Esfera centrada en el origen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

B=randsphere(n,dim,1); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%desplazamiento del conjunto hasta centrarlo en el (0,0)%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[A, c] = lowner(V', .01);

[U,Q,W]=svd(A);


for i=1:dim
       
    D(i)=1/sqrt(Q(i,i));
    COEFF(:,i)=g*D(i)*W(:,i);
    

end


B=COEFF*B';

B=B';



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Centering the set in the center of the ellipsoid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:dim
    
    E(:,i)=c(i);
end


B=B+E;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Output of the final set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

auxs1=B;
auxs2=A;
auxs3=c;



