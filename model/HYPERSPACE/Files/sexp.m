%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%sexp.m
%
%Given a set of viable points, it samples uniformly in an ellipsoid with
%the same direction as the minimum volume ellipoid of the initial set but larger axes.  
%(The growth rate is adaptive)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [B EjesA c g vol]=sexp(V,dim,n,g0,dv,grate)
%
%Input: V=set of viable points
%       dim=dimension of the parameter space
%       n = points sampled
%       g0=axes growth rate in the last iteration
%       dv=new viable points found in the last iteration
%       grate = scaling parameter for the following ellipsoid expanisions
%
%Output: 
%        B = set of sampled parameter points
%        EjesA = matrix of the minimum volume ellipsoid that encloses V    
%        c = center of the ellipsoid
%        g=axes growth rate
%        vol=sampled volume    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [auxs1 auxs2 auxs3 auxs5 auxs6]=sexp(V,dim,n,g0,dv,grate)   

         

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D=zeros(dim);
E=ones(n,dim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Choosing the growth rate %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (dv>n/5)
    g=g0+(g0-1)/grate;
    
elseif (dv>n/10)
    g=g0;
    
else
    g=g0-(g0-1)/(grate+1);
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Definición del conjunto. Esfera centrada en el origen
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

B=randsphere(n,dim,1); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%desplazamiento del conjunto hasta centrarlo en el (0,0)%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[A, c] = lowner(V', .005);

[U,Q,W]=svd(A);


for i=1:dim
       
    D(i)=1/sqrt(Q(i,i));
    COEFF(:,i)=g*D(i)*W(:,i);
    

end

vol=volellip(dim,g*D);

B=COEFF*B';

B=B';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Centering the set in the mean value of V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:dim

    E(:,i)=c(i);
end


B=B+E; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Output of the final set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


auxs1=B;
auxs2=A;
auxs3=c;
auxs5=g;
auxs6=vol;


