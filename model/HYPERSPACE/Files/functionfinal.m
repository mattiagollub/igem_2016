function [varargout] = functionfinal(vk)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% functionfinal
% Generated: 09-Sep-2011 14:37:29
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Global variables
global cuenta;
global viable;
global threshold;

dim=length(vk);

cuenta=cuenta+1;
cost=funtest(vk);

if cost< threshold

	varargout{1} =1;
	varargout{2} = cost;
	U=find(viable(:,1),1,'last');

	if isempty(U);

		viable(1,1:dim)=vk; 
		viable(1,dim+1)=cost;

	else 

		viable(U+1,1:dim)=vk;
		viable(U+1,dim+1)=cost;

	end 

else 

	varargout{1} =0;
	varargout{2} = cost;

end 
