%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%volsph.m
%
%Calculate the volume of a multidimensional sphere with unit radius
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Vol = volsph(dim)
%
%Input: 
%       dim = dimension of the parameter space
%       
%Output: 
%       Vol = volume of a multidimensional sphere with unit radius
%             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function auxs=volsph(dim)


if (dim==0)
    
    auxs=1;
    
elseif (dim==1)
    
    auxs=2;
    
else
    
    auxs=((2*pi)/(dim))*volsph(dim-2);
    
end
    

