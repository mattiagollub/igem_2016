%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%choose.m
%
%From the whole set of parameter points V, it finds a point that is 
%typically far from the mean of the set of points K   
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       index=choose(V,K)
%
%
%Input: 
%       V = set of parameter points
%       K = set of parameter points
%       
%
%Output: 
%       index= index of the choosen point in the set V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function auxs=choose(Vtotal,K)

%%% Sample from the distribution P(y)=2*y %%%%%%%

x=rand;
y=sqrt(x);

%%%%%%%%%%% Mean value %%%%%%%%%%%%%%%%%%%%%%%%%%

if (size(K,1)>1)
    meank=mean(K);
else
    meank=K;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

maximo=-Inf;
minimo=+Inf;

for i=1:size(Vtotal,1)
    
    re=norm(Vtotal(i,:)-meank);
    
    if re>maximo
        
        maximo=re;
               
    end
        
    if re<minimo
        
        minimo=re;
        
    end
    
end

disran=(maximo-minimo)*y+minimo;
dmin=Inf;



for i=1:size(Vtotal,1)
    
    re=norm(Vtotal(i,:)-meank);
    dist=abs(re-disran);
    
    if (dist<dmin)
        
        dmin=dist;
        kchos=i;
        
    end
        
   
end


auxs=kchos;


        
        
        
        
    
    
    
    
    
    










