%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%draw.m
%
%Given an initial set of parameter points, it returns a set 
%with randomly choosen parameters from the initial set
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [W]=draw(V,number)
%
%Input:
%       V = set of parameter points.
%       number = number of randomly choosen parameters.
%       
%Output:
%       W = number randomly choosen parameters
%       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function auxs=draw(V,numero)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

W=zeros(numero,size(V,2));
s=size(V,1);
R=s*rand(1,numero);


for i=1:numero
    
    if (floor(R(i))==0)
          
        W(i,:)=V(1,:);
    else
        W(i,:)=V(floor(R(i)),:);
    end
  
    
end



auxs=W;
    











