%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%clusterize.m
%
%Clusterizes points and calculates the minimum volume ellipsoid (MVE)
%that encloses the points of every cluster
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [EjesA centccc Volumenelip IDX]=clusterize(V,dim)
%
%Input: 
%       V = set of parameter points 
%       dim = dimension of the parameter space
%
%Output: 
%       EjesA = Matrix dimxdimx(nº cluster). Every submatrix dimxdim stores
%              the direcctions of the MVE axes that encloses every cluster.
%       centccc = Matrix dimx1x(nº cluster) that stores center of
%                every ellipsoid.
%       Volumenelip = vector that stores the volume of every MVE.
%       IDX = vector that assigns a cluster to every point of V     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [auxs1 auxs2 auxs3 auxs4]=clusterize(V1,dim)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EjesA=zeros(dim,dim,0);
centccc=zeros(dim,1,0);
Volumenelip=zeros(1,0);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Clusterize the points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[nc IDX]=nclus(V1,dim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Calculate the MVE of every cluster
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:nc
    
    VVV=V1(IDX==i,:);
    [A, c]=lowner(VVV',0.005);
    EjesA(:,:,i)=A;
    centccc(:,:,i)=c;
end

% Calculate the volume of each ellipsoid

S=size(EjesA);

if (length(S)==2)

    Volumenelip=volellipnew(EjesA,centccc,dim);

else

    for i=1:S(3)

        Volumenelip(i)=volellipnew(EjesA(:,:,i),centccc(:,:,i),dim);

    end

end




auxs1=EjesA;
auxs2=centccc;
auxs3=Volumenelip;
auxs4=IDX;


