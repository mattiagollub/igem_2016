%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%MEBS.m
%
%Obtains viable parameter points through the Multiple Ellipsoids based
%sampling
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   [V flag]=MEBS(funcion,Vtotal,dim,niter,n,ni,bmax,bmin,tol,gini,grate)
%
%Input: 
%       funcion = function recieves a parameter point and check its viability
%       Vtotal = set of viable points
%       niter = maximum number of parameter evaluations
%       n = parameter points sampled inside the ellipsoids
%       ni = points sampled in order to check the convergence
%       dim = dimension of the parameter space
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%       tol = tolerance to accept the convergence
%       gini = initial scaling parameter for the first ellipsoid expanision
%       grate = scaling parameter for the following ellipsoid expanisions
%       
%Output: 
%       V = matrix with all the viable parameter points found by the algorithm
%       flag = structure with two fields. 
%              flag.vol = volume covered by the enclosing ellipsoids
%                        in every iteration.
%              flag.conv = 1(0) if the algorithm converged 
%                          (it did not converge)  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function [auxs1 auxs2]=MEBS(funcion,Vtotal,dim,niter,n,ni,bmax,bmin,tol,gini,grate)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global cuenta
global viable

cuenta=0;
viable=zeros(niter,dim);
i=0;
nrec=2000;
ncon=3;
ncal=30;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Clusterization of the initial points  %%%%%%%%%%%%%%%%%%%
%%%%% and determination of the number of independent %%%%%%%%%%
%%%%% ellipsoids expainsions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if size(Vtotal,1)==1

    [V Vbound]=iterinip(funcion,dim,Vtotal,bmax,bmin);


    Vtotal=zeros(0,dim);
    lvvv=length(Vtotal(:,1));
    lv=length(V(:,1));
    Vtotal(lvvv+1:(lvvv+lv),:)=V;

    lvvv=length(Vtotal(:,1));
    lv=length(Vbound(:,1));
    Vtotal(lvvv+1:(lvvv+lv),:)=Vbound;


    IDX=ones(length(Vtotal),1);
    Volumenelip=1;
    nc=1;
    [EjesA, centccc] = lowner(Vtotal', .01);

else


    [EjesA centccc Volumenelip IDX]=clusterize(Vtotal,dim);

    nc=length(Volumenelip);     %number of group of  expansions

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k0=zeros(0,dim,nc); %stores the initial parameter points of every expansion 
celula=cell(nc,1); 
step=nc;
Volexp=zeros(1,0);
volumenes=zeros(1,0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Seeds of every different expansions %%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for j=1:nc

    celula{j}=Vtotal(IDX==j,:);%divide the seeds of every group of expansion
end


%%%%%%%%%%%%%%%%%%%%%%%%
%%% Iterations %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

while (cuenta<niter)

    i=i+1;

    %%%%%% Inizialization: choosing the initial point%%%%%%%%%%%%
    
    %first iterations
    if (i<(nc+1))

        clus=mod(i,nc+1); %group  
        vstart=celula{mod(i,nc+1)};
        k0(1,:,mod(i,nc+1))=vstart(end,:); %the last element of the seed of every group
        [V Vbound]=iterinip(funcion,dim,k0(1,:,mod(i,nc+1)),bmax,bmin);

        %Rest of iterations

    else

        if (mod(i,nc)==0)

            clus=nc; %group
            vstart=celula{nc};
            km=choose(vstart,k0(:,:,nc));
            km=testkm(funcion,vstart,km,bmax,bmin);
            k0(floor(i/nc)+1,:,clus)=vstart(km,:);
            [V Vbound]=iterinip(funcion,dim,k0(floor(i/nc)+1,:,clus),bmax,bmin);

        else

            clus=mod(i,nc);
            vstart=celula{mod(i,nc)};
            km=choose(vstart,k0(:,:,mod(i,nc)));
            km=testkm(funcion,vstart,km,bmax,bmin);
            k0(floor(i/nc)+1,:,clus)=vstart(km,:);
            [V Vbound]=iterinip(funcion,dim,k0(floor(i/nc)+1,:,clus),bmax,bmin);

        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%% Ellipsoid expansions around the initial points %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    cuenta0=cuenta;
    V=expanellip(funcion,V,dim,n,bmax,bmin,gini,grate);
    

    for marca=1:length(Volumenelip)

        V=esn2(V,EjesA(:,:,marca),centccc(:,:,marca),dim);
        Vbound=esn2(Vbound,EjesA(:,:,marca),centccc(:,:,marca),dim);
    end

    disp('Frequency of new points');
    disp((length(V(:,1))+length(Vbound(:,1)))/(cuenta-cuenta0));
    

    lc=length(celula{clus});
    lv=length(V(:,1));
    vstart=celula{clus};
    
    vstart(lc+1:(lc+lv),:)=V;
    celula{clus}=vstart;
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% We keep the points that are not inside the ellipsoids %%%%
    %%%%%%%%%%%% defined by the points found by MMM %%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if (i==1)

        Vtotaldef=Vtotal;
        [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);
        lvtd=size(Vtotaldef,1);


        lvvv=length(Vtotal(:,1));
        lv=length(V(:,1));
        Vtotal(lvvv+1:(lvvv+lv),:)=V;

        lvvv=length(Vtotal(:,1));
        lv=length(Vbound(:,1));
        Vtotal(lvvv+1:(lvvv+lv),:)=Vbound;

    
    else

        if (size(Vtotal,1)-lvtd)<nrec

            lvvv=length(Vtotal(:,1));
            lv=length(V(:,1));
            Vtotal(lvvv+1:(lvvv+lv),:)=V;

            lvvv=length(Vtotal(:,1));
            lv=length(Vbound(:,1));
            Vtotal(lvvv+1:(lvvv+lv),:)=Vbound;

    
        else


            lvvv=length(Vtotal(:,1));
            lv=length(V(:,1));
            Vtotal(lvvv+1:(lvvv+lv),:)=V;

            lvvv=length(Vtotal(:,1));
            lv=length(Vbound(:,1));
            Vtotal(lvvv+1:(lvvv+lv),:)=Vbound;


            V=Vtotal(lvtd:end,:);

            for marca=1:length(Volumenelip)

                V=esn2(V,EjesA(:,:,marca),centccc(:,:,marca),dim);
                
            end


            lv=length(V(:,1));
            Vtotaldef(lvtd+1:(lvtd+lv),:)=V;
            Vtotal=Vtotaldef;
            lvtd=size(Vtotaldef,1);


            [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);

        end

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check convergence to stop the simulation%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    if (rem(i,step)==0)

        %%%%%%%%%%%%%%%%%%%%%%%%%

        V=Vtotal(lvtd:end,:);

        for marca=1:length(Volumenelip)

            V=esn2(V,EjesA(:,:,marca),centccc(:,:,marca),dim);
            
        end


        lv=length(V(:,1));
        Vtotaldef(lvtd+1:(lvtd+lv),:)=V;
        Vtotal=Vtotaldef;
        lvtd=size(Vtotaldef,1);


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        volvec=zeros(1,0);
        for cu=1:ncal
            
            [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);
            [res]=intelintg2(@unidad,EjesA,centccc,Volumenelip,dim,ni,bmax,bmin);
            volvec(cu)=sum(res);
        end

        m=i/step;
        
        Volexp(m)=mean(volvec);
        
        disp('Number of sampled parameters');
        disp(cuenta);
        disp('Covered volume');
        disp(Volexp);
                
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        if (m>ncon)

            for k=1:ncon

                volumenes(1,k)=abs(((Volexp(m-k))/(Volexp(m)))-1);

            end

            if max(volumenes)<tol;

                break
            end

        end

    end


end


U=find(viable(:,1),1,'last');
flag.vol=Volexp;

if (j<niter)
    
    flag.conv=1;
    
else
    
    flag.conv=0;
    
end
    

auxs1=viable(1:U,:);
auxs2=flag;






