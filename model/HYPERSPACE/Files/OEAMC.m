%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OEAMC.m
%
%It carries out the out-of-equilibrium adaptive Monte Carlo 
%exploration of the parameter space.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%      [V flag]=OEAMC(funcion2,n,x0,bmax,bmin,tol,OPTIONS)
%
%
%Input:
%       funcion2 = function that recieves a parameter point and
%               return two variables. The first variable is 1(0)
%               if the parameter point is viable (nonviable).
%               The second is the value of the cost function for this
%               parameter point.
%       n = maximum number of parameter point evaluations.
%       x0 = initial parameter point of the exploration.
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%       tol = tolerance to accept the convergence of the algorithm.
%        OPTIONS = structure with internal parameters of the algorithm.
%                OPTIONS.tol = tolerance to accep the convergence
%                OPTIONS.nint = points sampled in order to check the convergence 
%                OPTIONS.gr = number of parameter evaluations among updates
%                OPTIONS.Be = initial value of \Beta;
%                OPTIONS.viamax = maximum frequency of viable points that maintain constant \Beta
%                OPTIONS.viamin = minimum frequency of viable points that maintain constant \Beta
%                OPTIONS.tramax = maximum frequency of accepted transitions to maintain constant the covariance matrix
%                OPTIONS.tramin = minimum frequency of accepted transitions to maintain constant the covariance matrix
%                OPTIONS.tbemax = maximum relative change of \Beta among updates
%                OPTIONS.tsmax = maximum relative change of the covariance
%                matrix among updates
%
%Output: 
%       V = matrix of all the viable paremeter points found.
%       flag = structure with two fields. 
%              flag.vol = volume covered by the enclosing ellipsoids
%                        in every iteration.
%              flag.conv = 1(0) if the algorithm converged 
%                          (it did not converge)  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function [auxs1 auxs2]=OEAMC(funcion2,n,x0,bmax,bmin,OPTIONS)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global cuenta;
global viable;


cuenta=0; %number of funcion2 evaluations
sumador=0; %number of update without finding a single viable point.
dim=length(x0); %dimenison of the parameter space
step=floor(n/10); %parameter evaluations among two convergence tests
ni=OPTIONS.nint;%points used to calculate the volume of the ellipsoids which cover the parameter points
gr=OPTIONS.gr;%number of iterations among updates
tol=OPTIONS.tol;
ca=0; %Stores the number of transitions among updates
cont=0; %Stores the number of viable parameters among updates
comienzos=0; %Stores the number of restarts.
ntol=3;% Number of iterations compared to check convergence
j=1;
first=0; % It is 0 if the viable points have not been grouped yet. It is 1 in the other case.
Volexp=zeros(1,0);%Stores the volume of the ellipsoids in the different iterations

tbemax=OPTIONS.tbemax;%maximum relative change of \Beta among updates
tsmax=OPTIONS.tsmax;%maximum relative change of the covariance matrix among updates

viamax=OPTIONS.viamax;% maximum frequency of viable points that maintain constant \Beta
viamin=OPTIONS.viamin;% minimum frequency of viable points that maintain constant \Beta

tramax=OPTIONS.tramax;% maximum frequency of accepted transitions to maintain constant the covariance matrix
tramin=OPTIONS.tramin;% minimum frequency of accepted transitions to maintain constant the covariance matrix



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%% \Beta and \Sigma initialization    %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Be=OPTIONS.Be; %initial value of \Beta
sigm=OPTIONS.sigm;%initial value of the covariance matrix



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Markov Chain Metropolis%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


while (j<n)


    if (j==1)

        [DIA A]=feval(funcion2,x0);

    end



    %%%%%%%% sampling and testing of the new parameter %%%%%%%%%%%%%%%%%%%%

    %Choose the new parameter
    y0=mvnrnd(x0,sigm);
    contb=0;


    % If it does not belong to the parameter space it samples a new one
    while (any(y0>bmax) || any(y0<bmin))

        y0=mvnrnd(x0,sigm);
        contb=contb+1;
        if (rem(contb,1000)==0)
            disp('The elements of the covatiance matrix are reduced due to countour problems');
            sigm=sigm./10;
        end

    end

    %Evaluates the new parameter
    [DIB B]=feval(funcion2,y0);

    cont=cont+DIB;
    j=j+1;

    %%%% Metropolis acceptance ratio %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Accept the new point if the cost function is smaller
    if (B<A)

        x0=y0;
        A=B;

        ca=ca+1;



        % If it is larger, compare it with the Boltzman distribution
    elseif (exp(-Be*(B-A))>rand)

        x0=y0;
        A=B;

        ca=ca+1;


        % If it is smaller we keep the old point

    end


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%% Updating the temperature and the covariance matrix %%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    % Condition to update: Clustering and minimum volume ellipsoids
    if (rem(j,gr)==0)

        U=find(viable(:,1),1,'last'); %Total number of viable points

        if (U>2*(dim+1)) && (first==0)


            Vtotaldef=viable(1:U,1:dim);% Total viable parameter points
            U1=U;

            %Clustering of the viable points and minimum volume ellipsoids
            %for every cluster
            [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);
            first=1; % Already grouped the viable points

            lvtd=size(Vtotaldef,1);


        elseif (U<=2*(dim+1)) && (first==0)

            Vtotaldef=viable(1:U,1:dim);  %%% Total number of viable parameter points
            U1=U;

        else


            V=viable(U1:U,1:dim); % Found viable points among updates.
            U1=U;

            % Obtaining viable points out of the MVE
            for marca=1:length(Volumenelip)

                V=esn2(V,EjesA(:,:,marca),centccc(:,:,marca),dim);

            end

            lv=length(V(:,1));
            Vtotaldef(lvtd+1:(lvtd+lv),:)=V;
            lvtd=size(Vtotaldef,1);

            [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);


            %%%%%%%%%%%%%%%%%%%%%%%%%%
            disp('Total number of viable parameter points found');
            disp(U);
            disp('Number of parameter points necessary to define the enclosing ellipsoids');
            disp(length(Vtotaldef(:,1)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%



        end



        %%% Updating the temperature %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        disp('Total number of parameters points checked');
        disp(j);
        disp('Frequency of accepted transitions');
        disp(ca/gr);
        disp('Frequency of viable parameters points');
        disp(cont/gr);
        disp('Old value of \Beta');
        disp(Be);
        disp('Old value of \Sigma');
        disp(sigm(1,1));
        disp('Number of restarts');
        disp(comienzos);

        % if the frequency of viable points is higher than the threshold
        if (cont>(gr*viamax))

            tasa=tbemax*(cont-gr*viamax)/cont; % proportional increase of the temperature
            Be=Be/(1+tasa);
            sumador=0; %number of updates without viable points

        elseif (cont<=(gr*viamin))


            % If it did not find any viable point between updates
            if cont==0

                sumador=sumador+1;

                % only updates the temperatures if carries out a minimum
                % number of transitions
                if ca>gr*tramin

                    tasa=tbemax;
                    Be=Be*(1+tasa);

                end


                % If it is lost choose a viable point to restart the search
                if sumador>10


                    U=find(viable(:,1),1,'last');
                    ch=floor(rand*U);
                    while (ch<1)||(ch>U)
                        ch=floor(rand*U);
                    end

                    disp('Maximum number of iterations without a single viable parameter point');
                    disp('It starts from a different viable parameter points');


                    Be=Be*tbemax;
                    x0=viable(ch,1:dim);
                    comienzos=comienzos+1;


                end


            else

                tasa=tbemax*(gr*viamin-cont)/(gr*viamin);
                Be=Be*(1+tasa);
                sumador=0;

            end


        end



        %%%% Updating the covariance matrix %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


        if (ca>gr*tramax)&&(cont>0)

            tasa=tsmax*(ca-gr*tramax)/ca;
            sigm=sigm*(1+tasa);


        elseif (ca<gr*tramin)

            tasa=tsmax*(gr*tramin-ca)/(gr*tramin);
            sigm=sigm/(1+tasa);


        end

        ca=0; 
        cont=0;



        disp('New value of \Beta');
        disp(Be);
        disp('New value of \Sigma');
        disp(sigm(1,1));
        disp('Enclosed Volume');
        disp(Volexp);



    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Check convergence to stop the simulation%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    if (rem(j,step)==0)

        volumenes=zeros(1,0);
        m=j/step;

        disp('Fraction of the simulation');
        disp(j/n);


        U=find(viable(:,1),1,'last');


        if (U>2*(dim+1)) && (first==0)

            first=1;
            Vtotaldef=viable(1:U,1:dim); % Total number of viable parameter points
            U1=U;

            [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim);
            [res]=intelintg2(@unidad,EjesA,centccc,Volumenelip,dim,ni,bmax,bmin);  % Volume enclosed by the ellipsoids
            Volexp(m)=sum(res);

            lvtd=size(Vtotaldef,1);

            
        elseif (U<=2*(dim+1)) && (first==0)     
            
            Volexp(m)=0;
            
            
        else

            V=viable(U1:U,1:dim);
            U1=U;

            for marca=1:length(Volumenelip)

                V=esn2(V,EjesA(:,:,marca),centccc(:,:,marca),dim);
                
            end

            lv=length(V(:,1));
            Vtotaldef(lvtd+1:(lvtd+lv),:)=V;

            %%% Calculation of the mean volume enclosed by the ellipsoids 
            volvec=zeros(1,0);
            for cu=1:30

                [EjesA centccc Volumenelip]=clusterize(Vtotaldef,dim); %clusterization and ellipsoid calculation
                [res]=intelintg2(@unidad,EjesA,centccc,Volumenelip,dim,ni,bmax,bmin);  %%% Volume enclosed by the ellipsoids
                volvec(cu)=sum(res);

            end

            
            Volexp(m)=mean(volvec);
            Vtotaldef=cleaning(Vtotaldef,dim);
            lvtd=size(Vtotaldef,1);
                        


        end



        %%% Check convergence

        if (m>ntol)

            for k=1:ntol

                volumenes(1,k)=abs(((Volexp(m-k))/(Volexp(m)))-1); % Relative difference in the enclosed volumes

            end

            % Condition to stop the exprloration  
            if max(volumenes)<tol;

                break 
            end


        end




    end

end

U=find(viable(:,1),1,'last');

flag.vol=Volexp;

if (j<n)
    
    flag.conv=1;
    
else
    
    flag.conv=0;
    
end
    
    
    
    

auxs1=viable(1:U,:);
auxs2=flag;
















