%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%intelintg2.m
%
%Performs Monte Carlo integrations inside a set of ellipsoids.
%The integration in the regions shared by two or more ellipsoids is only
%performed once 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Input: 
%       funcion = function to be integrated.
%       EjesA = Matrix dimxdimx(nº ellipsoids). Every submatrix dimxdim
%             stores the direcctions of an ellipsoid axes 
%       centccc = Matrix dimx1x(nº cluster) that stores the centroids of
%               every ellipsoid
%       Volumenelip = vector that stores the volume of every ellipsoid
%       n = number of sampled points
%       dim = dimension of the parameter space
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%       
%
%Output: 
%        res = vector with the results of the integral for every ellipsoid
%        err = vector with the error of the integral for every ellipsoid   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





function [auxs1 auxs2]=intelintg2(funcion,EjesA,centccc,Volumenelip,dim,n,bmax,bmin)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


nc=length(Volumenelip);
res=zeros(1,nc);
err=zeros(1,nc);
npe=zeros(1,length(Volumenelip));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Monte Carlo integration for every ellipsoid %%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
for i=1:nc
    %number of sampled inside of every ellipsoid 
    npe(i)=floor(n*Volumenelip(i)/sum(Volumenelip));
    
    %Uniform sampling in every ellipsoid
    [B Vol]=homosamp(EjesA(:,:,i),centccc(:,:,i),npe(i),dim);

    if (i==1)
        
        [res(i) err(i)]=integrator(funcion,B,Vol,bmax,bmin,0,0,dim);
        
    else
        
        [res(i) err(i)]=integrator(funcion,B,Vol,bmax,bmin,EjesA(:,:,1:i-1),centccc(:,:,1:i-1),dim);
    end


end
    
    
auxs1=res;
auxs2=err;
            
    
    
    
    
    
    
    
    
    
        
        


