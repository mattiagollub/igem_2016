%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%esn2.m
%
%Given a set of points and an ellipsoid. Choose the points of the set that
%are outside of the ellipsoid.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Vout=esn2(V,A,c,dim)
%
%Input: 
%       V = matrix with the set of points
%       A = This matrix dimxdim contains all the 
%             information regarding the ellipsoid    
%       c = center of the ellipsoid.  
%       dim = dimension of the parameter space
%                                                       
%Output: 
%       Vout = matrix with all the points of V that 
%           are outside of the ellipsoid  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function auxs=esn2(V,A,c,dim)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D=zeros(1,dim);
Pr=zeros(1,size(V,1));
Ce=zeros(size(V,1),dim);

for i=1:size(V,1)
    
    Ce(i,:)=c';
    
end


%%% Set of points V written in the system of reference of the ellipsoid 
[U,Q,W]=svd(A);
DD2=(V-Ce)*W;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Check if the points stored in V are inside the ellipsoid [A,c] %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%length of the axes
for i=1:dim
    D(i)=1/sqrt(Q(i,i));
end

for i=1:length(V(:,1))
  
   M=(DD2(i,:)./D).^2;
   Pr(i)=sum(M); 
   
  
end

% All the points that are out of the ellipsoid
V1=V(Pr>1,:);


auxs=V1;


