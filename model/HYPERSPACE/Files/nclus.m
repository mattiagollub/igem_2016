%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%nclus.m
%
%Clusterizes a set of points and finds the suitable number of clusters
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [nc IDX C]=nclus(V,dim)
%
%Input:
%       V = set of parameter points.
%       dim = dimension of the parameter space.
%
%Output:
%        nc = number of clusters.
%        IDX = vector that assigns a cluster to every point of V.
%        C = centroids of every cluster.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [auxs1 auxs2 auxs3]=nclus(V,dim)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RE=zeros(1,dim);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Perform k-means clustering, from k=dim till k=1 %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=dim:-1:1


    % Divide the set V in i clusters
    [IDX C]= kmeans(V,i,'emptyaction','singleton');

    RE(i)=1;


    % If in a cluster there are not enough points, the clusterization
    % is not valid
    for w=1:i

        if (length(V(IDX==w))<2*(dim+1))

            RE(i)=0;
            break

        end

    end

    % If all the clusters contain enough points the algorithm stops
    if (RE(i)==1)
        nc=i;


        auxs1=nc;

        auxs2=IDX;

        auxs3=C;


        break

    end

end

