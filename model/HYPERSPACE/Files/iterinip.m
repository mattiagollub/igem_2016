%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%iterinip.m
%
%Given a viable point, it finds points near the intersection of the
%cartesian axes and the border of the viable volume
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [V Vbound]=iterinip(funcion,dim,k0,bmax,bmin)
%
%Input: 
%       funcion = function recieves a point and check if it is 
%               viable or not;
%       dim = dimension of the parameter space
%       k0 = viable point
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%
%Output: 
%       V = matrix with 2*dim viable parameter points viable points inside
%       the viable space and placed in the direction of the cartesian axes
%       centered in k0
%       Vbound = matrix with 2*dim viable parameter points near the boundary of
%       the viable space and placed in the direction of the cartesian axes
%       centered in k0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [auxs1 auxs2]=iterinip(funcion,dim,k0,bmax,bmin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V=zeros(0,dim);
Vb=zeros(0,dim);
Bordes=zeros(0,dim);
tol=0.2;
contador=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Finding viable points in the cartesian axes %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for i=1:(2*dim) %loop over all dimensions (positive and negative values) 

    P=k0;
    j=0;
    
    if (rem(i,2)==1)
        
        puntoa=k0(i-floor(i/2));
        puntob=bmax(i-floor(i/2));
        
    else

        puntoa=k0(i-floor(i/2));
        puntob=bmin(i-floor(i/2));
        
        
    end
    
     
    err=abs(puntoa-puntob)/min(abs(puntoa),abs(puntob));
    P(i-floor(i/2))=puntob;
    A=feval(funcion,P);
    
    if (A==1)
    
        V(i,:)=P;
        contador=contador+1;
        Bordes(contador,:)=P;
    
    else
        
        while ((err>tol)||(puntoa==k0(i-floor(i/2))));

            j=j+1;
            
            x = (puntob+puntoa)/2;
            P(i-floor(i/2)) = x;
            A = feval(funcion,P);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              
            if (j>50)&&(puntoa ~= k0(i-floor(i/2)))
                
                break
            end
            
                        
            if (j>100)
                j
                auxs1=0;
                auxs2=0;
                k0
                
                return
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            

            if (A==1)
                
                puntoa=x;
                contador=contador+1;
                Bordes(contador,:)=P;
    
            else
                puntob=x;
                
            end

            err=abs(puntoa-puntob)/min(abs(puntoa),abs(puntob));
            
        end

        P(i-floor(i/2))=puntoa;
        V(i,:)=P;
        
    end

end

%%% Choose points in the interior of the viable regions

for i=1:2*dim
    
    Vb(i,:)=((V(i,:)-k0)/3)+k0;
    A=feval(funcion,Vb(i,:));
    
    if ~(A)
        
        Vb(i,:)=V(i,:);
        
    end

end


auxs1=Vb;
auxs2=Bordes;
