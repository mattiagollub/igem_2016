%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%integrator_vol.m
%
%Performs a Monte Carlo integration inside an ellipsoid.
%The sampled points that lie inside a set of other ellipsoids are 
%not counted 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       [res err]=integrador_vol(funcion,B,Vol,bmax,bmin,EjesA,centccc,dim)
%
%Input: 
%       funcion = function to integrate.
%       B = set of points to be sampled inside the ellipsoid.
%       Vol = volume of the ellipsoid.
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%       EjesA = This matrix (dimxdimx(nº ellipsoids) contains all the 
%             information regarding the set of other ellipsoid where we
%             will test if the points of B lie there.   
%       centccc = center of the set of other ellipsoids.  
%       dim = dimension of the parameter space
%       
%Output: 
%        res = result of the integration inside our ellipsoid.
%        err = error of this integration.   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [auxs1 auxs2]=integrator_vol(funcion,B,Vol,bmax,bmin,EjesA,centccc,dim)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global baysamp
global cuentbay


sumt=0;
sumt2=0;
S=size(EjesA);
cont=0;
n=size(B,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Check the number of elipsoids %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(S)==2
    
    nelip=1;

else

    nelip=S(3);
    
    
    
end


if (EjesA~=0)

    for j=1:nelip %check if the points are in a previous ellipsoid

        B=esn2(B,EjesA(:,:,j),centccc(:,:,j),dim);

    end

end



if (isempty(B))

    auxs1=0;
    auxs2=0;
    
    return
    

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Perform a Monte carlo integration%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


SB=size(B);

for i=1:SB(1) %loop over all the points

    %check that the parameter points are inside the boundaries
    if ~((any(B(i,:)>bmax))||(any(B(i,:)<bmin)))

        [AA cost]=feval(funcion,B(i,:));
        sumt=sumt+AA;
        sumt2=sumt2+AA^2;
        cont=cont+1;
                
        if AA==1
            cuentbay=cuentbay+1;
            baysamp(cuentbay,1:dim)=B(i,:);
            baysamp(cuentbay,dim+1)=cost;
        end
        
    end

end

%result of the Monte Carlo integration
res=(Vol)*sumt/n;

%error of the Monte Carlo integration
Err=(Vol)*sqrt(((sumt2/n)-(sumt/n)^2)/n);

auxs1=res;
auxs2=Err;
