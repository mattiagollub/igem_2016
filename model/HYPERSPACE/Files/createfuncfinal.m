function createfuncfinal(functionName)

% OPEN FILE FOR WRITING
fid = fopen(['functionfinal.m'],'w');

% WRITE FUNCTION DEFINITION
fprintf(fid,'function [varargout] = %s(vk)\n','functionfinal');
fprintf(fid,'\n');

% WRITE THE HEADER
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'%% %s\n','functionfinal');
fprintf(fid,'%% Generated: %s\n',datestr(now));
fprintf(fid,'%% \n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'\n');


% GLOBAL VARIABLES
fprintf(fid,'%% Global variables\n');
fprintf(fid,'global cuenta;\n');
fprintf(fid,'global viable;\n');
fprintf(fid,'global threshold;\n');
fprintf(fid,'\n');

% LOCAL VARIABLES
fprintf(fid,'dim=length(vk);\n');
fprintf(fid,'\n');

% PARAMETER EVALUATION

fprintf(fid,'cuenta=cuenta+1;\n');
fprintf(fid,'cost=%s(vk);\n',functionName);
fprintf(fid,'\n');

% VIABILITY TESTING

fprintf(fid,'if cost< threshold\n');

fprintf(fid,'\n');
fprintf(fid,'\tvarargout{1} =1;\n');
fprintf(fid,'\tvarargout{2} = cost;\n');  
fprintf(fid,'\tU=find(viable(:,1),1,''last'');\n');      
fprintf(fid,'\n');

fprintf(fid,'\tif isempty(U);\n');

fprintf(fid,'\n');
fprintf(fid,'\t\tviable(1,1:dim)=vk; \n');  
fprintf(fid,'\t\tviable(1,dim+1)=cost;\n');  
fprintf(fid,'\n');

fprintf(fid,'\telse \n');

fprintf(fid,'\n');    
fprintf(fid,'\t\tviable(U+1,1:dim)=vk;\n');
fprintf(fid,'\t\tviable(U+1,dim+1)=cost;\n');
fprintf(fid,'\n');        

fprintf(fid,'\tend \n');

fprintf(fid,'\n');
fprintf(fid,'else \n');  


fprintf(fid,'\n');
fprintf(fid,'\tvarargout{1} =0;\n');
fprintf(fid,'\tvarargout{2} = cost;\n');  
fprintf(fid,'\n');

fprintf(fid,'end \n');



