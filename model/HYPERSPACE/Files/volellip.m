%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%volellip.m
%
%Calculates the volume of a single multidimensional ellipsoid
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Vol=volellip(dim,a)
%
%Input: 
%       dim = dimension of the parameter space
%       a = vector with the length of every ellipsoid axe
%
%Output: 
%       Vol = volume of a multidimensional ellipsoid
%             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function auxs=volellip(dim,a)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

P1=1;

%%%%%%Product of the axes-length

for i=1:length(a)
       
    P1=P1*a(i);
    
end

auxs=P1*volsph(dim);
