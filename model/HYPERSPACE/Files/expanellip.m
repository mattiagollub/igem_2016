%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%expanellip.m
%
%Carries out a complete iteration of ellipsoids expansions till the
%ellipsoid converges.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%Input: [auxs1 auxs2 ]=expanellip(funcion,V,dim,n,bmax,bmin,gini,grate)
%       
%       funcion = function recieves a point and check if it is 
%               viable or not;
%       V = set of viable parameter points
%       dim = dimension of the parameter space
%       n = points sampled inside the boxes 
%       bmax = vector with the upper bound of our parameter space.
%       bmin = vector with the lower bound of our parameter space.
%       gini = initial scaling parameter for the first ellipsoid expanision
%       grate = scaling parameter for the following ellipsoid expanisions
%
%Output: 
%        V = set of viable points that include the new ones found and the
%          original set
%        cont=number of points checked
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [auxs1 auxs2]=expanellip(funcion,V,dim,n,bmax,bmin,gini,grate)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cont=0;
p=0;
Vol=zeros(1,0);
g=zeros(1,0);

lv=length(V(:,1));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Iterations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


while(1)    
    
    clear W;
    p=p+1;
    j=0;
    W=zeros(0,dim);
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%% Choosing sample points %%%%%%%%%%%%%%%%%%%%%%%  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if (p<3)

        g0=gini;    
        [B Ejes centro]=sexpini(V,dim,n,gini);
        cont=0;
    else

        [B Ejes centro g(p-2) Vol(p-2)]=sexp(V,dim,n,g0,lw1*n/cont,grate);
        g0=g(p-2);
        cont=0;

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%% Checking if they are viable %%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for i=1:n

        if ~((any(B(i,:)>bmax))||(any(B(i,:)<bmin)))

            cont=cont+1;
            A=feval(funcion,B(i,:));

            if (A==1)    

                j=j+1;
                W(j,:)=B(i,:);

            end

        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Choosing viable points out of the regions %%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%% previously explored %%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    if (p<2)

        lw=length(W(:,1));
        V(lv+1:(lv+lw),:)=W;
        lv=lv+lw;

    else

        W1=esn2(W, Ejes,centro,dim);
        lw1=length(W1(:,1));
        V(lv+1:(lv+lw1),:)=W1;
        lv=lv+lw1;

    end

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Checking convergence to finish the iterations %%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    if (g0<1.2)
        
        break
    
    elseif (p>50)
        
        break
        
    elseif (p>5)&&(conver(g,0.05))&&(conver(Vol,0.05))
        
        break;
        
    end
    

end


auxs1=V;
auxs2=cont;




