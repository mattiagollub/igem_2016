function Out = ELexp(varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ELexp.m
%
%Obtains viable parameter points through the Multiple Ellipsoids Based
%Method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Out = ELexp(function,threshod,V,bmax,bmin,n)
%       = ELexp(function,threshod,V,bmax,bmin,n,OPTIONS)
%
%Input: 
%       function : function that recieves a parameter point and checks its cost.
%       threshold : scalar that defines the maximum value of the viable cost       
%       V : matrix with viable parameter points. Every vaible parameter
%           point corresponds to a row of the matrix
%       bmax : row  vector with the upper bound of our parameter space
%       bmin : row vector with the lower bound of our parameter space
%       n : scalar that defines the maximum number of parameter
%       evaluations
%
%       OPTIONS : structure with internal parameters of the algorithm.
%                OPTIONS.tol : tolerance to accept the convergence
%                OPTIONS.nins : number of parameter points sampled 
%                               inside the ellipsoids 
%                OPTIONS.nint : points sampled in order to check the convergence 
%                OPTIONS.gini : initial scaling parameter for the
%                               first ellipsoid expanision 
%                OPTIONS.grate : scaling parameter for the following 
%                                ellipsoid expanisions 
% 
%       DEFAULT VALUES:
%       ===============
%                OPTIONS.tol = 0.05
%                OPTIONS.nins = 100 
%                OPTIONS.nint = 1e5 
%                OPTIONS.gini = 2 
%                OPTIONS.grate = 3 
%    
%       
%Output: 
%       Out : structure with three fields.
%             Out.V : matrix with all the viable parameter points found by
%                     the algorithm. Every vaible parameter point corresponds
%                     to a row of the matrix
%             Out.cost : column vector with the cost of the viable parameter
%                        points present in Out.V
%             Out.flag : structure with two fields
%                Out.flag.vol : row vector with the volume covered by the
%                               enclosing ellipsoids in every iteration
%                Out.flag.conv : scalar equal to 1(0) if the algorithm 
%                                converged (it did not converge)  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


global cuenta;
global viable;
global threshold;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 6

    fun = varargin{1};
    threshold = varargin{2};
    V = varargin{3};
    bmax = varargin{4};
    bmin = varargin{5};
    n = varargin{6};

    tol=0.05;
    nins=100;
    nint=1e5;
    gini=2;
    grate=3;

elseif nargin == 7

    fun = varargin{1};
    threshold = varargin{2};
    V = varargin{3};
    bmax = varargin{4};
    bmin = varargin{5};
    n = varargin{6};

    tol=varargin{7}.tol;
    nins=varargin{7}.nins;
    nint=varargin{7}.nint;
    gini=varargin{7}.gini;
    grate=varargin{7}.grate;
    
else

    error('Incorrect number of arguments');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if size(V,2)==size(bmax,2) && size(V,2)==size(bmin,2)


else

    error('The elements of V, bmax, and bmin muss have the same dimensions');

end


    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dim=length(V(1,:));
viable=zeros(n,dim+1);
cuenta=0;
createfuncfinal(fun);
funcion1= @(y)functionfinal(y);


if (size(V,1)>(2*dim+1))
    V=cleaning(V(:,1:dim),dim);
end

[Velip flagelip]=MEBS(funcion1,V,dim,n,nins,nint,bmax,bmin,tol,gini,grate);
U = find(viable(:,1),1,'last');

Out.V=viable(1:U,1:dim);
Out.cost=viable(1:U,dim+1);
Out.flag=flagelip;


clear cuenta viable threshold


return