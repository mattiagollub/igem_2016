%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%volellipnew.m
%
%Sum the volumes of a set of ellipsoids
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Vol=volellipnew(EjesA,centccc,dim)
%
%Input:
%       EjesA = Matrix dimxdimx(nº ellipsoids). Every submatrix
%             dimxdim stores the exes direcctions of a single ellipsoid
%       centccc = Matrix dimx1x(nº cluster) that stores the centroids of
%                every ellipsoid
%       dim = dimension of the parameter space
%
%Output:
%       Vol = sum of the volumes of all the ellipsoids
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function auxs=volellipnew(EjesA,centccc,dim)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Declarations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

D=zeros(dim);
Vol=0;
S=size(EjesA);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Volume calculation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%If there is only one ellipsoid
if (length(S)<3)

    [U,Q]=svd(EjesA);


    for j=1:dim
        %length of the j-axes
        D(j)=1/sqrt(Q(j,j));

    end

    Vol=Vol+volellip(dim,D);

    auxs=Vol;

    %If there is more than one ellipsoid
else

    for i=1:S(3)

        [U,Q]=svd(EjesA(:,:,i));

        for j=1:dim

            D(j)=1/sqrt(Q(j,j));

        end
        %Sum the volumes of all the ellipsoids
        Vol=Vol+volellip(dim,D);

    end


    auxs=Vol;


end
