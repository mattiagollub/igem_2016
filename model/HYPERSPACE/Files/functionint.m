function [varargout] = functionint(vk)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% functionfinal
% Generated: 09-Sep-2011 14:40:27
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Global variables
global threshold;

cost=funtest(vk);

if cost< threshold

	varargout{1} =1;
	varargout{2} = cost;

else 

	varargout{1} =0;
	varargout{2} = cost;

end 
