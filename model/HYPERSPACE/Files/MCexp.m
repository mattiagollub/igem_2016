
function Out = MCexp(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%MCexp.m
%
%Obtains viable parameter points through the Out-of-equilibrium Adaptive
%Monte Carlo method
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   Out = MCexp(function,threshod,x0,bmax,bmin,n)
%       = MCexp(function,threshod,x0,bmax,bmin,n,OPTIONS)
%
%Input: 
%       function : function that recieves a parameter point and checks its
%                  cost
%       threshold : scalar that defines the maximum value of the viable cost       
%       x0 : row vector with the coordinates of a viable parameter point
%       bmax : row  vector with the upper bound of our parameter space
%       bmin : row vector with the lower bound of our parameter space
%       n : scalar that defines the maximum number of parameter evaluations
%
%       OPTIONS : structure with internal parameters of the algorithm.
%                OPTIONS.tol : tolerance to accept the convergence
%                OPTIONS.nint : points sampled to check the convergence 
%                OPTIONS.gr : number of parameter evaluations among updates
%                OPTIONS.Be : initial value of \Beta;
%                OPTIONS.viamax : maximum frequency of viable points that
%                                 maintains constant \Beta
%                OPTIONS.viamin : minimum frequency of viable points that
%                                 maintains constant \Beta
%                OPTIONS.tramax : maximum frequency of accepted transitions
%                                 that maintains constant the covariance matrix
%                OPTIONS.tramin : minimum frequency of accepted transitions
%                                 that maintains constant the covariance matrix
%                OPTIONS.tbemax : maximum relative change of \Beta among updates
%                OPTIONS.tsmax : maximum relative change of the covariance matrix
%                                among updates   
%                 
% 
%       DEFAULT VALUES:
%       ===============
%                OPTIONS.tol=0.05;
%                OPTIONS.nint=1e5;
%                OPTIONS.gr=2000;
%                OPTIONS.Be=1;
%                OPTIONS.viamax=0.015;
%                OPTIONS.viamin=0.0075;
%                OPTIONS.tramax=0.3;
%                OPTIONS.tramin=0.2;
%                OPTIONS.tbemax=2;
%                OPTIONS.tsmax=2;
%    
%       
%Output: 
%       Out : structure with three fields.
%             Out.V : matrix with all the viable parameter points found by
%                     the algorithm. Every vaible parameter point corresponds
%                     to a row of the matrix
%             Out.cost : column vector with the cost of the viable parameter
%                        points present in Out.V
%             Out.flag : structure with two fields. 
%                Out.flag.vol : row vector with the volume covered by the
%                               enclosing ellipsoids in every iteration.
%                Out.flag.conv : scalar equal to 1(0) if the algorithm 
%                                converged (it did not converge)  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global cuenta;
global viable;
global threshold;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin == 6

    fun = varargin{1};
    threshold = varargin{2};
    x0 = varargin{3};
    bmax = varargin{4};
    bmin = varargin{5};
    n = varargin{6};
    dim=length(x0);

    OPTIONS.tol=0.1;
    OPTIONS.nint=1e5;
    OPTIONS.gr=2000;
    OPTIONS.Be=1;
    OPTIONS.viamax=0.015;
    OPTIONS.viamin=0.0075;
    OPTIONS.tramax=0.3;
    OPTIONS.tramin=0.2;
    OPTIONS.tbemax=2;
    OPTIONS.tsmax=2;
    
    
    OPTIONS.sigm=zeros(dim,dim);
    for i=1:dim

        OPTIONS.sigm(i,i)=((bmax(i)-bmin(i))/1000).^2; %initial value of the elements of the covariance matrix

    end

    
    
    
elseif nargin == 7

    fun = varargin{1};
    threshold = varargin{2};
    x0 = varargin{3};
    bmax = varargin{4};
    bmin = varargin{5};
    n = varargin{6};
    dim=length(x0);
    
    OPTIONS.tol=varargin{7}.tol;
    OPTIONS.nint=varargin{7}.nint;
    OPTIONS.gr=varargin{7}.gr;
    OPTIONS.Be=varargin{7}.Be;
    OPTIONS.viamax=varargin{7}.viamax;
    OPTIONS.viamin=varargin{7}.viamin;
    OPTIONS.tramax=varargin{7}.tramax;
    OPTIONS.tramin=varargin{7}.tramin;
    OPTIONS.tbemax=varargin{7}.tbemax;
    OPTIONS.tsmax=varargin{7}.tsmax;
    OPTIONS.sigm=varargin{7}.sigm;
    
else

    error('Incorrect number of arguments');

end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(x0,1)==1


else

    error('Incorrect number of rows of x0. x0 muss contain a single viable parameter point');

end


if size(x0,2)==size(bmax,2) && size(x0,2)==size(bmin,2)


else

    error('x0, bmax, and bmin muss have the same dimensions');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

viable=zeros(n,dim+1);
cuenta=0;
createfuncfinal(fun);
funcion1= @(y)functionfinal(y);


[Vmonte flagmonte]=OEAMC(funcion1,n,x0,bmax,bmin,OPTIONS);
U = find(viable(:,1),1,'last');

Out.V=viable(1:U,1:dim);
Out.cost=viable(1:U,dim+1);
Out.flag=flagmonte;


clear cuenta viable threshold


return



