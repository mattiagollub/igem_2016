function createfuncint(functionName)

% OPEN FILE FOR WRITING
fid = fopen(['functionint.m'],'w');

% WRITE FUNCTION DEFINITION
fprintf(fid,'function [varargout] = %s(vk)\n','functionint');
fprintf(fid,'\n');

% WRITE THE HEADER
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'%% %s\n','functionfinal');
fprintf(fid,'%% Generated: %s\n',datestr(now));
fprintf(fid,'%% \n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'\n');


% GLOBAL VARIABLES
fprintf(fid,'%% Global variables\n');
fprintf(fid,'global threshold;\n');
fprintf(fid,'\n');

% LOCAL VARIABLES

% PARAMETER EVALUATION

fprintf(fid,'cost=%s(vk);\n',functionName);
fprintf(fid,'\n');

% VIABILITY TESTING

fprintf(fid,'if cost< threshold\n');

fprintf(fid,'\n');
fprintf(fid,'\tvarargout{1} =1;\n');
fprintf(fid,'\tvarargout{2} = cost;\n');  
fprintf(fid,'\n');

fprintf(fid,'else \n');  

fprintf(fid,'\n');
fprintf(fid,'\tvarargout{1} =0;\n');
fprintf(fid,'\tvarargout{2} = cost;\n');  
fprintf(fid,'\n');

fprintf(fid,'end \n');



