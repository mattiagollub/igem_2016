%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Volint.m
%
%Calculates the viable volume through a Monte Carlo integration and it also
%returns a set of uniformly distributed viable points
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%       Out=Volint(function,threshold,V,bmax,bmin,n)
%
%Input: 
%       function : function that recieves a parameter point and checks its cost.
%       threshold : scalar that defines the maximum value of the viable cost       
%       V : matrix with viable parameter points. Every vaible parameter
%           point corresponds to a row of the matrix
%       bmax : row  vector with the upper bound of our parameter space
%       bmin : row vector with the lower bound of our parameter space
%       n : scalar that defines the number of parameter
%           evaluations carried out in the Monte Carlo integration       
%
%
%Output: 
%       Out : structure with four fields.
%             Out.V : matrix with the set of uniformly distributed viable
%                     parameter points. Every vaible parameter point 
%                     corresponds to a row of the matrix
%             Out.cost : column vector that contains the cost of the
%                        viable parameter points present in Out.V
%             Out.vol : scalar with the  estimation of the viable volume.
%             Out.err : scalar with estimation of the error in the viable volume. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function Out = Volint(funcion,thres,V,bmax,bmin,nint) 

global baysamp;
global cuentbay;
global threshold;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if size(V,1)>= 10*(size(bmax,2)+1) 


else
    
    %fprintf('The minimum number of elements in V is %g',size(bmax,2)+1);
    error('ErrorTests:convertTest',...
        'The number of viable parameter points in V is not enough. The minimum number of elements in V is %g',10*(size(bmax,2)+1));

end


if size(V,2)==size(bmax,2) && size(V,2)==size(bmin,2)


else

    error('The elements of V, bmax, and bmin muss have the same dimensions');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


threshold=thres;
createfuncint(funcion);
funcion2= @(y)functionint(y);
dim=size(V,2);
Vtotal=cleaning(V(:,1:dim),dim);

sumat=Inf;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Choosing the integration domain %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


for i=1:10
   
    [EjesAb centcccb Volumenelipb]=clusterize(Vtotal,dim);
    [res0b err0b]=intelintg2(@unidad,EjesAb,centcccb,Volumenelipb,dim,1e5,bmax,bmin);
    
    if (sum(res0b)<sumat)
        
        sumat=sum(res0b);
        EjesA=EjesAb;
        centccc=centcccb;
        Volumenelip=Volumenelipb;
        res0=res0b;
        err0=err0b;
        
    end

    
end

% number of points sampled
nintf=nint*sum(Volumenelip)/sum(res0);


baysamp=zeros(nint,dim+1);
cuentbay=0;

[res err]=intelintg2_vol(funcion2,EjesA,centccc,Volumenelip,dim,nintf,bmax,bmin);
   

Out.vol=sum(res);
Out.err=sum(err);
Out.V=baysamp(1:cuentbay,1:dim);
Out.cost=baysamp(1:cuentbay,dim+1);

clear threshold baysamp cuentbay