function model = createAndGateLactatePartMAmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);    % nM, Total concentration of plasmids
    pnortot= 24.9081;        %concentration of plasmid
    %% Function
    clamp=@(x)feval(symengine,'max',x,0);
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0,0,0,0,0,pnortot];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'Lac'};
    
    %% Define symbolic variables for species and parameters
    syms Lac Pyr LldR LldD DLldR DLldR_Lac1 DLldR_Lac2 G_off G_off_1 G_on
    syms klac_pyr k_lac_pyr kdlldr k_dlldr kgoff k_goff kdlldr_lac...
         k_dlldr_lac kgoff_lac k_goff_lac dlld kllddProd kl klldrProd ...
         nlact Klact
    
    model.specieNames = [Lac Pyr LldR LldD DLldR DLldR_Lac1 DLldR_Lac2 ...
                         G_off G_off_1 G_on];
    model.paramNames = [klac_pyr k_lac_pyr kdlldr k_dlldr kgoff k_goff ...
                        kdlldr_lac k_dlldr_lac kgoff_lac k_goff_lac dlld ...
                        kllddProd kl klldrProd nlact Klact]; 
    
    %% Stoichiometric and rate matrices
    % Lac_out | Lac_in | Pyr | LldR | LldD | DLldR | DLldR_Lac1 | DLldR_Lac2 |G_off| G_off_1 | G_on |
    
    model.N = [
     % 0,   1,   0,   0,   0,   0,   0,   0,   0,   0,   0;
      -1,   1,   0,   0,   0,   0,   0,   0,   0,   0;  % LldD + Lac ->Pyr + LldD
       0,  -1,   0,   0,   0,   0,   0,   0,   0,   0;  %
       0,   0,  -2,   0,   1,   0,   0,   0,   0,   0;  % 2*LldR -> DLldR
       0,   0,   2,   0,  -1,   0,   0,   0,   0,   0;  %
       0,   0,   0,   0,  -1,   0,   0,   1,   0,  -1;  % DLldR + G_on -> G_off
       0,   0,   0,   0,   1,   0,   0,  -1,   0,   1;  %  
      -1,   0,   0,   0,  -1,   1,   0,   0,   0,   0;  % DLldR + Lac -> DLldR_Lac1
       1,   0,   0,   0,   1,  -1,   0,   0,   0,   0;  %
      -1,   0,   0,   0,   0,  -1,   1,   0,   0,   0;  % DLldR_Lac1 + Lac -> DLldR_Lac2
       1,   0,   0,   0,   0,   1,  -1,   0,   0,   0;  %
      -1,   0,   0,   0,   0,   0,   0,  -1,   1,   0;  % G_off + Lac -> G_off_1
       1,   0,   0,   0,   0,   0,   0,   1,  -1,   0;  %
      -1,   0,   0,   0,   0,   0,   1,   0,  -1,   1;  % G_off_1 + Lac -> G_on + DLldR_Lac2
       1,   0,   0,   0,   0,   0,  -1,   0,   1,  -1;  %
       0,   0,   0,  -1,   0,   0,   0,   0,   0,   0;  % LldD ->
       0,   0,   0,   1,   0,   0,   0,   0,   0,   0;  % -> LldD 
       0,   0,  -1,   0,   0,   0,   0,   0,   0,   0;  % LldR ->
       0,   0,   1,   0,   0,   0,   0,   0,   0,   0;  % -> LldR
       0,   0,   0,   0,  -1,   0,   0,   0,   0,   0;  % DLldR ->
       0,   0,   0,   0,   0,   0,  -1,   0,   0,   0;  % DLldR_Lac2 ->
      
     ]';
    
    %% Reaction rates
    model.rates = [
      %Lac_out;
      (Lac*(LldD))/(klac_pyr + Lac);                      % LldD + Lac ->Pyr + LldD
      k_lac_pyr*Pyr;                          %
      kdlldr*LldR*(LldR);                   % 2*LldR -> DLldR
      k_dlldr*DLldR;                          %
      kgoff*DLldR*G_on;                       % DLldR + G_on -> G_off
      k_goff*G_off;                           %  
      kdlldr_lac*DLldR*clamp(Lac-Pyr);        % DLldR + Lac -> DLldR_Lac1
      k_dlldr_lac*DLldR_Lac1;                 %
      kdlldr_lac*DLldR_Lac1*clamp(Lac-Pyr);   % DLldR_Lac1 + Lac -> DLldR_Lac2
      k_dlldr_lac*DLldR_Lac2;                 %
      kgoff_lac*G_off*clamp(Lac-Pyr);         % G_off + Lac -> G_off_1
      k_goff_lac*G_off_1;                     %
      kgoff_lac*G_off_1*clamp(Lac-Pyr);       % G_off_1 + Lac -> G_on + DLldR_Lac2
      k_goff_lac*G_on*DLldR_Lac2;             %
      dlld*LldD;                              % LldD ->
     (kllddProd*kl)+(kllddProd*(1-kl)*Ptot);  % -> LldD 
      dlld*LldR;                              % LldR ->
     (klldrProd*kl)+(klldrProd*(1-kl)*Ptot);  % -> LldR
      dlld*DLldR;                             % DLldR ->
      dlld*DLldR_Lac2;                        % DLldR_Lac2 -> 
    ];
    % Lac | Pyr | LldR | LldD | DLldR | DLldR_Lac1 | DLldR_Lac2 |G_off| G_off_1 | G_on |

model.specieFunctions = [
        
        Lac;
        Pyr;
       %(Lac*(LldD))/(klac_pyr + LldD); 
        LldR;
        LldD;
        DLldR;
        DLldR_Lac1;
        DLldR_Lac2;
        G_off;
        G_off_1;
        G_on;
        ];
    
    
    %% Reaction propensities
    model.a = [
     % 0.1*Lac_out;
      n2nM(klac_pyr)*LldD*clamp(Lac-Pyr);     % LldD + Lac ->Pyr + LldD
      k_lac_pyr*Pyr;                         %
      n2nM(kdlldr)/2*LldR*(LldR-1);           % 2*LldR -> DLldR
      k_dlldr*DLldR;                          %
      n2nM(kgoff)*DLldR*G_on;                 % DLldR + G_on -> G_off
      k_goff*G_off;                           %  
      n2nM(kdlldr_lac)*DLldR*clamp(Lac-Pyr);  % DLldR + Lac -> DLldR_Lac1
      k_dlldr_lac*DLldR_Lac1;                 %
      n2nM(kdlldr_lac)*DLldR_Lac1*clamp(Lac-Pyr);% DLldR_Lac1 + Lac -> DLldR_Lac2
      k_dlldr_lac*DLldR_Lac2;                 %
      n2nM(kgoff_lac)*G_off*clamp(Lac-Pyr);   % G_off + Lac -> G_off_1
      k_goff_lac*G_off_1;                     %
      n2nM(kgoff_lac)*G_off_1*clamp(Lac-Pyr); % G_off_1 + Lac -> G_on + DLldR_Lac2
      k_goff_lac*G_on*DLldR_Lac2;             %
      dlld*LldD;                              % LldD ->
     (kllddProd*kl)+(kllddProd*(1-kl)*Ptot);  % -> LldD 
      dlld*LldR;                              % LldR ->
     (klldrProd*kl)+(klldrProd*(1-kl)*Ptot);  % -> LldR
      dlld*DLldR;                             % DLldR ->
      dlld*DLldR_Lac2;                        % DLldR_Lac2 -> 
    ];

end