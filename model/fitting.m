function [] = fitting()
%% AND_GATE_DATA FITTING    Example fitting parameters between the deterministic model
%   and the simplified one the and gate. 
%   Simulates the concentration of the and gate species using an example
%   input.

%%
global modelAHL TAHL SAHL AHLparamMap ...
       modelNO  TNO SNO NOparamMap 
   
%% Generate AHL model
 modelAHL = createAndGateAHLpartMAmodel();
 TAHL = modelAHL.timesMap;
 SAHL = modelAHL.speciesMap;
 AHLparamMap=loadParameters('andgate_eth_igem_2014');
 
 dxdt = modelAHL.N * modelAHL.rates;
 matlabFunction(dxdt, 'File', 'HYPERSPACE/c', ...
       'Vars', { modelAHL.specieNames, modelAHL.paramNames });

if isfield(modelAHL, 'specieFunctions')
        sf = modelAHL.specieFunctions;
    else
        sf = modelAHL.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/AHLsf', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
  
%% Generate NO part model
 modelNO = createAndGateNOpartMAmodel();
 TNO = modelNO.timesMap;
 SNO = modelNO.speciesMap;
 NOparamMap=loadParameters('andgate_eth_igem_2014');
 
 dxdt = modelNO.N * modelNO.rates;
 matlabFunction(dxdt, 'File', 'HYPERSPACE/NOode', ...
       'Vars', { modelNO.specieNames, modelNO.paramNames });
   
 if isfield(modelNO, 'specieFunctions')
        sf = modelNO.specieFunctions;
    else
        sf = modelNO.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/NOsf', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 60*6;
tspan = [t0, tf];

NO_value=30;
AHL_value=30;

NO_max=1;
AHL_max=2;


AHL_range=10:5:100;
NO_range=10:5:100;
output_maxPfree=zeros(1,length(AHL_range));
output_maxPnorv3=zeros(1,length(NO_range));

%time=1:100:tf;



for i=1:length(AHL_range)
        % Input: NO and AHL profile
        points = 100;
        
        AHL_stimulus = AHL_range(i) * 1e9 / (Na * V);
       
        t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
        
        t_AHL = (0:points)/points*(tf-t0) + t0;
        
        AHL = zeros(size(t_AHL));
       
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Setup model
        TAHL('AHL') = t_AHL;
        SAHL('AHL') = AHL;

        modelAHL.paramsSet = AHLparamMap;

        nParams = size(modelAHL.paramNames, 2);
        parametersAHL = zeros(1, nParams);

        for k=1:nParams
            paramName = char(modelAHL.paramNames(k));
            parametersAHL(k) = AHLparamMap(paramName);
        end
        
        
        %% Create ODE system from the model
        nInputs = size(modelAHL.inputs, 2);
        nSpecies = length(modelAHL.specieNames);
        % Run simulation for input
        [t, x] = ode15s( ...
            @(t, c) AHLode(AHLsf([interpInputs(modelAHL, t); c(nInputs+1:end)]')',parametersAHL), ...
            tspan, modelAHL.c0, modelAHL.odeOptions);
        % Apply specie functions
        for j=1:size(x, 1)
            x(j,:) = AHLsf([interpInputs(modelAHL, t(j))', x(j,nInputs+1:nSpecies)]);
        end
        % Write output
        for j=nInputs+1:nSpecies
            specieName = char(modelAHL.specieNames(j));
            modelAHL.timesMap(specieName)      = t;
            modelAHL.speciesMap(specieName)    = x(:,j);
        end
        
        %%output
        figure(2);
        hold on
        plot((modelAHL.speciesMap('Pfree')));
        output_maxPfree(i)=max(modelAHL.speciesMap('Pfree'));
       
  
end
%% Plot results

% mRNAinv input profile
figure(1);
clf;
xdata = AHL_range;
ydata = output_maxPfree;
fun = @(x,xdata)(24.95.*x(1).*xdata.^x(2))./(x(3)+xdata.^x(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.5 2 0.1];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x = lsqcurvefit(fun,x0,xdata,ydata,lb,ub,options)
times = linspace(xdata(1),xdata(end));
%plotting
semilogx(xdata,ydata,'ko',times,fun(x,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pfree')


for j=1:length(NO_range)
        % Input: NO and AHL profile
        points = 100;
        NO_stimulus = NO_range(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs
      
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.5/tf*points);
        
        t_NO = (0:points)/points*(tf-t0) + t0;
      
        NO = zeros(size(t_NO));
      
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
       

        %% Setup model
        TNO('NO') = t_NO;
        SNO('NO') = NO;

        modelNO.paramsSet = NOparamMap;

        nParams = size(modelNO.paramNames, 2);
        parametersNO = zeros(1, nParams);

        for k=1:nParams
            paramName = char(modelNO.paramNames(k));
            parametersNO(k) = NOparamMap(paramName);
        end
        
        %% Create ODE system from the model
        nInputs = size(modelNO.inputs, 2);
        nSpecies = length(modelNO.specieNames);
        % Run simulation for input
        [t, x] = ode15s( ...
            @(t, c) NOode(NOsf([interpInputs(modelNO, t); c(nInputs+1:end)]')',parametersNO), ...
            tspan, modelNO.c0, modelNO.odeOptions);
        % Apply specie functions
        for j=1:size(x, 1)
            x(j,:) = NOsf([interpInputs(modelNO, t(j))', x(j,nInputs+1:nSpecies)]);
        end
        % Write output
        for j=nInputs+1:nSpecies
            specieName = char(modelNO.specieNames(j));
            modelNO.timesMap(specieName)      = t;
            modelNO.speciesMap(specieName)    = x(:,j);
        end
        
        %% output
        figure(4);
        hold on
        plot((modelNO.speciesMap('PnorV3')));
        output_maxPnorv3(j)= max(modelNO.speciesMap('PnorV3'));
  
end
figure(3);
clf;
xdata1 = NO_range;
ydata = output_maxPnorv3;
fun = @(x1,xdata1)(24.95.*x1(1).*xdata1.^x1(2))./(x1(3)+xdata1.^x1(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.1 2 0.001];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x1 = lsqcurvefit(fun,x0,xdata1,ydata,lb,ub,options)
times = linspace(xdata(1),xdata1(end));
%plotting
semilogx(xdata1,ydata,'ko',times,fun(x1,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pnor3')
end

function inputs = interpInputs(model, t)
    
    % Get number of inputs
    nInputs = size(model.inputs, 2);
    inputs = zeros(nInputs, 1);
    
    % Interpolate inputs
    for i=1:nInputs
        inputName = model.inputs{i};
        inputs(i) = interp1( ...
            model.timesMap(inputName), ...
            model.speciesMap(inputName), t);
    end
end
