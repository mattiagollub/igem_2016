function h = createPropensitiesFunc(model)
%CRAETEPROPENSITIESFUNCTION Creates a propensities function handle
%   Builds a function handle that returns the reaction propensities of the
%   specified model at a given state.
%
%       h = createPropensitiesFunc(model)
%
%   Returns:
%       h:              function handle in the form h = @(c) prop(c)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%
    nParams = length(model.paramNames);
    a = model.a;
    
    %% Insert parameters
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    a = subs(a, model.paramNames, parameters);
    
    %% Create matlab function object for the propensities
    h = matlabFunction(a, 'File', 'tmpPropensitiesFunc', ...
        'Vars', {model.specieNames, 'p', 't'});
end