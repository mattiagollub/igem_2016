% FINDX Single-variable linear level finding ("inverse" INTERP1).
%   XI = findX(X,Y,YI) estimates the XI values at which the dependent
%   variables Y reach or cross a specified target level YI (scalar value).
%   If there are multiple solutions, findX finds all of them.
%   It may be seen as the "inverse" operation of the MATLAB's function
%   INTERP1, under 'linear' method.
%
%   X is a vector containing a sequence of monotone increasing values of
%   the independent variable. Y is a vector containing the discrete values
%   of the dependent variable (underlying function).
%
%   [XI,IDEXACT] = findX(X,Y,YI) returns in IDEXACT the indices where the
%   original values of Y exactly reach the target level YI,
%   i.e. Y(IDEXACT)=YI, so interpolation was not needed.
%
% Antoni J. Canos.
% Microwave Heating Group, GEA.
% ITACA, Technical University of Valencia.
% Valencia (Spain), April 2009
function [XI,IDEXACT] = findX(X,Y,YI)

    % Check dimensions of input arrays
    m=length(X);
    r=length(Y);

    if ~isvector(X) | ~isvector(Y)
        error('X and Y must be vectors.');
    end

    if (m ~= r)
        error('Lengths of X and Y vectors must be the same.');
    end

    % Initialize outputs
    XI = [];
    IDEXACT = [];
    IDINTERP = [];

    % Subtract target level to simplify subsequent code
    Y = Y - YI;

    % Find exact values. They can be crossings or "kisses".
    IDEXACT = find(Y==0);

    % Find crossings
    IDINTERP = find ( Y (1:end-1) .* Y(2:end) < 0 );

    % Calculating XI values and combining
    XI = union(X(IDEXACT),X(IDINTERP) + (X(IDINTERP+1) - X(IDINTERP)) .* Y (IDINTERP) ./ (Y(IDINTERP) - Y(IDINTERP+1)));

% Copyright (c) 2009, Antoni J. Can�s
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.