% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range focr the experiment
t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input : AHL Activity
stimulus = 100;   % Assume all promoters active
[t_NO, NO] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_NO = t_NO * (tf - t0) + t0;
%% Setup model
modelAHL = createAndGateAHLpartMAmodel();
modelAHL.paramsSet = 'andgate_eth_igem_2014';
modelNO = createAndGateNOpartMAmodel();
modelNO.paramsSet = 'andgate_eth_igem_2014';
model = linkModels({'NO', 'AHL'}, [], ...
        modelNO, modelAHL);
T = model.timesMap;
S = model.speciesMap;
% T('NO') = t_NO;
% S('NO') = NO;
model.paramsSet = 'andgate_eth_igem_2014';



AHLrange=[0.1, 1,10,100, 1000, 10000];
NOrange=[1, 10, 100,1000, 10000, 100000];
M=ANDgate2DMap(model, 'NO', 'AHL', NOrange, AHLrange);




    
    
    [N1,N2]=meshgrid(NOrange, AHLrange);

    [I1,I2]=meshgrid(logspace(log10(NOrange(1)),log10(NOrange(end)),800),...
                   logspace(log10(AHLrange(1)),log10(AHLrange(end)),800));

    
%     [N1,N2]=meshgrid([0:1:7], [0:1:7]);
%     [I1,I2]=meshgrid([0:0.01:7], [0:0.01:7]);
    A=interp2(N1, N2, M, I1, I2);
    figure;
    colormap('jet');
    yticklabels=NOrange;
    xticklabels=AHLrange;
    imagesc(flipud(A));
    ytick = linspace(1, size(I1, 1), 6);
    xtick = linspace(1, size(I1, 2), 6);
%     ytick = logspace(log10(NOrange(1)),log10(NOrange(end)),9)
%     xtick = logspace(log10(AHLrange(1)),log10(AHLrange(end)),9);
    set(gca, 'XTick', xtick, 'XTickLabel', xticklabels);
    set(gca, 'YTick', ytick, 'YTickLabel',flipud(yticklabels(:)));
    grid on
    title('number of active hybrid promoter for range of input values');
    
    xlabel('AHL input concentration rate [nM]');
    ylabel('NO input concentration rate [nM]');
    

    colorbar;