function h = createODEfunc(model)
%CRAETEODEFUNCTION Creates an ODE function handle for the given model.
%   Builds an ODE system from the stoichiometric and rate matrices
%   contained in the specified model. Inserts the parameters in the system
%   and returns a function handle that computes the differential of the
%   system in a given state.
%
%       h = createODEfunc(model)
%
%   Returns:
%       h:              function handle in the form h = @(c) dxdt(c)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%
    %% Compute expressions for the ODEs
    dxdt = model.N * model.rates;
    nParams = size(model.paramNames, 2);
    
    %% Insert parameters
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    dxdt = subs(dxdt, model.paramNames, parameters);
    
    %% Create matlab function object for the ODEs
    h = matlabFunction(dxdt, 'Vars', { model.specieNames });
end