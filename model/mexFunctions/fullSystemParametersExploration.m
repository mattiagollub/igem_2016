clear;
addpath('HYPERSPACE/Files');
addpath('mexFunctions');

%% Generate step functions
 modelAHL = createAndGateAHLpartMAmodel();
 TAHL = modelAHL.timesMap;
 SAHL = modelAHL.speciesMap;
 
 modelNO = createAndGateNOpartMAmodel();
 TNO = modelNO.timesMap;
 SNO = modelNO.speciesMap;
 
 modelSwitch = createSwitchV1MAmodel();
 Tswitch = modelSwitch.timesMap;
 Sswitch = modelSwitch.speciesMap;
 
 modelOutput = createOutputMAmodel();
 TOutput = modelOutput.timesMap;
 SOutput = modelOutput.speciesMap;
 
     %% Compute expressions for the ODEs
    dxdt = modelAHL.N * modelAHL.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/AHLode', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
    
    dxdt = modelNO.N * modelNO.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/NOode', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
    
    dxdt = modelSwitch.N * modelSwitch.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/Switchode', ...
        'Vars', { modelSwitch.specieNames, modelSwitch.paramNames });
    
    dxdt = modelOutput.N * modelOutput.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/Outputode', ...
        'Vars', { modelOutput.specieNames, modelOutput.paramNames });

     %% Insert parameters
    if isfield(modelAHL, 'specieFunctions')
        sf = modelAHL.specieFunctions;
    else
        sf = modelAHL.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/AHLsf', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
   %
   %
    if isfield(modelNO, 'specieFunctions')
        sf = modelNO.specieFunctions;
    else
        sf = modelNO.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/NOsf', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
   %
   %
    if isfield(modelSwitch, 'specieFunctions')
        sf = modelSwitch.specieFunctions;
    else
        sf = modelSwitch.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/Switchsf', ...
        'Vars', { modelSwitch.specieNames, modelSwitch.paramNames });
   %
   %
    if isfield(modelOutput, 'specieFunctions')
        sf = modelOutput.specieFunctions;
    else
        sf = modelOutput.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/Outputsf', ...
        'Vars', { modelOutput.specieNames, modelOutput.paramNames });
    
    
%cost = parameterExploration([3, 3, 4, 0.1, 1.6]);
OutM = MCexp('parameterExploration', 0.0008, [3 3 4 0.1 1.6], [10 10 10 1 10], [0 0 0 0 0], 100000)