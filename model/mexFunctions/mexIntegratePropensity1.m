function [Y, tid] = mexIntegratePropensity1(A, B, t, x, t0, state, params) %#codegen
% INTEGRATEPROPENSITY Integrate using trapezoidal rule until the desired result.
%   
    %% First trapezoid
    tid = t0;
    maxTid = length(t);
    x_ts = mexLinInterp(t(tid), t(tid+1), x(tid), x(tid+1), A);
    a_ts = mexPropensityFunc1TDI([x_ts, state], params);
    a_tid2 = mexPropensityFunc1TDI([x(tid+1), state], params);
    area = (t(tid+1)-A)/2*(a_tid2+a_ts);
    
    %% Add last trapezoid if necessary
    while t(tid+1) < B
        tid = tid + 1;
        if tid >= maxTid
            Y = area + (B-t(tid))*a_tid2;
            return;
        end
        a_tid = a_tid2;
        a_tid2 = mexPropensityFunc1TDI([x(tid+1), state], params);
        area = area + (t(tid+1)-t(tid))/2*(a_tid+a_tid2);
    end
    
    %% Subtract end of trapezoid (points with t > B)
    dt = t(tid+1) - B;
    x_B = mexLinInterp(t(tid), t(tid+1), x(tid), x(tid+1), B);
    a_B = mexPropensityFunc1TDI([x_B, state], params);
    Y = area - dt/2*(a_B+a_tid2);
end