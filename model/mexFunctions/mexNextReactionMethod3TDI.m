function x = mexNextReactionMethod3TDI(N, tspan, x0, tInput, xInput, params) %#codegen
%MEXNEXTREACTIONMETHOD3TDI Next-Reaction Method variant of the Gillespie algorithm
%   Simulate one trajectory of the stochasti simulation algorithm over the
%   specified timespan using the Next-Reaction Method version of the
%   Gillespie algorithm. This function is meant only to be used by the
%   stochasticSimulationFRM function, which generates the propensity
%   functions and compiles everything to MEX.
%   This version of the function supports one reaction with time-dependent
%   propensity.
%
%   Based on: David F. Anderson. (2007) A modified next reaction method for
%   simulating chemical systems with time dependent propensities and delays
%
%       x = mexNextReactionMethod3TDI(N, tspan, x0, tInput, xInput)
%
%   Returns:
%       x:              (nStates x nSpecies) matrix containing the specie
%                       molecule counts at the timestamps defined in tspan.
%
%   Required:
%       N:              (nRxn x nSpecies) stoichiometric matrix of the
%                       system.
%       tspan:          (nStates x 1) vector of timestamps at which the
%                       state of the system must be evaluated.
%       x0:             (nSpecies x 1) vector of initial molecule counts.
%       tInput:         Timestamps of the input specie profile.
%       xInput:         Values of the input specie profile.
%       params:         Vector containing the parameters of the system.
%
    %% Initialize algorithm
    nRxn = size(N, 1);
    nSpecies = size(N, 2);
    nStates = length(tspan);
    x = zeros(nStates, nSpecies);
    Tk = zeros(nRxn, 1);
    
    t = 0;
    tid1 = 1;
    tid2 = 1;
    tid3 = 1;
    stateIdx = 1;
    x(stateIdx,:) = x0;

    %% Generate uniform random numbers for each reaction
    rk = rand(nRxn, 1);
    Pk = log(1./rk);
    Dtk = zeros(size(Pk));

    while t < tspan(end)
        
        %% Update propensities
        ak = abs(mexPropensitiesFunc(x(stateIdx,:), params));
        
        %% Update times to next reaction 
        Dtk(1) = mexIntegratePropensity1To(t, Pk(1) - Tk(1), ...
            tInput{1}, xInput{1}, tid1, x(stateIdx,:), params) - t;
        Dtk(2) = mexIntegratePropensity2To(t, Pk(2) - Tk(2), ...
            tInput{2}, xInput{2}, tid2, x(stateIdx,:), params) - t;
        Dtk(3) = mexIntegratePropensity3To(t, Pk(3) - Tk(3), ...
            tInput{3}, xInput{3}, tid3, x(stateIdx,:), params) - t;
        Dtk(4:end) = (Pk(4:end) - Tk(4:end)) ./ ak;
        
        %% Pick the next firing reaction
        [D, mu] = min(Dtk);

        %% Update time and carry fire reaction mu
        tOld = t;
        xOld = x(stateIdx,:);
        t = t + D;
        if t > tspan(end)
            x(nStates,:) = x(stateIdx,:);
            break;
        end
        while t > tspan(stateIdx)
            x(stateIdx+1,:) = x(stateIdx,:);
            stateIdx = stateIdx + 1;
        end
        x(stateIdx,:) = x(stateIdx,:) + N(mu,:);

        %% Update internal times and firing times
        [D1, tid1] = mexIntegratePropensity1(tOld, t, ...
            tInput{1}, xInput{1}, tid1, xOld, params);
        [D2, tid2] = mexIntegratePropensity2(tOld, t, ...
            tInput{2}, xInput{2}, tid2, xOld, params);
        [D3, tid3] = mexIntegratePropensity3(tOld, t, ...
            tInput{3}, xInput{3}, tid3, xOld, params);
        Tk(1) = Tk(1) + D1;
        Tk(2) = Tk(2) + D2;
        Tk(3) = Tk(3) + D3;
        Tk(4:end) = Tk(4:end) + ak*D;
        Pk(mu) = Pk(mu) + log(1/rand());        
    end
    
    % x = x';
end