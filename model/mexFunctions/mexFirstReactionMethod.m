function x = mexFirstReactionMethod(N, tspan, x0, params) %#codegen
%MEXFIRSTREACTIONMETHOD First-Reaction Method variant of the Gillespie algorithm
%   Simulate one trajectory of the stochasti simulation algorithm over the
%   specified timespan using the First-Reaction Method version of the
%   Gillespie algorithm. This function is meant only to be used by the
%   stochasticSimulationFRM function, which generates the propensity
%   functions and compiles everything to MEX.
%
%       x = mexFirstReactionMethod(N, tspan, x0)
%
%   Returns:
%       x:              (nStates x nSpecies) matrix containing the specie
%                       molecule counts at the timestamps defined in tspan.
%
%   Required:
%       N:              (nRxn x nSpecies) stoichiometric matrix of the
%                       system.
%       tspan:          (nStates x 1) vector of timestamps at which the
%                       state of the system must be evaluated.
%       x0:             (nSpecies x 1) vector of initial molecule counts.
%       params:         Vector containing the parameters of the system.
%
    %% Initialize algorithm
    nRxn = size(N, 1);
    nSpecies = size(N, 2);
    nStates = length(tspan);
    x = zeros(nStates, nSpecies);
    
    t = 0;
    stateIdx = 1;
    x(stateIdx,:) = x0;
    
    while t < tspan(end)

        %% Update propensities
        ak  = abs(mexPropensitiesFunc(x(stateIdx,:), params));

        %% Pick the next firing reaction
        rk = rand(nRxn, 1);
        Dtk = -log(rk) ./ ak;
        [D, mu] = min(Dtk);

        %% Update time and fire reaction mu
        t = t + D;
        if t > tspan(end)
            x(nStates,:) = x(stateIdx,:);
            break;
        end
        while t > tspan(stateIdx)
            x(stateIdx+1,:) = x(stateIdx,:);
            stateIdx = stateIdx + 1;
        end
        x(stateIdx,:) = x(stateIdx,:) + N(mu,:);
    end  
end

