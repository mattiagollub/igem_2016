function x = mexNextReactionMethod1TDI(N, tspan, x0, tInput, xInput, params) %#codegen
%MEXNEXTREACTIONMETHOD1TDI Next-Reaction Method variant of the Gillespie algorithm
%   Simulate one trajectory of the stochasti simulation algorithm over the
%   specified timespan using the Next-Reaction Method version of the
%   Gillespie algorithm. This function is meant only to be used by the
%   stochasticSimulationFRM function, which generates the propensity
%   functions and compiles everything to MEX.
%   This version of the function supports one reaction with time-dependent
%   propensity.
%
%   Based on: David F. Anderson. (2007) A modified next reaction method for
%   simulating chemical systems with time dependent propensities and delays
%
%       x = mexNextReactionMethod1TDI(N, tspan, x0, tInput, xInput)
%
%   Returns:
%       x:              (nStates x nSpecies) matrix containing the specie
%                       molecule counts at the timestamps defined in tspan.
%
%   Required:
%       N:              (nRxn x nSpecies) stoichiometric matrix of the
%                       system.
%       tspan:          (nStates x 1) vector of timestamps at which the
%                       state of the system must be evaluated.
%       x0:             (nSpecies x 1) vector of initial molecule counts.
%       tInput:         Timestamps of the input specie profile.
%       xInput:         Values of the input specie profile.
%       params:         Vector containing the parameters of the system.
%
    %% Initialize algorithm
    nRxn = size(N, 1);
    nSpecies = size(N, 2);
    nStates = length(tspan);
    x = zeros(nStates, nSpecies);
    Tk = zeros(nRxn, 1);
    
    t = 0;
    tid1 = 1;
    stateIdx = 1;
    x(stateIdx,:) = x0;

    %% Generate uniform random numbers for each reaction
    rk = rand(nRxn, 1);
    Pk = log(1./rk);
    Dtk = zeros(size(Pk));

    while t < tspan(end)
        
        %% Update propensities
        ak = abs(mexPropensitiesFunc(x(stateIdx,:), params));
        
        %% Update times to next reaction 
        Dtk(1) = mexIntegratePropensity1To(t, Pk(1) - Tk(1), ...
            tInput{1}, xInput{1}, tid1, x(stateIdx,:), params) - t;
        Dtk(2:end) = (Pk(2:end) - Tk(2:end)) ./ ak;
        
        %% Pick the next firing reaction
        [D, mu] = min(Dtk);

        %% Update time and carry fire reaction mu
        tOld = t;
        xOld = x(stateIdx,:);
        t = t + D;
        if t > tspan(end)
            x(nStates,:) = x(stateIdx,:);
            break;
        end
        while t > tspan(stateIdx)
            x(stateIdx+1,:) = x(stateIdx,:);
            stateIdx = stateIdx + 1;
        end
        x(stateIdx,:) = x(stateIdx,:) + N(mu,:);
        
        %% Update internal times and firing times
        [D1, tid1] = mexIntegratePropensity1(tOld, t, ...
            tInput{1}, xInput{1}, tid1, xOld, params);
        Tk(1) = Tk(1) + D1;
        Tk(2:end) = Tk(2:end) + ak*D;
        Pk(mu) = Pk(mu) + log(1/rand());        
    end
end

