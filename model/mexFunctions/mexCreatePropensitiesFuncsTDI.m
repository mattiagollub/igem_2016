function mexCreatePropensitiesFuncsTDI(model, timeDepReactions, reactionInputs)
%MEXCREATEPROPENSITIESFUNCTION Creates a propensities function handle
%   Builds a function handle that returns the reaction propensities of the
%   specified model at a given state.
%
%       h = createHybridPropensitiesFunc(model)
%
%   Returns:
%       h:              function handle in the form h = @(c) prop(c)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%    
    %% Get time-dependent and constant propensity functions
    aTimeDep = model.a(timeDepReactions);
    aConst = model.a(~timeDepReactions);
    
    aTimeDep1 = aTimeDep(1);
    if length(aTimeDep) >= 2
        aTimeDep2 = aTimeDep(2);
    end
    if length(aTimeDep) == 3
        aTimeDep3 = aTimeDep(3);
    end
    
    %% Create matlab function object for the propensities
    specieNames = [ ...
        model.specieNames(1), sym('dummy'), model.specieNames(2:end);
        model.specieNames([2, 1]), sym('dummy'), model.specieNames(3:end)
    ];
    matlabFunction(aTimeDep1, 'File', ...
        'mexFunctions/tmp/mexPropensityFunc1TDI', ...
        'Vars', { specieNames(reactionInputs(1),:), model.paramNames });
    if length(aTimeDep) >= 2
        matlabFunction(aTimeDep2, 'File', ...
            'mexFunctions/tmp/mexPropensityFunc2TDI', ...
            'Vars', { specieNames(reactionInputs(2),:), model.paramNames });
    end
    if length(aTimeDep) == 3
        matlabFunction(aTimeDep3, 'File', ...
            'mexFunctions/tmp/mexPropensityFunc3TDI', ...
            'Vars', { specieNames(reactionInputs(3),:), model.paramNames });
    end
    matlabFunction(aConst, 'File', ...
        'mexFunctions/tmp/mexPropensitiesFunc', ...
        'Vars', {model.specieNames, model.paramNames});
end