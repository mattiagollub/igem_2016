function cost = mexParameterExploration(parametersToExplore) %#codegen
%   FULL SYSTEM SIMULATION
%   Deterministically simulates the concentration of the and gate species
%   using an example input and compares it withe the simplifeid model
%   result
%   input = [kesarProd, knorProd, kmRNAinv, kl, d_bxb1]
display(parametersToExplore);
%% Simulation settings
kesarProd           = parametersToExplore(1);
knorProd            = parametersToExplore(2);
kmRNAinv            = parametersToExplore(3);
kl                  = parametersToExplore(4);
d_bxb1              = parametersToExplore(5);

% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;
nP = 15;            % Number of plasmids
Ptot = n2nM(nP);% Avogadro constant

% Time range for the experiment
t0   = 0;
tf   = 60*6;

% Input: NO activity
noinput=[1.15,30];
ahlinput=[1.15,30];
costresults =zeros(1,2);

% Input: NO activity
for i=1:length(noinput)
    points = 100;
    NO_stimulus = noinput(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.2/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
    % Input: AHL activity
    points = 100;
    AHL_stimulus = ahlinput(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_AHL_ON = ceil((tf-t0)*0.15/tf*points);
    t_AHL_OFF = floor((tf-t0)*0.25/tf*points);
    t_AHL = (0:points)/points*(tf-t0) + t0;
    AHL = zeros(size(t_AHL));
    AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;
    %% Setup model
    modelAHL = createAndGateAHLpartMAmodel();
    TAHL = modelAHL.timesMap;
    SAHL = modelAHL.speciesMap;
    
    modelNO = createAndGateNOpartMAmodel();
    TNO = modelNO.timesMap;
    SNO = modelNO.speciesMap;
    
    TAHL('AHL') = t_AHL;
    SAHL('AHL') = AHL;
    AHLparamMap=loadParameters('andgate_eth_igem_2014');
    AHLparamMap('kesarProd')=kesarProd;
    modelAHL.paramsSet = AHLparamMap;
    
    TNO('NO') = t_NO;
    SNO('NO') = NO;
    NOparamMap=loadParameters('andgate_eth_igem_2014');
    NOparamMap('knorProd')=knorProd;
    modelNO.paramsSet = NOparamMap;
    
    %% Run simulation for input
    [t, x] = ode15s( ...
        @(t, c) dxdt(sf([interpInputs(model, t); c(nInputs+1:end)]')'), ...
        tspan, model.c0, model.odeOptions);
    
    deterministicSimulation([t0, tf], modelNO);
    %% Run Simulation for switch
    modelSwitch = createSwitchV1MAmodel();
    Tswitch = modelSwitch.timesMap;
    Sswitch = modelSwitch.speciesMap;
    
    SwitchparamMap = loadParameters('switch_bonnet');
    
    SwitchparamMap('k_mRNAinv')=kmRNAinv;
    SwitchparamMap('d_Bxb1')=d_bxb1;
    modelSwitch.paramsSet = SwitchparamMap;
    %
    %
    a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
    b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
    c=abs((kl+(1-kl)*(a.*b)/Ptot^2));
    %
    %
    Tswitch('Phyb_ON') = TNO('NO');
    Sswitch('Phyb_ON') = c;
    
    deterministicSimulation([t0, tf], modelSwitch);
    
    %%
    modelOutput = createOutputMAmodel();
    
    TOutput = modelOutput.timesMap;
    SOutput = modelOutput.speciesMap;
    modelOutput.paramsSet = 'output_eth_igem_2014';
    
    TOutput('Pout_flipped') =Tswitch('Pout_flipped');
    SOutput('Pout_flipped') = Sswitch('Pout_flipped');
    
    TOutput('Pout_free') =TAHL('Pout');
    SOutput('Pout_free') = SAHL('Pout');
    
    deterministicSimulation([t0, tf], modelOutput);
    
    %% outout
    costresults(i) = max(SOutput('GFP'));
end
cost=costresults(1)/costresults(2);