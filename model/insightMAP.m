function P = insightMAP(resultsName)
% INSIGHTMAP Calculate the MAP result of a INSIGHT parameter estimation.
%
%       insightMAP(resultsName)
%
%   Required:
%       resultsFile:    Name of the INSIGHT output file.
%  
    %% Load data
    resultsFile = strcat(resultsName, '.txt');
    weightsFile = strcat(resultsName, '_weights.txt');
    resultsFID = fopen(resultsFile,'r');
    weightsFID = fopen(weightsFile,'r');
    
    R = fscanf(resultsFID, '%f');
    W = fscanf(weightsFID, '%f');
    nSets = length(W);
    nParams = length(R) / nSets;
    R = reshape(R, nParams, nSets);
    
    %% Setup plots
    figure();
    clf;
    wPlots = ceil(sqrt(nParams));
    hPlots = ceil(nParams / wPlots);
    
    %% Estimate MAP parameters using Gaussian kernels
    P = zeros(1, nParams);
    K = @(u) 1 / sqrt(2 * pi) * exp(-1/2*u.*u);
   
    for p=1:nParams
        % Rule of thumb: https://en.wikipedia.org/wiki/Kernel_density_estimation
        h = (4*std(R(p,:))^5 / (3*nSets))^(1/5) / 2;
        f = @(x) 1/(nSets*h) * sum(W .* K((R(p,:)-x*ones(1, nSets))' / h));
        
        subplot(hPlots, wPlots, p);
        x = linspace(min(R(p,:)), max(R(p,:)), 500);
        y = arrayfun(f, x);
        hold on;
        area(x, y, 'linewidth', 2);
        [M, I] = max(y);
        P(p) = x(I);
        plot(P(p), M, 'gx', 'linewidth', 2);
    end
    
    %% Close files
    fclose(resultsFID);
    fclose(weightsFID);
end