% DOSE RESPONSE FITTING Deterministic estimation of the
% Pconst_esabox_sfgfp+EsaR parameters
%    

%% Simulation settings
clear;

%% Setup data
times = [320,320,320,320,320,320];

dose_0 = 0*1e3;
dose_001 = 0.01*1e3;
dose_01 = 0.1*1e3;
dose_1 = 1*1e3;
dose_10 = 10*1e3;
dose_100 = 100*1e3;

doses = [dose_0; dose_001; dose_01; dose_1; dose_10; dose_100];

mean_reporter_0 = 1008;
mean_reporter_001 = 1475;
mean_reporter_01 = 1964;
mean_reporter_1 = 1926;
mean_reporter_10 = 1985;
mean_reporter_100 = 1909;

mean_reporter_s = [mean_reporter_0; mean_reporter_001; mean_reporter_01; mean_reporter_1; mean_reporter_10; mean_reporter_100];

%% Setup model
model = createAndGateAHLpartMAmodel();
model.paramsSet = containers.Map();

%% Setup experimental data
experiments.inputTimes = { ...
    containers.Map({'AHL'}, {times}) ...
    containers.Map({'AHL'}, {times}) ...
    containers.Map({'AHL'}, {times}) ...
    containers.Map({'AHL'}, {times}) ...
    containers.Map({'AHL'}, {times}) ...
    containers.Map({'AHL'}, {times}) ...
};
experiments.inputSpecies = { ...
    containers.Map({'AHL'}, {dose_0}) ...
    containers.Map({'AHL'}, {dose_001}) ...
    containers.Map({'AHL'}, {dose_01}) ...
    containers.Map({'AHL'}, {dose_1}) ...
    containers.Map({'AHL'}, {dose_10}) ...
    containers.Map({'AHL'}, {dose_100}) ...
};
experiments.outputTimes = { ...
    containers.Map({'Bxb1'}, {times}) ...    
    containers.Map({'Bxb1'}, {times}) ...
    containers.Map({'Bxb1'}, {times}) ...
    containers.Map({'Bxb1'}, {times}) ...
    containers.Map({'Bxb1'}, {times}) ...
    containers.Map({'Bxb1'}, {times}) ...
};
experiments.outputSpecies = { ...
    containers.Map({'Bxb1'}, {mean_reporter_0}) ...
    containers.Map({'Bxb1'}, {mean_reporter_001}) ...
    containers.Map({'Bxb1'}, {mean_reporter_01}) ...
    containers.Map({'Bxb1'}, {mean_reporter_1}) ...
    containers.Map({'Bxb1'}, {mean_reporter_10}) ...
    containers.Map({'Bxb1'}, {mean_reporter_100}) ...
};

%% Specify estimation options
options.targetParams = { 'k5' 'k_5' 'k_6' 'k6' 'k7' 'k_7'...
                        'kmrna' 'kl' 'kesarProd' 'desar' 'dmrna' 'k_Bxb1' 'd_Bxb1' 'desarahl'};
options.subsFunc = @(P) P;
options.lowerBounds = [0.0001, 0.0001, 0.0001, 0.0001,  0.0001, 0.0001, 0.01, 0.01, n2nM(2),0.001, 0.001, 0.001, 0.001,0.001];
options.upperBounds = [0.5, 0.5, 0.5, 0.5,   0.5,  0.5, 1,    1,  n2nM(1000), 1,1,1,1,1];
options.startValues = [0.1, 1.4, 200,  1.0,  0.4,  4.0,  0.02,   0.1,  n2nM(80), 0.1,0.1,0.1,0.1,0.1];

%% Estimate parameters
newParams = estimateDeterministicParams(options, experiments, model);

display('Parameter estimation completed. Result:\n');
display(newParams);


%% Run simulation with new parameters
model.paramsSet = newParams;
model.timesMap = containers.Map();
model.speciesMap = containers.Map();
Tn = model.timesMap;
Sn = model.speciesMap;
Tn('AHL') = times;

%% Plot results
figure;
clf;
M=zeros(1,6);
for i=1:6
    Sn('AHL') = doses(i,:).*1e3;
    deterministicSimulation([t0, tf], model);
    
    hold on;
    plot(Tn('Bxb1'), Sn('Bxb1'), 'linewidth', 2);
    plot(times, mean_reporter_s(i,:), 'k+', 'linewidth', 2);
    grid on;
    title('Ptet_sfGFP simulation');
    xlabel('Time [min]');
    ylabel('Fluorescence');
end
legend('0ng/uL simulated', '0ng/uL data', '001ng/uL simulated', '001ng/uL data',...
    '01ng/uL simulated', '01ng/uL data','1ng/uL simulated', '1ng/uL data',...
    '10ng/uL simulated', '10ng/uL data','100ng/uL simulated', '100ng/uL data'); 