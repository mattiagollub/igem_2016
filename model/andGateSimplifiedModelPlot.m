% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input and compares it withe the simplifeid model
%   result
% 
%% Simulation settings
clear;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
tf   = 60*6;
lineWidth = 1.5;
% Input: NO activity
points = 100;
    NO_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.2/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
%% Setup model
model1 = createAndGateNOpartMAmodel();
T1 = model1.timesMap;
S1 = model1.speciesMap;

model2 = createAndGateNOpartSimplifiedmodel();
T2 = model2.timesMap;
S2 = model2.speciesMap;

T1('NO') = t_NO;
S1('NO') = NO;
model1.paramsSet = 'andgate_eth_igem_2014';

T2('NO') = t_NO;
S2('NO') = NO;
model2.paramsSet = 'andgate_eth_igem_2014';

%% Run simulation
deterministicSimulation([t0, tf], model1);
deterministicSimulation([t0, tf], model2);
%% Plot results
% mRNAinv input profile
figure(1);
clf;
subplot(2, 2, 1);
stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
grid on;
title('Input: NO')
xlabel('Time [min]');
ylabel('Concentration [nMol]');
legend('NO');

 %% Plot NOR system
    ax = subplot(2, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(T1('NoR'), S1('NoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(T2('NoR'), S2('NoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('NOR system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NoR MA', 'NoR Simplified');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    hold on
    p = plot(T1('mRNAinv'), S1('mRNAinv'), 'linewidth', lineWidth);
    p = plot(T2('mRNAinv'), S2('mRNAinv'), 'linewidth', lineWidth);
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('mRNAinv MA', 'mRNAinv Simplified');
    
     %% Plot PnorV behavior
    ax = subplot(2, 2, 4);
    ax.ColorOrderIndex = 1;
    p = plot(T1('NoR_NO'), S1('NoR_NO'), 'linewidth', lineWidth);
    hold on
    p = plot(T2('NoR_NO'), S2('NoR_NO'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('NoR_NO behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NoR_NO MA', 'NoR_NO Simplified');
    
   
    %% Plot sensitivities for Pout_flipped only
% figure(2)
% nParams = length(model.paramNames);
% nSpecies = length(model.specieNames);
% nInputs = length(model.inputs);
% hold on
% specie = 'mRNAinv';
% names = cell(size(model.paramNames));
% for p=1:nParams
%     names{p} = char(model.paramNames(p));
%     name = strcat('S_', specie, '_', names{p});
%     plot(T(name), S(name), 'linewidth', 2);
% end
% grid on;
% title('Sensitivity of the parameters w.r.t. mRNAinv');
% xlabel('Time [min]');
% ylabel('Sensitivity');
% l = legend(names{:});
% set(l, 'Interpreter', 'none') 
% 
% %% Print matrix with maximum sensitivities
% S = zeros(nParams, nSpecies);
% for s=nInputs+1:nSpecies
% 	for p=1:nParams
%         name = strcat('S_', char(model.specieNames(s)), ...
%                       '_',  char(model.paramNames(p)));
%         S(p, s) = max(model.speciesMap(name));
%     end
% end
% S = S / max(abs(S(:)));
% display(S);
