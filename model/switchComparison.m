function switchComparison()
% SWITCHCOMPARISON Comparison between different switch models.
%
    %% Simulation settings

    % Parameter set
    params_set = 'switch_bonnet';

    % Time range for the experiment
    t0   = 0;
    tf   = 120;
    tspan = t0:0.1:tf;
    nRuns = 50;

    % Input: mRNAinv profile
    stimulus = 1;    % Assume all promoters active
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

    %% Setup models
    modelMA = createSwitchV2MAmodel();
    modelQSS = createSwitchV2QSSmodel();
    modelSSA = createSwitchV2MAmodel();

    TMA = modelMA.timesMap;
    SMA = modelMA.speciesMap;
    TQSS = modelQSS.timesMap;
    SQSS = modelQSS.speciesMap;
    TSSA = modelSSA.timesMap;
    SSSA = modelSSA.speciesMap;

    TMA('Phyb_ON') = t_Phyb_ON;
    SMA('Phyb_ON') = v_Phyb_ON;
    TQSS('Phyb_ON') = t_Phyb_ON;
    SQSS('Phyb_ON') = v_Phyb_ON;
    TSSA('Phyb_ON') = t_Phyb_ON;
    SSSA('Phyb_ON') = v_Phyb_ON;

    modelMA.paramsSet = params_set;
    modelQSS.paramsSet = params_set;
    modelSSA.paramsSet = params_set;

    %% Run full mass action simulation
    deterministicSimulation([t0, tf], modelMA);

    %% Run simplified mass action simulation
    deterministicSimulation([t0, tf], modelQSS);

    %% Plot results
    figure(1);
    clf;

    % Pout_ON input profile
    subplot(3, 1, 1);
    plot(t_Phyb_ON, v_Phyb_ON, 'linewidth', 2);
    grid on;
    title('Input: Fraction of Phyb activity')
    xlabel('Time [min]');
    ylabel('Phyb_{ON} fraction');
    legend('Phyb_{ON}');
    ylim([0, stimulus * 1.3]);

    % Bxb1 dimerization
    ax = subplot(3, 1, 2);
    hold on
    plot(TMA('Bxb1'), SMA('Bxb1'), 'linewidth', 2);
    plot(TMA('DBxb1'), SMA('DBxb1'), 'linewidth', 2);
    ax.ColorOrderIndex = 5;
    plot(TQSS('Bxb1'), SQSS('Bxb1'), 'linewidth', 2);
    grid on;
    title('Bxb1 dimerization');
    xlabel('Time [min]');
    ylabel('Concentration [nM]');
    legend('Bxb1 (MA)', 'DBxb1 (MA)', 'Bxb1 (QSS)'); 
    drawnow;

    % Integrase behavior
    subplot(3, 1, 3);
    hold on;
    pPoutMA = plot(TMA('Pout_flipped'), nM2n(SMA('Pout_flipped')), ...
        'linewidth', 2);
    pPoutQSS = plot(TQSS('Pout_flipped'), nM2n(SQSS('Pout_flipped')), ...
        'linewidth', 2);
    grid on;
    title('Promoter flipping');
    xlabel('Time [min]');
    ylabel('Number of flipped promoters');
    legend('Pout^{flipped} (MA)', 'Pout^{flipped} (QSS)');
    drawnow;

    %% Stochastic simulation

    drawFunctions = containers.Map( ...
        {'Pout_flipped'}, ...
        { @(t, x) drawInSubplot(t, x, 3, 3, 1, 20 / nRuns)});

    stochasticSimulationNRM(tspan, modelSSA, nRuns, drawFunctions);

    l = legend('Pout^{flipped} (MA)', ...
               'Pout^{flipped} (QSS)', ...
               'Pout^{flipped} (SSA)');
    l.Location = 'southeast';
    uistack(pPoutQSS,'top');
    uistack(pPoutMA,'top');
end

function drawInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(3, 1, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, nM2n(x), 'linewidth', lineWidth);
    p.Color(4) = alpha;
end