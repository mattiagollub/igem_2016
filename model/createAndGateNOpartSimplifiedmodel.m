function model = createAndGateNOpartSimplifiedmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'NO'};
    
    %% Define symbolic variables for species and parameters
    syms NO NoR NoR_NO PnorV3 mRNAinv
    syms knor_no k_nor_no Kno Kmno nno...
         kmrna kl knorProd dnor dnor_no dmrna
    
    model.specieNames = [NO NoR NoR_NO  ...
                         PnorV3 mRNAinv];
    model.paramNames = [knor_no k_nor_no Kno Kmno nno...
                        kmrna kl knorProd dnor dnor_no dmrna]; 
    
    %% Stoichiometric and rate matrices
    % NO | NoR | NoR_NO | PnorV3 | mRNAinv |
    model.N = [       
     0,   -1,  1,  0,  0;    % NoR + NO -> NoR_NO
     0,    1, -1,  0,  0;    % NoR + NO <- NoR_NO
     0,    0,  0,  0,  1;    % PnorV3-> mRNAinv      
     0,    1,  0,  0,  0;    % -> NoR     
     0,   -1,  0,  0,  0;    % NOR ->
     0,    0,  0,  0, -1;    % mRNAinv ->      
     ]';
    
    %% Reaction rates
    model.rates = [
            knor_no*NoR*NO;                % NoR + NO -> NoR_NO
            k_nor_no*NoR_NO;               % NoR + NO <- NoR_NO
           (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; %PnorV3 -> mrna inv
           (knorProd*kl)+(knorProd*(1-kl)*Ptot) ;  % -> NoR
            dnor*NoR;                      % NOR ->
            dmrna*mRNAinv;                 % mRNAinv ->
    ];
    
    %% Special species functions
    model.specieFunctions = [
        NO;
        NoR;
        NoR_NO; 
        Ptot*Kno*((NoR_NO)^nno)/(Kmno + (NoR_NO)^nno);
        mRNAinv;
    ];
    model.specieFunctions = subs(model.specieFunctions, ...
        NoR_NO, (knor_no*NO*NoR)/(dnor_no + k_nor_no));

    %% Reaction propensities
    model.a = [
           knor_no*NoR*NO;                % NoR + NO -> NoR_NO
            k_nor_no*NoR_NO;               % NoR + NO <- NoR_NO
           (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; %PnorV3 -> mrna inv
           (knorProd*kl)+(knorProd*(1-kl)*Ptot) ;  % -> NoR
            dnor*NoR;                      % NOR ->
            dmrna*mRNAinv;                 % mRNAinv ->
	];
end