function cost = parameterExplorationV2(parametersToExplore)
%   FULL SYSTEM SIMULATION
%   Deterministically simulates the concentration of the and gate species
%   using an example input and compares it withe the simplifeid model
%   result
%   input = [kesarProd, knorProd, kmRNAinv, kl, d_bxb1]
%parametersToExplore

global modelAHL TAHL SAHL AHLparamMap ...
       modelNO  TNO SNO NOparamMap ...
       modelSwitch Tswitch Sswitch SwitchparamMap...
       modelOutput TOutput SOutput OutputparamMap
   
%% Simulation settings
kesarProd           = parametersToExplore(1);
knorProd            = parametersToExplore(2);
d_bxb1              = parametersToExplore(3);
kl=0.1;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;
nP = 15;            % Number of plasmids
Ptot = n2nM(nP);% Avogadro constant

% Time range for the experiment
t0   = 0;
tf   = 60*6;
tspan = [t0, tf];

% Input: NO activity
noinput=[1.15,30];
ahlinput=[1.15,30];
costresults =zeros(1,2);

% Input: NO activity
for i=1:length(noinput)
    tic;
    points = 100;
    NO_stimulus = noinput(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.2/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
    
    % Input: AHL activity
    points = 100;
    AHL_stimulus = ahlinput(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_AHL_ON = ceil((tf-t0)*0.15/tf*points);
    t_AHL_OFF = floor((tf-t0)*0.25/tf*points);
    t_AHL = (0:points)/points*(tf-t0) + t0;
    AHL = zeros(size(t_AHL));
    AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;
    
    %% Setup model
    TAHL('AHL') = t_AHL;
    SAHL('AHL') = AHL;
    
    parametersAHL=changeParamInModel(modelAHL,AHLparamMap,'kesarProd',kesarProd);
    
    TNO('NO') = t_NO;
    SNO('NO') = NO;
    
    parametersNO=changeParamInModel(modelNO,NOparamMap,'knorProd',knorProd);
    
    setupModel(modelAHL,'AHLode','AHLsf', tspan, parametersAHL);
    
    setupModel(modelNO,'NOode','NOsf', tspan, parametersNO);
    
    
    % Run Simulation for switch
    %modelSwitch = createSwitchV1MAmodel();
    
    a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
    b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
    c=abs((kl+(1-kl)*(a.*b)/Ptot^2));
    %
    %
    Tswitch('Phyb_ON') = TNO('NO');
    Sswitch('Phyb_ON') = c;
   
    parametersSwitch=changeParamInModel(modelSwitch,SwitchparamMap,'d_Bxb1',d_bxb1);
  
    setupModel(modelSwitch,'Switchode','Switchsf', tspan, parametersSwitch);
    
   
    %modelOutput = createReporterMAmodel();
    
    TOutput = modelOutput.timesMap;
    SOutput = modelOutput.speciesMap;
    
    
    TOutput('Pout_flipped') =Tswitch('Pout_flipped');
    SOutput('Pout_flipped') = Sswitch('Pout_flipped');
    
    TOutput('Pout_free') =TAHL('Pout');
    SOutput('Pout_free') = SAHL('Pout');
    
    modelOutput.paramsSet = OutputparamMap;
    
    parametersOutput=changeParamInModel(modelOutput,OutputparamMap);
    
    setupModel(modelOutput,'Outputode','Outputsf', tspan, parametersOutput);

    %% outout
    costresults(i) = max(SOutput('GFP'));
    toc
end
cost=costresults(1)/costresults(2);
display(cost);
end

function inputs = interpInputs(model, t)
    
    % Get number of inputs
    nInputs = size(model.inputs, 2);
    inputs = zeros(nInputs, 1);
    
    % Interpolate inputs
    for i=1:nInputs
        inputName = model.inputs{i};
        inputs(i) = interp1( ...
            model.timesMap(inputName), ...
            model.speciesMap(inputName), t);
    end
end