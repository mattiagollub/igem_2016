addpath('plotting');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range for the experiment
t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

figure;
hold on
setColorPalette([0 1], 'blue',12);


model = createAndGateAHLpartMAmodel();
% Input : AHL Activity
AHLRange=horzcat([0, 0.001, 0.1],[1,5,10],[20,50,100],[200,500,1000]);

for i=AHLRange
stimulus = i;   % Assume all promoters active
[t_AHL, AHL] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_AHL = t_AHL * (tf - t0) + t0;
%% Setup model

T = model.timesMap;
S = model.speciesMap;
T('AHL') = t_AHL;
S('AHL') = AHL;
model.paramsSet = 'andgate_eth_igem_2014';
%paramRange


deterministicSimulation([t0, tf], model);
%p = plot(T('Bxb1'), S('Bxb1'),'+-');
p = plot(T('Pfree'), nM2n(S('Pfree'))/15);
grid on
title('time response for different AHL input values');
xlabel('time (min)');
ylabel('fraction of active promoter');
drawnow;
end