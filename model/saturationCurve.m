function [M, threshold] = saturationCurve(model,input,output,range)


%% Parameters
pnortot= 24.9081;
p=nM2n(pnortot);
%clear;
% Common constants
nRuns = 500;
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 2;
M=zeros(1, length(range));

%%



parfor i=1:length(range)
    
    i
    range(i)
    %%

    [t_input, input_vector] = makeStimulusProfile(0.3, 1.0, range(i), 100);
    t_input = t_input * (tf - t0) + t0;
    %%
    
    
%     points = 100;
%     input_stimulus = range(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_input_ON = ceil((tf-t0)*0.2/tf*points);
%     t_input_OFF = floor((tf-t0)*1/tf*points);
%     t_input = (0:points)/points*(tf-t0) + t0;
%     input_vector = zeros(size(t_input));
%     input_vector(1, t_input_ON:t_input_OFF) = input_stimulus;
    
    T = model.timesMap;
    S = model.speciesMap;

    T(input) = t_input;
    S(input) = input_vector;
    
    deterministicSimulation([t0, tf], model);
     R=S(output);
     M(i)=R(end);
    %M(i)=max(S(output));
    
%     figure(1);
%     hold on
%     p = plot(T(output), S(output), 'linewidth', lineWidth);
%     grid on;
%     title('output for different iinput concentration');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     drawnow;
end
%  x=[range(1):1:range(end)];
%  y=interp1(range, nM2n(M),x,'pchip');
x=[range(1):10000:range(end)];
y=interp1(log(range), nM2n(M),log(x),'pchip');
%  x=range;
%  y=nM2n(M);


dno=diff(x);
dcc=diff(y);
derivative=dcc./dno;
k=find(derivative==(max(derivative)));

if length(k)>1 && range(k(1))==0;
    threshold=100
elseif length(k)>1
       threshold=x(k(1))
else
     threshold=x(k)
end




figure(100);
hold on
setColorPalette([0 1], 'blue',8);
%set(gca, 'ColorOrder', colororder);
hold on
plot(x,y/(p+3.5),'linewidth', lineWidth);
title('dose response for different kesarProd parameters curve');
xlabel('input NO concentration [nM]');
ylabel('Phyb activation [%]');
set(gca,'xscale','log');
drawnow;

figure(101);
hold on
plot(x(1:end-1),derivative,'linewidth', lineWidth);
grid on;
title('derivative curve');
xlabel('derivative [nM/min]');
ylabel('Concentration [nMol]');
set(gca,'xscale','log');
drawnow;
