function parameters = changeParamInModel(model,modelPAramMap, nameOfParam, valueOfParam)
        paramToChange = false;
        
        if exist('nameOfParam','var')&& exist('modelPAramMap','var')&& exist('valueOfParam','var')
            paramToChange = true;
        end
        
        if paramToChange
            modelPAramMap(nameOfParam) = valueOfParam;
            model.paramsSet = modelPAramMap;
        end
        
        nParams = size(model.paramNames, 2);
        parameters = zeros(1, nParams);

        for k=1:nParams
            paramName = char(model.paramNames(k));
            parameters(k) = modelPAramMap(paramName);
        end

end