function J = PARAMETER_ESTIMATION_AHL_PART_PB(x)
% PARAMETERESTIMATIONPROBLEM Error function of the parameter estimation. 
%   Computes an error measure of a set of parameters. The error is defined
%   as the sum of squared errors of the simulated datapoints from the
%   experimental data. This function is intended to be used with the MEIGO
%   toolbox.
%
    %% Change the IntegrationTolNotMet warning into an error 
    warnstate = warning;
    warnId_ode45 = 'MATLAB:ode45:IntegrationTolNotMet';
    warnId_ode15s = 'MATLAB:ode15s:IntegrationTolNotMet';
    warning('error', warnId_ode45);
    warning('error', warnId_ode15s);
 
    model = createAndGateAHLpartMAmodel();
    
    options.targetParams = { 'k5' 'k_5' 'k_6' 'k6' 'k7' 'k_7'...
                        'kmrna' 'kl' 'kesarProd' 'desar' 'dmrna' 'k_Bxb1'...
                        'd_Bxb1' 'desarahl' };
                    
    options.subsFunc = @(P) P;
    options.lowerBounds = [0.0001, 0.0001, 0.001, 0.0001,  0.0001, 0.00001, 0.0001, 0.001, 0.001,0.0001, 0.18, 25, 0.001, 0.00001];
    options.upperBounds = [0.9, 0.5, 0.5, 0.5,   0.5,  0.5, 0.1,    1,  n2nM(20), 0.1,0.2,26,0.002, 0.1];
    options.startValues = [0.478817, 0.00159551, 0.05, 0.001,  0.00292825,  0.00013413,  0.0462825,   0.182,  1, 0.00123625, 0.182, 25.5, 0.00123625, 0.01];
    
   times_in = [0, 119, 120, 160, 190, 220, 270, 320];

    dose_0 = [0, 0, 0, 0, 0, 0, 0, 0]*1e3;
    dose_1 = [0, 0, 1, 1, 1, 1, 1, 1]*1e3;
    dose_10 = [0, 0, 10, 10, 10, 10, 10, 10]*1e3;
    dose_100 = [0, 0, 100, 100, 100, 100, 100, 100]*1e3;



    times_out = [0, 120, 160, 190, 220, 270, 320];

    mean_reporter_0 = [0, 667, 637, 589, 632, 697, 923];
    mean_reporter_1 = [0, 653, 1726, 2183, 2523, 2861, 3189];
    mean_reporter_10 = [0, 653, 1834, 2476, 2983, 3429, 3481];
    mean_reporter_100 = [0, 669, 1865, 2492, 2961, 3352, 3829];

    %% Adjust growth time
%     growthTime = 60*2;
%     times_in = [0, growthTime-0.01, times_in+growthTime];
%     times_out = [0, times_out+growthTime];

    %% Setup experimental data
     experiments.inputTimes = { ...
        containers.Map({'AHL'}, {times_in}) ...
        containers.Map({'AHL'}, {times_in}) ...
        containers.Map({'AHL'}, {times_in}) ...
        containers.Map({'AHL'}, {times_in}) ...
    };
    experiments.inputSpecies = { ...
        containers.Map({'AHL'}, {dose_0}) ...
        containers.Map({'AHL'}, {dose_1}) ...
        containers.Map({'AHL'}, {dose_10}) ...
        containers.Map({'AHL'}, {dose_100}) ...
    };
    experiments.outputTimes = { ...
        containers.Map({'Bxb1'}, {times_out}) ...    
        containers.Map({'Bxb1'}, {times_out}) ...
        containers.Map({'Bxb1'}, {times_out}) ...
        containers.Map({'Bxb1'}, {times_out}) ...
    };
    experiments.outputSpecies = { ...
        containers.Map({'Bxb1'}, {mean_reporter_0}) ...
        containers.Map({'Bxb1'}, {mean_reporter_1}) ...
        containers.Map({'Bxb1'}, {mean_reporter_10}) ...
        containers.Map({'Bxb1'}, {mean_reporter_100}) ...
    };
    
    %% Insert parameters in the model
    c0 = model.c0;
    
    % Insert default parameters
    if ~isfield(model, 'paramsSet')
        P = containers.Map();
    elseif (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    
    % Insert extimated parameters
    for p=1:length(x)
        P(options.targetParams{p}) = x(p);
    end
    
    % Perform substitutions if necessary
    P = options.subsFunc(P);
    model.paramsSet = P;
    for i=1:length(model.paramNames)
        paramName = char(model.paramNames(i));
        model.c0 = subs(model.c0, model.paramNames(i), P(paramName));
    end
    model.c0 = double(model.c0);
    
    %% Iterate over all the experiments
    nExp = size(experiments.inputTimes, 2);
    J = 0;
    R = [];
    g = 0;
    
    for i=1:nExp
        
        EIT = experiments.inputTimes{i};
        EIS = experiments.inputSpecies{i};
        EOT = experiments.outputTimes{i};
        EOS = experiments.outputSpecies{i};
        
        model.timesMap = containers.Map();
        model.speciesMap = containers.Map();
        MT = model.timesMap;
        MS = model.speciesMap;
        
        %% Set inputs for the model
        inSpecies = keys(EIS);
        for s=1:length(inSpecies)
            specie = inSpecies(s);
            specie = specie{1};
            MT(specie) = EIT(specie);
            MS(specie) = EIS(specie);
        end
        
        %% Determine all required datapoints that the simulation
        reqTExp = 0;
        outSpecies = keys(EOS);
        for s=1:length(outSpecies)
            specie = outSpecies(s);
            specie = specie{1};
            reqTExp = [reqTExp, EOT(specie)];
        end
        reqTExp = unique(reqTExp);
        
        %% Run the simulation with the given parameters
        failed = false;
        try
            deterministicSimulation(reqTExp, model);
        catch ERR
            if strcmp(ERR.identifier, warnId_ode45) || ...
                strcmp(ERR.identifier, warnId_ode15s)
                failed = true;
            else
                throw(ERR);
            end
        end
        
        %% Compute least squares error w.r.t. the experimental results
        if (failed)
            % If the integration failed then we probably got very bad
            % parameters
            yExp = EOS(specie);
            J = 100000000000000;
            break;
        else
            species = keys(EOS);
            for s=1:length(species)
                specie = species(s);
                specie = specie{1};
                tExp = EOT(specie);
                yExp = EOS(specie);
                yOut = interp1(MT(specie), MS(specie), tExp);
                nPoints = size(yExp, 2);
                
                Rt = (yOut - yExp);
                
                if ~isreal(J)
                    J = 100000000000000;
                else
                    J = J + sum(Rt.^2) / nPoints;
                end
            end
        end
    end
    
    model.c0 = c0;
    display(J);
    display(x);
    
    warning(warnstate);
end