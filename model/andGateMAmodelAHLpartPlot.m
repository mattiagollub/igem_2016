%function andGateMAmodelAHLpartPlot()
% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input.
% 
%% Simulation settings
%clear;
nRuns = 500;
addpath('mexFunctions');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range for the experiment
t0   = 0;
ts   = 10;
tf   = 320;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input: NO activity
%     points = 100;
%     AHL_stimulus = 300 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_AHL_ON = ceil((tf-t0)*0.1/tf*points);
%     t_AHL_OFF = floor((tf-t0)*0.2/tf*points);
%     t_AHL = (0:points)/points*(tf-t0) + t0;
%     AHL = zeros(size(t_AHL));
%     AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

% Input : AHL Activity
    stimulus = 10000
   ;   % Assume all promoters active
    [t_AHL, AHL] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
    t_AHL = t_AHL * (tf - t0) + t0;
%% Setup model
model = createAndGateAHLpartMAmodel();
T = model.timesMap;
S = model.speciesMap;

T('AHL') = t_AHL;
S('AHL') = AHL;
model.paramsSet = 'experiment_EsaR_Part_AND_GATE';

% Histogram plotting
    histogramsX = 2;
    histogramsY = 2;
    nHistograms = histogramsX * histogramsY;

%% Plot results
% mRNAinv input profile
    figure;
    clf;
    subplot(2, 1, 1);
    stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: AHL')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('AHL');

    drawFunctions = containers.Map( ...
        {'Esar', 'DEsar', 'DEsar_AHL1', 'DEsar_AHL', 'mRNAinv', 'Pdesar1_AHL', 'Pdesar1', 'Pfree', 'Pout'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 4, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 4, 1, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 3, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 4, lineWidth, 0.2), ...
        });
    %% Run simulation
deterministicSimulation([t0, tf], model, true);
%stochasticSimulationNRM(tspan, model, nRuns);%, drawFunctions);
 %% Plot NOR system
%     ax = subplot(2, 1, 1);
%     hold on;
%     ax.ColorOrderIndex = 1;
%     p = plot(T('Esar'), S('Esar'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     hold on
%     p = plot(T('DEsar'), S('DEsar'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('DEsar_AHL1'), S('DEsar_AHL1'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('DEsar_AHL'), S('DEsar_AHL'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     grid on;
%     title('DEsar system');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('Esar', 'DEsar','DEsar_AHL1','DEsar_AHL');

    %% Plot PnorV behavior
    ax = subplot(2, 1, 2);
    ax.ColorOrderIndex = 1;
    p = plot(T('Bxb1'),nM2n(S('Bxb1')), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('mRNAinv');
    
%     %% Plot DEsaR behavior
%     ax = subplot(2, 2, 4);
%     hold on;
%     ax.ColorOrderIndex = 2;
%     p = plot(T('Pdesar1_AHL'), S('Pdesar1_AHL'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pdesar1'), S('Pdesar1'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pfree'), S('Pfree'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pout'), S('Pout'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     grid on;
%     title('Pfree behavior');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('Pdesar1_AHL','Pdesar1', 'Pfree','Pout');
%     drawnow;
    %% Plot DEsaR_AHL behavior
%     ax = subplot(3, 2, 5);
%     hold on;
%     ax.ColorOrderIndex = 3;
%     p = plot(T('Pnor0'), S('Pnor0'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pnor1'), S('Pnor1'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     hold on
%     p = plot(T('Pnor2'), S('Pnor2'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('PnorV0'), S('PnorV0'), 'linewidth', lineWidth);
%     grid on;
%     title('DesaR_AHL behavior');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('Pnor0', 'Pnor1', 'Pnor2', 'PnorV0');
    % Plot sensitivities for Pout_flipped only
% figure;
% nParams = [7, 10, 11];
% nSpecies = length(model.specieNames);
% nInputs = length(model.inputs);
% hold on
% specie = 'Pfree';
% names = cell(size(nParams));
% for p=nParams
%     p
%     names{p} = char(model.paramNames(p));
%     name = strcat('S_', specie, '_', names{p})
%     plot(T(name), S(name), 'linewidth', 2);
% end
% grid on;
% title('Sensitivity of the parameters w.r.t. Pfree activity');
% xlabel('Time [min]');
% ylabel('Sensitivity');
% l = legend('Esar translation rate','Esar transcription rate', 'Esar Degradation rate');
% set(l, 'Interpreter', 'none') 

%% Print matrix with maximum sensitivities
%% Plot histograms
% %    figure;
% %    clf;
% %     subplot(2, 1, 1);
% %     histsPfree = model.histogramsMap('Pfree');
% %     PfreeDist = fitdist(histsPfree(end, :)','Normal');
% %     histogram(histsPfree(end, :));
% %     hold on
% %     x_values = 0:1:30;
% %     y1 = pdf(PfreeDist,x_values);
% %     f = fit(x_values.',500*y1','gauss1')
% %     plot(f,x_values,500*y1)
% %     title(sprintf('Pfree distribution at t=%d min', tspan(end)));
% %     xlabel('Number of molecules');
% %     ylabel('Number of cells');
% %     
% %     subplot(2, 1, 2);
% %     histsPout = model.histogramsMap('Pout');
% %     PfoutDist = fitdist(histsPout(end, :)','Normal');
% %     histogram(histsPout(end, :));
% %     hold on
% %     x_values = 0:1:30;
% %     y2 = pdf(PfoutDist,x_values);
% %     f = fit(x_values.',500*y2','gauss1');
% %     plot(f,x_values,500*y2);
% %     title(sprintf('Pout distribution at t=%d min', tspan(end)));
% %     xlabel('Number of molecules');
% %     ylabel('Number of cells');
% %     
% %     figure;
% %     y1=std(histsPfree');
% %     y2=std(histsPout');
% %     hold on
% %     tnew=1:10:length(T('Pfree'));
% %     snew=S('Pfree');
% %     sout=S('Pout');
% %     s1=snew(tnew);
% %     s2=sout(tnew);
% %     ynew=y1(tnew);
% %     yout=y2(tnew);
% %     errorbar(tnew,s1,ynew);
% %     errorbar(tnew,s2,yout);
% %     title(sprintf('mean(Pfree,Pout)) behavior for the and gate modular model, input AHL = 100nM'));
% %     xlabel('Time [min]');
% %     ylabel('Concentration [nMol]');
% %     legend Pfree Pout
% end
% 
% 
% function drawInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = plot(t,nM2n(x), 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end
%   figure;
%     clf;
%     subplot(2, 1, 1);
%     stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
%     grid on;
%     title('Input: AHL')
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('AHL'); 
%     
%     
%     
%     
%     subplot(2, 2, 4);
%     hold on;
%     ax.ColorOrderIndex = 2;
%     p = plot(T('Pdesar1_AHL'), S('Pdesar1_AHL'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pdesar1'), S('Pdesar1'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pfree'), S('Pfree'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pout'), S('Pout'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     grid on;
%     title('Pfree behavior');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('Pdesar1_AHL','Pdesar1', 'Pfree','Pout');
%     drawnow;