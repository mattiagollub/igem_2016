function p = estimateDeterministicParamsMT(setupFunc)
% ESTIMATEDETERMINISTICPARAMS Infer parameters from experimental data
%   Infer the parameters of a mass action model from a set of
%   experimental data.
%
%       p = estimateDeterministicParams(options, experiments, model)
%
%   Returns:
%       p:              containers.Map containing the values of the
%                       estimated parameters.
%
%   Parameters:
%       setupFunc:      String containing the neame of a funciton that
%                       sets up options, experiments and model.
    %% Specify problem setings
    setupFn = str2func(setupFunc);
    [options, ~, model] = setupFn();

    nthreads = 2;               % number of threads
    n_iter = 10;                % number of cooperative iterations
    is_parallel = true;         % parallel (true) or sequential (false)
    maxtime_per_iteration =10;  % time limit for each iteration
    par_struct = get_CeSS_options(nthreads, ...
        length(options.startValues), maxtime_per_iteration);
    
    for i = 1:nthreads
        par_struct(i).opts.maxtime = maxtime_per_iteration;
        par_struct(i).opts.maxeval = 100;
        par_struct(i).opts.local.solver = 0; % Don't use local solver

        par_struct(i).problem.f = 'parameterEstimationProblem';
        par_struct(i).problem.x_L = options.lowerBounds;
        par_struct(i).problem.x_U = options.upperBounds;
    end
    
    paramsSet = model.paramsSet;
    
    %% Run CeSS optimization
    tic
    results = CeSS(par_struct,n_iter,is_parallel, setupFunc);
    save('results.mat', 'results');
    toc
    
    %% Return a map containing the parameters
    if (ischar(paramsSet))
        P = loadParameters(paramsSet);
    else
        P = paramsSet;
    end
    
    % Insert estimated parameters
    for p=1:length(results.xbest)
        P(options.targetParams{p}) = results.xbest(p);
    end
    
    % Perform substitutions if necessary
    p = options.subsFunc(P);
    
end