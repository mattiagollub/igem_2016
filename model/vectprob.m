function vectprob = vectprob(i,j);
vect = zeros(15,2);
vect(:,1)=[1:1:15];
for k =1:15
    if k<=i && k<=j && abs(i-k)<=(15-k) && abs(j-k)<=15-i
        vect(k,2)= nchoosek(15,k)*nchoosek(15-k,abs(i-k))*nchoosek(15-i,abs(j-k))/(nchoosek(30,i+j));
    else
        vect(k,2)=0;
    end
end
vectprob=vect;
end
