function J = parameterEstimationProblemMT(x)
% PARAMETERESTIMATIONPROBLEM Error function of the parameter estimation. 
%   Computes an error measure of a set of parameters. The error is defined
%   as the sum of squared errors of the simulated datapoints from the
%   experimental data. This function is intended to be used with the MEIGO
%   toolbox.
%
    %% Change the IntegrationTolNotMet warning into an error 
    warnstate = warning;
    warnId_ode45 = 'MATLAB:ode45:IntegrationTolNotMet';
    warnId_ode15s = 'MATLAB:ode15s:IntegrationTolNotMet';
    warning('error', warnId_ode45);
    warning('error', warnId_ode15s);
 
    model = createPtetXFPSuperDetailedModel();
    
    options.targetParams = { 'l_pTet', 'n_TetR', 'Km_TetR', 'k_mRNAxfp', ...
    'd_mRNAxfp', 'k_XFP', 'd_XFP', 'k_ON', 'k_OFF', 'TetR_tot' };
    options.subsFunc = @(P) P;
    options.lowerBounds = [0.0, 1.1, 1,    0.00001, 0.002, 0.01, 0.0025002, 0.0001, 0.0001, 2];
    options.upperBounds = [0.8, 2.9, 1000, 20,       1.4,   50.0, 1,   1000, 1000, 5000];
    options.startValues = [0.1, 1.4, 200,  0.1,     0.4,   4.0,  0.02, 1, 1, 1000];
    
    times_in = [0, 45, 100, 160, 220, 280];
    dose_0 = [0, 0, 0, 0, 0, 0, 0, 0];
    dose_2 = [0, 0, 4.32, 4.32, 4.32, 4.32, 4.32, 4.32];
    dose_20 = [0, 0, 43.2, 43.2, 43.2, 43.2, 43.2, 43.2];
    dose_2000 = [0, 0, 4320, 4320, 4320, 4320, 4320, 4320];

    times_out = [0, 45, 100, 160, 220, 280];
    xfp_0 = [0, 598, 708, 765, 796, 1030, 823];
    xfp_2 = [0, 690, 758, 840, 1045, 1052, 976];
    xfp_20 = [0, 646, 1005, 1164, 1121, 1203, 1147];
    xfp_2000 = [0, 685, 2765, 5590, 7262, 8062, 7392];

    %% Adjust growth time
    growthTime = 60*4;
    times_in = [0, growthTime-0.01, times_in+growthTime];
    times_out = [0, times_out+growthTime];

    %% Setup experimental data
    experiments.inputTimes = { ...
        containers.Map({'aTc'}, {times_in}) ...
        containers.Map({'aTc'}, {times_in}) ...
        containers.Map({'aTc'}, {times_in}) ...
    };
    experiments.inputSpecies = { ...
        containers.Map({'aTc'}, {dose_0}) ...
        containers.Map({'aTc'}, {dose_20}) ...
        containers.Map({'aTc'}, {dose_2000}) ...
    };
    experiments.outputTimes = { ...
        containers.Map({'XFP'}, {times_out}) ...    
        containers.Map({'XFP'}, {times_out}) ...
        containers.Map({'XFP'}, {times_out}) ...
    };
    experiments.outputSpecies = { ...
        containers.Map({'XFP'}, {xfp_0}) ...
        containers.Map({'XFP'}, {xfp_20}) ...
        containers.Map({'XFP'}, {xfp_2000}) ...
    };
    
    %% Insert parameters in the model
    c0 = model.c0;
    
    % Insert default parameters
    if ~isfield(model, 'paramsSet')
        P = containers.Map();
    elseif (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    
    % Insert extimated parameters
    for p=1:length(x)
        P(options.targetParams{p}) = x(p);
    end
    
    % Perform substitutions if necessary
    P = options.subsFunc(P);
    model.paramsSet = P;
    for i=1:length(model.paramNames)
        paramName = char(model.paramNames(i));
        model.c0 = subs(model.c0, model.paramNames(i), P(paramName));
    end
    model.c0 = double(model.c0);
    
    %% Iterate over all the experiments
    nExp = size(experiments.inputTimes, 2);
    J = 0;
    R = [];
    g = 0;
    
    for i=1:nExp
        
        EIT = experiments.inputTimes{i};
        EIS = experiments.inputSpecies{i};
        EOT = experiments.outputTimes{i};
        EOS = experiments.outputSpecies{i};
        
        model.timesMap = containers.Map();
        model.speciesMap = containers.Map();
        MT = model.timesMap;
        MS = model.speciesMap;
        
        %% Set inputs for the model
        inSpecies = keys(EIS);
        for s=1:length(inSpecies)
            specie = inSpecies(s);
            specie = specie{1};
            MT(specie) = EIT(specie);
            MS(specie) = EIS(specie);
        end
        
        %% Determine all required datapoints that the simulation
        reqTExp = 0;
        outSpecies = keys(EOS);
        for s=1:length(outSpecies)
            specie = outSpecies(s);
            specie = specie{1};
            reqTExp = [reqTExp, EOT(specie)];
        end
        reqTExp = unique(reqTExp);
        
        %% Run the simulation with the given parameters
        failed = false;
        try
            deterministicSimulation(reqTExp, model);
        catch ERR
            if strcmp(ERR.identifier, warnId_ode45) || ...
                strcmp(ERR.identifier, warnId_ode15s)
                failed = true;
            else
                throw(ERR);
            end
        end
        
        %% Compute least squares error w.r.t. the experimental results
        if (failed)
            % If the integration failed then we probably got very bad
            % parameters
            yExp = EOS(specie);
            J = 100000000000000;
            break;
        else
            species = keys(EOS);
            for s=1:length(species)
                specie = species(s);
                specie = specie{1};
                tExp = EOT(specie);
                yExp = EOS(specie);
                yOut = interp1(MT(specie), MS(specie), tExp);
                nPoints = size(yExp, 2);
                
                Rt = (yOut - yExp);
                
                if ~isreal(J)
                    J = 100000000000000;
                else
                    J = J + sum(Rt.^2) / nPoints;
                end
            end
        end
    end
    
    model.c0 = c0;
    display(J);
    display(x);
    
    warning(warnstate);
end