function x = linInterp(t0, t1, x0, x1, t)
% LININTERP Linear interpolation between two points.
%   Interpolate the value of t between the points (t0, x0) and (t1, x1).
%
%       x = linInterp(t0, t1, x0, x1, t)
%
%   Returns:
%       x:              The interpolated value
%
    %% Perform interpolation
    x = x0 + (x1-x0)*(t-t0)/(t1-t0);
end