function model = createDETANOMAmodel()
%CREATEREPORTERMAMODEL Defines a mass action model of the reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%       model = createReporterMAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
% 
    
%% Some variables

    Mno=30.01;%g�mol?1
    Mdeta=103.2;%g�mol?1
    Mo2= 16;%g�mol?1
    Ki=101.325*1.9e-3; %henri constant for NO 
    R=8.31446;%gaz constant
    T=331;%Temperature
    V=182e-6*1e-3;%volume of air on the reading plate (182uL -> m^3)
    
%% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 = [0,0,0,0,(40e-3)/Mo2,0];
    model.odeOptions = odeset('RelTol', 1e-10);
    model.inputs = {'AHL'};
    
    %% Define symbolic variables for species and parameters
    syms AHL DETA NOtot NO O2 NO2
    syms kox kdeta
    model.specieNames = [AHL DETA NOtot NO O2 NO2];
    model.paramNames = [kdeta kox]; 
    
    %% Stoichiometric and rate matrices
    % AHL | DETA | NOtot | NO | O2 | NO2 
    model.N = [
       0,  -1,  2,  0, 0, 0;% DETA -> NO
       0,   0,  0, -2, 0, 2; % 2NO + O2 -> 2 NO2 %O2 concentration is considered as constant because of the constant gas-vapor equilibrium
      %0,  0,  2,  1,-2;
     ]';  
    
    %% Reaction rates
    model.rates = [
        kdeta*DETA;      % DETA -> NO
        kox*(NO^2)*O2;%*60;  % 2NO + O2 -> 2 NO2
        %0.01*NO2;  % 2NO + O2 <- 2 NO
    ];

  model.specieFunctions = [
     AHL;
     DETA;
     NOtot
     (NOtot-NO2)/((Ki*V/R*T)+1);
     O2;
     NO2;
  ];
    %% Reaction propensities
    model.a = [
        kdeta*DETA*60;      % DETA -> NO
        4*kox*(NO^2)*O2*60*60;  % 2NO + O2 -> 2 NO 
        %kox*NO2;  % 2NO + O2 <- 2 NO
        
	];
    
end