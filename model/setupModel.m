function [] = setupModel(model,modeodename,modesfname, tspan,parameters)
  %% Create ODE system from the model
        nInputs = size(model.inputs, 2);
        nSpecies = length(model.specieNames);
        % Run simulation for input
       modelode=str2func(modeodename);
       modelsf=str2func(modesfname);
        [t, x] = ode15s( ...
            @(t, c) modelode(modelsf([interpInputs(model, t); c(nInputs+1:end)]')',parameters), ...
            tspan, model.c0, model.odeOptions);
        % Apply specie functions
        for j=1:size(x, 1)
            x(j,:) = modelsf([interpInputs(model, t(j))', x(j,nInputs+1:nSpecies)]);
        end
        % Write output
        for j=nInputs+1:nSpecies
            specieName = char(model.specieNames(j));
            model.timesMap(specieName)      = t;
            model.speciesMap(specieName)    = x(:,j);
        end
end

function inputs = interpInputs(model, t)
    
    % Get number of inputs
    nInputs = size(model.inputs, 2);
    inputs = zeros(nInputs, 1);
    
    % Interpolate inputs
    for i=1:nInputs
        inputName = model.inputs{i};
        inputs(i) = interp1( ...
            model.timesMap(inputName), ...
            model.speciesMap(inputName), t);
    end
end