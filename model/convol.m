function [vect] = convol (v1, v2)

m= length(v1);
n=length(v2);
l= m+n-1;
w=zeros(15,1);

for j=1:15
    for i=1:15
        v=vectprob(i,j);
        w=w+v(:,2)*v1(j)*v2(i);
    end
end
vect=w/sum(w);
sum(vect)
end