% SWITCHMAPARAMSEXAMPLE Example parameter estimation for the switch. 
%   Shows an example of a parameter estimation process. Experimental data
%   are faked by adding white noise to the resul of a simulation.
%    
%% Simulation settings
clear;

% Parameter set
params_set = 'switch_bonnet';

% Time range for the experiment
t0   = 0;
tf   = 60;

% Input: mRNAinv profile
stimulus = n2nM(15);   % Assume all promoters active
[t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.1, 0.3, stimulus, 100);
t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

%% Setup model
model = createSwitchV1MAmodel();
model.timesMap('Phyb_ON') = t_Phyb_ON;
model.speciesMap('Phyb_ON') = v_Phyb_ON;
model.paramsSet = 'switch_bonnet';

%% Run simulation for getting example data
deterministicSimulation([t0, tf], model);

T = model.timesMap;
S = model.speciesMap;

%% Interpolate data and add noise
tExp = t0:5:tf;
Pout_flipped_exp = interp1(...
    T('Pout_flipped'), S('Pout_flipped'), tExp);

noise = (rand(size(Pout_flipped_exp)) - 0.5) * 0.3 + 1;
Pout_flipped_exp = Pout_flipped_exp .* noise;

%% Setup model
model = createSwitchV1MAmodel();
model.paramsSet = 'switch_literature';

%% Setup experimental data
experiments.inputTimes = { ...
    containers.Map({'Phyb_ON'}, {t_Phyb_ON}) ...
};
experiments.inputSpecies = { ...
    containers.Map({'Phyb_ON'}, {v_Phyb_ON}) ...
};
experiments.outputTimes = { ...
    containers.Map({'Pout_flipped'}, {tExp}) ...
};
experiments.outputSpecies = { ...
    containers.Map({'Pout_flipped'}, {Pout_flipped_exp}) ...
};

%% Specify estimation options
options.targetParams = { ...
    'd_Bxb1', 'k_Bxb1', 'k_DBxb1', 'k_DBxb1_', ..._ 
    'k_attBP', 'k_attLR', 'k_flip', 'd_DBxb1', ...
    'k_mRNAinv', 'd_mRNAinv' };
options.subsFunc = @(P) [P; containers.Map( ...
    {'k_attBP_', 'k_attLR_'}, ...
    { P('k_attBP')*P('k_D_attBP'), P('k_attLR')*P('k_D_attLR')})];
options.lowerBounds = ones(1, 10) * 0.0001;
options.upperBounds = ones(1, 10) * 200;
options.startValues = ones(1, 10);

%% Estimate parameters
newParams = estimateDeterministicParams(options, experiments, model);

display('Parameter estimation completed. Result:\n');
display(newParams);

P = loadParameters('switch_literature');

% Insert extimated parameters
% for p=1:length(newParams.x)
%    P(options.targetParams{p}) = newParams.x(p);
% end
    
% Perform substitutions if necessary
% P = options.subsFunc(P);

%% Run simulation with new parameters
model.paramsSet = newParams;
model.timesMap = containers.Map();
model.speciesMap = containers.Map();
Tn = model.timesMap;
Sn = model.speciesMap;

Tn('Phyb_ON') = t_Phyb_ON;
Sn('Phyb_ON') = v_Phyb_ON;

deterministicSimulation([t0, tf], model);

%% Plot results
figure(1);
clf;

% Plot Phyb_ON input profile
subplot(3, 1, 1);
plot(T('Phyb_ON'), S('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Phyb_ON')
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Phyb_ON');

% Bxb1 dimerization
subplot(3, 1, 2);
hold on;
plot(T('Bxb1'), S('Bxb1'), 'linewidth', 2);
plot(T('DBxb1'), S('DBxb1'), 'linewidth', 2);
plot(Tn('Bxb1'), Sn('Bxb1'), 'linewidth', 2);
plot(Tn('DBxb1'), Sn('DBxb1'), 'linewidth', 2);
grid on;
title('Bxb1 dimerization');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Bxb1 (original)', 'DBxb1 (original)', ...
       'Bxb1 (inferred)', 'DBxb1 (inferred)'); 

% Integrase behavior
subplot(3, 1, 3);
hold on;
plot(T('Pout_flipped'), S('Pout_flipped'), 'linewidth', 2);
plot(T('S_0'), S('S_0'), 'linewidth', 2);
plot(T('S_1'), S('S_1'), 'linewidth', 2);
plot(T('S_2'), S('S_2'), 'linewidth', 2);
plot(Tn('Pout_flipped'), Sn('Pout_flipped'), 'linewidth', 2);
plot(Tn('S_0'), Sn('S_0'), 'linewidth', 2);
plot(Tn('S_1'), Sn('S_1'), 'linewidth', 2);
plot(Tn('S_2'), Sn('S_2'), 'linewidth', 2);
plot(tExp, Pout_flipped_exp, '+', 'linewidth', 3);
title('Integrase behavior');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Pout^{flipped} (original)', 'S_0 (original)', ...
       'S_1 (original)', 'S_2 (original)', 'Pout^{flipped} (inferred)', ...
       'S_0 (inferred)', 'S_1 (inferred)', 'S_2 (inferred)', ...
       'Pout^{flipped}  (perturbated)' ); 