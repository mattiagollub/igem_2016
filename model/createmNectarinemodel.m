function model = createsfGFPMAmodel()
%CREATEGFPMAMODEL Create a mass action of model for the sfGFP reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
    %% Common constants
    nP = 15;            % Number of plasmids
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();
    
    %% Input, initial condition and intgrator options
    model.c0 = [0,0,0];
    model.odeOptions = odeset('RelTol', 1e-1);
    model.inputs = {'Promoter'};
    
    %% Define symbolic variables for species and parameters
    syms P_ON mRNAsfgfp sfGFP
    syms k_mRNAsfgfp d_mRNAsfgfp k_sfGFP d_sfGFP ...
         
    model.specieNames = [P_ON mRNAsfgfp sfGFP];
    model.paramNames = [k_mRNAsfgfp d_mRNAsfgfp k_sfGFP d_sfGFP]; 
    
    %% Stoichiometric and rate matrices
    % P_ON | mRNAsfgfp | sfGFP
    model.N = [
        0,  1,  0;  % P_ON -> P_ON + mRNAsfgfp
        0, -1,  0;  % mRNAsfgfp ->
        0,  0,  1;  % mRNAsfgfp -> mRNAsfgfp + sfGFP
        0,  0, -1;  % sfGFP ->
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAsfgfp*P_ON*nP;    % P_ON -> P_ON + mRNAsfgfp
        d_mRNAsfgfp*mRNAsfgfp;  % mRNAsfgfp ->
        k_sfGFP*mRNAsfgfp;      % mRNAsfgfp -> mRNAsfgfp + sfGFP
        d_sfGFP*sfGFP;          % sfGFP ->
    ];
  
    %% Reaction propensities
    model.a = [
        k_mRNAsfgfp*P_ON*nP;    % P_ON -> P_ON + mRNAsfgfp
        d_mRNAsfgfp*mRNAsfgfp;  % mRNAsfgfp ->
        k_sfGFP*mRNAsfgfp;      % mRNAsfgfp -> mRNAsfgfp + sfGFP
        d_sfGFP*sfGFP;          % sfGFP ->
	];
end
