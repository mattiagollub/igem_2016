function [] = andGateMAmodelFittingV2()
%% AND_GATE_DATA FITTING    Example fitting parameters between the deterministic model
%   and the simplified one the and gate. 
%   Simulates the concentration of the and gate species using an example
%   input.

%%
addpath('HYPERSPACE');
addpath('HYPERSPACE/Files');
addpath('mexFunctions');

global modelAHL TAHL SAHL AHLparamMap ...
       modelNO  TNO SNO NOparamMap 
%%
globalizeModelODE();
%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
    
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 60*6;
tspan = [t0, tf];

NO_value=30;
AHL_value=30;

NO_max=1;
AHL_max=2;


AHL_range=0:5:100;
NO_range=0:5:100;
output_maxPfree=zeros(1,length(AHL_range));
output_maxPnorv3=zeros(1,length(NO_range));

%time=1:100:tf;
figure(1);
clf;

for i=1:length(AHL_range)
        % Input: NO and AHL profile
        points = 100;
        
        AHL_stimulus = AHL_range(i) * 1e9 / (Na * V);
        t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
        t_AHL = (0:points)/points*(tf-t0) + t0;       
        AHL = zeros(size(t_AHL));       
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Setup model
        TAHL('AHL') = t_AHL;
        SAHL('AHL') = AHL;

        modelAHL.paramsSet = AHLparamMap;

        parametersAHL=changeParamInModel(modelAHL,AHLparamMap);
        
        
        % Create ODE system from the model
        setupModel(modelAHL,'AHLode','AHLsf',tspan,parametersAHL);
        
        
        %%output
        figure(1);
        subplot(2, 2, 1);
        hold on
        plot(modelAHL.speciesMap('Pfree'));
        title('Pfree expressed in nM for a range of AHL');
        output_maxPfree(i)=max(modelAHL.speciesMap('Pfree'));
       
  
end
%% Plot results

% mRNAinv input profile
figure(1);
hold on
subplot(2, 2, 2);
xdata = AHL_range;
ydata = output_maxPfree;
fun = @(x,xdata)(24.95.*x(1).*xdata.^x(2))./(x(3)+xdata.^x(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.5 2 0.1];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x = lsqcurvefit(fun,x0,xdata,ydata,lb,ub,options)
times = linspace(xdata(1),xdata(end));
%plotting
semilogx(xdata,ydata,'ko',times,fun(x,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pfree')
drawnow

for j=1:length(NO_range)
        % Input: NO and AHL profile
        points = 100;
        NO_stimulus = NO_range(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs      
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.5/tf*points);        
        t_NO = (0:points)/points*(tf-t0) + t0;      
        NO = zeros(size(t_NO));      
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
       

        %% Setup model
        TNO('NO') = t_NO;
        SNO('NO') = NO;

        modelNO.paramsSet = NOparamMap;

        parametersNO=changeParamInModel(modelNO,NOparamMap);
        
        setupModel(modelNO,'NOode','NOsf',tspan,parametersNO);
        
        %% output
        figure(1);
        subplot(2, 2, 3);
        hold on
        plot(modelNO.speciesMap('PnorV3'));
        title('PnorV3 expressed in nM for a range of NO');
        colormap hot
        output_maxPnorv3(j)= max(modelNO.speciesMap('PnorV3'));
        
end
figure(1);
hold on
subplot(2, 2, 4);
xdata1 = NO_range;
ydata = output_maxPnorv3;
fun = @(x1,xdata1)(24.95.*x1(1).*xdata1.^x1(2))./(x1(3)+xdata1.^x1(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.1 2 0.001];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x1 = lsqcurvefit(fun,x0,xdata1,ydata,lb,ub,options)
times = linspace(xdata(1),xdata1(end));
%plotting
semilogx(xdata1,ydata,'ko',times,fun(x1,times),'b-')
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pnor3')
drawnow;
end


