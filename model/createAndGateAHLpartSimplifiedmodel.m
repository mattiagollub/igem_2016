function model = createAndGateAHLpartSimplifiedmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    %Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'AHL'};
    
    %% Define symbolic variables for species and parameters
    syms AHL Esar Pfree Pout mRNAinv
    syms Ptot Kahl nahl Kmahl k5 ...
         kmrna kl kesarProd desar dmrna
    
    model.specieNames = [AHL Esar Pfree Pout mRNAinv];
    model.paramNames = [Ptot Kahl nahl Kmahl k5...
                        kmrna kl kesarProd desar dmrna]; 
    
    %% Stoichiometric and rate matrices
    % AHL | Esar |Pfree| Pout | mRNAinv | 
    model.N = [       
     0,    0,  0,  0,  1;  % Pfree-> mRNAinv      
     0,    1,  0,  0,  0;     % -> EsaR     
     0,   -1,  0,  0,  0;    % EsaR ->
     0,    0,  0,  0, -1;    % mRNAinv ->      
     ]';
    
    %% Reaction rates
    model.rates = [
           (kmrna*kl)+(kmrna*(1-kl)*Pfree); %Pfree -> mrna inv
           (kesarProd*kl)+(kesarProd*(1-kl)*Ptot);  % -> Esar
            Esar*(k5*(Esar-1)+desar);  % EsaR ->
            dmrna*mRNAinv;          % mRNAinv ->
    ];


     model.specieFunctions = [
        AHL;
        Esar;
        Ptot*Kahl*((AHL)^nahl)/(Kmahl + (AHL)^nahl); 
        Ptot*Kahl*((AHL)^nahl)/(Kmahl + (AHL)^nahl); 
        mRNAinv;
        ];
    
    %% Reaction propensities
    model.a = [
           (kmrna*kl); %Pfree -> mrna inv
           (kesarProd*kl)+(kesarProd*(1-kl)*Ptot) ;  % -> Esar
            desar*Esar ;                 % EsaR ->
            dmrna*mRNAinv;          % mRNAinv ->
	];
end