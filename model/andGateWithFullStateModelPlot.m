% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input.
% 
%% Simulation settings
clear;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 1.5;
nRuns=1;
% Input: NO activity
% points = 100;
%     NO_stimulus = 50 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_NO_ON = ceil((tf-t0)*0.1/tf*points);
%     t_NO_OFF = floor((tf-t0)*0.5/tf*points);
%     t_NO = (0:points)/points*(tf-t0) + t0;
%     NO = zeros(size(t_NO));
%     NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
% Input: AHL activity
%     AHL_stimulus = 50 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
%     t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
%     t_AHL = (0:points)/points*(tf-t0) + t0;
%     AHL = zeros(size(t_AHL));
%     AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;



%Input : AHL Activity
    stimulus = 100;   
    [t_NO, NO] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_NO = t_NO * (tf - t0) + t0;
% Input : AHL Activity
    stimulus = 100;   
    [t_AHL, AHL] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_AHL = t_AHL * (tf - t0) + t0;
%% Setup model
model = createAndGateMAfullmodel();
model.paramsSet = 'andgate_eth_igem_2014';





T = model.timesMap;
S = model.speciesMap;

T('NO') = t_NO;
S('NO') = NO;

T('AHL') = t_AHL;
S('AHL') = AHL;


%% Run simulation
deterministicSimulation([t0, tf], model);
%stochasticSimulationNRM(tspan, model, nRuns);
%%% Plot results
% mRNAinv input profile
    figure;
    clf;
    
    subplot(2, 2, 1);
    stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
    hold on
    stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: NO, AHL')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NO','AHL');

    
    drawFunctions = containers.Map( ...
        {'NoR', 'DNoR_NO', 'NoR_NO', 'DNoR', 'DEsaR', 'DEsar_AHL', 'PnorV0_Pfree', 'PnorV0_Pesar', 'PnorV3_Pesar', 'PnorV3_Pfree'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 5, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 4, 1, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 3, lineWidth, 0.2), ...
            @(t, x) drawWithErrorBarInSubplot(t, x, 4, 4, lineWidth, 0.2), ...
        });
 %% Plot NOR system
    ax = subplot(2, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(T('NoR'), S('NoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(T('DNoR_NO'), S('DNoR_NO'), 'linewidth', lineWidth);
    hold on
    p = plot(T('NoR_NO'), S('NoR_NO'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DNoR'), S('DNoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('NOR system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NoR', 'DNoR_NO','NoR_NO','DNoR');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    p = plot(T('EsaR'), S('EsaR'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DEsaR'), S('DEsaR'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DEsar_AHL'), S('DEsar_AHL'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('EsaR','DEsaR', 'DEsar_AHL');
    
    %% Plot DEsaR behavior
    ax = subplot(2, 2, 4);
    hold on;
    ax.ColorOrderIndex = 2;
    p = plot(T('PnorV0_Pfree'), S('PnorV0_Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV0_Pesar'), S('PnorV0_Pesar'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV3_Pesar'), S('PnorV3_Pesar'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV3_Pfree'), S('PnorV3_Pfree'), 'linewidth', lineWidth);
    hold on
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('PnorV_Pfree behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0_Pfree','PnorV0_Pesar', 'PnorV3_Pesar','PnorV3_Pfree');
    drawnow;
    
 figure;
    clf;
    histsPnorV3_Pfree = model.histogramsMap('PnorV3_Pfree');
    PnorV3_PfreeDist = fitdist(histsPnorV3_Pfree(end, :)','Normal');
    histogram(histsPnorV3_Pfree(end, :));
    hold on
    x_values = 0:1:30;
    y = pdf(PnorV3_PfreeDist,x_values);
    f = fit(x_values.',500*y','gauss1');
    plot(f,x_values,500*y)
    title(sprintf('Phyb distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');
    
figure;
histsPnorV3_Pfree = model.histogramsMap('PnorV3_Pfree');
y=std(histsPnorV3_Pfree');
%errorbar(T('PnorV3_Pfree'), S('PnorV3_Pfree'), 'linewidth', lineWidth);
%plot(T('PnorV3_Pfree'), S('PnorV3_Pfree'), 'linewidth', lineWidth);
hold on
tnew=1:10:length(T('PnorV3_Pfree'));
snew=S('PnorV3_Pfree');
s=snew(tnew);
ynew=y(tnew);
%plot(T('PnorV3_Pfree'), S('PnorV3_Pfree'), 'linewidth', lineWidth);
errorbar(tnew,s,ynew);
%errorbar(T('PnorV3_Pfree'), S('PnorV3_Pfree'),y);
%plot(T('PnorV3_Pfree'), S('PnorV3_Pfree'));
title(sprintf('mean(Phyb) behavior for the full state model and gate, input NO = 100nM, AHL = 100 nM'));
xlabel('Time [min]');
ylabel('Concentration [nMol]');
    %end
figure;
histsPnorV3_Pfree = model.histogramsMap('PnorV3_Pfree');
stdDev=zeros(500,1);
for i=1:nRuns
    stdDev(i)=nM2n(std(histsPnorV3_Pfree(:,i)));
end
histogram(stdDev,100);
stdDec_Dist = fitdist(stdDev,'Normal');
hold on
x_values = 1:0.01:1.4;
y = pdf(stdDec_Dist,x_values);
%y=y/sum(y);
%plot(y);
f = fit(x_values.',y','gauss1');
plot(f,x_values,y)
title(sprintf('Phyb standart deviation distribution at t=%d min', tspan(end)));
xlabel('Number of molecules');
ylabel('Number of cells');

figure;
histsPnorV3_Pfree = model.histogramsMap('PnorV3_Pfree');
stdDev=zeros(500,1);
for i=1:nRuns
    stdDev(i)=nM2n(std(histsPnorV3_Pfree(:,i)));
end
moyenne=zeros(500,1);
for i=1:nRuns
    moyenne(i)=nM2n(mean(histsPnorV3_Pfree(:,i)));
end
plot(stdDev,moyenne);
title('does the standart deviatuib depends on the mean?');
% function drawWithErrorBarInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = errorbar(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end

% function drawInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = plot(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end