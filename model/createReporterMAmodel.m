function model = createReporterMAmodel()
%CREATEREPORTERMAMODEL Defines a mass action model of the reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%       model = createReporterMAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
% 
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0, 0, 0, 0, 0, 0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'Pout_flipped', 'Pout_free'};
    
    %% Define symbolic variables for species and parameters
    syms Pout_flipped Pout_free mRNAgfp GFP Pgfp_ON ...
         mRNAmnect mNect Pmnect_ON
    syms nP k_mRNAgfp d_mRNAgfp k_GFP d_GFP ...
         k_mRNAmnect d_mRNAmnect k_mNect d_mNect l_Pout
    
    model.specieNames = [Pout_flipped Pout_free mRNAgfp GFP ...
                         mRNAmnect mNect];
    model.paramNames = [nP k_mRNAgfp d_mRNAgfp k_GFP d_GFP ...
                        k_mRNAmnect d_mRNAmnect k_mNect d_mNect l_Pout]; 
    
    %% Stoichiometric and rate matrices
    % Pout_flipped | Pout_free | mRNAgfp | GFP | mRNAmnect | mNect
    model.N = [
        0,  0,  1,  0,  0,  0; % Pgfp_ON -> Pgfp_ON + mRNAgfp
        0,  0,  0,  0,  1,  0; % Pmnect_ON -> Pmnect_ON + mRNAmnect
        0,  0, -1,  0,  0,  0; % mRNAgfp ->
        0,  0,  0,  0, -1,  0; % mRNAmnect ->
        0,  0,  0,  1,  0,  0; % mRNAgfp -> mRNAgfp + GFP
        0,  0,  0,  0,  0,  1; % mRNAnect -> mRNAmnevt + mNect
        0,  0,  0, -1,  0,  0; % GFP ->
        0,  0,  0,  0,  0, -1; % mNect ->
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAgfp*Pgfp_ON;      % Pgfp_ON -> Pgfp_ON + mRNAgfp
        k_mRNAmnect*Pmnect_ON;  % Pmnect_ON -> Pmnect_ON + mRNAmnect
        d_mRNAgfp*mRNAgfp;      % mRNAgfp ->
        d_mRNAmnect*mRNAmnect;  % mRNAmnect ->
        k_GFP*mRNAgfp;          % mRNAgfp -> mRNAgfp + GFP
        k_mNect*mRNAmnect;      % mRNAmnect -> mRNAmnect + mNect
        d_GFP*GFP;              % GFP ->
        d_mNect*mNect;          % mNect ->
    ];

    model.rates = subs(model.rates, [Pgfp_ON, Pmnect_ON], ...
        [(l_Pout + (1-l_Pout)*Pout_flipped*Pout_free), ...
         (l_Pout + (1-l_Pout)*(n2nM(nP) - Pout_flipped)*Pout_free)]);
  
    %% Reaction propensities
    model.a = [
        k_mRNAgfp*Pgfp_ON;      % Pgfp_ON -> Pgfp_ON + mRNAgfp
        k_mRNAmnect*Pmnect_ON;  % Pmnect_ON -> Pmnect_ON + mRNAmnect
        d_mRNAgfp*mRNAgfp;      % mRNAgfp ->
        d_mRNAmnect*mRNAmnect;  % mRNAmnect ->
        k_GFP*mRNAgfp;          % mRNAgfp -> mRNAgfp + GFP
        k_mNect*mRNAmnect;      % mRNAmnect -> mRNAmnect + mNect
        d_GFP*GFP;              % GFP ->
        d_mNect*mNect;          % mNect ->
	];
    model.a = subs(model.a, [Pgfp_ON, Pmnect_ON], ...
        [(l_Pout + (1-l_Pout)*Pout_flipped*Pout_free), ...
         (l_Pout + (1-l_Pout)*(nP - Pout_flipped)*Pout_free)]);
end