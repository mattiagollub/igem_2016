function deterministicSimulation(tspan, model, computeSensitivities)
% DETERMINISTICSIMULATION Deterministically simulate a system
%   Perform a deterministic ODE simulation of the given model. If
%   required sensitivities will be computed as well. The results of the
%   simulation (concentrations and sensitivities) are saved in 
%   model.speciesMap and model.timesMap.
%
%       deterministicSimulation(tspan, model, computeSensitivities)
%
%   Required:
%       tspan:          Initial and final times, [t_initial, t_final]. In
%                       case length(tspan) > 2 the simulation will compute
%                       the concentrations exactly at the specified
%                       timestamps.
%       model:          MATLAB structure containing the definition of the
%                       model.
%
%	Optional:
%		computeSensitivities:
%                       If true the function will compute sensitivities as
%                       well and return them in model.speciesMap
%
    %% Check if sensitivities must be computed
    if ~exist('computeSensitivities','var')
        computeSensitivities = false;
    end

    %% Define helper functions
    mat2vec = @(mx) reshape(mx', numel(mx'), 1);
    vec2mat = @(vec, rows) reshape(vec, rows, numel(vec)/rows)';
    
    %% Create ODE system from the model
    dxdt = createODEfunc(model);
    sf = createSpecieFunctions(model);
    nInputs = size(model.inputs, 2);
    nParams = length(model.paramNames);
    nSpecies = length(model.specieNames);
    
    %% Create sensitivity ODE function for the model
    if computeSensitivities
        dcdtp = createSensitivityFunc(model);
    end
 
    %% Run simulation
    if computeSensitivities
        s0 = zeros(1, nSpecies * nParams);
        x0 = [model.c0, s0];
        [t, x] = ode15s( ...
            @(t, c) [
                dxdt(sf([interpInputs(model, t); c(nInputs+1:nSpecies)]')'); ...
                mat2vec(dcdtp( ...
                    [interpInputs(model, t); c(nInputs+1:nSpecies)]', ...
                    vec2mat(c(nSpecies+1:end), nParams))) ...
            ], tspan, x0, model.odeOptions);
    else
        [t, x] = ode15s( ...
            @(t, c) dxdt(sf([interpInputs(model, t); c(nInputs+1:end)]')'), ...
            tspan, model.c0, model.odeOptions);
    end
    
    %% Apply specie functions
    for i=1:size(x, 1)
        x(i,1:nSpecies) = sf([interpInputs(model, t(i))', x(i,nInputs+1:nSpecies)]);
    end
    
    %% Write output
    for i=nInputs+1:nSpecies
        specieName = char(model.specieNames(i));
        model.timesMap(specieName)      = t;
        model.speciesMap(specieName)    = x(:,i);
    end
    
    %% Write sensitivities if required
    if computeSensitivities
        for s=nInputs+1:nSpecies
            for p=1:nParams
                name = strcat('S_', char(model.specieNames(s)), ...
                              '_',  char(model.paramNames(p)));
                model.timesMap(name)    = t;
                model.speciesMap(name)  = ...
                    x(:, nSpecies + (s-1)*nParams + p);
            end
        end
    end
end 

function inputs = interpInputs(model, t)
    
    % Get number of inputs
    nInputs = size(model.inputs, 2);
    inputs = zeros(nInputs, 1);
    
    % Interpolate inputs
    for i=1:nInputs
        inputName = model.inputs{i};
        inputs(i) = interp1( ...
            model.timesMap(inputName), ...
            model.speciesMap(inputName), t);
    end
end
    