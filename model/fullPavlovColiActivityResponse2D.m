function fullPavlovColiActivityResponse2D()
% SWITCHACTIVITYRESPONSE Response of the switch to inputs. 
%   Simulates the concentration of the reporters with different activity
%   levels of the hybrid promoter.
%   
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 1000;
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);
    V = 1e-15;              % Cell volume in �m
    Na = 6.022140857e23;    % Avogadro constant
    kl=0.1;             %Estimated leakiness of the hybrid promoter

    
    % Dose inputs
    NO_levels = n2nM(0:10:100);
    NO_durations = 0:10:120;
    
    AHL_levels = n2nM(0:10:100);
    AHL_durations = 0:10:120;
    
    % Template input 1: Phyb activity
    NO_states = [
        0.0, 0.0, 1.0, 1.0, 0.0,  0.0;
        0,   9.8, 9.9, 10,  10.1, 160.0];

    % Template input 2: Pout activity
    AHL_states = [
         0.0, 0.0, 1.0, 1.0, 0.0,  0.0;
        0,   9.8, 9.9, 10,  10.1, 160.0];
    
    %% Setup model
    modelAHL = createAndGateAHLpartMAmodel();
    modelAHL.paramsSet = 'andgate_eth_igem_2014';
    modelNO = createAndGateNOpartMAmodel();
    modelNO.paramsSet = 'andgate_eth_igem_2014';
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], ...
        switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;
    
    TAHL = modelAHL.timesMap;
    SAHL = modelAHL.speciesMap;
    
    TNO = modelNO.timesMap;
    SNO = modelNO.speciesMap;
    %% Setup storage for the results
    meansGFP = zeros(length(NO_levels), length(NO_durations));
    meansmNect = zeros(length(NO_levels), length(NO_durations));
    stdsGFP = zeros(length(NO_levels), length(NO_durations));
    stdsmNect = zeros(length(NO_levels), length(NO_durations));
    
    %% Run simulation with different activity levels
    for l=1:length(NO_levels)
        for t=1:length(NO_durations)
            
            %% Setup inputs
            v_NO_ON = NO_states(1,:) * NO_durations(l);
            t_NO_ON = [NO_states(2,1:3), ...
                NO_states(2,4:end) + NO_durations(t)];
            v_AHL = AHL_states(1,:) * n2nM(100);
            t_AHL = [NO_states(2,1), ...
                AHL_states(2,2:end) + NO_durations(t)];
    
            TNO('NO') = t_NO_ON;
            SNO('NO') = v_NO_ON;
            TAHL('AHL') = t_AHL;
            SAHL('AHL') = v_AHL;

            %% Simulate system
            tspan = (0:0.01:1) * t_AHL(end);
            
            % Run simulation
            deterministicSimulation([0,tspan(end)], modelNO);
            deterministicSimulation([0,tspan(end)], modelAHL);
            
            %Compute Phyb_ON
            a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
            b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
            
            c=abs((kl+(1-kl)*(a.*b)/Ptot^2));
            T('Phyb_ON') = TNO('NO');
            S('Phyb_ON') = c;
            T('Pout_free') = TAHL('Pout');
            S('Pout_free') = SAHL('Pout')/Ptot;
            
            
            stochasticSimulationTDI(tspan, model, nRuns, containers.Map(), l*t <= 1);
            
            %% Extract statistics from the simulation
            histGFP = model.histogramsMap('GFP');
            histmNect = model.histogramsMap('mNect');
            meanGFP = mean(histGFP, 2);
            stdGFP = std(histGFP, 0, 2);
            meanmNect = mean(histmNect, 2);
            stdmNect = std(histmNect, 0, 2);
            
            meansGFP(l, t) = meanGFP(end);
            stdsGFP(l, t) = stdGFP(end);
            meansmNect(l, t) = meanmNect(end);
            stdsmNect(l, t) = stdmNect(end);
            
            %% Plot temporary result
            figure(1);
            clf;
            ax = subplot(3, 1, 1);
            hold on;
            ax.ColorOrderIndex = 5;
            errorbar(tspan, meanGFP, stdGFP);
            ax.ColorOrderIndex = 2;
            errorbar(tspan, meanmNect, stdmNect);
            grid on;
            title(sprintf( ...
                'Reporter proteins expression (Phyb_{ON} = %d, duration = %d min)', ...
                NO_levels(l), ...
                NO_durations(t)));
            xlabel('Time [min]');
            ylabel('Number of molecules');
            legend('GFP', 'mNect');
           
            subplot(3, 1, 2);
            histsGFP = model.histogramsMap('GFP');
            histogram(histsGFP(end, :), 40);
            title(sprintf('GFP distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');

            subplot(3, 1, 3);
            histsmNect = model.histogramsMap('mNect');
            histogram(histsmNect(end, :), 40);
            title(sprintf('mNectarine distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');
            
            %% Plot heatmaps
            figure(2);
            clf;
            colormap('hot');
            
            xticklabels = NO_durations;
            xticks = linspace(1, length(NO_durations), numel(NO_durations));
            yticklabels = nM2n(NO_levels);
            yticks = linspace(1, length(NO_levels), numel(NO_levels));
            
            ax = subplot(2, 2, 1);
            imagesc(flipud(meansGFP));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('GFP mean number of molecules');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 2);
            imagesc(flipud(meansmNect));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('mNectarine mean number of molecules');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 3);
            imagesc(flipud(stdsGFP));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('GFP standard deviation');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 4);
            imagesc(flipud(stdsmNect));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('mNectarine standard deviation');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
          
            drawnow;
        end
    end
end