function model = createPtetXFPSuperDetailedModel()
%CREATEXFPMAMODEL Create a mass action of model for the a fluorescent protein reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
    %% Common constants
    nP = 15;            % Number of plasmids
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();
    
    %% Define symbolic variables for species and parameters
    syms aTc mRNAxfp XFP TetR ITetR
    syms l_pTet n_TetR Km_TetR k_mRNAxfp d_mRNAxfp k_XFP d_XFP k_ON k_OFF TetR_tot
  
    model.specieNames = [aTc mRNAxfp XFP TetR ITetR];
    model.paramNames = [l_pTet n_TetR Km_TetR k_mRNAxfp d_mRNAxfp k_XFP ...
        d_XFP k_ON k_OFF TetR_tot]; 
    
    %% Input, initial condition and intgrator options
    model.c0 = [0,0,0,TetR_tot,0];
    model.odeOptions = odeset('RelTol', 1e-2);
    model.inputs = {'aTc'};

% 2h30
%   0.0247846
% 	1.51881
% 	2.57922
% 	0.168804
% 	0.182427
% 	25.9686
% 	0.00995047
% 	2.25771
% 	5.82923
% 	561.645

% 4h
%   0.00204625
% 	1.6312
% 	10.1515
% 	2.26198
% 	0.0124721
% 	3.9226
% 	0.0283264
% 	7.91305
% 	20.0706
% 	821.245
    
    %% Stoichiometric and rate matrices
    % aTc | mRNAxfp | XFP | TetR | ITetR
    model.N = [
        0,  1,  0,  0,  0;  % P_ON -> P_ON + mRNAxfp
        0, -1,  0,  0,  0;  % mRNAxfp ->
        0,  0,  1,  0,  0;  % mRNAxfp -> mRNAxfp + XFP
        0,  0, -1,  0,  0;  % XFP ->
        0,  0,  0, -1,  1;  % TetR -> ITetR
        0,  0,  0,  1, -1;  % ITetR -> TetR
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAxfp * (l_pTet + (1-l_pTet) / (Km_TetR^n_TetR + TetR^n_TetR))*nP;  % P_ON -> P_ON + mRNAxfp
        d_mRNAxfp*mRNAxfp;          % mRNAxfp ->
        k_XFP*mRNAxfp;              % mRNAxfp -> mRNAxfp + XFP
        d_XFP*XFP;                  % XFP ->
        k_ON*TetR*aTc;              % TetR + aTc -> ITetR
        k_OFF*ITetR;                % ITetR -> TetR + aTc
    ];

    %% Reaction propensities
    model.a = [ % TODO: fix constants
        k_mRNAxfp * (l_pTet + (1-l_pTet) / (Km_TetR^n_TetR + (TetR_tot-ITetR)^n_TetR))*nP;  % P_ON -> P_ON + mRNAxfp
        d_mRNAxfp*mRNAxfp;                  % mRNAxfp ->
        k_XFP*mRNAxfp;                      % mRNAxfp -> mRNAxfp + XFP
        d_XFP*XFP;                          % XFP ->
        n2nM(k_ON)*(TetR_tot-ITetR)*aTc;    % TetR + aTc -> ITetR
        k_OFF*ITetR;                        % ITetR -> TetR + aTc
	];
end
