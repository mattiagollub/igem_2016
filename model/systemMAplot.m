% SYSTEMMAPLOT Example simulation of the whole system. 
%   Deterministically simulates the concentration of the system species
%   using an example input.
%

%% Simulation settings
clear;
addpath('completeSystemModel');

% Time range for the experiment
t0   = 0;
tf   = 48 * 60;

% Input: NO and AHL
NO_stimulus = n2nM(100);
AHL_stimulus = n2nM(100);
[t_NO, v_NO] = makeStimulusProfile( ...
    1/48, 3/48, NO_stimulus, 1000);
[t_AHL, v_AHL] = makeStimulusProfile( ...
    [2/48, 36/48], [4/48, 38/48], ...
    [AHL_stimulus, AHL_stimulus], 1000);
t_NO = t_NO * (tf - t0) + t0;
t_AHL = t_AHL * (tf - t0) + t0;

%% Setup model
timesMap = containers.Map();
timesMap('NO') = t_NO;
timesMap('AHL') = t_AHL;

speciesMap = containers.Map();
speciesMap('NO') = v_NO;
speciesMap('AHL') = v_AHL;

switchModel = createSwitchV2MAmodel();
outputModel = createOutputMAmodel();
TS = switchModel.timesMap;
SS = switchModel.speciesMap;
TO = outputModel.timesMap;
SO = outputModel.speciesMap;

inputParamsSet = 'andgate_eth_igem_2014';
switchModel.paramsSet = 'switch_bonnet';
outputModel.paramsSet = 'output_eth_igem_2014';

%% Run simulations

% Simulate input
andGateMA([t0, tf], timesMap, speciesMap, inputParamsSet);

% Simulate switch
SS('mRNAinv') = speciesMap('mRNAinv');
TS('mRNAinv') = timesMap('mRNAinv');
deterministicSimulation([t0, tf], switchModel);

% Simulate output
TO('Pout_free') = timesMap('Pout');
SO('Pout_free') = speciesMap('Pout');
TO('Pout_flipped') = TS('Pout_flipped');
SO('Pout_flipped') = SS('Pout_flipped');
deterministicSimulation([t0, tf], outputModel);

%% Plot results
figure(1);
clf;

% NO and AHL inputs profile
subplot(2, 2, 1);
hold on
plot(timesMap('NO')/60, speciesMap('NO'), 'linewidth', 2);
plot(timesMap('AHL')/60, speciesMap('AHL'), 'linewidth', 2);
grid on;
title('Input: NO and AHL')
xlabel('Time [h]');
ylabel('Concentration [nMol]');
legend('NO', 'AHL');

% nRNAinv production
ax = subplot(2, 2, 3);
ax.ColorOrderIndex = 3;
hold on;
plot(TS('mRNAinv')/60, SS('mRNAinv'), 'linewidth', 2);
grid on;
title('Integrase mRNA');
xlabel('Time [h]');
ylabel('Concentration [nM]');
legend('nRNAinv'); 

% Pout states
ax = subplot(2, 2, 2);
ax.ColorOrderIndex = 7;
hold on;
plot(TS('Pout_flipped')/60, SS('Pout_flipped'), 'linewidth', 2);
plot(timesMap('Pout')/60, speciesMap('Pout'), 'linewidth', 2);
grid on;
title('Pout states');
xlabel('Time [h]');
ylabel('Concentration [nM]');
legend('Pout^{flipped}', 'Pout^{ON}');

% GFP expression
ax = subplot(2, 2, 4);
ax.ColorOrderIndex = 5;
hold on;
plot(TO('GFP')/60, SO('GFP'), 'linewidth', 2);
plot(TO('mRNAgfp')/60, SO('mRNAgfp'), 'linewidth', 2);
grid on;
title('GFP expression');
xlabel('Time [h]');
ylabel('Concentration [nMol]');
legend('GFP', 'mRNAgfp'); 