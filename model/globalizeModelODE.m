function [] = globalizeModelODE()
    clear;
addpath('HYPERSPACE');
addpath('HYPERSPACE/Files');
addpath('mexFunctions');

%%

global modelAHL TAHL SAHL AHLparamMap ...
       modelNO  TNO SNO NOparamMap ...
       modelSwitch Tswitch Sswitch SwitchparamMap...
       modelOutput TOutput SOutput OutputparamMap
 
tic;
%% Generate step functions
 modelAHL = createAndGateAHLpartMAmodel();
 TAHL = modelAHL.timesMap;
 SAHL = modelAHL.speciesMap;
 AHLparamMap=loadParameters('andgate_eth_igem_2014');
 
 modelNO = createAndGateNOpartMAmodel();
 TNO = modelNO.timesMap;
 SNO = modelNO.speciesMap;
 NOparamMap=loadParameters('andgate_eth_igem_2014');
 
 modelSwitch = createSwitchV1MAmodel();
 Tswitch = modelSwitch.timesMap;
 Sswitch = modelSwitch.speciesMap;
 SwitchparamMap = loadParameters('switch_bonnet');
 
 modelOutput = createReporterMAmodel();
 TOutput = modelOutput.timesMap;
 SOutput = modelOutput.speciesMap;
 OutputparamMap = loadParameters('reporter_literature');
 
     %% Compute expressions for the ODEs
    dxdt = modelAHL.N * modelAHL.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/c', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
    
    dxdt = modelNO.N * modelNO.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/NOode', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
    
    dxdt = modelSwitch.N * modelSwitch.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/Switchode', ...
        'Vars', { modelSwitch.specieNames, modelSwitch.paramNames });
    
    dxdt = modelOutput.N * modelOutput.rates;
    matlabFunction(dxdt, 'File', 'HYPERSPACE/Outputode', ...
        'Vars', { modelOutput.specieNames, modelOutput.paramNames });

     %% Insert parameters
    if isfield(modelAHL, 'specieFunctions')
        sf = modelAHL.specieFunctions;
    else
        sf = modelAHL.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/AHLsf', ...
        'Vars', { modelAHL.specieNames, modelAHL.paramNames });
   %
   %
    if isfield(modelNO, 'specieFunctions')
        sf = modelNO.specieFunctions;
    else
        sf = modelNO.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/NOsf', ...
        'Vars', { modelNO.specieNames, modelNO.paramNames });
   %
   %
    if isfield(modelSwitch, 'specieFunctions')
        sf = modelSwitch.specieFunctions;
    else
        sf = modelSwitch.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/Switchsf', ...
        'Vars', { modelSwitch.specieNames, modelSwitch.paramNames });
   %
   %
    if isfield(modelOutput, 'specieFunctions')
        sf = modelOutput.specieFunctions;
    else
        sf = modelOutput.specieNames';
    end
    matlabFunction(sf, 'File', 'HYPERSPACE/Outputsf', ...
        'Vars', { modelOutput.specieNames, modelOutput.paramNames });
    
 toc  
end