function h = createSpecieFunctions(model)
%CRAETESPECIEFUNCTIONS Creates a function handle for the given model.
%   Builds a function handle that allows to compute the concentration of a
%   specie as function of the current state of the system.
%
%       h = createSpecieFunctions(model)
%
%   Returns:
%       h:              function handle in the form h = @(c) sf(c)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%    
    %% Load parameters
    nParams = size(model.paramNames, 2);
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    %% Insert parameters
    if isfield(model, 'specieFunctions')
        sf = subs(model.specieFunctions, model.paramNames, parameters);
    else
        sf = model.specieNames';
    end
    
    %% Create matlab function object for the specie functions
    h = matlabFunction(sf, 'Vars', { model.specieNames });
end