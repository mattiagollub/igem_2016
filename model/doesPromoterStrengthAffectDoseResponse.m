function [] =doesPromoterStrengthAffectDoseResponse(parametersToExplore)
%   FULL SYSTEM SIMULATION
%   Deterministically simulates the concentration of the and gate species
%   using an example input and compares it withe the simplifeid model
%   result
%   input = [kesarProd, knorProd, kmRNAinv, kl, d_bxb1]
%parametersToExplore

global modelAHL TAHL SAHL AHLparamMap ...
       modelNO  TNO SNO NOparamMap ...
       modelSwitch Tswitch Sswitch SwitchparamMap...
       modelOutput TOutput SOutput OutputparamMap
   
%% Simulation settings
kesarProd           = parametersToExplore(1);
knorProd            = parametersToExplore(2);
d_bxb1              = parametersToExplore(3);
kl=0.1;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;
nP = 15;            % Number of plasmids
Ptot = n2nM(nP);% Avogadro constant

% Time range for the experiment
t0   = 0;
tf   = 60*6;
tspan = [t0, tf];

NO_value=30;
AHL_value=30;

NO_max=1;
AHL_max=2;


AHL_range=0:5:100;
NO_range=0:5:100;
resultsAHL=zeros(1,length(AHL_range));
resultsNO=zeros(1,length(NO_range));

%time=1:100:tf;
figure(1);
hold on
% Input: NO activity
for i=1:length(AHL_range)
        tic;
        points = 100;
        NO_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.2/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;

        % Input: AHL activity
        points = 100;
        AHL_stimulus = AHL_range(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_AHL_ON = ceil((tf-t0)*0.15/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.25/tf*points);
        t_AHL = (0:points)/points*(tf-t0) + t0;
        AHL = zeros(size(t_AHL));
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

        %% Setup model
        TAHL('AHL') = t_AHL;
        SAHL('AHL') = AHL;
        
        parametersAHL = changeParamInModel(modelAHL,AHLparamMap, 'kesarProd', kesarProd);

        TNO('NO') = t_NO;
        SNO('NO') = NO;
        
        parametersNO = changeParamInModel(modelNO,NOparamMap, 'knorProd', knorProd);

        setupModel(modelAHL,'AHLode','AHLsf', tspan, parametersAHL);
        
        setupModel(modelNO,'NOode','NOsf', tspan, parametersNO);

        % Run Simulation for switch
        %modelSwitch = createSwitchV1MAmo

        a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
        b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
        c=abs((kl+(1-kl)*(a.*b)/Ptot^2));
        %
        %
        Tswitch('Phyb_ON') = TNO('NO');
        Sswitch('Phyb_ON') = c;

        parametersSwitch=changeParamInModel(modelSwitch,SwitchparamMap,'d_Bxb1',d_bxb1);
  
        setupModel(modelSwitch,'Switchode','Switchsf', tspan, parametersSwitch);
        
        
        %modelOutput = createReporterMAmodel();

        TOutput('Pout_flipped') =Tswitch('Pout_flipped');
        SOutput('Pout_flipped') = Sswitch('Pout_flipped');

        TOutput('Pout_free') =TAHL('Pout');
        SOutput('Pout_free') = SAHL('Pout');

        modelOutput.paramsSet = OutputparamMap;

        parametersOutput=changeParamInModel(modelOutput,OutputparamMap);
    
        setupModel(modelOutput,'Outputode','Outputsf', tspan, parametersOutput);


        %% outout
        resultsAHL(i) = max(SOutput('GFP'));
        toc
   
end
figure(1);
    hold on
    subplot(1, 2, 1);
    xdata = AHL_range;
    ydata = resultsAHL;
    semilogx(xdata,ydata);
    
    title('Data and Fitted Curve Pfree');
    drawnow;
%%
 for i=1:length(NO_range)
        tic;
        points = 100;
        NO_stimulus = NO_range(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_NO_ON = ceil((tf-t0)*0.1/tf*points);
        t_NO_OFF = floor((tf-t0)*0.2/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;

        % Input: AHL activity
        points = 100;
        AHL_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_AHL_ON = ceil((tf-t0)*0.15/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.25/tf*points);
        t_AHL = (0:points)/points*(tf-t0) + t0;
        AHL = zeros(size(t_AHL));
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

       %% Setup model
        TAHL('AHL') = t_AHL;
        SAHL('AHL') = AHL;
        
        parametersAHL = changeParamInModel(modelAHL,AHLparamMap, 'kesarProd', kesarProd);

        TNO('NO') = t_NO;
        SNO('NO') = NO;
        
        parametersNO = changeParamInModel(modelNO,NOparamMap, 'knorProd', knorProd);

        setupModel(modelAHL,'AHLode','AHLsf', tspan, parametersAHL);
        
        setupModel(modelNO,'NOode','NOsf', tspan, parametersNO);

        % Run Simulation for switch
        %modelSwitch = createSwitchV1MAmo

        a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
        b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
        c=abs((kl+(1-kl)*(a.*b)/Ptot^2));
        %
        %
        Tswitch('Phyb_ON') = TNO('NO');
        Sswitch('Phyb_ON') = c;

        parametersSwitch=changeParamInModel(modelSwitch,SwitchparamMap,'d_Bxb1',d_bxb1);
  
        setupModel(modelSwitch,'Switchode','Switchsf', tspan, parametersSwitch);
        
        
        %modelOutput = createReporterMAmodel();

        TOutput('Pout_flipped') =Tswitch('Pout_flipped');
        SOutput('Pout_flipped') = Sswitch('Pout_flipped');

        TOutput('Pout_free') =TAHL('Pout');
        SOutput('Pout_free') = SAHL('Pout');

        modelOutput.paramsSet = OutputparamMap;

        parametersOutput=changeParamInModel(modelOutput,OutputparamMap);
    
        setupModel(modelOutput,'Outputode','Outputsf', tspan, parametersOutput);
        %% outout
        resultsNO(i) = max(SOutput('GFP'));
        toc
 end
    figure(1)
    hold on
    subplot(1, 2, 2);
    xdata1 = NO_range;
    ydata = resultsNO;
    %plotting
    semilogx(xdata1,ydata);
    title('Data and Fitted Curve Pnor3');
    drawnow;
end
