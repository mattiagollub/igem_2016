% AND gate is deterministically simulated
% Hybrid Promoter is computed by multiplying the ratio of free and
% PnorV3 promoters
% Switch and reporter are fued model and stochasttically simulated

% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*20;
tspan = t0:ts:tf;
lineWidth = 1.5;

nP = 15;            % Number of plasmids
Ptot = n2nM(nP);
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
kl=0.1;             %Estimated leakiness of the hybrid promoter

nRuns=10000;        %nomber of trajectories for the stochastic simulation

tic;
%%
% Input: NO activity
stimulus = 10000;   % Assume all promoters active
[t_NO, NO] = makeStimulusProfile(0.2, 0.5, stimulus, 100);
t_NO = t_NO * (tf - t0) + t0;

% Input: AHL activity
stimulus = 10000;   % Assume all promoters active
[t_AHL, AHL] = makeStimulusProfile([0.1, 0.8], [0.4, 1.0], [stimulus,stimulus], 100);
t_AHL = t_AHL * (tf - t0) + t0;


%% AHL Deterministic part AND gate
modelAHL = createAndGateAHLpartMAmodel();
TAHL = modelAHL.timesMap;
SAHL = modelAHL.speciesMap;

TAHL('AHL') = t_AHL;
SAHL('AHL') = AHL;
modelAHL.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(modelAHL.paramsSet);
ParamMap('kesarProd')=0.05;
ParamMap('desar')=8*0.2;

%% NO Deterministic part AND gate
modelNO = createAndGateNOpartMAmodel();
TNO = modelNO.timesMap;
SNO = modelNO.speciesMap;

TNO('NO') = t_NO;
SNO('NO') = NO;
modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(modelNO.paramsSet);
ParamMap('knorProd')=3;
ParamMap('dnor')=0.1;

%% Run simulation
deterministicSimulation([t0, tf], modelNO);
deterministicSimulation([t0, tf], modelAHL);

%Compute Phyb_ON
a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
c=abs((kl+(1-kl)*(a.*b)/Ptot^2));

%% Switch
switchModel = createSwitchV2MAmodel();
switchModel.paramsSet = 'switch_bonnet';

%% Reporter
reporterModel = createReporterMAmodel();
reporterModel.paramsSet = 'reporter_literature';

%% LinksModel
model = linkModels({'Phyb_ON', 'Pout_free'}, [], switchModel, reporterModel);
T = model.timesMap;
S = model.speciesMap;

T('Phyb_ON') = TNO('NO');
S('Phyb_ON') = c;
T('Pout_free') = TAHL('Pout');
S('Pout_free') = SAHL('Pout')/Ptot;

toc;

deterministicSimulation([t0, tf], model);

%% Plot Phyb_ON and Pout_free input profile
% set(0,'defaulttextinterpreter', 'latex');
% fontname = 'Helvetica';
% set(0,'defaulttextfontname', fontname);
% set(0,'defaultaxesfontname', fontname);
set(0,'defaulttextfontsize', 14);
set(0,'defaultaxesfontsize', 14);
figure(2);
clf;
subplot(2, 1, 1);
hold on
str_NO = sprintf('  NO');
str_AHL = sprintf('  AHL');
stairs(t_NO / 60, NO / 1000, 'Color', [66, 43, 156] ./ 255.0, ... 
    'linewidth', 4); % Stairstep graph of the input
stairs(t_AHL / 60, AHL / 1000, 'Color', [218, 87, 0] ./ 255.0, 'linewidth', 3); % Stairstep graph of the input
text(t_NO(end)/60, NO(end)/ 1000 + 0.6, str_NO);
text(t_AHL(end)/60, AHL(end)/ 1000 + 0.6, str_AHL);


grid on;
title('\bf{Inputs: NO, AHL}', 'FontSize', 18);
xlabel('Time (h)');
ylabel('Concentration (uM)');
ylim([0, 13]);
% legend('NO','AHL');

%% Plot GFP/mNect behavior
subplot(2, 1, 2);
hold on;
plot(T('GFP') ./ 60, nM2n(S('GFP')), 'Color', [10, 146, 0] ./ 255.0, 'linewidth', 3.5);
plot(T('mNect') ./ 60, nM2n(S('mNect')), 'Color', [175, 0, 0] ./ 255.0, 'linewidth', 2.5);
grid on;
title('\bf{Reporter proteins expression}', 'FontSize', 18);
xlabel('Time (h)');
ylabel('Fluorescence (a.u.)');
legend('GFP', 'mNect');
