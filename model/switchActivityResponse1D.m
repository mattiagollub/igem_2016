function switchActivityResponse1D()
% SWITCHACTIVITYRESPONSE Response of the switch to inputs. 
%   Simulates the concentration of the reporters with different activity
%   levels of the hybrid promoter.
%   
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 200;

    % Dose inputs
    Phyb_ON_levels = [0.4, 1];
    Phyb_ON_impulses = 0:20:240;
    Phyb_ON_durations = [
        Phyb_ON_impulses ./ Phyb_ON_levels(1);
        Phyb_ON_impulses ./ Phyb_ON_levels(2);];
    
    % Template input 1: Phyb activity
    Phyb_states = [
        0.0, 0.0, 1.0, 1.0, 0.0,  0.0;
        0,   9.8, 9.9, 10,  10.1, 160.0];

    % Template input 2: Pout activity
    Pout_states = [
        0.0, 0.0,   1.0,    1.0;
        0,   99.9, 100.0,  160.0];
    
    %% Setup model
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], ...
        switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;
    
    %% Setup storage for the results
    meansGFP = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    meansmNect = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    stdsGFP = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    stdsmNect = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    
    %% Run simulation with different activity levels
    for l=1:length(Phyb_ON_levels)
        for t=1:length(Phyb_ON_durations)
            
            %% Setup inputs
            v_Phyb_ON = Phyb_states(1,:) * Phyb_ON_levels(l);
            t_Phyb_ON = [Phyb_states(2,1:3), ...
                Phyb_states(2,4:end) + Phyb_ON_durations(l, t)];
            v_Pout_free = Pout_states(1,:) * n2nM(1);
            t_Pout_free = [Phyb_states(2,1), ...
                Pout_states(2,2:end) + Phyb_ON_durations(l, t)];
    
            T('Phyb_ON') = t_Phyb_ON;
            S('Phyb_ON') = v_Phyb_ON;
            T('Pout_free') = t_Pout_free;
            S('Pout_free') = v_Pout_free;

            %% Simulate system
            tspan = (0:0.01:1) * t_Pout_free(end);
            stochasticSimulationTDI( ...
                tspan, model, nRuns, containers.Map(), l*t <= 1);
            
            %% Extract statistics from the simulation
            histGFP = model.histogramsMap('GFP');
            histmNect = model.histogramsMap('mNect');
            meanGFP = mean(histGFP, 2);
            stdGFP = std(histGFP, 0, 2);
            meanmNect = mean(histmNect, 2);
            stdmNect = std(histmNect, 0, 2);
            
            meansGFP(l, t) = meanGFP(end);
            stdsGFP(l, t) = stdGFP(end);
            meansmNect(l, t) = meanmNect(end);
            stdsmNect(l, t) = stdmNect(end);
            
            %% Plot temporary result
            figure(1);
            clf;
            ax = subplot(3, 1, 1);
            hold on;
            ax.ColorOrderIndex = 5;
            errorbar(tspan, meanGFP, stdGFP);
            ax.ColorOrderIndex = 2;
            errorbar(tspan, meanmNect, stdmNect);
            grid on;
            title(sprintf( ...
                'Reporter proteins expression (Phyb_{ON} = %d, duration = %d min)', ...
                Phyb_ON_levels(l), ...
                Phyb_ON_durations(l, t)));
            xlabel('Time [min]');
            ylabel('Number of molecules');
            legend('GFP', 'mNect');
           
            subplot(3, 1, 2);
            histsGFP = model.histogramsMap('GFP');
            histogram(histsGFP(end, :), 40);
            title(sprintf('GFP distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');

            subplot(3, 1, 3);
            histsmNect = model.histogramsMap('mNect');
            histogram(histsmNect(end, :), 40);
            title(sprintf('mNectarine distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');
            
            %% Plot final result
            figure(2);
            clf;
            subplot(3, 1, 1);
            hold on;
            errorbar(Phyb_ON_impulses, ...
                meansGFP(1, :), stdsGFP(1, :));
            errorbar(Phyb_ON_impulses, ...
                meansGFP(2, :), stdsGFP(2, :));
            grid on;
            title('GFP expression');
            xlabel('Impulse [activity*min]');
            ylabel('Number of molecules');
            legend(sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(1)), ...
                sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(2)));
           
            subplot(3, 1, 2);
            hold on;
            errorbar(Phyb_ON_impulses, ...
                meansmNect(1, :), stdsmNect(1, :));
            errorbar(Phyb_ON_impulses, ...
                meansmNect(2, :), stdsmNect(2, :));
            grid on;
            title('mNectarine expression');
            xlabel('Impulse [activity*min]');
            ylabel('Number of molecules');
            legend(sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(1)), ...
                sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(2)));

            subplot(3, 1, 3);
            hold on;
            mr1 = meansmNect(1, :) ./ meansGFP(1, :);
            sr1 = stdsmNect(1, :) ./ stdsGFP(1, :);
            mr2 = meansmNect(2, :) ./ meansGFP(2, :);
            sr2 = stdsmNect(2, :) ./ stdsGFP(2, :);
            mr1(isinf(mr1) | isnan(mr1)) = 0;
            sr1(isinf(sr1) | isnan(sr1)) = 0;
            mr2(isinf(mr2) | isnan(mr2)) = 0;
            sr2(isinf(sr2) | isnan(sr2)) = 0;
            errorbar(Phyb_ON_impulses(2:end), mr1(2:end), sr1(2:end));
            errorbar(Phyb_ON_impulses(2:end), mr2(2:end), sr2(2:end));
            grid on;
            title('mNectarine/GFP ratio');
            xlabel('Impulse [activity*min]');
            ylabel('Ratio');
            legend(sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(1)), ...
                sprintf('Phyb_{ON}=%.1f', Phyb_ON_levels(2)));
            
            drawnow;
        end
    end
end