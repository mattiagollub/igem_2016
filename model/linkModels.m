function model = linkModels(inputs, renamedSpecies, varargin)
%LINKMODELS
%
    %% Verify input
    nModels = length(varargin);
    assert(nModels > 1, 'linkModels requires at least two models');

    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Rename species as required
    if ~isempty(renamedSpecies)
        for i=1:nModels
            m = varargin{i};
            
            % Rename inputs
            for j=1:length(m.inputs)
                m.inputs{j} = char(subs(sym(m.inputs{j}), ...
                    renamedSpecies(:,1), renamedSpecies(:,2)));
            end

            % Substitute specie names
            m.specieNames = subs(m.specieNames, ...
                renamedSpecies(:,1), renamedSpecies(:,2));

            % Substitute species in rate matrix
            if isfield(model, 'rates')
                m.rates = subs(m.rates, ...
                    renamedSpecies(:,1), renamedSpecies(:,2));
            end

            % Substitute species in propensity matrix
            if isfield(model, 'a')
                m.a = subs(m.a, ...
                    renamedSpecies(:,1), renamedSpecies(:,2));
            end

            % Substitute species in spiece functions
            if isfield(model, 'specieFunctions')
                m.specieFunctions = subs(m.specieFunctions, ...
                    renamedSpecies(:,1), renamedSpecies(:,2));
            end
            
            varargin{i} = m;
        end
    end
    
    %% Map species to columns and count reactions and parameters
    colMap = containers.Map();
    totSpecies = 0;
    totParams = 0;
    totRxns = 0;
    
    for i=1:length(inputs)
        totSpecies = totSpecies + 1;
        colMap(inputs{i}) = totSpecies;
    end
    
    for i=1:nModels
        m = varargin{i};
        totRxns = totRxns + length(m.rates);
        totParams = totParams + length(m.paramNames);
        nSpecies = length(m.specieNames);
        for s=1:nSpecies
            specieName = char(m.specieNames(s));
            if ~isKey(colMap, specieName)
                totSpecies = totSpecies + 1;
                colMap(specieName) = totSpecies;
            end
        end
    end
    
    %% Input, initial conditions and integrator options
    model.odeOptions = varargin{1}.odeOptions;
    model.inputs = inputs;
    model.c0 = zeros(1, totSpecies);
    model.specieNames = sym('a', [1 totSpecies]);
    
    for i=1:nModels
        m = varargin{i};
        nSpecies = length(m.specieNames);
        for s=1:nSpecies
            specieName = char(m.specieNames(s));
            model.c0(colMap(specieName)) = m.c0(s);
            model.specieNames(colMap(specieName)) = m.specieNames(s);
        end
    end
    
    %% Parameters names and values
    model.paramNames = sym('p', [1 totParams]);
    startId = 1;
    model.paramsSet = containers.Map();
    
    for i=1:nModels
        m = varargin{i};
        numParams = length(m.paramNames);
        model.paramNames(startId:startId+numParams-1) = m.paramNames;
        startId = startId + numParams;
        
        if (ischar(m.paramsSet))
            P = loadParameters(m.paramsSet);
        else
            P = m.paramsSet;
        end
        model.paramsSet = [model.paramsSet; P];
    end
    
    %% Assemble stoichiometric, rate, specie and propensitiy matrices
    model.N = zeros(totSpecies, totRxns);
    model.rates = sym('r', [totRxns 1]);
    model.specieFunctions = sym('s', [totSpecies 1]);
    model.a = sym('a', [totRxns 1]);
    
    startId = 1;
    for i=1:nModels
        m = varargin{i};
        numRxns = size(m.N, 2);
        
        for s=1:length(m.specieNames)
            name = char(m.specieNames(s));
            model.N(colMap(name), startId:startId+numRxns-1) = m.N(s, 1:end);
            
            if isfield(m, 'specieFunctions')
                model.specieFunctions(colMap(name)) = m.specieFunctions(s);
            else
                model.specieFunctions(colMap(name)) = m.specieNames(s);
            end    
        end
   
        model.rates(startId:startId+numRxns-1) = m.rates;
        model.a(startId:startId+numRxns-1) = m.a;
   
        startId = startId + numRxns;
    end
end