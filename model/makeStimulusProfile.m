function [t, x] = makeStimulusProfile(starts, ends, values, nPoints)
% MAKESTIMULUSPROFILE    Creates a stimulus profile. 
%   Creates a stimulus profile on the time interval [0, 1] where the
%   intervals [starts(i), ends(i)] are set to values(i). The remaining
%   intervals are set to 0. The profile is built using nPoints points.
%
%       [t, x] = makeStimulusProfile(starts, ends, values, nPoints)
%
%   Returns:
%       t:              vector of timestamps corresponding to the points
%                       of the profile.
%       x:              values of the stimulus profile.
%
%   Parameters:
%       starts:         Vector containing the starting times of the stimuli
%       ends:           Vector containing the ending times of the stimuli
%       values:         Vector containing the values of the stimuli
%       nPoints:        NUmber of points to use for the profile
%
    %% Initialize variables
    n = length(starts);
    t = (0:nPoints) ./ nPoints;
    x = zeros(size(t));

    %% Set stimuli on the intervals
    for i=1:n
        x(floor(starts(i)*nPoints+1):ceil(ends(i)*nPoints)+1) ...
            = values(i);
    end
end