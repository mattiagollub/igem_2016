function N = parameterSetColorMap(model, paramName1, paramName2,paramRange1, paramRange2, stimulus)

t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;

%N1=zeros(1,length(paramRange1));
%N2=zeros(1,length(paramRange2));
[N1,N2]=meshgrid(paramRange1, paramRange2);
[I1,I2]=meshgrid(paramRange1(1):0.1:paramRange1(end),...
                  paramRange2(1):0.1:paramRange2(end));

M=zeros(length(paramRange2), length(paramRange1));
% modelNO = createAndGateNOpartMAmodel();
% modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(model.paramsSet);

figure;
colormap('jet');
yticklabels=linspace(0, 100, 11);
xticklabels=linspace(0, 100, 11);

    
for j=1:length(paramRange1)
    for i=1:length(paramRange2)
        tic
    j
    i
    paramRange1(j)
    paramRange2(i)
    
    ParamMap(paramName1)=paramRange1(j);
    ParamMap(paramName2)=paramRange2(i);
    
    model.paramsSet = ParamMap;
    
    [t_NO, NO] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
    t_NO = t_NO * (tf - t0) + t0;
    T = model.timesMap;
    S = model.speciesMap;
    T('Lac') = t_NO;
    S('Lac') = NO;
    
    deterministicSimulation([t0, tf], model);
    R=nM2n(S('G_on')/15);
    M(i,j)=R(end);
    %K=interp2(N1, N2, M, I1, I2,'cubic');
    %surf(I1,I2,K);
    imagesc(flipud(interp2(N1, N2, M, I1, I2)));
    %surf(N1,N2,M);
    xtick = linspace(1, size(I1, 2), 11)
    ytick = linspace(1, size(I1, 1), 11)
    set(gca, 'XTick', xtick, 'XTickLabel', xticklabels);
    set(gca, 'YTick', ytick, 'YTickLabel',flipud(yticklabels(:)));

    grid on
    title('ratio of active promoter for parameters values');
    
    xlabel('LldR production rate [nM min^-1]');
    ylabel('LldD degradation rate (min^-1)');
    

    colorbar;
    drawnow;
    toc;
    end
end


