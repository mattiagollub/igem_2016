function pTet_mNectarine_plot()
% PTET_MNECTARINE_PLOT Simulation of the Ptet_mNectarine construct
%
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 2000;

    % Time range for the experiment
    t0   = 0;
    ts   = 2;
    tf   = ts*4;
    tspan = t0:ts:tf;
    
    %% Setup model
    model = createPtetXFPMAModel();
    model.paramsSet = containers.Map( ...
        {'l_Ptet', 'n_aTc', 'Km_aTc', 'k_mRNAxfp', 'd_mRNAxfp', ...
        'k_XFP', 'd_XFP', 'k_TetR', 'd_TetR', 'k_ON', 'k_OFF'}, ...
        {0.05, 1.6, 1500, 2.0, 1.0, 2.0, 0.3, 0.3, 0.02, 1.0, 0.1});
    T = model.timesMap;
    S = model.speciesMap;

    T('aTc') = [t0, tf];
    S('aTc') = n2nM([200, 200]);

    %% Simulate system
    stochasticSimulationNRM(tspan, model, nRuns, containers.Map(), false);
    
    %% Draw histograms of the reporters
    figure(1);
    for i=1:4
        subplot(2, 2, i);
        hist = model.histogramsMap('XFP');
        
        histogram(hist(i+1,:), 40, 'edgecolor','none');
        title(sprintf('GFP distribution at t=%d min', tspan(i+1)));
        xlim([0.1, 40]);
        xlabel('Number of molecules');
        ylabel('Number of cells');
    end
end