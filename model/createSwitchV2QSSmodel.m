function model = createSwitchV1QSSmodel()
%CRAETESWITCHQSSMODEL Defines a simplified model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1QSSmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    S_tot = n2nM(nP);   % nM, Total concentration of plasmids
    
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0, 0, 0, S_tot, 0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'Phyb_ON'};
    
    %% Define symbolic variables for species and parameters
    syms Phyb_ON mRNAinv Bxb1 S_0 S_2 Pout_flipped
    syms k_mRNAinv d_mRNAinv d_Bxb1 k_Bxb1 k_flip k_m1 k_m2
    
    model.specieNames = [Phyb_ON mRNAinv Bxb1 S_0 Pout_flipped];
    model.paramNames = [k_mRNAinv d_mRNAinv d_Bxb1 k_Bxb1 ...
                        k_flip k_m1 k_m2]; 
    
    %% Stoichiometric and rate matrices
    % Phyb_ON | mRNAinv | Bxb1 | S_0 | Pout_flipped
    model.N = [
        0,  1,  0,  0,  0; % Phyb_ON -> Phyb_ON + mRNAinv
        0, -1,  0,  0,  0; % mRNAinv ->
        0,  0,  1,  0,  0; % mRNAinv -> mRNAinv + Bxb1
        0,  0,  0, -1,  1; % S_0 + 4*Bxb1 -> Pout_flipped  + 4*Bxb1
        0,  0, -1,  0,  0; % Bxb1 ->
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAinv*Phyb_ON*S_tot;    % Phyb_ON -> Phyb_ON + mRNAinv
        d_mRNAinv*mRNAinv;          % mRNAinv ->
        k_Bxb1*mRNAinv;             % mRNAinv -> mRNAinv + Bxb1
        k_flip*S_2;                 % S_0 -> Pout_flipped
        d_Bxb1*Bxb1;                % Bxb1 ->
    ];
    model.rates = subs(model.rates, S_2, ...
        Bxb1^4*(S_tot-Pout_flipped)/(k_m1*k_m2+k_m1*Bxb1^2+Bxb1^4));
end