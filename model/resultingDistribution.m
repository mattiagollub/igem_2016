function [dist]= resultingDistribution (dist1, dist2, nCells)
%RESULTINGDISTRIBUTION compute Phyb distribution from PnorV3 and Pfree
%
% dist1 : PnorV3 distribution
% dist2 : Pfree distribution
% nCells : nbRuns

% returns:
%     dist :  Phyb distribution
%
%
    %% Split input distribution components
    mu1 = dist1.mu;
    sigma1 = dist1.sigma;
    
    mu2 = dist2.mu;
    sigma2 = dist2.sigma;
    
    %% Recreate Gaussian
    
    x_values = [0:1:30];
    norm1 = normpdf(x_values,mu1,sigma1);
    norm2 = normpdf(x_values,mu2,sigma2);
    
    Phybnorm = nCells*convol(norm1,norm2);
    
    dist = Phybnorm;
    
    
    





end