function plotExpDistributions(experiment)

    %% Setup plots layout
    hPlots = 4;
    wPlots = 6;
    figure();
    
    %% Setup model
%     T = model.timesMap;
%     S = model.speciesMap;
    prevT = 1000000;
    
    %% Generate comparison for each timepoint
    length(experiment.dataPoints)
    for i=1:length(experiment.dataPoints);
        i
        subplot(hPlots, wPlots, i);
        point = experiment.dataPoints{i};
        data = csvread(strcat(experiment.dataDir, 'export_pconst_esabox_', ...
            point{2}, '_', point{1}, '.csv'));
        
       % x = linspace(2, 3.1, 60);
       x = linspace(1, 3.0, 60);
        xExp = 10.^x;
        
        %% Plot FACS data
        histogram(max(xExp(1), data(:,5)), xExp, 'facealpha', .6, 'edgecolor','none');
        
        %% Simulate system with the estimated parameters
%         if (point{3} < prevT)
%             prevT = 0;
%             prevS = 0;
%             prevC = model.c0;
%         else
%             hist = model.histogramsMap('XFP');
%             prevC = hist(:,end);
%         end
%         T('aTc') = [prevT, point{3}];
%         S('aTc') = n2nM([prevS, point{4}]);
%         model.c0 = n2nM(prevC);
%         tspan = linspace(prevS, point{4}, 200);
%         stochasticSimulationNRM(tspan, model, nRuns, containers.Map(), false);
%         
%         %% Plot simulated data
%         hist = model.histogramsMap('XFP');
%         histogram(max(xExp(1), hist), xExp, 'facealpha', .6, 'edgecolor','none');
%         
        %% Make plots nice
        set(gca, 'Xscale', 'log');
        xlim(xExp([1, end]));
        title(sprintf('t=%.0fmin, dose=%.1f', point{4}, point{3}));
        drawnow;
    end
end