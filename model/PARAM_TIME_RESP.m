addpath('plotting');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range for the experiment
t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input : AHL Activity
stimulus = 100;   % Assume all promoters active
[t_AHL, AHL] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_AHL = t_AHL * (tf - t0) + t0;
%% Setup model
model = createAndGateAHLpartMAmodel();
T = model.timesMap;
S = model.speciesMap;
T('AHL') = t_AHL;
S('AHL') = AHL;
model.paramsSet = 'andgate_eth_igem_2014';
%paramRange
paramRange=[0:3:30];

plotTimeResponse(model, 'kesarProd', paramRange);