% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input and compares it withe the simplifeid model
%   result
% 
%% Simulation settings
clear;
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
tf   = 60*6;
lineWidth = 1.5;
% Input: NO activity
points = 100;
    AHL_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_AHL_ON = ceil((tf-t0)*0.1/tf*points);
    t_AHL_OFF = floor((tf-t0)*0.2/tf*points);
    t_AHL = (0:points)/points*(tf-t0) + t0;
    AHL = zeros(size(t_AHL));
    AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;
%% Setup model
model1 = createAndGateAHLpartMAmodel();
T1 = model1.timesMap;
S1 = model1.speciesMap;

model2 = createAndGateAHLpartSimplifiedmodel();
T2 = model2.timesMap;
S2 = model2.speciesMap;

T1('AHL') = t_AHL;
S1('AHL') = AHL;
model1.paramsSet = 'andgate_eth_igem_2014';

T2('AHL') = t_AHL;
S2('AHL') = AHL;
model2.paramsSet = 'andgate_eth_igem_2014';

%% Run simulation
deterministicSimulation([t0, tf], model1);
deterministicSimulation([t0, tf], model2);
%% Plot results
% mRNAinv input profile
figure(1);
clf;
subplot(2, 2, 1);
stairs(t_AHL, AHL, 'r','linewidth',2); % Stairstep graph of the input
grid on;
title('Input: NO')
xlabel('Time [min]');
ylabel('Concentration [nMol]');
legend('AHL');

 %% Plot NOR system
    ax = subplot(2, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(T1('Esar'), S1('Esar'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(T2('Esar'), S2('Esar'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('Esar system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('Esar MA', 'Esar Simplified');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    p = plot(T1('mRNAinv'), S1('mRNAinv'), 'linewidth', lineWidth);
    hold on
    p = plot(T2('mRNAinv'), S2('mRNAinv'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('mRNAinv MA', 'mRNAinv Simplified');
    
     %% Plot PnorV behavior
    ax = subplot(2, 2, 4);
    ax.ColorOrderIndex = 1;
    p = plot(T1('Pfree'), S1('Pfree'), 'linewidth', lineWidth);
    hold on
    p = plot(T2('Pfree'), S2('Pfree'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('Pfree behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('Pfree MA', 'Pfree Simplified');
    
   
    %% Plot sensitivities for Pout_flipped only
% figure(2)
% nParams = length(model.paramNames);
% nSpecies = length(model.specieNames);
% nInputs = length(model.inputs);
% hold on
% specie = 'mRNAinv';
% names = cell(size(model.paramNames));
% for p=1:nParams
%     names{p} = char(model.paramNames(p));
%     name = strcat('S_', specie, '_', names{p});
%     plot(T(name), S(name), 'linewidth', 2);
% end
% grid on;
% title('Sensitivity of the parameters w.r.t. mRNAinv');
% xlabel('Time [min]');
% ylabel('Sensitivity');
% l = legend(names{:});
% set(l, 'Interpreter', 'none') 
% 
% %% Print matrix with maximum sensitivities
% S = zeros(nParams, nSpecies);
% for s=nInputs+1:nSpecies
% 	for p=1:nParams
%         name = strcat('S_', char(model.specieNames(s)), ...
%                       '_',  char(model.paramNames(p)));
%         S(p, s) = max(model.speciesMap(name));
%     end
% end
% S = S / max(abs(S(:)));
% display(S);
