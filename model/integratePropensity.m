function [Y, tid] = integratePropensity(a, A, B, t, x, t0, state)
% INTEGRATEPROPENSITY Integrate using trapezoidal rule until the desired result.
%   
    %% First trapezoid
    tid = t0;
    maxTid = length(t);
    a_tid = a([x(tid), state(2:end)]);
    a_tid2 = a([x(tid+1), state(2:end)]);
    area = (t(tid+1)-t(tid))/2*(a_tid+a_tid2);
    
    %% Remove start of the trapezoid
    x_ts = linInterp(t(tid), t(tid+1), x(tid), x(tid+1), A);
    a_ts = a([x_ts, state(2:end)]);
    area = area - (A-t(tid))/2*(a_tid+a_ts);
    
    %% Add last trapezoid if necessary
    while t(tid+1) < B
        tid = tid + 1;
        if tid >= maxTid
            Y = area + (B-t(tid))*a_tid2;
            return;
        end
        a_tid = a_tid2;
        a_tid2 = a([x(tid+1), state(2:end)]);
        area = area + (t(tid+1)-t(tid))/2*(a_tid+a_tid2);
    end
    
    %% Subtract end of trapezoid (points with t > B)
    dt = t(tid+1) - B;
    x_B = linInterp(t(tid), t(tid+1), x(tid), x(tid+1), B);
    a_B = a([x_B, state(2:end)]);
    Y = area - dt/2*(a_B+a_tid2);
end