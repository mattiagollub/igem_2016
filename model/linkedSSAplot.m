function linkedSSAplot()
% LINKEDSSAPLOT Example simulation of two linked models. 
%   Simulates the concentration of the switch and reporter species using an
%   example input and a stochastic method.
%
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 151;
    nBins = 100;
    lineWidth = 0.5;

    % Time range for the experiment
    t0   = 0;
    ts   = 2;
    tf   = 250;
    tspan = t0:ts:tf;

    % Input 1: Phyb activity
    Phyb_ON_stimulus = n2nM(0.9);
    Phyb_states = [
        0.05, 0.05, 0.2, 0.2, 1.0, 1.0, 0.05, 0.05;
        0,    20,   25,  60,  65,  95,  100,  tf];
    v_Phyb_ON = Phyb_states(1,:) * Phyb_ON_stimulus;
    t_Phyb_ON = Phyb_states(2,:);
    
    % Input: Pout activity
    Pout_free_stimulus = n2nM(1.0);
    Pout_states = [
        0.05, 0.05, 0.2, 0.2, 1.0, 1.0, 0.05, 0.05, 1,   1;
        0,    20,   25,  60,  65,  95,  100,  195,  200, tf];
    v_Pout_free = Pout_states(1,:) * Pout_free_stimulus;
    t_Pout_free = Pout_states(2,:);
    
    %% Setup model
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;

    T('Phyb_ON') = t_Phyb_ON;
    S('Phyb_ON') = v_Phyb_ON;
    T('Pout_free') = t_Pout_free;
    S('Pout_free') = v_Pout_free;
    
    %% Plot Phyb_ON and Pout_free input profile
    figure(1);
    clf;
    subplot(2, 2, 1);
    hold on;
    plot(T('Phyb_ON'), nM2n(S('Phyb_ON')), 'linewidth', 2);
    plot(T('Pout_free'), nM2n(S('Pout_free')), 'linewidth', 2);
    grid on;
    title('Input: Fraction of Phyb activity');
    xlabel('Time [min]');
    ylabel('Promoter activity fraction');
    legend('Phyb^{ON}', 'Pout^{free}');
    ylim([0, 1.3]);

    drawFunctions = containers.Map( ...
        {'Bxb1', 'DBxb1', 'Pout_flipped', 'GFP', 'mNect'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 3, 1, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 5, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
        });

    %% Simulate system
    stochasticSimulationTDI(tspan, model, nRuns, drawFunctions);

    %% Plot Bxb1 dimerization
    subplot(2, 2, 2);
    hold on;
    plot(T('Bxb1'), nM2n(S('Bxb1')), 'k', 'linewidth', 2);
    plot(T('DBxb1'), nM2n(S('DBxb1')), 'k', 'linewidth', 2);
    grid on;
    title('Bxb1 dimerization');
    xlabel('Time [min]');
    ylabel('Number of molecules');
    legend('Bxb1', 'DBxb1');

    %% Plot Pout_flipped behavior
    subplot(2, 2, 3);
    plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'k', 'linewidth', 2);
    grid on;
    title('Pout flipping');
    xlabel('Time [min]');
    ylabel('Number of promoters');
    
     %% Plot GFP/mNect behavior
    subplot(2, 2, 4);
    hold on;
    plot(T('GFP'), nM2n(S('GFP')), 'k', 'linewidth', 2);
    plot(T('mNect'), nM2n(S('mNect')), 'k', 'linewidth', 2);
    grid on;
    title('Reporter proteins expression');
    xlabel('Time [min]');
    ylabel('Number of molecules');
    legend('GFP', 'mNect');
    
    %% Draw histograms of the reporters
    figure(2);
    subplot(3, 1, 1);
    histsGFP = model.histogramsMap('GFP');
    histogram(histsGFP(end, :), nBins);
    title(sprintf('GFP distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');
    
    subplot(3, 1, 2);
    histsmNect = model.histogramsMap('mNect');
    histogram(histsmNect(end, :), nBins);
    title(sprintf('mNectarine distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');
    
    subplot(3, 1, 3);
    histogram(min(histsmNect(end, :) ./ histsGFP(end, :), 0.4), nBins);
    title(sprintf('Distribution of reporter ratios at t=%d min', tspan(end)));
    xlabel('mNectarine/GFP ratio');
    ylabel('Number of cells');
end

function drawInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(2, 2, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, nM2n(x), 'linewidth', lineWidth);
    p.Color(4) = alpha;
end