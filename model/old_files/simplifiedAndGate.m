function dxdt = simplifiedAndGate(t, c, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, par)

% x inactivates X
% y inactivates Y
% X inactivates Z
% Y inactivates Z
% type of transcription function for Z: "AND gate" 

 NO_inactive = 1; % We assume constitutive production of X
 AHL_inactive = 1;
 if t >= t_Sx_ON && t< t_Sx_OFF
     NO = NO_inactive;
 else
     NO = 0;
 end 
 
 if t >= t_Sy_ON && t< t_Sy_OFF
     AHL = AHL_inactive;
 else
     AHL = 0;
 end 
 
% x = c(1);
% y = c(2);
%NorR_s_NO = c(1);
%EsaR_AHL = c(2);
% nu_max_app =c(3);
% K_m_app = c(4);
%GFP = c(3);
 mrna=c(1);
%  bxb=c(2);
%  Pflipped=c(3);
%  mrnagfp=c(4);
%  pnorV=c(2);
%  pfree=c(3);
%--------------------- Parameters old
%  k1_norR  = par(1);
%  NorR_s_0  = par(2);
%  Kh_norR  = par(3);
%  k1_esaR  = par(4);
%  EsaR_0  = par(5);
%  Kh_esaR  = par(6);
%  k1 = par(7);
%  k_1 = par(8);
%  k2 = par(9);
%  k3 = par(10);
%  k_3  = par(11);
%  k5  = par(12);
%  k_5  = par(13);
%  n_noR  = par(14);
%  n_esaR  = par(15);
%  E_0  = par(16);
%---------------------parametres first and gate
          
Ptot= par(1);
Kahl= par(2);
Kno= par(3);
nno= par(4);
nahl= par(5);
Kmahl= par(6);
Kmno= par(7);
k1= par(8);
k_1= par(9);
kmrna= par(10);
nortot= par(11);
%-------------------------------parametres switch + second and gate
pswitchtot = par(12);
K9 = par(13);
Stot =par(14);
k13 =par(15);
k14 =par(16);
kmrnagfp =par(17);
dmrnagfp =par(18);
klgfp =par(19);
kmAS=par(20);
%-------------------------systeme
%mrna=20;
dmrna=0.001;
kl=0.001;
%NOR=((k1/k_1)*nortot*NO)/(1+(k1/k_1)*NO);
% pfree=(Ptot*Kahl*(AHL)^nahl)/(Kmahl + (AHL)^nahl);
% pnorV=(Ptot*Kno*(NOR)^nno)/(Kmno + (NOR)^nno);
% Phyb=pfree*pnorV/Ptot;



% Equations 
dxdt = zeros(1,1);
%first and gate
NOR=((k1/k_1)*nortot*NO)/(1+(k1/k_1)*NO);
pfree=(Ptot*Kahl*(AHL)^nahl)/(Kmahl + (AHL)^nahl);
pnorV=(Ptot*Kno*(NOR)^nno)/(Kmno + (NOR)^nno);
Phyb=(pfree/Ptot)*(pnorV/Ptot)*Ptot ;
%sxitch
Pswitchfree=K9*pswitchtot*(AHL)^2;
% AS_bxb1=(Stot*(bxb)^2.6)/(kmAS + (bxb)^2.6);
% Pon=Pflipped*Pswitchfree/pswitchtot;

dxdt(1)= kmrna*Ptot*(kl+(1-kl)*Phyb)-0.1*mrna;%mrnabxb
% dxdt(2)= k14*mrna-0.1*bxb;%bxb1
% dxdt(3)= k13*AS_bxb1;%Pflipped
% dxdt(4)= kmrnagfp*pswitchtot*(klgfp+(1-klgfp)*Pon)-0.1*mrnagfp;%mrnagfp

%plot
% NO=(0:1:1000);
% AHL=(0:1:1000);
% pfree(NO)=(Ptot*Kahl*(AHL).^nahl)/(Kmahl + (AHL).^nahl);
% pnorV(AHL)=(Ptot*Kno*(NOR).^nno)/(Kmno + (NOR).^nno);
% figure
% plot(AHL,pfree)
% title('pfree AHL');
% 
% figure
% plot(NO,pnorV);
% title('pnorV NO');

%---------------------------syteme old
%  nu_max= k2* E_0;
%  Kiu=k_5/k5;
%  Kic= k_3/k3;
%  K_m=(k_1 + k2)/k1;
 
% d_norR=0.2;
% d_EsaR=0.2;
% d_gfp=0.12
% 
%  % Equations 
%  dxdt = zeros(1,1);
% %  dxdt(1)= 0;
% %  dxdt(2)= 0; 
%         
%  dxdt(1) = (3*k1_norR*NorR_s_0*NO^n_noR/(Kh_norR + NO^n_noR)) - d_norR*NorR_s_NO;
%  dxdt(2) = (2*k1_esaR*EsaR_0*AHL^n_esaR/(Kh_esaR + AHL^n_esaR)) - d_EsaR*EsaR_AHL;
%  
%  nu_max_app = nu_max/(1+NorR_s_0 - NorR_s_NO );
%  K_m_app = (K_m * (1+((NorR_s_0 - NorR_s_NO)/Kic)))/(1 + ((NorR_s_0 - NorR_s_NO)/Kiu));
%  
%  dxdt(3) = (nu_max_app*E_0/(K_m_app + E_0))- d_gfp*GFP;