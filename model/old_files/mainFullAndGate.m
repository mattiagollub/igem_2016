% clear all;
% clc;
% clf;

% time range for the experiment
t0   = 0;
tend = 100;

% switching times for the signal Sx
t_Sx_ON  = 10;
t_Sx_OFF = 100;

% switching times for the signal Sy
t_Sy_ON  = 30;
t_Sy_OFF = 100;

% stimulus
t_sti  = [t0, t_Sx_ON,t_Sy_ON, t_Sx_OFF, t_Sy_OFF, tend];
Sx_sti = [ 0,       1,        1,      1,        1,    1];
Sy_sti = [ 0,       0,        1,      1,        1,    1];

% tspan for simulation
tstep = 0.01;
tspan = t0:tstep:tend;

% 
%%------------------ parametres new
Ptot = 20;
Kahl = 1;
Kno = 1;
nno = 2.3;
nahl =2.1;
Kmahl =0.001;
Kmno =0.001;
k1 =0.001;
k_1 =0.001;
kmrna =0.1;
nortot = 100;
pswitchtot = 20;
K9 = 1;
Stot =20;
k13 =1;
k14 =1;
kmrnagfp = 0.1;
dmrnagfp = 0.1;
klgfp = 0.001;
kmAS =0.001;
origPars = [Ptot Kahl Kno nno nahl Kmahl Kmno k1 k_1 kmrna nortot pswitchtot K9 Stot k13 k14 kmrnagfp dmrnagfp klgfp kmAS];

%  nu_max= k2* E_0;
%  K_m=(k_1 + k2)/k1;

% initial conditions for simulation
%x0 = [1,1,0];
nortot=1000;
pnortot=24;

esartot=1000;
stot=24;
pconsttot=24;
pdesar=20;
x0 = [nortot,0,pnortot,0,0,0,esartot,0,0,0,pdesar,0];%0,0,stot,0,0,0,pconsttot,0,0,0];

% integrator options
opt = odeset('AbsTol', 1e-1, 'RelTol', 1e-1);
%yp0=zeros(3,1);
% 
%% equations
[t1, x1] = ode45(@andGateFullModel, tspan, x0, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, origPars);
%plot
figure(1)
subplot(3,1,1);% Stimulus x
fig1 = stairs(t_sti, Sx_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus Nitric Oxyde')
xlabel('t');
ylabel('Sx');

subplot(3,1,2);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Sy');

subplot(3,1,3); % X response
plot(t1, x1(:,12), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('mRNA');
xlabel('t');
ylabel('X');

figure(2)
plot(t1, x1(:,3), 'linewidth', 2);
hold on
plot(t1, x1(:,4), 'linewidth', 2);
hold on
plot(t1, x1(:,5), 'linewidth', 2);
hold on
plot(t1, x1(:,6), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('PnorV');
xlabel('t');
ylabel('X');
legend Pnor0 Pnor1 Pnor2 Pnor3

figure(3)
plot(t1, x1(:,10), 'linewidth', 2);
hold on
plot(t1, x1(:,11), 'linewidth', 2);
hold on
plot(t1, x1(:,12), 'linewidth', 2);
grid on;
title('Pfree');
xlabel('t');
ylabel('X');
legend Pfree Pesar mrna