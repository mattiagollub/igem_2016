
%% COMPARISON    Example simulation of the COMPARISON. 
%   Simulates the concentration of the switch species using an example
%   input.
%   default NO and AHL values are 60 (range between 0 and 60)
%% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
 
% Parameter set
params_set = 'andgate_eth_igem_2014';
%% Simulation settings
% Time range for the experiment
t0   = 0;
tf   = 1000;

% Input: NO and AHL profile
points = 100;
NO_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
AHL_stimulus = 30 * 1e9 / (Na * V);
t_NO_ON = ceil((tf-t0)*0.1/tf*points);
t_NO_OFF = floor((tf-t0)*0.5/tf*points);
t_AHL_ON = ceil((tf-t0)*0.2/tf*points);
t_AHL_OFF = floor((tf-t0)*0.6/tf*points);
t_NO = (0:points)/points*(tf-t0) + t0;
t_AHL = (0:points)/points*(tf-t0) + t0;
NO = zeros(size(t_NO));
AHL = zeros(size(t_AHL));
NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;

%% Run simulation
timesMap = containers.Map();
timesMap('NO') = t_NO;
timesMap('AHL') = t_AHL;

speciesMap = containers.Map();
speciesMap('NO') = NO;
speciesMap('AHL') = AHL;

andGateSimplified([t0, tf], timesMap, speciesMap, params_set);

%% Plot results

% mRNAinv input profile
figure(1);
subplot(3,1,1);% Stimulus x
stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
grid on;
title('stimulus Nitric Oxyde')
xlabel('t');
ylabel('Sx');

subplot(3,1,2);% Stimulus y
stairs(t_AHL, AHL, 'r', 'linewidth', 2);% Stairstep graph of the input
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Concentration [nMol]');

subplot(3,1,3); % X response
plot(timesMap('mRNAinv'), speciesMap('mRNAinv'), 'linewidth', 2);
hold on
andGateMA([t0, tf], timesMap, speciesMap, params_set);
plot(timesMap('mRNAinv'), speciesMap('mRNAinv'), 'linewidth', 2);
grid on;
title('mRNA');
xlabel('t');
ylabel('Concentration [nMol]');
legend simplifiedModel massActionDeterministicModel