clear all;
clc;
clf;

% time range for the experiment
t0   = 0;
tend = 100;

% switching times for the signal Sx
t_Sx_ON  = 10;
t_Sx_OFF = 100;

% switching times for the signal Sy
t_Sy_ON  = 30;
t_Sy_OFF = 100;

% stimulus
t_sti  = [t0, t_Sx_ON,t_Sy_ON, t_Sx_OFF, t_Sy_OFF, tend];
Sx_sti = [ 0,       1,        1,      1,        1,    1];
Sy_sti = [ 0,       0,        1,      1,        1,    1];

% tspan for simulation
tstep = 0.01;
tspan = t0:tstep:tend;

%----------------- parameters old
%  k1_norR  = 0.9;
%  NorR_s_0  = 1;
%  Kh_norR  = 0.7;
%  k1_esaR  = 0.9;
%  EsaR_0  = 1;
%  Kh_esaR  = 0.7;
%  k1 = 0.8;
%  k_1 = 0.6;
%  k2 = 0.8;
%  k3 = 0.9;
%  k_3  = 0.5;
%  k5  = 0.9;
%  k_5  = 0.3;
%  n_noR  = 3;
%  n_esaR  = 2;
%  E_0  = 1;

% 
%%------------------ parametres new
Ptot = 20;
Kahl = 1;
Kno = 1;
nno = 2.3;
nahl =2.1;
Kmahl =0.001;
Kmno =0.001;
k1 =0.001;
k_1 =0.001;
kmrna =0.1;
nortot = 100;
pswitchtot = 20;
K9 = 1;
Stot =20;
k13 =1;
k14 =1;
kmrnagfp = 0.1;
dmrnagfp = 0.1;
klgfp = 0.001;
kmAS =0.001;
origPars = [Ptot Kahl Kno nno nahl Kmahl Kmno k1 k_1 kmrna nortot pswitchtot K9 Stot k13 k14 kmrnagfp dmrnagfp klgfp kmAS];

%  nu_max= k2* E_0;
%  K_m=(k_1 + k2)/k1;

% initial conditions for simulation
%x0 = [1,1,0];
nortot=1000;
pnortot=24;

esartot=1000;
stot=24;
pconsttot=24;
pdesar=0;
x0 = [nortot,0,pnortot,0,0,0,esartot,0,0,0,pdesar,0,0,0,stot,0,0,0,pconsttot,0,0,0];

% integrator options
opt = odeset('AbsTol', 1e-1, 'RelTol', 1e-1);
%yp0=zeros(3,1);
% 
%% equations
[t1, x] = ode45(@fullModel, tspan, x0, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, origPars);
plotstuff(t1, x, 1, '\bf simpleAndGate', t_sti, Sx_sti,Sy_sti, t0, tend, 0);
%[t1, x1] = ode45(@plotspecies, tspan, x0)%, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF);
% 

