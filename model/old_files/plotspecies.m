function dxdt = plotspecies(tspan,x0)
% time range for the experiment
t0   = 0;
tend = 100;

% switching times for the signal Sx
t_Sx_ON  = 10;
t_Sx_OFF = 50;

% switching times for the signal Sy
t_Sy_ON  = 30;
t_Sy_OFF = 70;

% stimulus
t_sti  = [t0, t_Sx_ON,t_Sy_ON, t_Sx_OFF, t_Sy_OFF, tend];
Sx_sti = [ 0,       1,        1,      0,        0,    0];
Sy_sti = [ 0,       0,        1,      1,        0,    0];

% tspan for simulation
tstep = 0.01;
tspan = t0:tstep:tend;

%parameters
Ptot = 20;
Kahl = 1;
Kno = 1;
nno = 2.3;
nahl =2.1;
Kmahl =0.1;
Kmno =0.1;
k1 =0.01;
k_1 =0.01;
kmrna =0.001;
nortot = 1000;

%-------------------------systeme
mrna=20;
dmrna=0.001;
kl=0.5;


%plot
NOR=(0:0.001:1);
AHL=(0:0.001:1);

pfree=zeros(1001);
pnorV=zeros(1001);

for i=1:1001
% display(i);
% display(AHL(i));
pfree(i)=(Ptot*Kahl*((AHL(i))^nahl))/(Kmahl + ((AHL(i))^nahl));
% display(pfree(i));
% display('----');
pnorV(i)=(Ptot*Kno*((NOR(i))^nno))/(Kmno + ((NOR(i))^nno));
% display(NOR(i));
% display(pnorV(i));
% display('----');
% display('----');
end
figure(1)
plot(pfree)
title('pfree AHL');

figure(2)
plot(pnorV);
title('pnorV NO');

%------
 NO_inactive = 1; % We assume constitutive production of X
 AHL_inactive = 1;
 
 for i = 1:length(tspan);
 if tspan(i) >= t_Sx_ON && tspan(i)< t_Sx_OFF
     NO1(i) = NO_inactive;
 else
     NO1(i) = 0;
 end 
 
 if tspan(i) >= t_Sy_ON && tspan(i)< t_Sy_OFF
     AHL1(i) = AHL_inactive;
 else
     AHL1(i) = 0;
 end 
 pfree1(i)=(Ptot*Kahl*((AHL1(i))^nahl))/(Kmahl + ((AHL1(i))^nahl));
 pnorV1(i)=(Ptot*Kno*((NO1(i))^nno))/(Kmno + ((NO1(i))^nno));
 end
 figure(3)
 plot(AHL1);
 hold on
 plot(NO1);
 hold on
 plot(pfree1);
 title('pfree AHL - pnorV NO');
 hold on
 plot(pnorV1);
 legend AHL NO pfree pnorV
 
%  transpose(pfree1)
%  pnorV1
 Phyb=pfree1.*pnorV1/Ptot;

figure(4)
 plot(Phyb);
 
%dsolve(diff(y) == y + 1)
%
 %dxdt(1) = transpose(kmrna*Ptot*(kl+(1-kl)*Phyb)-0.00001);
 %dsolve(diff(y) == kmrna*Ptot*(kl+(1-kl)*x)-0.00001);
 
S=dsolve('Dy = kmrna*Ptot*(kl+(1-kl)*Phyb)-0.01*y','tspan');
C2=10;
C1=110;
for x=1:length(Phyb)
mrna(x)= C1*exp(-tspan(x)/100) + 100*Phyb(x)*Ptot*kmrna - 100*Phyb(x)*Ptot*kl*kmrna;
%+ 100*Ptot*kl*kmrna
end
figure(5)
plot(mrna);