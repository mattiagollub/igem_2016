function [] = plotTimeCourse(t, x, fig, label, t_sti, Sx_sti, Sy_sti , t0, tend, thr)

figure(1)
subplot(3,1,1);% Stimulus x
fig1 = stairs(t_sti, Sx_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus Nitric Oxyde')
xlabel('t');
ylabel('Sx');

subplot(3,1,2);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Sy');

subplot(3,1,3); % X response
plot(t, x(:,1), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('mRNA');
xlabel('t');
ylabel('X');

figure(2)
subplot(3,1,1); % X response
plot(t, x(:,1), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('mRNA');
xlabel('t');
ylabel('X');

subplot(3,1,2); % X response
plot(t, x(:,2), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('bxb');
xlabel('t');
ylabel('bxb');

subplot(3,1,3); % X response
plot(t, x(:,4), 'g','linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('mrnagfp');
xlabel('t');
ylabel('mrnagfp');




% subplot(5,1,4); % Y response
% plot(t, x(:,2), 'linewidth', 2);
% xlim([t0 tend]);
% %ylim([-0.2 2.2]);
% grid on;
% title('EsaR complex response');
% xlabel('t');
% ylabel('Y');
% 
% subplot(5,1,5); % Z response
% plot(t, x(:,3), 'g', 'linewidth', 2);
% if thr>0
%     hold on;
%     plot(t,ones(length(x(:,2)),1)*thr, '--b');
% end
% xlim([t0 tend]);
% ylim([-0.2 2.2]);
% grid on;
% title('output response');
% xlabel('t');
% ylabel('Z');

if thr>0
    hold off;
end

ha = axes('Position', [0 0 1 1], 'Xlim', [0 1], 'Ylim',[0 1], 'Box', 'off', 'Visible', 'off', 'Units', 'normalized', 'clipping', 'off');
text(0.5, 1, label, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top')

end