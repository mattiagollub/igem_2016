function dxdt = fullModel(t, c, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, par)

% x inactivates X
% y inactivates Y
% X inactivates Z
% Y inactivates Z
% type of transcription function for Z: "AND gate" 

 NO_inactive = 1; % We assume constitutive production of X
 AHL_inactive = 1;
 if t >= t_Sx_ON && t< t_Sx_OFF
     NO = NO_inactive;
 else
     NO = 0;
 end 
 
 if t >= t_Sy_ON && t< t_Sy_OFF
     AHL = AHL_inactive;
 else
     AHL = 0;
 end 
 
% x = c(1);
% y = c(2);
%NorR_s_NO = c(1);
%EsaR_AHL = c(2);
% nu_max_app =c(3);
% K_m_app = c(4);
%GFP = c(3);
 NoR=c(1);
 NoR_NO=c(2);
 Pnor0=c(3);
 Pnor1=c(4);
 Pnor2=c(5);
 Pnor3=c(6);
 EsaR=c(7);
 DesaR=c(8);
 DesaR_AHL=c(9);
 Pfree=c(10);
 Pesar=c(11);
 mrna=c(12);
 Bxb1=c(13);
 Dbxb1=c(14);
 IS0=c(15);
 IS1=c(16);
 AS=c(17);
 Pflipped=c(18);
 Pconst0=c(19);
 Pconst1=c(20);
 Pconstfree=c(21);
 mrnagfp=c(22);
%  pnorV=c(2);
%  pfree=c(3);
%--------------------- Parameters old
%  k1_norR  = par(1);
%  NorR_s_0  = par(2);
%  Kh_norR  = par(3);
%  k1_esaR  = par(4);
%  EsaR_0  = par(5);
%  Kh_esaR  = par(6);
%  k1 = par(7);
%  k_1 = par(8);
%  k2 = par(9);
%  k3 = par(10);
%  k_3  = par(11);
%  k5  = par(12);
%  k_5  = par(13);
%  n_noR  = par(14);
%  n_esaR  = par(15);
%  E_0  = par(16);
%---------------------parametres first and gate
          
Ptot= par(1);
Kahl= par(2);
Kno= par(3);
nno= par(4);
nahl= par(5);
Kmahl= par(6);
Kmno= par(7);
k1= par(8);
k_1= par(9);
kmrna= 1;%par(10);
nortot= par(11);
%-------------------------------parametres switch + second and gate
pswitchtot = par(12);
K9 = par(13);
Stot =par(14);
k13 =par(15);
k14 =par(16);
kmrnagfp =par(17);
dmrnagfp =par(18);
klgfp =par(19);
kmAS=par(20);
%-------------------------systeme
%mrna=20;
dmrna=0.001;
kl=0.1;
%NOR=((k1/k_1)*nortot*NO)/(1+(k1/k_1)*NO);
% pfree=(Ptot*Kahl*(AHL)^nahl)/(Kmahl + (AHL)^nahl);
% pnorV=(Ptot*Kno*(NOR)^nno)/(Kmno + (NOR)^nno);
% Phyb=pfree*pnorV/Ptot;

%Parametres
k1=1;
k_1=0.01;
k2=0.001;
k_2=0.001;
k3=0.001;
k_3=0.001;
k4=0.001;
k_4=0.001;
k5=0.001;
k_5=0.001;
k6=0.001;
k_6=0.001;
k7=0.001;
k_7=0.6;
k8=0.1;
k_8=0.01;
k9=0.1;
k_9=0.01;
k10=0.1;
k_10=0.01;
k11=0.1;
k_11=0.001;
k12=1;
k_12=1e-6;
k13=0.3;
k14=0.1;

% Equations 
dxdt = zeros(22,1);
%first and gate
% NOR=((k1/k_1)*nortot*NO)/(1+(k1/k_1)*NO);
% pfree=(Ptot*Kahl*(AHL)^nahl)/(Kmahl + (AHL)^nahl);
% pnorV=(Ptot*Kno*(NOR)^nno)/(Kmno + (NOR)^nno);
% Phyb=(pfree/Ptot)*(pnorV/Ptot)*Ptot ;
% %sxitch
% Pswitchfree=K9*pswitchtot*(AHL)^2;
% AS_bxb1=(Stot*(bxb)^2.6)/(kmAS + (bxb)^2.6);
% Pon=Pflipped*Pswitchfree/pswitchtot;
pnor=0.1;
dnor=0.1;
dnor_no=0.1;
pesar=0.1;
pconsttot=20;

dxdt(1)= -k1*NoR*NO+k_1*NoR_NO-dnor*NoR+pnor;%NoR
dxdt(2)= k1*NoR*NO-k_1*NoR_NO-k2*NoR_NO*Pnor0+k_2*Pnor1-dnor_no*NoR_NO;%NoR_NO
dxdt(3)= -k2*NoR_NO*Pnor0+k_2*Pnor1;%Pnor0
dxdt(4)= k2*NoR_NO*Pnor0-k_2*Pnor1+k_3*Pnor2-k3*Pnor1*NoR_NO;%Pnor1
dxdt(5)= -k_3*Pnor2+k3*Pnor1*NoR_NO-k4*NoR_NO*Pnor2+k_4*Pnor3;%Pnor2
dxdt(6)= k4*NoR_NO*Pnor2-k_4*Pnor3;%Pnor3
% nortot=NoR_NO+NoR;
% Pnortot=Pnor0+Pnor1+Pnor2+Pnor3;
dxdt(7)= 1/2*(-k5*EsaR^2 +k_5*DesaR)-0.5*EsaR+pesar;%EsaR
dxdt(8)= k5*DesaR-(k5+0.5)*EsaR^2-k6*AHL^2*DesaR+k_6*DesaR_AHL-k7*DesaR*Pfree+k_7*Pesar;%DesaR
dxdt(9)= k6*AHL^2*DesaR-(k_6+0.5)*DesaR_AHL;%DesaR_AHL
dxdt(10)= -k7*DesaR*Pfree+k_7*Pesar;%Pfree
dxdt(11)= k7*DesaR*Pfree-k_7*Pesar;%Pesar
% Ptot=Pfree-Pesar;
% AHLtot=AHL+DesaR_AHL;
Phyb=(Pfree/Ptot)*(Pnor3/Ptot)*Ptot ;
dxdt(12)= kmrna*Ptot*(kl+(1-kl)*Phyb)-0.852*mrna;%mran (bxb)
dxdt(13)= 1/2*(-k10*Bxb1^2 +k_10*Dbxb1)-0.1*Bxb1+k14*mrna-0.01*Bxb1+k13*AS;%Bxb1
dxdt(14)= k10*Bxb1^2-(k_10+0.1)*Dbxb1-k11*IS0*Dbxb1+k_11*IS1+k_12*AS-k12*IS1*Dbxb1;%Dbxb1
dxdt(15)= -k11*IS0*Dbxb1+k_11*IS1;%IS0
dxdt(16)= k11*IS0*Dbxb1-k_11*IS1+k_12*AS-k12*IS1*Dbxb1;%IS1
dxdt(17)= -k_12*AS+k12*IS1*Dbxb1-k13*AS;%AS
dxdt(18)= k13*AS;%Pflipped
dxdt(19)= -k8*Pconst0*AHL+k_8*Pconst1;%Pconst0
dxdt(20)= k8*Pconst0*AHL-k_8*Pconst1-k9*Pconst1*AHL+k_9*Pconstfree;%Pconst1
dxdt(21)= k9*Pconst1*AHL-k_9*Pconstfree;%Pconstfree
Pon=Pflipped*Pconstfree/pconsttot;
dxdt(22)= kmrnagfp*Pconstfree*(klgfp+(1-klgfp)*Pon)-0.852*mrnagfp;%mrnagfp

%plot
% NO=(0:1:1000);
% AHL=(0:1:1000);
% pfree(NO)=(Ptot*Kahl*(AHL).^nahl)/(Kmahl + (AHL).^nahl);
% pnorV(AHL)=(Ptot*Kno*(NOR).^nno)/(Kmno + (NOR).^nno);
% figure
% plot(AHL,pfree)
% title('pfree AHL');
% 
% figure
% plot(NO,pnorV);
% title('pnorV NO');

%---------------------------syteme old
%  nu_max= k2* E_0;
%  Kiu=k_5/k5;
%  Kic= k_3/k3;
%  K_m=(k_1 + k2)/k1;
 
% d_norR=0.2;
% d_EsaR=0.2;
% d_gfp=0.12
% 
%  % Equations 
%  dxdt = zeros(1,1);
% %  dxdt(1)= 0;
% %  dxdt(2)= 0; 
%         
%  dxdt(1) = (3*k1_norR*NorR_s_0*NO^n_noR/(Kh_norR + NO^n_noR)) - d_norR*NorR_s_NO;
%  dxdt(2) = (2*k1_esaR*EsaR_0*AHL^n_esaR/(Kh_esaR + AHL^n_esaR)) - d_EsaR*EsaR_AHL;
%  
%  nu_max_app = nu_max/(1+NorR_s_0 - NorR_s_NO );
%  K_m_app = (K_m * (1+((NorR_s_0 - NorR_s_NO)/Kic)))/(1 + ((NorR_s_0 - NorR_s_NO)/Kiu));
%  
%  dxdt(3) = (nu_max_app*E_0/(K_m_app + E_0))- d_gfp*GFP;