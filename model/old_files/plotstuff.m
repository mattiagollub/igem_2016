function [] = plotstuff(t, x, fig, label, t_sti, Sx_sti, Sy_sti , t0, tend, thr)

figure(3)
subplot(2,1,1);% Stimulus x
fig1 = stairs(t_sti, Sx_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus Nitric Oxyde')
xlabel('t');
ylabel('Sx');


subplot(2,1,2); % X response
% plot(t, x(:,3), 'linewidth', 2);
% hold on
plot(t, x(:,4), 'linewidth', 2);
hold on
plot(t, x(:,5), 'linewidth', 2);
hold on
plot(t, x(:,6), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('PnorV');
xlabel('t');
ylabel('X');

figure(4)
subplot(2,1,1);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Sy');

subplot(2,1,2); % X response
plot(t, x(:,10), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('pfree');
xlabel('t');
ylabel('X');

figure(5)
subplot(2,1,1); % X response
plot(t, x(:,12), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('bxb1');
xlabel('t');
ylabel('bxb');

subplot(2,1,2); % X response
% plot(t, x(:,15), 'linewidth', 2);
% hold on
plot(t, x(:,16), 'linewidth', 2);
hold on
plot(t, x(:,17), 'linewidth', 2);
hold on
plot(t, x(:,18), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('ActiveSite switch');
legend IS0 IS1 AS
xlabel('t');
ylabel('X');

figure(6)
subplot(2,1,1);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Sy');

subplot(2,1,2); % X response
% plot(t, x(:,19), 'linewidth', 2);
% hold on
plot(t, x(:,20), 'linewidth', 2);
hold on
plot(t, x(:,21), 'linewidth', 2);
hold on
plot(t, x(:,18), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('Pconst');
xlabel('t');
ylabel('X');

figure(7)
subplot(3,1,1);% Stimulus x
fig1 = stairs(t_sti, Sx_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus Nitric Oxyde')
xlabel('t');
ylabel('Sx');

subplot(3,1,2);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 3.2]);
set(fig1);
grid on;
title('stimulus AHL')
xlabel('t');
ylabel('Sy');

subplot(3,1,3); % X response
plot(t, x(:,22), 'linewidth', 2);
xlim([t0 tend]);
%ylim([-0.2 2.2]);
grid on;
title('mRNAgfp');
xlabel('t');
ylabel('X');
% subplot(5,1,4); % Y response
% plot(t, x(:,2), 'linewidth', 2);
% xlim([t0 tend]);
% %ylim([-0.2 2.2]);
% grid on;
% title('EsaR complex response');
% xlabel('t');
% ylabel('Y');
% 
% subplot(5,1,5); % Z response
% plot(t, x(:,3), 'g', 'linewidth', 2);
% if thr>0
%     hold on;
%     plot(t,ones(length(x(:,2)),1)*thr, '--b');
% end
% xlim([t0 tend]);
% ylim([-0.2 2.2]);
% grid on;
% title('output response');
% xlabel('t');
% ylabel('Z');

if thr>0
    hold off;
end

ha = axes('Position', [0 0 1 1], 'Xlim', [0 1], 'Ylim',[0 1], 'Box', 'off', 'Visible', 'off', 'Units', 'normalized', 'clipping', 'off');
text(0.5, 1, label, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top')

end