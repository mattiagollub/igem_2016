function [s,p] = SP(x, te_on_ind, te_off_ind)

s = abs(max(x)-x(te_on_ind-1));
p = 1/abs(x(te_on_ind-1)-x(te_off_ind-1));


end