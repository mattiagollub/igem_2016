function [] = plotTimeCourse(t, x, fig, label, t_sti, Sx_sti, Sy_sti , t0, tend, thr)

figure(fig);
subplot(5,1,1);% Stimulus x
fig1 = stairs(t_sti, Sx_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 2.2]);
set(fig1);
grid on;
title('stimulus x')
xlabel('t');
ylabel('Sx');

subplot(5,1,2);% Stimulus y
fig1 = stairs(t_sti, Sy_sti, 'r','linewidth',2); % Stairstep graph of the input
xlim([t0 tend]);
ylim([-0.2 2.2]);
set(fig1);
grid on;
title('stimulus y')
xlabel('t');
ylabel('Sy');

subplot(5,1,3); % X response
plot(t, x(:,1), 'linewidth', 2);
xlim([t0 tend]);
ylim([-0.2 2.2]);
grid on;
title('X response');
xlabel('t');
ylabel('X');


subplot(5,1,4); % Y response
plot(t, x(:,2), 'linewidth', 2);
xlim([t0 tend]);
ylim([-0.2 2.2]);
grid on;
title('Y response');
xlabel('t');
ylabel('Y');

subplot(5,1,5); % Z response
plot(t, x(:,3), 'g', 'linewidth', 2);
if thr>0
    hold on;
    plot(t,ones(length(x(:,2)),1)*thr, '--b');
end
xlim([t0 tend]);
ylim([-0.2 2.2]);
grid on;
title('Z response');
xlabel('t');
ylabel('Z');

if thr>0
    hold off;
end

ha = axes('Position', [0 0 1 1], 'Xlim', [0 1], 'Ylim',[0 1], 'Box', 'off', 'Visible', 'off', 'Units', 'normalized', 'clipping', 'off');
text(0.5, 1, label, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top')

end