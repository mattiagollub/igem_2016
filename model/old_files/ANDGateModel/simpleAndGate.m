function dxdt = simpleAndGate(t, c, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, par)

% x inactivates X
% y inactivates Y
% X inactivates Z
% Y inactivates Z
% type of transcription function for Z: "AND gate" 

 x_inactive = 3; % We assume constitutive production of X
 y_inactive = 3;
 if t >= t_Sx_ON && t< t_Sx_OFF
     x = x_inactive;
 else
     x = 0;
 end 
 
 if t >= t_Sy_ON && t< t_Sy_OFF
     y = y_inactive;
 else
     y = 0;
 end 
 
% x = c(1);
% y = c(2);
X = c(1);
Y = c(2);
Z = c(3);
 
 % Parameters
 a_X  = par(1);
 a_Y  = par(2);
 a_z  = par(3);
 k_X  = par(4);
 k_Y  = par(5);
 k_z  = par(6);
 K_xX = par(7);
 K_yY = par(8);
 K_Xz = par(9);
 K_Yz = par(10);
 d_x  = par(11);
 d_y  = par(12);
 d_X  = par(13);
 d_Y  = par(14);
 d_z  = par(15);
 n_X  = par(16);
 n_Y  = par(17);

 
 % Equations 
 dxdt = zeros(3,1);
%  dxdt(1)= 0;
%  dxdt(2)= 0;
 dxdt(1) = a_X - d_X*X + K_xX/(1+(x/K_xX)^n_X) ; %a_X + k_X/(1+(x/K_xX)^n_X)-d_X*X;
 dxdt(2) = a_Y - d_Y*Y + K_yY/(1+(y/K_yY)^n_Y) ; %a_Y + k_Y/(1+(y/K_yY)^n_Y)-d_Y*Y;
 dxdt(3) = a_z + k_z*((K_Xz)^n_X/(1+(X/K_Xz)^n_X))*((K_Yz)^n_Y/(1+(Y/K_Yz)^n_Y))-d_z*Z;