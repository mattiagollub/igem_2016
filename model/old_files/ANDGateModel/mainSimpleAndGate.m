clear all;
clc;
clf;

% time range for the experiment
t0   = 0;
tend = 100;

% switching times for the signal Sx
t_Sx_ON  = 10;
t_Sx_OFF = 50;

% switching times for the signal Sy
t_Sy_ON  = 30;
t_Sy_OFF = 70;

% stimulus
t_sti  = [t0, t_Sx_ON,t_Sy_ON, t_Sx_OFF, t_Sy_OFF, tend];
Sx_sti = [ 0,       3,        3,      0,        0,    0];
Sy_sti = [ 0,       0,        3,      3,        0,    0];

% tspan for simulation
tstep = 0.001;
tspan = t0:tstep:tend;

% parameters
a_X  = 0;
a_Y  = 0;
a_z  = 0;
k_X  = 1;
k_Y  = 1;
k_z  = 1;
K_xX = 0.1;
K_yY = 0.1;
K_Xz = 0.9;
K_Yz = 0.9;
d_x  = 1;
d_y  = 1;
d_X  = 0.1;
d_Y  = 0.1;
d_z  = 0.1;
n_X  = 6;
n_Y  = 6;
origPars = [a_X a_Y a_z k_X k_Y k_z K_xX K_yY K_Xz K_Yz d_x d_y d_X d_Y d_z n_X n_Y];

% initial conditions for simulation
x0 = [2,2,0];

% integrator options
opt = odeset('AbsTol', 1e-13, 'RelTol', 1e-10);

% 
%%
[t1, x1] = ode45(@simpleAndGate, tspan, x0, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, origPars);
plotTimeCourse(t1, x1, 1, '\bf simpleAndGate', t_sti, Sx_sti,Sy_sti, t0, tend, 0);

% 
%%
thr = 0.1; % threshold in Z

stimLength  = 0.73;
dt5 = log(k_Y/(k_Y-K_Yz*d_Y))/d_Y;

t_Sx_OFF_5 = t_Sx_ON + stimLength;
t_sti_5    = [t0, t_Sx_ON, t_Sx_OFF_5,t_Sy_ON, t_Sy_OFF tend];

[t3, x3] = ode45(@simpleAndGate, tspan, x0, opt, t_Sx_ON, t_Sx_OFF_5,t_Sy_ON, t_Sy_OFF, origPars);
plotTimeCourse(t3, x3, 3, '\bf C1FFL AND K_{yz}=0.5', t_sti_5, Sx_sti,Sy_sti, t0, tend, thr);

display([stimLength, dt5]);





% 2.
% 2a
%%
% [t, x] = ode45(@simpleAndGate, tspan, x0, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, origPars);
% plotTimeCourse(t, x, 6, '\bf simpleAndGate', t_sti, Sx_sti, Sy_sti, t0, tend, 0);
% % sensitivity and precision of adaptation
% [s1, p1] = SP(x(:,2), t_Sx_ON/tstep, t_Sx_OFF/tstep);
% s1
% p1
%%
% [t, x] = ode45(@simpleAndGate, tspan, x0, opt, t_Sx_ON, t_Sx_OFF,t_Sy_ON, t_Sy_OFF, origPars);
% plotTimeCourse(t, x, 7, '\bf simpleAndGate', t_sti, Sx_sti,Sy_sti, t0, tend, 0);
% % sensitivity and precision of adaptation
% [s2, p2] = SP(x(:,2), t_Sx_ON/tstep, t_Sx_OFF/tstep);
% s2
% p2

