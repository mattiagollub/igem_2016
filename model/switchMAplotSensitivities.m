% SWITCHMAPLOTSENSITIVITIES Example sensitivity analysis of the switch. 
%   Computes the sensitivities of the switch species using example
%   input and parameter set.
%
%% Simulation settings
clear;

% Time range for the experiment
t0   = 0;
tf   = 120;

% Input: Phyb activity
stimulus = 1;   % Assume all promoters active
[t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.1, 0.3, stimulus, 100);
t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

%% Setup model
model = createSwitchV1MAmodel();
T = model.timesMap;
S = model.speciesMap;

T('Phyb_ON') = t_Phyb_ON;
S('Phyb_ON') = v_Phyb_ON;
model.paramsSet = 'switch_bonnet';

%% Run simulation
deterministicSimulation([t0, tf], model, true);

%% Plot results
figure(1);
clf;

% Plot Phyb_ON input profile
subplot(3, 1, 1);
plot(T('Phyb_ON'), S('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Fraction of Phyb activity')
xlabel('Time [min]');
ylabel('Phyb_{ON} fraction');
legend('Phyb^{ON}');
ylim([0, stimulus * 1.3]);

% Bxb1 dimerization
subplot(3, 1, 2);
hold on;
plot(T('Bxb1'), S('Bxb1'), 'linewidth', 2);
plot(T('DBxb1'), S('DBxb1'), 'linewidth', 2);
grid on;
title('Bxb1 dimerization');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Bxb1', 'DBxb1'); 

% Pout flipping
subplot(3, 1, 3);
hold on;
plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'linewidth', 2);
plot(T('S_0'), nM2n(S('S_0')), 'linewidth', 2);
plot(T('S_1'), nM2n(S('S_1')), 'linewidth', 2);
plot(T('S_2'), nM2n(S('S_2')), 'linewidth', 2);
grid on;
title('Pout flipping');
xlabel('Time [min]');
ylabel('Number of promoters');
legend('Pout^{flipped}', 'S_0', 'S_1', 'S_2');

%% Plot sensitivities
figure(2);
clf;    
axColors = [ ...
    0         0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840
    0.4000    0.4000    0.4000
    0.0500    0.0500    0.0500
];
ax = gca;
ax.ColorOrder = axColors;
hold on;

%% Plot sensitivities for Pout_flipped only
nParams = length(model.paramNames);
nSpecies = length(model.specieNames);
nInputs = length(model.inputs);

specie = 'Pout_flipped';
names = cell(size(model.paramNames));
for p=1:nParams
    names{p} = char(model.paramNames(p));
    name = strcat('S_', specie, '_', names{p});
    plot(T(name), S(name), 'linewidth', 2);
end
grid on;
title('Sensitivity of the parameters w.r.t. Pout^{flipped}');
xlabel('Time [min]');
ylabel('Sensitivity');
l = legend(names{:});
set(l, 'Interpreter', 'none') 

%% Print matrix with maximum sensitivities
S = zeros(nParams, nSpecies);
for s=nInputs+1:nSpecies
	for p=1:nParams
        name = strcat('S_', char(model.specieNames(s)), ...
                      '_',  char(model.paramNames(p)));
        S(p, s) = max(model.speciesMap(name));
    end
end
S = S / max(abs(S(:)));
display(S);