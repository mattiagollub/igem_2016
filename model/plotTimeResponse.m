function [] = plotTimeResponse(model, paramName,paramRange)

t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

figure;
hold on
setColorPalette([0 1], 'red',22);

N=zeros(1,length(paramRange));
% modelNO = createAndGateNOpartMAmodel();
% modelNO.paramsSet = 'andgate_eth_igem_2014';
ParamMap=loadParameters(model.paramsSet);

stimulus = 100;   % Assume all promoters active
[t_AHL, AHL] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_AHL = t_AHL * (tf - t0) + t0;
T = model.timesMap;
S = model.speciesMap;
T('AHL') = t_AHL;
S('AHL') = AHL;

for j=1:length(paramRange)
    j
    paramRange(j)
    ParamMap(paramName)=paramRange(j);
    model.paramsSet = ParamMap;
    deterministicSimulation([t0, tf], model);
    p = plot(T('Pfree'), nM2n(S('Pfree'))/15);
    grid on
    title('time response for different Esar production rate values');
    xlabel('time (min)');
    ylabel('ratio activated promoter');
    %legend ('show');
    drawnow;

end
