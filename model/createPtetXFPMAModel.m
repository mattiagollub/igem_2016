function model = createPtetXFPMAModel()
%CREATEXFPMAMODEL Create a mass action of model for the a fluorescent protein reporter.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
    %% Common constants
    nP = 15;            % Number of plasmids
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();
    
    %% Input, initial condition and intgrator options
    model.c0 = [0,0,0];
    model.odeOptions = odeset('RelTol', 1e-1);
    model.inputs = {'aTc'};
    
    %% Define symbolic variables for species and parameters
    syms aTc mRNAxfp XFP
    syms l_Ptet n_TetR Km_TetR k_mRNAxfp d_mRNAxfp k_XFP d_XFP Ka TetR_tot
    
%   0.045994
% 	1.90133
% 	325.265
% 	1.56697
% 	1.0053
% 	4.85984
% 	0.0177154
    
    model.specieNames = [aTc mRNAxfp XFP];
    model.paramNames = [l_Ptet n_TetR Km_TetR k_mRNAxfp d_mRNAxfp k_XFP ...
        d_XFP]; 
    
    %% Stoichiometric and rate matrices
    % aTc | mRNAxfp | XFP
    model.N = [
        0,  1,  0;  % P_ON -> P_ON + mRNAxfp
        0, -1,  0;  % mRNAxfp ->
        0,  0,  1;  % mRNAxfp -> mRNAxfp + XFP
        0,  0, -1;  % XFP ->
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAxfp*(l_Ptet + (1-l_Ptet) * (aTc)^n_TetR / (Km_TetR^n_TetR + (aTc)^n_TetR))*nP;  % P_ON -> P_ON + mRNAxfp
        d_mRNAxfp*mRNAxfp;  % mRNAxfp ->
        k_XFP*mRNAxfp;      % mRNAxfp -> mRNAxfp + XFP
        d_XFP*XFP;          % XFP ->     
    ];

    %% Reaction propensities
    model.a = [ % TODO: fix constants
        k_mRNAxfp*(l_Ptet + (1-l_Ptet) * (aTc)^n_TetR / (Km_TetR^n_TetR + (aTc)^n_TetR))*nP;  % P_ON -> P_ON + mRNAxfp
        d_mRNAxfp*mRNAxfp;  % mRNAxfp ->
        k_XFP*mRNAxfp;      % mRNAxfp -> mRNAxfp + XFP
        d_XFP*XFP;          % XFP ->
	];
end
