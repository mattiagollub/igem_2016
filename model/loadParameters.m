function params = loadParameters(set)
% LOADPARAMETERS     Loads the parameters of the model
%   Load the parameters of the model from an excel file. The function
%   returns a map where the parameters can be accessed using the
%   parameter name as key. 'set' specifies wich set of parameters to load.
%
%       params = loadParameters(set)
%
%   Returns:
%       params:         containers.Map containing the values of the
%                       parameters.
%
%   Parameters:
%       set:            Name of the file containing the parameters. 
%
    %% Load data from the excel file
    fileName = strcat('parameters/', set, '.xlsx');
    [~, ~, raw] = xlsread(fileName, 'parameters', 'B5:C100');
    params = containers.Map();
    n = size(raw, 1);
    
    %% Parse parameters and store them in the params map
    for i=1:n
        key = raw(i, 1);
        value = raw(i, 2);
        
        if (iscellstr(key) && ~isnan(value{1}))
            params(strjoin(key)) = value{1};
        end
    end

