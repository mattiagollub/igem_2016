function p = estimateDeterministicParams(options, experiments, model)
% ESTIMATEDETERMINISTICPARAMS Infer parameters from experimental data
%   Infer the parameters of a mass action model from a set of
%   experimental data.
%
%       p = estimateDeterministicParams(options, experiments, model)
%
%   Returns:
%       p:              containers.Map containing the values of the
%                       estimated parameters.
%
%   Parameters:
%       options:        Structure containing the options about the 
%                       parameters to be estimated.
%       experiments:    Structure containing teh experimental data.
%       model:          The model using the parameters.
%
    %% Specify problem setings
    problem.f = 'parameterEstimationProblem';
    problem.x_L = options.lowerBounds;
    problem.x_U = options.upperBounds;
    problem.x_0 = options.startValues;
    
    opts.maxeval = 5000;
    opts.maxtime = 60*20;
    opts.log_var = 1:length(options.startValues);
    opts.local.solver = 'dhc';
    opts.inter_save = 0;
    
    paramsSet = model.paramsSet;
    
    %% Run optimization
    results = MEIGO(problem, opts, 'ESS', options, experiments, model);
    
    %% Return a map containing the parameters
    if (ischar(paramsSet))
        P = loadParameters(paramsSet);
    else
        P = paramsSet;
    end
    
    % Insert estimated parameters
    for p=1:length(results.xbest)
        P(options.targetParams{p}) = results.xbest(p);
    end
    
    % Perform substitutions if necessary
    p = options.subsFunc(P);
    
end