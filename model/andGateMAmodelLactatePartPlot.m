%function andGateMAmodelNOpartPlot()
% AND GATE LACTATE PART Example simulation of the lactate module. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input.
% 
%% Simulation settings
clear;
% Common constants
nRuns = 500;
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 1.5;
% Input: Lac activity
% points = 100;
%     Lac_stimulus = 1000 * 1e9 / (Na * V);    % Assume ~500 mRNAs
%     t_Lac_ON = ceil((tf-t0)*0.1/tf*points);
%     t_Lac_OFF = floor((tf-t0)*1/tf*points);
%     t_Lac = (0:points)/points*(tf-t0) + t0;
%     Lac = zeros(size(t_Lac));
%     Lac(1, t_Lac_ON:t_Lac_OFF) = Lac_stimulus;
    
% Input : AHL Activity
    stimulus = 1000;   % Assume all promoters active
    [t_Lac, Lac] = makeStimulusProfile(0.3, 1.0, stimulus, 100);
    t_Lac = t_Lac * (tf - t0) + t0;
%% Setup model
modelLac = createAndGateLactatePartMAmodel();
modelLac.paramsSet = 'lactate_module_parameters';
LactateParamMap=loadParameters(modelLac.paramsSet);


T = modelLac.timesMap;
S = modelLac.speciesMap;

T('Lac') = t_Lac;
S('Lac') = Lac;

% Histogram plotting
histogramsX = 2;
histogramsY = 2;
nHistograms = histogramsX * histogramsY;


%% Plot results
% mRNAinv input profile
    figure;
    %clf;
    subplot(2, 2, 1);
    stairs(t_Lac, Lac, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: Lac')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('lac');

drawFunctions = containers.Map( ...
        {'LldR', 'DLldR', 'DLldR_Lac1', 'DLldR_Lac2', 'Pyr', 'Lac', 'G_on', 'G_off_1', 'G_off'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 4, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 4, 1, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 3, lineWidth, 0.2), ...
        });
%% Run simulation
%  kllddprod=[0, 10, 30, 60, 100];
%  for i=1:length(kllddprod);
%      i
%      kllddprod(i)
%  LactateParamMap('kllddProd')=kllddprod(i);
%  modelLac.paramsSet = LactateParamMap;
deterministicSimulation([t0, tf], modelLac);
%stochasticSimulationNRM(tspan, modelLac, nRuns);%, drawFunctions)

% % %Plot NOR system
% %     ax = subplot(2, 2, 2);
% %     hold on;
% %     %ax.ColorOrderIndex = 1;
% %     p = plot(T('Pyr'), S('Pyr'), 'linewidth', lineWidth);
% %     title('Pyr behavior');
% %     xlabel('Time [min]');
% %     ylabel('Concentration [nMol]');
% %     legend('Pyr');
% %     %% Plot PnorV behavior
% %     ax = subplot(2, 2, 3);
% %     %ax.ColorOrderIndex = 1;
% %     hold on
% %     b = interp1(T('Pyr'),S('Pyr'),T('Lac'));
% %     p = plot(T('Lac'), max(0,S('Lac')-b), 'linewidth', lineWidth);
% %     %p.Color(4) = 1 / nRuns;
% %     grid on;
% %     title('Lac inside the cell behavior');
% %     xlabel('Time [min]');
% %     ylabel('Concentration [nMol]');
% %     legend('Lac in');
% %  %% Plot DEsaR behavior
% %     ax = subplot(2, 2, 4);
% %     hold on;
% %     %ax.ColorOrderIndex = 2;
% %     p = plot(T('G_on'), S('G_on'), 'linewidth', lineWidth);
% %     grid on;
% %     title('acivated promoter');
% %     xlabel('Time [min]');
% %     ylabel('Concentration [nMol]');
% %     legend('promoter');
% %    
    
    
%end
    %p.Color(4) = 1 / nRuns;
 %% Plot NOR system
    ax = subplot(2, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(T('LldR'), S('LldR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(T('DLldR'), S('DLldR'), 'linewidth', lineWidth);
    hold on
    p = plot(T('LldD'), S('LldD'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DLldR_Lac2'), S('DLldR_Lac2'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('LldR system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('LldR', 'DLldR','LldD','DLldR_Lac2');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    p = plot(T('Pyr'), S('Pyr'), 'linewidth', lineWidth);
    hold on
    b = interp1(T('Pyr'),S('Pyr'),T('Lac'));
    p = plot(T('Lac'), max(0,S('Lac')-b), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('input behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('Pyr','Lac in');
    
    %% Plot DEsaR behavior
    ax = subplot(2, 2, 4);
    hold on;
    ax.ColorOrderIndex = 2;
    p = plot(T('G_on'), S('G_on'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('G_off_1'), S('G_off_1'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('G_off'), S('G_off'), 'linewidth', lineWidth);
%     hold on
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('Promoter behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('G_on')%,'G_off_1', 'G_off');
    drawnow;
%% Print matrix with maximum sensitivities
% S = zeros(nParams, nSpecies);
% for s=nInputs+1:nSpecies
% 	for p=1:nParams
%         name = strcat('S_', char(model.specieNames(s)), ...
%                       '_',  char(model.paramNames(p)));
%         S(p, s) = max(model.speciesMap(name));
%     end
% end
% S = S / max(abs(S(:)));
% display(S);
%% Plot histograms
%end
%     figure;
%     clf;
%     histsG_on = model.histogramsMap('G_on');
%     G_onDist = fitdist(histsG_on(end, :)','Normal');
%     histogram(histsG_on(end, :));
%     hold on
%     x_values = 0:1:30;
%     y = pdf(G_onDist,x_values);
%     f = fit(x_values.',500*y','gauss1');
%     plot(f,x_values,500*y)
%     title(sprintf('G_on distribution at t=%d min', tspan(end)));
%     xlabel('Number of molecules');
%     ylabel('Number of cells');
%   
%     
%     figure;
%     y=std(G_onDist');
%     hold on
%     tnew=1:10:length(T('G_on'));
%     snew=S('G_on');
%     s=snew(tnew);
%     ynew=y(tnew);
%     errorbar(tnew,s,ynew);
%     title(sprintf('mean(G_on) behavior for the and gate modular model, input NO = 100nM'));
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     
    



% function drawWithErrorBarInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = errorbar(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end

% function drawInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = plot(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end