function switchPlotsWiki()
% SWITCHPLOTSWIKI Plots of the switch module for the wiki
%
    %% Clear environment and close figures
    clear;
    close all;
    addpath('plotting');
    
    %% Define example setup
    t0   = 0;
    ts   = 0.5;
    tf   = 6*60;
    tspan = t0:ts:tf;
    maxRatio = 0.32;
    
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.0, 1.0, 1.0, 100);
    t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;
    
    %% Setup model    
    modelOld = createSwitchV1MAmodel();
    modelOld.timesMap('Phyb_ON') = t_Phyb_ON;
    modelOld.speciesMap('Phyb_ON') = v_Phyb_ON;
    modelOld.paramsSet = 'switch_bonnet';
    
    model = createSwitchV2MAmodel();
    model.timesMap('Phyb_ON') = t_Phyb_ON;
    model.speciesMap('Phyb_ON') = v_Phyb_ON;
    model.paramsSet = 'switch_bonnet';
    
    %% Call plot functions
    % simplePlot(tspan, model);
%    compareVersions(modelOld, model);
%    plotSensitivities(model);
    leakiness = 0.1;
    [minRatio, OK1] = activityResponse(model, 4*24*60, leakiness, 8*60);
    [maxRatio, OK2] = leakinessResponse(model, 4*24*60, leakiness);
%   degradationDynamics(model, (maxRatio+minRatio)/2);
    
%    minRatio = activityResponse(model);
%    maxRatio = leakinessResponse(model);
%   degradationDynamics(model, maxRatio);
%     compareORIs(model);
    
end

function simplePlot(tspan, model)
% Simple plot of the system bahavior

    T = model.timesMap;
    S = model.speciesMap;
    
    %% Plot input
    newFigure('Input: Fraction of AND gate activity');
    plot(T('Phyb_ON'), S('Phyb_ON')*100, 'linewidth', 2);
    xlabel('Time [min]');
    ylabel('Promoter activity [%]');
    ylim([0, 130]);
    
    %% Simulate system
    newFigure('Switching of the reporter promoters');
    drawFunctions = containers.Map( ...
        {'Pout_flipped'}, ...
        { @(t, x) drawIn1x1Subplot(t / 60, x, 1, 1, 1, 0.4)} ...
    );
    stochasticSimulationNRM(tspan, model, 500, drawFunctions);
    children = get(gca, 'children');
    h1 = children(1);
    
    h2 = plot(model.timesMap('Pout_flipped') / 60, ...
         nM2n(model.speciesMap('Pout_flipped')), 'k', 'linewidth', 2);
    set(gca,'FontSize',14);
    xlabel('Time [h]');
    ylabel('Number of flipped promoters');
    title('Switching of the reporter promoters');
    ylim([0 18]);
    legend([h1 h2], 'Single cells', 'Mean', 'Location', 'southeast');
    grid on;
    
    % stochasticSimulationNRM(tspan, model, 5000, containers.Map());
    % newFigure('');
end

function plotSensitivities(model)
    
    %% Define active input
    tspan = 0:1:6*60;
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(...
        0.0, 1.0, 1.0, 100);
    t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
    model.timesMap('Phyb_ON') = t_Phyb_ON;
    model.speciesMap('Phyb_ON') = v_Phyb_ON;
            
    specie = 'Pout_flipped';
    params = {'k_mRNAinv', 'd_mRNAinv', 'd_Bxb1', 'k_Bxb1', 'k_flip', 'd_DBxb1'};
    
    %% Run simulation for activation
    deterministicSimulation(tspan, model, true);
    
    %% Plot sensitivities for Pout_flipped only    
    newFigure('Promoter flipping sensitivity to parameters under activation');
    for p=1:numel(params)
        name = strcat('S_', specie, '_', params{p});
        plot(model.timesMap(name) /60, ...
            model.speciesMap(name), 'linewidth', 2);
    end
    
    xlabel('Time [h]');
    ylabel('Normalized sensitivity');
    l = legend(params{:});
    set(l, 'Interpreter', 'none');
    
    %% Define leaky input
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(...
        0.0, 1.0, 0.05, 100);
    t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
    model.timesMap('Phyb_ON') = t_Phyb_ON;
    model.speciesMap('Phyb_ON') = v_Phyb_ON;
    
    %% Run simulation for leakiness
    deterministicSimulation(tspan, model, true);
    
    %% Plot sensitivities for Pout_flipped only    
    newFigure('Promoter flipping sensitivity to parameters under leakiness');
    for p=1:numel(params)
        name = strcat('S_', specie, '_', params{p});
        plot(model.timesMap(name) /60, ...
            model.speciesMap(name), 'linewidth', 2);
    end
    
    xlabel('Time [h]');
    ylabel('Normalized sensitivity');
    l = legend(params{:});
    set(l, 'Interpreter', 'none');
end

function [minRatio, OK] = activityResponse(model, duration, leakiness, signalDuration)
% Plot a color map showing the response of the system

    model.paramsSet = loadParameters(model.paramsSet);
    
    %% Define parameter rages
    minFlipping = 10;
    interval = [0.001, 10];
    signal = 1;
    tspan = [0, duration];
    nPoints = 20;
    smoothing = 50;
    finePoints = nPoints * smoothing;
    k_range = logspace(log10(interval(1)), log10(interval(2)), nPoints);
    d_range = logspace(log10(interval(1)), log10(interval(2)), nPoints);
    [D, K] = meshgrid(d_range, k_range);
    k_fine = logspace(log10(interval(1)), log10(interval(2)), finePoints);
    d_fine = logspace(log10(interval(1)), log10(interval(2)), finePoints);
    [D_fine, K_fine] = meshgrid(d_fine, k_fine);
    S = zeros(length(k_range), length(d_range));
    
    %% Test different combinations of parameters
    newFigure(sprintf( ...
        'Fraction of flipped promoters after %.0fh exposure', signalDuration/60));
    colormap(jet);
    for i=1:length(k_range)
        for j=1:length(d_range)
            
            k = k_range(i);
            d = d_range(j);
            
            %% Define leaky input
            [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile( ...
                [0.0, signalDuration / duration], ...
                [signalDuration / duration, 1.0], ...
                [signal, leakiness], 10*duration);
            t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
            model.timesMap('Phyb_ON') = t_Phyb_ON;
            model.speciesMap('Phyb_ON') = v_Phyb_ON;
            
            %% Insert parameters
            model.paramsSet('k_Bxb1') = k;
            model.paramsSet('d_Bxb1') = d;
            model.paramsSet('d_DBxb1') = d / 5;
            
            %% Simulate system
            deterministicSimulation(tspan, model);
            
            %% Retrieve results
            meanPout = model.speciesMap('Pout_flipped');
            S(i, j) = nM2n(meanPout(end)) / 15 * 100;
            
            %% Plot colormap
            clf;
            xticklabels = {'10^{-3}', '10^{-2}', '10^{-1}', '10^{0}', '10^{1}'};
            xticks = [smoothing/2, smoothing*numel(d_range)/4*1, ...
                smoothing*numel(d_range)/4*2, smoothing*numel(d_range)/4*3, ...
                smoothing*numel(d_range)-smoothing/2];
            yticklabels = {'10^{-3}', '10^{-2}', '10^{-1}', '10^{0}', '10^{1}'};
            yticks = [smoothing/2, smoothing*numel(d_range)/4*1, ...
                smoothing*numel(d_range)/4*2, smoothing*numel(d_range)/4*3, ...
                smoothing*numel(d_range)-smoothing/2];
            im = flipud(interp2(D, K, S, D_fine, K_fine));
            imagesc(im);
            hold all;
            contour(im, [1, 1] * minFlipping, 'w', 'linewidth', 3);
            set(gca, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(gca, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            set(gca,'FontSize', 14);
            xlabel('Integrase degradation rate [min^{-1}]');
            ylabel('Integrase RBS strength [min^{-1}]');
            title(sprintf( ...
                'Fraction of flipped promoters after %.0fh exposure', signalDuration/60));
            hcb = colorbar;
            set(hcb, 'yticklabel', toPercentage(get(hcb,'YTick')));
            drawnow;
        end
    end
    
    SI = interp2(D, K, S, D_fine, K_fine);
    OK = SI >= minFlipping;
    ratios = K_fine ./ D_fine;
    minRatio = min(ratios(OK));
    fprintf('Min k/d ratio: %f\n', minRatio);
end

function [maxRatio, OK] = leakinessResponse(model, duration, leakiness)
    % Plot a color map showing the effects of leakiness
    model.paramsSet = loadParameters(model.paramsSet);
    
    %% Define parameter rages
    interval = [0.001, 10];
    tspan = [0, duration];
    nPoints = 20;
    smoothing = 50;
    finePoints = nPoints * smoothing;
    maxFlipping = 80;
    k_range = logspace(log10(interval(1)), log10(interval(2)), nPoints);
    d_range = logspace(log10(interval(1)), log10(interval(2)), nPoints);
    [D, K] = meshgrid(d_range, k_range);
    k_fine = logspace(log10(interval(1)), log10(interval(2)), finePoints);
    d_fine = logspace(log10(interval(1)), log10(interval(2)), finePoints);
    [D_fine, K_fine] = meshgrid(d_fine, k_fine);
    S = zeros(length(k_range), length(d_range));
    
    %% Test different combinations of parameters
    newFigure(sprintf( ...
        'Fraction of flipped promoters after %.0fh of leakiness', duration/60));
    colormap(jet);
    for i=1:length(k_range)
        for j=1:length(d_range)
            
            k = k_range(i);
            d = d_range(j);
            
            %% Define leaky input
            [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(...
                0.0, 1.0, leakiness, 100);
            t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
            model.timesMap('Phyb_ON') = t_Phyb_ON;
            model.speciesMap('Phyb_ON') = v_Phyb_ON;
            
            %% Insert parameters
            model.paramsSet('k_Bxb1') = k;
            model.paramsSet('d_Bxb1') = d;
            model.paramsSet('d_DBxb1') = d / 5;
            
            %% Simulate system
            deterministicSimulation(tspan, model);
            
            %% Retrieve results
            meanPout = model.speciesMap('Pout_flipped');
            S(i, j) = nM2n(meanPout(end)) / 15 * 100;
            
            %% Plot colormap
            clf;
            xticklabels = {'10^{-3}', '10^{-2}', '10^{-1}', '10^{0}', '10^{1}'};
            xticks = [smoothing/2, smoothing*numel(d_range)/4*1, ...
                smoothing*numel(d_range)/4*2, smoothing*numel(d_range)/4*3, ...
                smoothing*numel(d_range)-smoothing/2];
            yticklabels = {'10^{-3}', '10^{-2}', '10^{-1}', '10^{0}', '10^{1}'};
            yticks = [smoothing/2, smoothing*numel(d_range)/4*1, ...
                smoothing*numel(d_range)/4*2, smoothing*numel(d_range)/4*3, ...
                smoothing*numel(d_range)-smoothing/2];
            im = flipud(interp2(D, K, S, D_fine, K_fine));
            imagesc(im);
            hold all;
            contour(im, [1, 1] * maxFlipping, 'w', 'linewidth', 3);
            set(gca, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(gca, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            set(gca, 'FontSize', 14);
            xlabel('Integrase degradation rate [min^{-1}]');
            ylabel('Integrase RBS strength [min^{-1}]');
            title(sprintf( ...
                'Fraction of flipped promoters after %.0fh of leakiness', duration/60));
            hcb = colorbar;
            set(hcb, 'yticklabel', toPercentage(get(hcb,'YTick')));
            drawnow;
        end
    end
   
    SI = interp2(D, K, S, D_fine, K_fine);
    NOK = SI > maxFlipping;
    OK = SI <= maxFlipping;
    ratios = K_fine ./ D_fine;
    maxRatio = min(ratios(NOK));
    fprintf('Max k/d ratio: %f\n', maxRatio);
end

function degradationDynamics(model, ratio)
% Plot the influence of the degradation rate on the dynamics of
% the system.

    model.paramsSet = loadParameters(model.paramsSet);
    
    %% Define parameter rages
    interval = [0.001, 1];
    signal = 1.0;
    tspan = 0:0.2:(16*60);
    nPoints = 10;
    d_range = logspace(log10(interval(1)), log10(interval(2)), nPoints);
    
    %% Test different combinations of parameters
    newFigure('Steady state promoter flipping');
    setColorPalette([0, 1], 'black', length(d_range));
    for i=1:length(d_range)
        d = d_range(i);
        k = ratio * d;
        
        ST = 0:8;
        SR = zeros(size(ST));
        
        for j=1:8
            
            %% Define input
            [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(...
                0.0, ST(j+1)/16, signal, 100);
            t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
            model.timesMap('Phyb_ON') = t_Phyb_ON;
            model.speciesMap('Phyb_ON') = v_Phyb_ON;

            %% Insert parameters
            model.paramsSet('k_Bxb1') = k;
            model.paramsSet('d_Bxb1') = d;
            model.paramsSet('d_DBxb1') = d / 5;

            %% Simulate system
            deterministicSimulation(tspan, model);

            %% Retrieve results
            x = nM2n(model.speciesMap('Pout_flipped')) / 15 * 100;
            SR(j+1) = x(end);
        end
            
        %% Plot curve
        plot(ST, SR, 'linewidth', 2);
        drawnow;
    end
    xlabel('Exposure time [h]');
    ylabel('Flipping of the reporter promoters');
    set(gca, 'yticklabel', toPercentage(get(gca,'YTick')));
    legend(cellstr(num2str(d_range', 3)));
end

function compareVersions(model1, model2)
% Compare behavior of two different placements of the integrase gene

    figureSize = [100, 100, 680, 340];

    %% Input: Phyb activity
    tspan = 0:1:(6*60);
    stimulus_1 = 0.1;   % Assume leakiness
    stimulus_2 = 1;   % Assume all promoters active
    [t_Phyb_ON_1, v_Phyb_ON_1] = makeStimulusProfile(0.0, 1.0, stimulus_1, 100);
    [t_Phyb_ON_2, v_Phyb_ON_2] = makeStimulusProfile(0.0, 1.0, stimulus_2, 100);
    t_Phyb_ON_1 = t_Phyb_ON_1 * (tspan(end) - tspan(1)) + tspan(1);
    t_Phyb_ON_2 = t_Phyb_ON_2 * (tspan(end) - tspan(1)) + tspan(1);
    
    %% Set leakiness conditions
    model1.timesMap('Phyb_ON') = t_Phyb_ON_1;
    model1.speciesMap('Phyb_ON') = v_Phyb_ON_1;
    model2.timesMap('Phyb_ON') = t_Phyb_ON_1;
    model2.speciesMap('Phyb_ON') = v_Phyb_ON_1;

    %% Run simulation on both models
    deterministicSimulation(tspan, model1);
    deterministicSimulation(tspan, model2);
    
    %% Plot leakiness behavior
    newFigure('Reporter promoter flipping under leakiness', figureSize);
    plot(model1.timesMap('Pout_flipped') / 60, ...
        nM2n(model1.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    plot(model2.timesMap('Pout_flipped') / 60, ...
        nM2n(model2.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    xlabel('Time [h]');
    ylabel('Percentage of flipped promoters');
    legend('Initial version - leakiness', 'Current version - leakiness', 'Location', 'southeast');
    set(gca, 'yticklabel', toPercentage(get(gca,'YTick')));
      
    newFigure('Reporter promoter flipping with different designs', figureSize);
    plot(model1.timesMap('Pout_flipped') / 60, ...
        nM2n(model1.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    plot(model2.timesMap('Pout_flipped') / 60, ...
        nM2n(model2.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    xlabel('Time [h]');
    ylabel('Percentage of flipped promoters');
    set(gca, 'yticklabel', toPercentage(get(gca,'YTick')));
    
    %% Set signal conditions
    model1.timesMap('Phyb_ON') = t_Phyb_ON_2;
    model1.speciesMap('Phyb_ON') = v_Phyb_ON_2;
    model2.timesMap('Phyb_ON') = t_Phyb_ON_2;
    model2.speciesMap('Phyb_ON') = v_Phyb_ON_2;

    %% Run simulation on both models
    deterministicSimulation(tspan, model1);
    deterministicSimulation(tspan, model2);
    
    %% Plot signal behavior
    plot(model1.timesMap('Pout_flipped') / 60, ...
        nM2n(model1.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    plot(model2.timesMap('Pout_flipped') / 60, ...
        nM2n(model2.speciesMap('Pout_flipped')) / 15 * 100, 'linewidth', 2);
    xlabel('Time [h]');
    ylabel('Percentage of flipped promoters');
    legend('Initial version - leakiness', 'Current version - leakiness', ...
        'Initial version - activation', 'Current version - activation', 'Location', 'southeast');
    set(gca, 'yticklabel', toPercentage(get(gca,'YTick')));
    
end

function compareORIs(model)
% Comparison of the qalitative behavior with two different ORIs

    %% Input: Phyb activity
    tspan = 0:1:(6*60);
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.0, 1.0, 1.0, 100);
    t_Phyb_ON = t_Phyb_ON * (tspan(end) - tspan(1)) + tspan(1);
    
    %% Setup models
    figureSize = [100, 100, 500, 240];
    model5 = createSwitchV2MAmodel(5);
    model5.timesMap('Phyb_ON') = t_Phyb_ON;
    model5.speciesMap('Phyb_ON') = v_Phyb_ON;
    model5.paramsSet = 'switch_bonnet';
    
    model15 = createSwitchV2MAmodel(15);
    model15.timesMap('Phyb_ON') = t_Phyb_ON;
    model15.speciesMap('Phyb_ON') = v_Phyb_ON;
    model15.paramsSet = 'switch_bonnet';
    
    %% Simulate system
    drawFunctions = containers.Map( ...
        {'Pout_flipped'}, ...
        { @(t, x) drawIn1x1Subplot(t / 60, x, 1, 1, 1, 0.4)} ...
    );

    newFigure('Switching of the reporter promoters on a XXX plasmid', figureSize);
    xlabel('Time [h]');
    ylabel('Number of flipped promoters');
    stochasticSimulationNRM(tspan, model5, 100, drawFunctions);
    grid on;
    
    newFigure('Switching of the reporter promoters on a XXX plasmid', figureSize);
    xlabel('Time [h]');
    ylabel('Number of flipped promoters');
    stochasticSimulationNRM(tspan, model15, 100, drawFunctions);
    grid on;
end

function drawIn1x1Subplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(1, 1, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, nM2n(x), 'linewidth', lineWidth);
    p.Color(4) = alpha;
end

function newFigure(titleText, figureSize)
    reset(groot);
    if ~exist('figureSize', 'var')
        figure();
    else
        figure('Position', figureSize);
    end
    clf;
    grid on;
    hold on;
    set(gca,'FontSize',14);
    title(titleText);
end

function ticks = toPercentage(values)
    a = cellstr(num2str(values', 3));
    pct = char(ones(size(a,1),1) * '%'); 
    ticks = [char(a), pct];
end