function model = createAndGateAHLpartMAfullmodel()
%CREATEANDGATEFULLMAMODEL Defines a mass action model of the AHL PART OF THE GATE.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%   
%   V1: The Bxb1 gene is placed outside the flipping cassette
%
%       model = createSwitchV1MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0,0,0,0,pnortot,0,0,0,0,0,0,0,0,0,0,0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'DEsaR','DEsar_AHL','AHL','DNoR_NO'};
    
    %% Define symbolic variables for species and parameters
    syms DEsaR DEsar_AHL  AHL   DNoR_NO...
         PnorV0_Pfree  PnorV1_Pfree  PnorV2_Pfree  PnorV3_Pfree ...
         PnorV0_Pesar  PnorV1_Pesar  PnorV2_Pesar  PnorV3_Pesar ...
         PnorV0_Pesar1  PnorV1_Pesar1  PnorV2_Pesar1  PnorV3_Pesar1 ... 
         
    syms k7 k_7 knorV1 k_norV1 knorV2 k_norV2 knorV3 k_norV3
    
    model.specieNames = [ DEsaR DEsar_AHL  AHL   DNoR_NO...
                         PnorV0_Pfree  PnorV1_Pfree  PnorV2_Pfree  PnorV3_Pfree ...
                         PnorV0_Pesar  PnorV1_Pesar  PnorV2_Pesar  PnorV3_Pesar ...
                         PnorV0_Pesar1  PnorV1_Pesar1  PnorV2_Pesar1  PnorV3_Pesar1 ... 
                         ];
    model.paramNames = [k7 k_7 knorV1 k_norV1 knorV2 k_norV2 knorV3 k_norV3]; 
    
    %% Stoichiometric and rate matrices
    % PnorV0_Pfree | PnorV1_Pfree | PnorV2_Pfree | PnorV3_Pfree |
    % PnorV0_Pesar | PnorV1_Pesar | PnorV2_Pesar | PnorV3_Pesar |
    % PnorV0_Pesar1 | PnorV1_Pesar1 | PnorV2_Pesar1 | PnorV3_Pesar | 
    %   DEsaR       | DEsar_AHL    | AHL        | DNoR_NO
    model.N = [ 
          %1  %2  %3  %4  %5  %6  %7  %8  %9  %10 %11 %12 %13  %14 %15 %16
          -1,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0;  % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar
           1,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0;
         
           0, -1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1
           0,  1,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
            0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0;
         
         %%
            0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0; % PnorV0_Pfree + NO <-> PnorV1_Pfree
            0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0; % PnorV0_Pesar + NO <-> PnorV1_Pesar
            0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0 ;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0; % PnorV0_Pesar1 + NO <-> PnorV1_Pesar1
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0;
         
         %%
           -1,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0; % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
            1,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0 ;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0 ; % PnorV1_Pesar + AHL <-> PnorV1_Pesar1
            0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0;
         
          0,  1,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0  ; % PnorV1_Pesar1  <-> PnorV1_Pfree + DEsar_AHL
          0, -1,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0 ;
         
         %%
            0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0; % PnorV1_Pfree + NO <-> PnorV2_Pfree
            0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0; % PnorV1_PseaR + NO <-> PnorV2_Pesar
            0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0; % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0 ;
         
         %%
          -1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0,  0 ; % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
           1,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0; % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0;
         
          0,  1,  0,  0,   0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1,  0 ; % PnorV2_Pesar1  <-> PnorV2_Pfree + DEsar_AHL
          0, -1,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1,  0 ;
         
         %%
            0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0,  0,  0,  0,  0; % PnorV2_Pfree + NO <-> PnorV3_Pfree
            0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0,  0,  0,  0,  0;
         
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1,  0,  0,  0,  0  ; % PnorV2_Pesar + NO <-> PnorV3_Pesar
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1,  0,  0,  0,  0;
         
           0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  1   ; % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
           0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1, -1 ;
         
         %%
          -1,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1,  0,  0,  0,  0 ; % PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
           1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1,  0,  0,  0,  0 ;
         
           0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, -1,  0,  0,  0,  1 ; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
           0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0, -1 ;
         
          0,  1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0, -1  ; % PnorV3_Pesar1  <-> PnorV3_Pfree + DEsar_AHL
          0, -1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  1;
         
     ]';
    
    %% Reaction rates
    model.rates = [
          k_7*PnorV0_Pfree*DEsaR; % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar 
          k7*PnorV0_Pesar;
          
          k_7*PnorV0_Pfree*DEsar_AHL; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1 +AHL
          k7*PnorV0_Pesar1*AHL;
          
          k7*PnorV0_Pesar*AHL; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
          k_7*PnorV0_Pesar1;
          
          knorV1*PnorV0_Pfree*DNoR_NO; % PnorV0_Pfree + NO <-> PnorV1_Pfree
          k_norV1*PnorV1_Pfree;
          
          knorV1*PnorV0_Pesar*DNoR_NO;% PnorV0_Pesar + NO <-> PnorV1_Pesar
          k_norV1*PnorV1_Pesar;
          
          knorV1*PnorV0_Pesar1*DNoR_NO;% PnorV0_Pesar1 + NO <-> PnorV1_Pesar1
          k_norV1*PnorV1_Pesar1;
          
          k7*PnorV1_Pfree*DEsaR; % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
          k_7*PnorV1_Pesar;
          
          k7*PnorV1_Pesar*AHL;% PnorV1_Pesar + AHL <-> PnorV1_Pesar1
          k_7*PnorV1_Pesar1;
          
          k7*PnorV1_Pesar1*AHL;% PnorV1_Pesar1 +AHL <-> PnorV1_Pfree + DEsar_AHL
          k_7*PnorV1_Pfree*DEsar_AHL;
          
          knorV2*PnorV1_Pfree*DNoR_NO; % PnorV1_Pfree + NO <-> PnorV2_Pfree
          k_norV2*PnorV2_Pfree;
          
          knorV2*PnorV1_Pesar*DNoR_NO % PnorV1_PseaR + NO <-> PnorV2_Pesar
          k_norV2*PnorV2_Pesar;
          
          knorV2*PnorV1_Pesar1*DNoR_NO; % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
          k_norV2*PnorV2_Pesar1;
          
          k7*PnorV2_Pfree*DEsaR; % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
          k_7*PnorV2_Pesar;
          
          k7*PnorV2_Pesar*AHL; % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
          k_7*PnorV2_Pesar1;
          
          k7*PnorV2_Pesar1*AHL ; % PnorV2_Pesar1 + AHL  <-> PnorV2_Pfree + DEsar_AHL
          k_7*PnorV2_Pfree*DEsar_AHL;
          
          knorV3*PnorV2_Pfree*DNoR_NO; % PnorV2_Pfree + NO <-> PnorV3_Pfree
          k_norV3*PnorV3_Pfree;
          
          knorV3*PnorV2_Pesar*DNoR_NO; % PnorV2_Pesar + NO <-> PnorV3_Pesar
          k_norV3*PnorV3_Pesar;
          
          knorV3*PnorV2_Pesar1*DNoR_NO; % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
          k_norV3*PnorV3_Pesar1;
          
          k7*PnorV3_Pfree*DEsaR% PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
          k_7*PnorV3_Pesar;
          
          k7*PnorV3_Pesar*AHL; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
          k_7*PnorV3_Pesar1;
          
          k7*PnorV3_Pesar1*AHL; % PnorV3_Pesar1 + AHL <-> PnorV3_Pfree + DEsar_AHL
          k_7*PnorV3_Pfree*DEsar_AHL;
    ];

    %% Reaction propensities
    model.a = [
          n2nM(k_7)*PnorV0_Pfree*DEsaR; % PnorV0_Pfree + DEsaR <-> PnorV0_Pesar 
          k7*PnorV0_Pesar;
          
          n2nM(k_7)*PnorV0_Pfree*DEsar_AHL; % PnorV0_Pfree + DEsar_AHL <-> PnorV0_Pesar1 +AHL
          n2nM(k7)*PnorV0_Pesar1*AHL;
          
          n2nM(k7)*PnorV0_Pesar*AHL; % PnorV0_Pesar + AHL <-> PnorV0_Pesar1
          k_7*PnorV0_Pesar1;
          
          n2nM(knorV1)*PnorV0_Pfree*DNoR_NO; % PnorV0_Pfree + NO <-> PnorV1_Pfree
          k_norV1*PnorV1_Pfree;
          
          n2nM(knorV1)*PnorV0_Pesar*DNoR_NO;% PnorV0_Pesar + NO <-> PnorV1_Pesar
          k_norV1*PnorV1_Pesar;
          
          n2nM(knorV1)*PnorV0_Pesar1*DNoR_NO;% PnorV0_Pesar1 + NO <-> PnorV1_Pesar1
          k_norV1*PnorV1_Pesar1;
          
          n2nM(k7)*PnorV1_Pfree*DEsaR; % PnorV1_Pfree + DEsaR <-> PnorV1_Pesar
          k_7*PnorV1_Pesar;
          
          n2nM(k7)*PnorV1_Pesar*AHL;% PnorV1_Pesar + AHL <-> PnorV1_Pesar1
          k_7*PnorV1_Pesar1;
          
          n2nM(k7)*PnorV1_Pesar1*AHL;% PnorV1_Pesar1 +AHL <-> PnorV1_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV1_Pfree*DEsar_AHL;
          
          n2nM(knorV2)*PnorV1_Pfree*DNoR_NO; % PnorV1_Pfree + NO <-> PnorV2_Pfree
          k_norV2*PnorV2_Pfree;
          
          n2nM(knorV2)*PnorV1_Pesar*DNoR_NO % PnorV1_PseaR + NO <-> PnorV2_Pesar
          k_norV2*PnorV2_Pesar;
          
          n2nM(knorV2)*PnorV1_Pesar1*DNoR_NO; % PnorV1_PseaR1 + NO <-> PnorV2_Pesar1
          k_norV2*PnorV2_Pesar1;
          
          n2nM(k7)*PnorV2_Pfree*DEsaR; % PnorV2_Pfree + DEsaR <-> PnorV2_Pesar
          k_7*PnorV2_Pesar;
          
          n2nM(k7)*PnorV2_Pesar*AHL; % PnorV2_Pesar + AHL <-> PnorV2_Pesar1
          k_7*PnorV2_Pesar1;
          
          n2nM(k7)*PnorV2_Pesar1*AHL ; % PnorV2_Pesar1 + AHL  <-> PnorV2_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV2_Pfree*DEsar_AHL;
          
          n2nM(knorV3)*PnorV2_Pfree*DNoR_NO; % PnorV2_Pfree + NO <-> PnorV3_Pfree
          k_norV3*PnorV3_Pfree;
          
          n2nM(knorV3)*PnorV2_Pesar*DNoR_NO; % PnorV2_Pesar + NO <-> PnorV3_Pesar
          k_norV3*PnorV3_Pesar;
          
          n2nM(knorV3)*PnorV2_Pesar1*DNoR_NO; % PnorV2_Pesar1 + NO <-> PnorV3_Pesar1
          k_norV3*PnorV3_Pesar1;
          
          n2nM(k7)*PnorV3_Pfree*DEsaR% PnorV3_Pfree + DEsaR <-> PnorV3_Pesar
          k_7*PnorV3_Pesar;
          
          n2nM(k7)*PnorV3_Pesar*AHL; % PnorV3_Pesar + AHL <-> PnorV_Pesar1
          k_7*PnorV3_Pesar1;
          
          n2nM(k7)*PnorV3_Pesar1*AHL; % PnorV3_Pesar1 + AHL <-> PnorV3_Pfree + DEsar_AHL
          n2nM(k_7)*PnorV3_Pfree*DEsar_AHL;
    ];
end