function [] = HeatMapAndGate(model, NOrange, AHLRange)
 

%% Parameters
%clear;
% Common constants
nRuns = 500;
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*6;
tspan = t0:ts:tf;
lineWidth = 1.5;
M=zeros(length(NOrange), length(AHLrange));

%%
parfor i=1:length(NOrange)
    
      points = 100;
        NO_stimulus = NOrange(i) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_NO_ON = ceil((tf-t0)*0.2/tf*points);
        t_NO_OFF = floor((tf-t0)*1/tf*points);
        t_NO = (0:points)/points*(tf-t0) + t0;
        NO = zeros(size(t_NO));
        NO(1, t_NO_ON:t_input_OFF) = NO_stimulus;
        
        
    for j= 1:length(AHLrange)
        
        [i,j]
        NOrange(i)
        AHLrange(j)

      

         AHL_stimulus = AHLrange(j) * 1e9 / (Na * V);    % Assume ~500 mRNAs
        t_AHL_ON = ceil((tf-t0)*0.1/tf*points);
        t_AHL_OFF = floor((tf-t0)*0.2/tf*points);
        t_AHL = (0:points)/points*(tf-t0) + t0;
        AHL = zeros(size(t_AHL));
        AHL(1, t_AHL_ON:t_AHL_OFF) = AHL_stimulus;
        
        T = model.timesMap;
        S = model.speciesMap;

        T('NO') = t_NO;
        S('NO') = NO;

        T('AHL') = t_AHL;
        S('AHL') = AHL;
        deterministicSimulation([t0, tf], model);
        M(i,j)=max(S('Phyb'));
    
    end
end

    figure;
    surf(NOrange,AHLrange, M);
    view(2);
    axis equal;
    xlabel NO
    ylabel AHL
    zlabel Phyb
    title (' heatmap for Phyb (AHD Gate) as a function of the 2 input')