function switchActivityResponse2D()
% SWITCHACTIVITYRESPONSE Response of the switch to inputs. 
%   Simulates the concentration of the reporters with different activity
%   levels of the hybrid promoter.
%   
    %% Environment setup
    clear;
    
    %% Simulation settings
    nRuns = 1000;

    % Dose inputs
    Phyb_ON_levels = n2nM(0:0.1:1);
    Phyb_ON_durations = 0:10:120;
    
    % Template input 1: Phyb activity
    Phyb_states = [
        0.0, 0.0, 1.0, 1.0, 0.0,  0.0;
        0,   9.8, 9.9, 10,  10.1, 160.0];

    % Template input 2: Pout activity
    Pout_states = [
        0.0, 0.0,   1.0,    1.0;
        0,   99.9, 100.0,  160.0];
    
    %% Setup model
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], ...
        switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;
    
    %% Setup storage for the results
    meansGFP = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    meansmNect = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    stdsGFP = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    stdsmNect = zeros(length(Phyb_ON_levels), length(Phyb_ON_durations));
    
    %% Run simulation with different activity levels
    for l=1:length(Phyb_ON_levels)
        for t=1:length(Phyb_ON_durations)
            
            %% Setup inputs
            v_Phyb_ON = Phyb_states(1,:) * Phyb_ON_levels(l);
            t_Phyb_ON = [Phyb_states(2,1:3), ...
                Phyb_states(2,4:end) + Phyb_ON_durations(t)];
            v_Pout_free = Pout_states(1,:) * n2nM(1);
            t_Pout_free = [Phyb_states(2,1), ...
                Pout_states(2,2:end) + Phyb_ON_durations(t)];
    
            T('Phyb_ON') = t_Phyb_ON;
            S('Phyb_ON') = v_Phyb_ON;
            T('Pout_free') = t_Pout_free;
            S('Pout_free') = v_Pout_free;

            %% Simulate system
            tspan = (0:0.01:1) * t_Pout_free(end);
            stochasticSimulationTDI(tspan, model, nRuns, l*t <= 1);
            
            %% Extract statistics from the simulation
            histGFP = model.histogramsMap('GFP');
            histmNect = model.histogramsMap('mNect');
            meanGFP = mean(histGFP, 2);
            stdGFP = std(histGFP, 0, 2);
            meanmNect = mean(histmNect, 2);
            stdmNect = std(histmNect, 0, 2);
            
            meansGFP(l, t) = meanGFP(end);
            stdsGFP(l, t) = stdGFP(end);
            meansmNect(l, t) = meanmNect(end);
            stdsmNect(l, t) = stdmNect(end);
            
            %% Plot temporary result
            figure(1);
            clf;
            ax = subplot(3, 1, 1);
            hold on;
            ax.ColorOrderIndex = 5;
            errorbar(tspan, meanGFP, stdGFP);
            ax.ColorOrderIndex = 2;
            errorbar(tspan, meanmNect, stdmNect);
            grid on;
            title(sprintf( ...
                'Reporter proteins expression (Phyb_{ON} = %d, duration = %d min)', ...
                Phyb_ON_levels(l), ...
                Phyb_ON_durations(t)));
            xlabel('Time [min]');
            ylabel('Number of molecules');
            legend('GFP', 'mNect');
           
            subplot(3, 1, 2);
            histsGFP = model.histogramsMap('GFP');
            histogram(histsGFP(end, :), 40);
            title(sprintf('GFP distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');

            subplot(3, 1, 3);
            histsmNect = model.histogramsMap('mNect');
            histogram(histsmNect(end, :), 40);
            title(sprintf('mNectarine distribution at t=%d min', tspan(end)));
            xlabel('Number of molecules');
            ylabel('Number of cells');
            
            %% Plot heatmaps
            figure(2);
            clf;
            colormap('hot');
            
            xticklabels = Phyb_ON_durations;
            xticks = linspace(1, length(Phyb_ON_durations), numel(Phyb_ON_durations));
            yticklabels = nM2n(Phyb_ON_levels);
            yticks = linspace(1, length(Phyb_ON_levels), numel(Phyb_ON_levels));
            
            ax = subplot(2, 2, 1);
            imagesc(flipud(meansGFP));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('GFP mean number of molecules');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 2);
            imagesc(flipud(meansmNect));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('mNectarine mean number of molecules');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 3);
            imagesc(flipud(stdsGFP));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('GFP standard deviation');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
            
            ax = subplot(2, 2, 4);
            imagesc(flipud(stdsmNect));
            set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
            set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
            title('mNectarine standard deviation');
            xlabel('Phyb_{ON} activity duration');
            ylabel('Phyb_{ON} activity intensity');
            colorbar;
          
            drawnow;
        end
    end
end