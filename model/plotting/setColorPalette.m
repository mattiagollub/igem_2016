function setColorPalette(range, varargin)
% SETCOLORPALETTE Set the color palette for the current figure.
%
%       setColorPalette(varargin)
%
%	Optional:
%       varargin:       Cell array containing the colors to be used in the
%                       color palette and their number of shades.
%
    
    %% Define color sets
    colors = zeros(8, 5, 3);
    
    % Primary color (blue)
    colors(1,:,:) = [
        0.027,0.129,0.306 
        0.773,0.859,1 
        0.282,0.525,0.941 
        0.024,0.216,0.549 
        0.02,0.071,0.153 
    ];

    % Secondary color 1 (red)
    colors(2,:,:) = [
        0.447,0,0.043 
        1,0.749,0.773 
        0.996,0.224,0.298 
        0.8,0,0.075 
        0.22,0.012,0.031 
    ];

    % Secondary color 2 (green)
    colors(3,:,:) = [
        0.067,0.384,0 
        0.792,1,0.749 
        0.349,0.973,0.22 
        0.122,0.69,0 
        0.039,0.192,0.008 
    ];

    % Complementary color (orange)
    colors(4,:,:) = [
        0.463,0.298,0 
        1,0.91,0.749 
        1,0.722,0.224 
        0.827,0.533,0 
        0.227,0.149,0.012 
    ];

    % Second primary color (pink)
    colors(5,:,:) = [
        0.365,0,0.22 
        1,0.808,0.925 
        0.965,0.259,0.686 
        0.671,0,0.404 
        0.196,0.024,0.129 
    ];

    % Second secondary color 1 (brown)
    colors(6,:,:) = [
        0.463,0.2,0 
        1,0.89,0.808 
        1,0.584,0.267 
        0.851,0.369,0 
        0.251,0.125,0.027 
    ];

    % Second secondary color 2 (petrol)
    colors(7,:,:) = [
        0,0.286,0.263 
        0.808,1,0.984 
        0.251,0.933,0.882 
        0,0.522,0.482 
        0.016,0.153,0.145
    ];

    % Second complementary color (olive)
    colors(8,:,:) = [
        0.298,0.435,0 
        0.941,1,0.808 
        0.765,0.988,0.263 
        0.549,0.796,0 
        0.169,0.235,0.027 
    ];

    % Black
    colors(9,:,:) = [
        0.1, 0.1, 0.1 
        0.9, 0.9, 0.9 
        0.7, 0.7, 0.7 
        0.4, 0.4, 0.4 
        0, 0, 0 
    ];

    %% Pick selected color scheme
    if isempty(varargin)
        set(groot, 'defaultAxesColorOrder', squeeze(colors(:,4,:)));
    end
    
    R = [];
    G = [];
    B = [];
    
    for i=1:2:length(varargin)
        colorName = varargin{i};
        numShades = varargin{i+1};
        
        if strcmp(colorName, 'blue')
            selectedColors = colors(1,2:5,:);
        elseif strcmp(colorName, 'red')
            selectedColors = colors(2,2:5,:);
        elseif strcmp(colorName, 'green')
            selectedColors = colors(3,2:5,:);
        elseif strcmp(colorName, 'orange')
            selectedColors = colors(4,2:5,:);
        elseif strcmp(colorName, 'pink')
            selectedColors = colors(5,2:5,:);
        elseif strcmp(colorName, 'brown')
            selectedColors = colors(6,2:5,:);
        elseif strcmp(colorName, 'petrol')
            selectedColors = colors(7,2:5,:);
        elseif strcmp(colorName, 'olive')
            selectedColors = colors(8,2:5,:);
        elseif strcmp(colorName, 'black')
            selectedColors = colors(9,2:5,:);
        else
            error('Invalid color name.');
        end
        selectedColors = squeeze(selectedColors);

        %% Interpolate missing shades
        basePoints = linspace(0, 1, length(selectedColors));
        targetPoints = linspace(range(1), range(2), numShades);
        r = min(max(spline(basePoints, selectedColors(:,1), targetPoints), 0), 1);
        g = min(max(spline(basePoints, selectedColors(:,2), targetPoints), 0), 1);
        b = min(max(spline(basePoints, selectedColors(:,3), targetPoints), 0), 1);
        
        R = [R, r];
        G = [G, g];
        B = [B, b];
    end
    
    %% Set selected color to the current figure
    R = [R, 0];
    G = [G, 0];
    B = [B, 0];
    % set(groot, 'ColorOrder', [R; G; B]');
    set(gca, 'ColorOrder', [R; G; B]');
end