function plotLine(data, name, wells, plotStd)
% PLOTLINE Add a single line to the plot, averaging from multiple wells if
%   necessary.
%
%       plotLine(data, name, wells, plotStd)
%
%	Required:
%       data:           Data of the experiment, as returned by
%                       loadPlateReaderData()
%       name:           Name of the plotted line
%       wells:          Cell array containing the names of the wells
%
%   Optional:
%       plotStd:        If true, plots the standard deviations together
%                       with the data.
%

    %% Set default number of shades
    if ~exist('plotStd', 'var')
        plotStd = false;
    end
    %% Get line data
    wellIds = zeros(size(wells));
    for i=1:length(wellIds)
        wellIds(i) = data.labelsMap(wells{i});
    end
    
    %% Plot data
    times = data.data(1,:) / 3600;
    wellsMean = mean(data.data(wellIds, :), 1);
    wellsStd = std(data.data(wellIds, :), 1);
    plot(times, wellsMean, 'DisplayName', name, 'linewidth', 2);
    if plotStd
        half = ceil(length(times)/2);
        ax = gca;
        ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
        h = errorbar(times(1:half-1), wellsMean(1:half-1), wellsStd(1:half-1), ...
            '.', 'linewidth', 1);
        set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        ax.ColorOrderIndex = ax.ColorOrderIndex - 1;
        h = errorbar(times(half:end), wellsMean(half:end), wellsStd(half:end), ...
            '.', 'linewidth', 1);
        set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end
    
end