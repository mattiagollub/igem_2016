data = loadPlateReaderData('test.xlsx', {'H12'});

setColorPalette([0, 0.95], 'blue', 5);
newPlot('Test plot', 'time [h]', 'fluorescence/OD [au]');
plotLine(data, 'p16+p87 0nM', {'A1', 'B1', 'C1'}, true);
plotLine(data, 'p16+p87 1nM', {'A2', 'B2', 'A2'}, true);
plotLine(data, 'p16+p87 10nM', {'A3', 'B3', 'C3'}, true);
plotLine(data, 'p16+p87 100nM', {'A4', 'B4', 'C4'}, true);
plotLine(data, 'p16+p87 1uM', {'A5', 'B5', 'C5'}, true);

legend('show');
set(gca, 'xscale', 'log');