function newPlot(figureTitle, xLabel, yLabel)
% NEWPLOT Create a new plot area.
%
%       newPlot(figureTitle, xLabel, yLabel)
%
%	Required:
%       figureTitle:    Title of the plot.
%       xLabel:         Label for the X axis
%       yLabel:         Label for the Y axis
%

    %% Create plot area
    figure(1);
    clf;
    title(figureTitle);
    xlabel(xLabel);
    ylabel(yLabel);
    grid on;
    hold on;
end