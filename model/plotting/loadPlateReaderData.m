function data = loadPlateReaderData(file, blankWells)
% LOADPLATEREADERDATA Load plate reader data from an excel file.
%
%       loadPlateReaderData(file, blankWells)
%
%	Required:
%       file:           Path to the excel file containing the plate reader
%                       measurements.
%       blankWells:     Cell array containing the 
%

    %% Load data regions from the excel file
    OD600 = xlsread(file, 'Sheet2', 'B53:CW150');
    Fl = xlsread(file, 'Sheet2', 'B154:CW251');
    [~, labels, ~] = xlsread(file, 'Sheet2', 'A53:A150');
    
    %% Map labels to rows
    labelsMap = containers.Map();
    
    for i=1:size(labels, 1)
        labelsMap(labels{i}) = i;
    end
    
    %% Subtract blank data
    blankIds = zeros(size(blankWells));
    for i=1:length(blankIds)
        blankIds(i) = labelsMap(blankWells{i});
    end
    
    OD600(3:end,:) = OD600(3:end,:) - ...
        ones(size(OD600, 1)-2, 1) * mean(OD600(blankIds, :), 1);
    Fl(3:end,:) = Fl(3:end,:) - ...
        ones(size(Fl, 1)-2, 1) * mean(Fl(blankIds, :), 1);
    
    %% Normalize and return data
    Fl(3:end,:) = Fl(3:end,:) ./ OD600(3:end,:);
    data.data = Fl;
    data.labelsMap = labelsMap;
end