function [B, tid] = integratePropensityTo(a, A, Y, t, x, t0, state)
% INTEGRATEPROPENSITY Integrate using trapezoidal rule until the desired result.
%   
    %% First trapezoid
    tid = t0;
    maxTid = length(t);
    a_tid = a([x(t0), state(2:end)]);
    a_tid2 = a([x(t0+1), state(2:end)]);
    area = (t(t0+1)-t(t0))/2*(a_tid+a_tid2);
    
    %% Remove start of the trapezoid
    x_ts = linInterp(t(tid), t(tid+1), x(tid), x(tid+1), A);
    a_ts = a([x_ts, state(2:end)]);
    area = area - (A-t(t0))/2*(a_tid+a_ts);
    
    %% Add trapezoids if necessary
    while area < Y
        tid = tid + 1;
        if tid >= maxTid
            B = t(maxTid) + 1;
            return;
        end
        a_tid = a_tid2;
        a_tid2 = a([x(tid+1), state(2:end)]);
        area = area + (t(tid+1)-t(tid))/2*(a_tid+a_tid2);
    end
    
    %% Subtract end of trapezoid (points with t > B)
    diff = area - Y;
    
    if a_tid2 - a_tid == 0
        dt = diff / a_tid2;
    elseif a_tid < a_tid2
        tmp_a = -(a_tid2 - a_tid) / ((t(tid+1)-t(tid)) * 2);
        dt = (-a_tid2 + sqrt(a_tid2^2 + 4*tmp_a*diff)) / (2*tmp_a);
    else
        tmp_a = (a_tid - a_tid2) / ((t(tid+1)-t(tid)) * 2);
        dt = (-a_tid2 + sqrt(a_tid2^2 + 4*tmp_a*diff)) / (2*tmp_a);
    end
    B = t(tid+1) - dt;
end