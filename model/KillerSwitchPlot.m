% SWITCHMAPLOT Example simulation of the switch. 
%   Deterministically simulates the concentration of the switch species
%   using an example input.
% 
%% Simulation settings
clear;
nRuns = 500;
addpath('mexFunctions');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range for the experiment
t0   = 0;
ts   = 30;
tf   = 24*60;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input: sigECF activity
 stimulus = 360;   % Assume all promoters active
    [t_sigECF, v_sigECF] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_sigECF = t_sigECF * (tf - t0) + t0;

%% Setup model
model = createCollaborationKillerSwitchModel();
T = model.timesMap;
S = model.speciesMap;

T('sigECF') = t_sigECF;
S('sigECF') = v_sigECF;
model.paramsSet = 'collaboration_killer_switch';
%model.paramsSet = 'andgate_eth_igem_2014';

% Histogram plotting
    histogramsX = 2;
    histogramsY = 2;
    nHistograms = histogramsX * histogramsY;

%% Plot results
figure;
clf;

subplot(2, 2, 1);
plot(T('sigECF'), S('sigECF'), 'linewidth', 2);
grid on;
title('Input: aTc concentration')
xlabel('Time [min]');
ylabel('inducer (aTc) nM)');
legend('inducer (aTc)');

drawFunctions = containers.Map( ...
        {'Esar', 'DEsar', 'DEsar_AHL1', 'DEsar_AHL', 'mRNAinv', 'Pdesar1_AHL', 'Pdesar1', 'Pfree', 'Pout'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 4, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 4, 1, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 3, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 4, lineWidth, 0.2), ...
        });

%% Run simulation
%stochasticSimulationNRM(tspan, model, nRuns);
deterministicSimulation([t0, tf], model);
% Invertase mRNA production
subplot(2, 2, 2);
hold on;
plot(T('MazE'), S('MazE'), 'linewidth', 2);
hold on;
plot(T('DMazF'), S('DMazF'), 'linewidth', 2);
hold on;
plot(T('DMazF_E2'), S('DMazF_E2'), 'linewidth', 2);
hold on;
plot(T('antiMaz'), S('antiMaz'), 'linewidth', 2);
grid on;
title('System MazE/F overview');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('MazE' ,'DMazF','DMazF_E2','antiMaz');

% Bxb1 dimerization
subplot(2, 2, 3);
hold on;
%plot(T('antiMaz'), S('antiMaz'), 'linewidth', 2);
plot(T('Pconst'), S('Pconst'), 'linewidth', 2);
plot(T('Pblock'), S('Pblock'), 'linewidth', 2);
grid on;
title('mazE/F Promoter state ');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('Pconst', 'Pblock'); 

% Pout flipping
% subplot(2, 2, 4);
% hold on;
% semilogy(T('cfu'), S('cfu'), 'linewidth', 2);
% grid on;
% title('cfu');
% xlabel('Time [min]');
% ylabel('cfu');

figure;
plot(T('mRNAmazE'), S('mRNAmazE'), 'linewidth', 2);
hold on;
plot(T('mRNAmazF'), S('mRNAmazF'), 'linewidth', 2);
legend('mRNAmazE', 'mRNAmazF');