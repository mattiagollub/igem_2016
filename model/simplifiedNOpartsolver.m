clear P DNoR_NO Km PnorV3 k2 NO DNoR k4 NoR_NO ksum k_3 k3 k_4 k1 NoR k_1 nno Kmno Kno Ptot
syms P DNoR_NO Km PnorV3 k2 NO DNoR k4 NoR_NO ksum k_3 k3 k_4 k1 NoR k_1 nno Kmno Kno Ptot
eqns = (NoR_NO^2)+ (k_3/k4)*NoR_NO -(k3/k4)*NoR*NO -(k_4/k4)*DNoR_NO==0,NoR_NO ;
S = solve(eqns)
S1=simplify(S)
% [ DNoR_NO-((k4/ksum)*NoR_NO)-((k2/ksum)*(NO^2)*DNoR) == 0 , ...
%          DNoR-((k1/(k_1-k2*(NO^2)))*(NoR^2))-((k_4*DNoR_NO)/(k_1-k2*(NO^2))), ...

eqn=(DNoR_NO-((k4/ksum)*(Km+0.5*sqrt(k1+k2*NoR*NO*DNoR_NO))-((k2/ksum)*(NO^2)*((k1/(k_1-k2*(NO^2)))*(NoR^2))-((k_4*DNoR_NO)/(k_1-k2*(NO^2)))) == 0) ;
S1=simplify(eqn)

eqns=[P*((DNoR_NO)^3)/(Km + (DNoR_NO)^3),...
      DNoR_NO-((k4/ksum)*NoR_NO)-((k2/ksum)*(NO^2)*((k1/(k_1-k2*(NO^2)))*(NoR^2))-((k_4*DNoR_NO)/(k_1-k2*(NO^2)))) == 0,...
      (NoR_NO^2)+ (k_3/k4)*NoR_NO -(k3/k4)*NoR*NO -(k_4/k4)*DNoR_NO==0,NoR_NO ];

 S = solve(eqns,DNoR_NO)
 
 
 
 p=[k4 k_3/k4 -(k3/k4)*NoR*NO -(k_4/k4)*DNoR_NO];
 r = root(p);
 
eqn=NoR_NO==Km+0.5*sqrt(k1+k2*NoR*NO*DNoR_NO) ;
S = solve(eqn,V)
S1=simplify(S)

 eqn=DNoR_NO ==((k4/ksum)*(Km+0.5*sqrt(k1+k2*NoR*NO*DNoR_NO))-...
               ((k2/ksum)*(NO^2)*((k1/(k_1-k2*(NO^2)))*(NoR^2))-...
               ((k_4*DNoR_NO)/(k_1-k2*(NO^2)))));
           
 S1=simplify(eqn)
 
 pretty(simplifyFraction(Ptot*Kno*((((k4/ksum)*(Km+0.5*sqrt(k1+k2*NoR*NO*DNoR_NO))-...
               ((k2/ksum)*(NO^2)*((k1/(k_1-k2*(NO^2)))*(NoR^2))-...
               ((k_4*DNoR_NO)/(k_1-k2*(NO^2))))))^nno)/(Kmno + (((k4/ksum)*(Km+0.5*sqrt(k1+k2*NoR*NO*DNoR_NO))-...
               ((k2/ksum)*(NO^2)*((k1/(k_1-k2*(NO^2)))*(NoR^2))-...
               ((k_4*DNoR_NO)/(k_1-k2*(NO^2))))))^nno)))