function count = nM2n(concentration)
%N2NM Convert a nanoMolar concentration in molecule count.
% 
%       concentration = n2nM(count)
%
%   Returns:
%       count:          Number of molecules.
%
%   Parameters:
%       concentration:  Concentration in nM.
%
    %% Assume a cell volume of 1 �m^3
    V = 1e-15;                      % Cell volume in L
    Na = 6.022140857e23;            % Avogadro constant
    count = concentration * (Na * V) / 1e9;
end