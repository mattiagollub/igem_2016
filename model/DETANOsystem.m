clear;
%% Some variables
Mno=30.01;%g�mol?1
Mdeta=103.2;%g�mol?1
Mo2= 16;%g�mol?1
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
tf   = 600;

% Input: NO activity
points = 100;
    AHL_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_AHL_ON = ceil((tf-t0)*0.1/tf*points);
    t_AHL_OFF = floor((tf-t0)*0.2/tf*points);
    t_AHL = (0:points)/points*(tf-t0) + t0;
    AHL = zeros(size(t_AHL));
    
%% Setup model
inicc=[1e-5,3e-5, 5e-5, 1e-4, 3e-4, 5e-4];%, 1e-3, 3e-3, 5e-3, 1e-2, 3e-2, 5e-2 1e-1];
%inicc=[1e-3, 3e-3, 5e-3, 1e-2, 3e-2, 5e-2 1e-1];
for i=1:length(inicc)
    i
% modelDETA = createDETAMAmodel();
% modelDETA.c0=[0, inicc(i), 0]
% TDETA = modelDETA.timesMap;
% SDETA = modelDETA.speciesMap;
% TDETA('AHL') = t_AHL;
% SDETA('AHL') = AHL;
% modelDETA.paramsSet = 'andgate_eth_igem_2014';
% 
% deterministicSimulation([t0, tf], modelDETA);

model = createDETANOMAmodel();
model.c0=[0, inicc(i),0,0,(40e-3)/Mo2,0];  %(40e-3)/Mo2
T = model.timesMap;
S = model.speciesMap;

T('AHL') = t_AHL;
S('AHL') = AHL;
% T('DETA') = TDETA('DETA');
% S('DETA') = SDETA('DETA');
model.paramsSet = 'andgate_eth_igem_2014';

%% Run simulation
deterministicSimulation([t0, tf], model);

%% Plot results

% Pout_flipped input profile
figure(2);

subplot(2, 1, 1);
hold on
plot(T('DETA'), S('DETA'), 'linewidth', 2);
grid on;
title('Input: DETA')
xlabel('Time [min]');
ylabel('concentration of DETA (M)');
legend('DETA');
drawnow;
colormap=jet;
% Pout_free input profile
subplot(2, 1, 2);
hold on
plot(T('NO'), S('NO'), 'linewidth', 2);
grid on;
title('Nitric Oxyde Diffusion')
xlabel('Time [min]');
ylabel('NO concentrtion (M)');
legend('NO');
drawnow;
colormap=jet;
% Use appropriate colors for GFP and mNectarine


% GFP and mNectarine mRNA production
% subplot(2, 2, 3);
% 
% hold on
% plot(T('O2'), S('O2'), 'linewidth', 2);
% plot(T('NO2'), S('NO2'), 'linewidth', 2);
% grid on;
% title('subspecies NO2 and O2');
% xlabel('Time [min]');
% ylabel('Concentration [M]');
% legend('O2','NO2');
% drawnow;
end
