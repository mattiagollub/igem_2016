function concentration = n2nM(count)
%N2NM Convert a molecule count in nanoMolar concentration.
% 
%       concentration = n2nM(count)
%
%   Returns:
%       concentration:  Concentration in nM.
%
%   Parameters:
%       count:          Number of molecules.
%
    %% Assume a cell volume of 1 �m^3
    V = 1e-15;                      % Cell volume in �m^3
    Na = 6.022140857e23;            % Avogadro constant
    concentration = count * 1e9 / (Na * V);
end