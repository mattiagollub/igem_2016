ydata = [1 1.5 2 8 10 15 30 40 50 70 80 90 110 250 1500 3000]*1.5e-22;
xdata = [1 2.5 3 5 6 7 7.5 8 9 9.5 13 14 18 19 50 100]/444e9;
fun = @(x,xdata)(x(1).*xdata.^x(2))./(x(3)+xdata.^x(2));
%We arbitrarily set our initial point x0 as follows: c(1) = 1, lam(1) = 1, c(2) = 1, lam(2) = 0:
x0 = [0.5 2 0.1];
%We run the solver and plot the resulting fit.
options = optimoptions('lsqcurvefit','Algorithm','levenberg-marquardt');
lb = [];
ub = [];
x = lsqcurvefit(fun,x0,xdata,ydata,lb,ub,options)
times = linspace(xdata(1),xdata(end));
%plotting
figure;
semilogy(xdata,ydata,'ko',times,fun(x,times),'b-');
legend('Data','Fitted hill function')
title('Data and Fitted Curve Pfree')