addpath('plotting');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range focr the experiment
t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input : AHL Activity
stimulus = 100;   % Assume all promoters active
[t_NO, NO] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_NO = t_NO * (tf - t0) + t0;
%% Setup model
model = createAndGateLactatePartMAmodel();
T = model.timesMap;
S = model.speciesMap;
% T('NO') = t_NO;
% S('NO') = NO;
model.paramsSet = 'lactate_module_parameters';
%paramRange=[0.1,1,5,10,50,100]
%paramRange=horzcat([0:0.2:1], [2:2:10],20,50,100);
%paramRange=1;
%paramRange=horzcat(0, 0.01, 0.1, 1, [2:2:10], 50, 100);
%paramRange=horzcat(0, 0.01, 0.1, 1, 5, 10);
paramRange=horzcat(0 , 1, 5, 50,70, 100, 300)

[x, y] = parameterSet(model, 'klldrProd', paramRange);

figure;
plot(x(2:end),y(2:end), 'linewidth',lineWidth);
grid on
title('input threshold for different transcription rate values');
xlabel('param (nM min^-1)');
ylabel('Concentration [nMol]');
set(gca,'xscale','log');