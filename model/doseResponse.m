function doseResponse()
%DOSERESPONSE simulqtes the behavior of the model under different inputs of
%

 %% Time range for the experiment
    t0   = 0;
    ts   = 0.5;
    tf   = 120;
    tspan = t0:ts:tf;
     
%% Setup Mpodel
    % NO part
     modelNO = createAndGateNOpartMAmodel();
     TNO = modelNO.timesMap;
     SNO = modelNO.speciesMap;
     NOparamMap=loadParameters('andgate_eth_igem_2014');
     modelNO.paramsSet = NOparamMap;
     
    % AHL part
     modelAHL = createAndGateAHLpartMAmodel();
     TAHL = modelAHL.timesMap;
     SAHL = modelAHL.speciesMap;
     AHLparamMap=loadParameters('andgate_eth_igem_2014');
     modelAHL.paramsSet = AHLparamMap;
     
     % Switch
     modelSwitch = createSwitchV1MAmodel();
     Tswitch = modelSwitch.timesMap;
     Sswitch = modelSwitch.speciesMap;
     SwitchparamMap = loadParameters('switch_bonnet');
     modelSwitch.paramsSet = SwitchparamMap;
     
     % Output
     modelOutput = createReporterMAmodel();
     TOutput = modelOutput.timesMap;
     SOutput = modelOutput.speciesMap;
     OutputparamMap = loadParameters('output_eth_igem_2014');
     modelOutput.paramsSet = OutputparamMap;
     
     % full model
     model = linkModels({'NO', 'AHL'},modelNO, modelAHL, modelSwitch, modelOutput);
    
    T = model.timesMap;
    S = model.speciesMap;  

%% Input species NO and AHL range
    noinput=0:10000:200000;
    ahlinput=0:10:200;
%% mean an variance initializarion matixes
mean=zeros(length(noinput), length(ahlinput));
variance=zeros(length(noinput), length(ahlinput));
%% Start loop
    for i=1:length(noinput)
        for j=1:length(ahlinpiut)
            % Input:NO
            NO_stimulus = noinput(i);   % Assume all promoters active
            [t_NO, v_NO] = ...
                makeStimulusProfile(0.2, 0.4, NO_stimulus, 100);
            t_NO = t_NO * (tf - t0) + t0;

            % Input : AHL
             AHL_stimulus = ahlinput(j);
             [t_AHL, v_AHL] = ...
                makeStimulusProfile(0.25, 0.45, AHL_stimulus, 100);
             t_AHL = t_AHL * (tf - t0) + t0;

             % NO part input
             TNO('NO') = t_NO;
             SNO('NO') = NO_stimulus;

             % AHL part input    
             TAHL('AHL') = t_AHL;
             SAHL('AHL') = AHL_stimulus;

             % switsh input
             a=interp1(TNO('PnorV3'),SNO('PnorV3'),TNO('NO'));
             b=interp1(TAHL('Pfree'),SAHL('Pfree'),TNO('NO'));
             c=abs((kl+(1-kl)*(a.*b)/Ptot^2));

             Tswitch('Phyb_ON') = TNO('NO');
             Sswitch('Phyb_ON') = c;

             % output input
             TOutput('Pout_flipped') =Tswitch('Pout_flipped');
             SOutput('Pout_flipped') = Sswitch('Pout_flipped');

             TOutput('Pout_free') =TAHL('Pout');
             SOutput('Pout_free') = SAHL('Pout');

             % full model
             T('NO') = t_NO;
             S('NO') = v_NO;

             T('AHL') = t_AHL;
             S('AHL') = v_AHL;

         % Simulate system
            mexStochasticSimulation2(tspan, model, nRuns, drawFunctions, 2000);
            index=indexOf(T('GFP'),0.35* (tf - t0) + t0);
            meanGFP=S('GFP');
            varianceGFP=V('GFP');
            mean(i,j)= meanGFP(index);
            variance(i,j)=varianceGFP(index);
        end
    end
   
    %% HeatMap plotting
    meanMap= HeatMap(mean);
    addTitle(meanMap, 'Mean of the GFP stochastic simulation as a function of AHL and NO');
    addXLabel(meanMap, 'NO');
    addYLabel(meanMap, 'AHL');
    
    varianceMap= HeatMap(variance);
    addTitle(varianceMap, 'Variance of the GFP stochastic simulation as a function of AHL and NO');
    addXLabel(varianceMap, 'NO');
    addYLabel(varianceMap, 'AHL');
end