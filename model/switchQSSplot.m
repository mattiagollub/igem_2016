% SWITCHQSSPLOT  Example simulation of the switch. 
%   Simulates the concentration of the switch species using an example
%   input and a simplified model.
%
%% Simulation settings
clear;

% Time range for the experiment
t0   = 0;
tf   = 60;

% Input: Phyb activity
stimulus = 1;   % Assume all promoters active
[t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.1, 1.0, stimulus, 100);
t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

%% Setup model
model = createSwitchV1QSSmodel();
T = model.timesMap;
S = model.speciesMap;

T('Phyb_ON') = t_Phyb_ON;
S('Phyb_ON') = v_Phyb_ON;
model.paramsSet = 'switch_bonnet';

%% Run simulation
deterministicSimulation([t0, tf], model);

%% Plot results
figure(1);
clf;

% Plot Phyb_ON input profile
subplot(2, 1, 1);
plot(T('Phyb_ON'), S('Phyb_ON'), 'linewidth', 2);
grid on;
title('Input: Fraction of Phyb activity');
xlabel('Time [min]');
ylabel('Phyb_{ON} fraction');
legend('Phyb^{ON}');
ylim([0, stimulus * 1.3]);

% Integrase behavior
subplot(2, 1, 2);
hold on;
plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'linewidth', 2);
grid on;
title('Pout flipping');
xlabel('Time [min]');
ylabel('Number of promoters');
legend('Pout^{flipped}'); 