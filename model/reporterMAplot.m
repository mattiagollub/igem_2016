% REPORTERMAPLOT Example simulation of the reporter. 
%   Simulates the concentration of the reporter species using an example
%   input.
%
%% Simulation settings
clear;

% Time range for the experiment
t0   = 0;
tf   = 60*6;

% Input: Pout state
Pout_flipped_stimulus = n2nM(10);   % Assume 66% plasmids flipped
Pout_free_stimulus = 1.0;

[t_Pout_flipped, v_Pout_flipped] = ... 
    makeStimulusProfile(0.1, 1.0, Pout_flipped_stimulus, 100);
[t_Pout_free, v_Pout_free] = ...
    makeStimulusProfile(0.3, 0.6, Pout_free_stimulus, 100);
t_Pout_flipped = t_Pout_flipped * (tf - t0) + t0;
t_Pout_free = t_Pout_free * (tf - t0) + t0;

%% Setup model
model = createReporterMAmodel();
T = model.timesMap;
S = model.speciesMap;

T('Pout_flipped') = t_Pout_flipped;
S('Pout_flipped') = v_Pout_flipped;
T('Pout_free') = t_Pout_free;
S('Pout_free') = v_Pout_free;
model.paramsSet = 'reporter_literature';

%% Run simulation
deterministicSimulation([t0, tf], model);

%% Plot results

% Pout_flipped input profile
figure(1);
clf;
subplot(2, 2, 1);
plot(T('Pout_flipped'), nM2n(S('Pout_flipped')), 'linewidth', 2);
grid on;
title('Input: Pout flipping')
xlabel('Time [min]');
ylabel('Number of flipped promoters');
legend('Pout^{flipped}');

% Pout_free input profile
subplot(2, 2, 2);
plot(T('Pout_free'), S('Pout_free'), 'linewidth', 2);
grid on;
title('Input: Fraction of free promoters')
xlabel('Time [min]');
ylabel('Pout^{free} fraction');
legend('Pout^{free}');

% Use appropriate colors for GFP and mNectarine
axColors = [
    0.4660    0.6740    0.1880
    0.8500    0.3250    0.0980
];

% GFP and mNectarine mRNA production
subplot(2, 2, 3);
ax = gca;
ax.ColorOrder = axColors;
hold on
plot(T('mRNAgfp'), S('mRNAgfp'), 'linewidth', 2);
plot(T('mRNAmnect'), S('mRNAmnect'), 'linewidth', 2);
grid on;
title('mRNA expression');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('mRNAgfp', 'mRNAmnect');

% GFP and mNectarine expression
subplot(2, 2, 4);
ax = gca;
ax.ColorOrder = axColors;
hold on
plot(T('GFP'), S('GFP'), 'linewidth', 2);
plot(T('mNect'), S('mNect'), 'linewidth', 2);
grid on;
title('Fluorecence proteins expression');
xlabel('Time [min]');
ylabel('Concentration [nM]');
legend('GFP', 'mNect'); 
