function systemDoseResponse2D()
% SWITCHACTIVITYRESPONSE Response of the switch to inputs. 
%   Simulates the concentration of the reporters with different activity
%   levels of the hybrid promoter.
%   
    %% Environment setup
    clear;
    
    %% Simulation settings
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);
    
    % Dose inputs
    NO_lims = [0.1, 100];
    AHL_lims = [0.1, 100];
    NO_range = logspace(log(NO_lims(1)), log(NO_lims(2)));
    AHL_range = logspace(log(AHL_lims(1)), log(AHL_lims(2)));
    exposures = [3, 6, 9];
    
    % Template input 1: Phyb activity
    states = [
        0.0, 0.0, 1.0, 1.0, 0.0, 0.0,   1.0,     1.0;   % NO
        0.0, 0.0, 1.0, 1.0, 0.0, 0.0,   1.0,     1.0;   % AHL
        0,   59,  60,  60,  61,  13*60, 13*60+1, 17*60  % Times
    ];

    %% Setup model for the NO part of the sensor
    modelNO = createAndGateNOpartMAmodel();
    TNO = modelNO.timesMap;
    SNO = modelNO.speciesMap;
    modelNO.paramsSet = 'andgate_eth_igem_2014';

    %% Setup model for the AHL part of the sensor
    modelAHL = createAndGateAHLpartMAmodel();
    TAHL = modelAHL.timesMap;
    SAHL = modelAHL.speciesMap;
    modelAHL.paramsSet = 'andgate_eth_igem_2014';

    %% Setup model for switch and reporters
    switchModel = createSwitchV2MAmodel();
    switchModel.paramsSet = 'switch_bonnet';
    reporterModel = createReporterMAmodel();
    reporterModel.paramsSet = 'reporter_literature';
    model = linkModels({'Phyb_ON', 'Pout_free'}, [], ...
        switchModel, reporterModel);
    
    T = model.timesMap;
    S = model.speciesMap;
    
    %% Setup storage for the results
    meansGFP = zeros(length(NO_range), length(AHL_range));
    meansmNect = zeros(length(NO_range), length(AHL_range));
    
    %% Run simulation with different activity levels
    for e=1:length(exposures)
        for i=1:length(NO_range)
            for l=1:length(AHL_range)

                %% Setup inputs
                v_NO = states(1,:) * NO_range(i);
                v_AHL = states(2,:) * AHL_range(l);
                t_input = [states(3,1:2), states(3,3:end) + exposures(e)];

                TNO('NO') = t_input;
                SNO('NO') = v_NO;
                TAHL('AHL') = t_input;
                SAHL('AHL') = v_AHL;

                %% Simulate sensor module
                deterministicSimulation(t_input([1, end]), modelNO);
                deterministicSimulation(t_input([1, end]), modelAHL);

                %Compute Phyb_ON
                tspan = t_input(end) * linspace(0, 1, 17*100);
                a = interp1(TNO('PnorV3'), SNO('PnorV3'), tspan);
                b = interp1(TAHL('Pfree'), SAHL('Pfree'), tspan);

                c = abs((kl+(1-kl)*(a.*b)/Ptot^2));
                T('Phyb_ON') = tspan;
                S('Phyb_ON') = c;
                T('Pout_free') = TAHL('Pout');
                S('Pout_free') = SAHL('Pout')/Ptot;
                
                %% Simulate switch and reporter
                deterministicSimulation(t_input([1, end]), model);

                %% Extract statistics from the simulation
                meanGFP = model.speciesMap('GFP');
                meanmNect = model.speciesMap('mNect');
                meansGFP(l, t) = meanGFP(end);
                meansmNect(l, t) = meanmNect(end);

                %% Plot heatmaps
                figure(e);
                clf;
                colormap('jet');

                xticklabels = Phyb_ON_durations;
                xticks = linspace(1, length(Phyb_ON_durations), numel(Phyb_ON_durations));
                yticklabels = nM2n(Phyb_ON_levels);
                yticks = linspace(1, length(Phyb_ON_levels), numel(Phyb_ON_levels));

                ax = subplot(1, 2, 1);
                imagesc(flipud(meansGFP));
                set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
                set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
                title('GFP mean fluorescence');
                xlabel('Phyb_{ON} activity duration');
                ylabel('Phyb_{ON} activity intensity');
                colorbar;

                ax = subplot(1, 2, 2);
                imagesc(flipud(meansmNect));
                set(ax, 'XTick', xticks, 'XTickLabel', xticklabels);
                set(ax, 'YTick', yticks, 'YTickLabel', flipud(yticklabels(:)));
                title('mNectarine mean fluorescence');
                xlabel('Phyb_{ON} activity duration');
                ylabel('Phyb_{ON} activity intensity');
                colorbar;

                drawnow;
            end
        end
    end
end