function [h, c] = createHybridPropensitiesFuncs(model)
%CRAETEPROPENSITIESFUNCTION Creates a propensities function handle
%   Builds a function handle that returns the reaction propensities of the
%   specified model at a given state.
%
%       h = createHybridPropensitiesFunc(model)
%
%   Returns:
%       h:              function handle in the form h = @(c) prop(c)
%
%   Required:
%       model:          MATLAB structure containing the definition of the
%                       model.
%
    nParams = length(model.paramNames);
   
    aInput = model.a(1); 
    aConst = model.a(2:end);
    
    %% Insert parameters
    if (ischar(model.paramsSet))
        P = loadParameters(model.paramsSet);
    else
        P = model.paramsSet;
    end
    parameters = zeros(1, nParams);
    
    for i=1:nParams
        paramName = char(model.paramNames(i));
        parameters(i) = P(paramName);
    end
    
    aInput = subs(aInput, model.paramNames, parameters);
    aConst = subs(aConst, model.paramNames, parameters);
    
    %% Create matlab function object for the propensities
    h = matlabFunction(aInput, 'File', 'tmpInputPropensitiesFunc', ...
        'Vars', {model.specieNames});
    c = matlabFunction(aConst, 'File', 'tmpConstPropensitiesFunc', ...
        'Vars', {model.specieNames});
end