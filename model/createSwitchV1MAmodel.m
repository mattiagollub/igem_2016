function model = createSwitchV1MAmodel()
%CREATESWITCHMAMODEL Defines a mass action model of the switch.
%   Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%   V1: The Bxb1 gene is placed inside the flipping cassette
%
%       model = createSwitchV2MAmodel()
%
%   Returns:
%       model:          MATLAB structure containing the definition of the
%                       model.
%  
    %% Common constants
    nP = 15;            % Number of plasmids
    S_tot = n2nM(nP);   % nM, Total concentration of plasmids
    
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();

    %% Input, initial conditions and integrator options
    model.c0 =  [0, 0, 0, 0, S_tot, 0, 0, 0, 0];
    model.odeOptions = odeset('RelTol', 1e-3);
    model.inputs = {'Phyb_ON'};
    
    %% Define symbolic variables for species and parameters
    syms Phyb_ON mRNAinv Bxb1 DBxb1 S_0 S_1 S_2 Pout_flipped Sf_0 Pout_nf
    syms k_mRNAinv d_mRNAinv d_Bxb1 k_Bxb1 k_DBxb1 k_DBxb1_ ...
         k_attBP k_attBP_ k_attLR k_attLR_ k_flip d_DBxb1
    
    model.specieNames = [Phyb_ON mRNAinv Bxb1 DBxb1 S_0 S_1 S_2 ...
                         Pout_flipped Sf_0];
    model.paramNames = [k_mRNAinv d_mRNAinv d_Bxb1 k_Bxb1 k_DBxb1 k_DBxb1_ ...
                        k_attBP k_attBP_ k_attLR k_attLR_ k_flip d_DBxb1]; 
    
    %% Stoichiometric and rate matrices
    % Phyb_ON | mRNAinv | Bxb1 | DBxb1 | S_0 | S_1 | S_2 | Pout_flipped | Sf_0
    model.N = [
        0,  1,  0,  0,  0,  0,  0,  0,  0; % Phyb_ON -> Phyb_ON + mRNAinv
        0, -1,  0,  0,  0,  0,  0,  0,  0; % mRNAinv ->
        0,  0,  1,  0,  0,  0,  0,  0,  0; % mRNAinv -> mRNAinv + Bxb1
        0,  0, -2,  1,  0,  0,  0,  0,  0; % Bxb1 + Bxb1 -> DBxb1
        0,  0,  2, -1,  0,  0,  0,  0,  0; % DBxb1 -> Bxb1 + Bxb1
        0,  0,  0, -1, -1,  1,  0,  0,  0; % S_0 + DBxb1 -> S_1
        0,  0,  0,  1,  1, -1,  0,  0,  0; % S_1 -> S_0 + DBxb1
        0,  0,  0, -1,  0, -1,  1,  0,  0; % S_1 + DBxb1 -> S_2
        0,  0,  0,  1,  0,  1, -1,  0,  0; % S_2 -> S_1 + DBxb1
        0,  0,  0,  0,  0,  0, -1,  1,  0; % S_2 -> Pout_flipped
        0,  0, -1,  0,  0,  0,  0,  0,  0; % Bxb1 ->
        0,  0,  0, -1,  0,  0,  0,  0, -1; % Sf_0 + Dbxb1 -> Sf_1
        0,  0,  0,  1,  0,  0,  0,  0,  1; % Sf_1 -> Sf_0 + Dbxb1
        0,  0,  0, -1,  0,  0,  0,  0,  0; % DBxb1 ->
    ]';
    
    %% Reaction rates
    model.rates = [
        k_mRNAinv*Phyb_ON*Pout_nf;      % Phyb_ON -> Phyb_ON + mRNAinv
        d_mRNAinv*mRNAinv;              % mRNAinv ->
        k_Bxb1*mRNAinv;                 % mRNAinv -> mRNAinv + Bxb1
        k_DBxb1*Bxb1^2;                 % Bxb1 + Bxb1 -> DBxb1
        k_DBxb1_*DBxb1;                 % DBxb1 -> Bxb1 + Bxb1
        k_attBP*S_0*DBxb1*2;            % S_0 + DBxb1 -> S_1
        k_attBP_*S_1;                   % S_1 -> S_0 + DBxb1
        k_attBP*S_1*DBxb1;              % S_1 + DBxb1 -> S_2
        k_attBP_*S_2*2;                 % S_2 -> S_1 + DBxb1
        k_flip*S_2;                     % S_2 -> Pout_flipped
        d_Bxb1*Bxb1;                    % Bxb1 ->
        k_attLR*Sf_0*DBxb1;             % Sf_0 + Dbxb1 -> Sf_1
        k_attLR_*(Pout_flipped-Sf_0);   % Sf_1 -> Sf_0 + Dbxb1
        d_DBxb1*DBxb1;                  % DBxb1 ->
    ];
    model.rates = subs(model.rates, Pout_nf, S_tot-Pout_flipped); 
    
    %% Reaction propensities
    model.a = [
        k_mRNAinv*n2nM(Phyb_ON)*Pout_nf;% Phyb_ON -> Phyb_ON + mRNAinv
        d_mRNAinv*mRNAinv;              % mRNAinv ->
        k_Bxb1*mRNAinv;                 % mRNAinv -> mRNAinv + Bxb1
        n2nM(k_DBxb1)/2*Bxb1*(Bxb1-1);  % Bxb1 + Bxb1 -> DBxb1
        k_DBxb1_*DBxb1;                 % DBxb1 -> Bxb1 + Bxb1
        n2nM(k_attBP)*S_0*DBxb1*2;      % S_0 + DBxb1 -> S_1
        k_attBP_*S_1;                   % S_1 -> S_0 + DBxb1
        n2nM(k_attBP)*S_1*DBxb1;        % S_1 + DBxb1 -> S_2
        k_attBP_*S_2*2;                 % S_2 -> S_1 + DBxb1
        k_flip*S_2;                     % S_2 -> Pout_flipped
        d_Bxb1*Bxb1;                    % Bxb1 ->
        n2nM(k_attLR)*Sf_0*DBxb1;       % Sf_0 + Dbxb1 -> Sf_1
        k_attLR_*(Pout_flipped-Sf_0);   % Sf_1 -> Sf_0 + Dbxb1
        d_DBxb1*DBxb1;                  % DBxb1 ->
	];
    model.a = subs(model.a, Pout_nf, nP-Pout_flipped);
end