function switchCpf1Comparison()
% SWITCHCpf1COMPARISON Comparison between different Cpf1 switch models.
%
    %% Simulation settings

    % Parameter set
    params_set = 'cpf1_assumed';

    % Time range for the experiment
    t0   = 0;
    tf   = 120;
    tspan = t0:0.1:tf;
    nRuns = 50;

    % Input: mRNAinv profile
    stimulus = 1;    % Assume all promoters active
    [t_Phyb_ON, v_Phyb_ON] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
    t_Phyb_ON = t_Phyb_ON * (tf - t0) + t0;

    %% Setup models
    modelMA = createCpf1SwitchMAmodel();
    modelSSA = createCpf1SwitchMAmodel();

    TMA = modelMA.timesMap;
    SMA = modelMA.speciesMap;
    TSSA = modelSSA.timesMap;
    SSSA = modelSSA.speciesMap;

    TMA('Phyb_ON') = t_Phyb_ON;
    SMA('Phyb_ON') = v_Phyb_ON;
    TSSA('Phyb_ON') = t_Phyb_ON;
    SSSA('Phyb_ON') = v_Phyb_ON;

    modelMA.paramsSet = params_set;
    modelSSA.paramsSet = params_set;

    %% Run full mass action simulation
    deterministicSimulation([t0, tf], modelMA);

    %% Plot results
    figure(1);
    clf;

    % Pout_ON input profile
    subplot(3, 1, 1);
    plot(t_Phyb_ON, v_Phyb_ON, 'linewidth', 2);
    grid on;
    title('Input: Fraction of Phyb activity')
    xlabel('Time [min]');
    ylabel('Phyb_{ON} fraction');
    legend('Phyb_{ON}');
    ylim([0, stimulus * 1.3]);

    % Cpf1 binding with sgRNA
    subplot(3, 1, 2);
    hold on
    plot(TMA('Cpf1'), SMA('Cpf1'), 'linewidth', 2);
    plot(TMA('Cpf1_I'), SMA('Cpf1_I'), 'linewidth', 2);
    plot(TMA('Cpf1_II'), SMA('Cpf1_II'), 'linewidth', 2);
    grid on;
    title('Cpf1 binding with sgRNA');
    xlabel('Time [min]');
    ylabel('Concentration [nM]');
    legend('Cpf1', 'Cpf1^{I}', 'Cpf1^{II}'); 
    drawnow;

    % Integrase behavior
    subplot(3, 1, 3);
    hold on;
    p_C_a = plot(TMA('C_a'), nM2n(SMA('C_a')), 'linewidth', 2);
    p_dead = plot(TMA('dead'), nM2n(SMA('dead')), 'linewidth', 2);
    grid on;
    title('Cell switching and death');
    xlabel('Time [min]');
    ylabel('Number of cells');
    legend('Switched', 'Dead');
    drawnow;

    %% Stochastic simulation

    drawFunctions = containers.Map( ...
        {'C_a', 'dead'}, ...
        {
            @(t, x) drawInSubplot(t, x, 3, 1, 2, 0.1), ...
            @(t, x) drawInSubplot(t, x, 3, 2, 2, 0.1)
        });

    stochasticSimulationNRM(tspan, modelSSA, nRuns, drawFunctions);

    l = legend('Switched', 'Dead');
    l.Location = 'southeast';
    uistack(p_C_a,'top');
    uistack(p_dead,'top');
end

function drawInSubplot(t, x, p, c, lineWidth, alpha)
    ax = subplot(3, 1, p);
    hold on;
    ax.ColorOrderIndex = c;
    p = plot(t, nM2n(x), 'linewidth', lineWidth);
    p.Color(4) = alpha;
end