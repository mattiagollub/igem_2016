addpath('plotting');
% Common constants
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant

% Time range for the experiment
t0   = 0;
ts   = 10;
tf   = 600;
tspan = t0:ts:tf;
lineWidth = 1.5;

% Input : AHL Activity
stimulus = 10000000;   % Assume all promoters active
[t_NO, NO] = makeStimulusProfile(0.2, 1.0, stimulus, 100);
t_NO = t_NO * (tf - t0) + t0;
%% Setup model
model = createAndGateLactatePartMAmodel();
% T = model.timesMap;
% S = model.speciesMap;
% T('AHL') = t_AHL;
% S('AHL') = AHL;
model.paramsSet = 'lactate_module_parameters';
%paramRange
paramRange1=[0:10:100];
paramRange2=[0:10:100];

parameterSetColorMap(model, 'klldrProd','kllddProd', paramRange1, paramRange2, stimulus);