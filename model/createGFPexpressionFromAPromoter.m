function model = createGFPexpressionFromAPromoter ()
%INDEPENDANT gfp EXPRESSION MODULE
%    Creates a MATLAB structure containing definition for the
%   soichiometric, rates and propensities matrices, integration settings,
%   initial conditions and parameters.
%
%
%

%% Common constants
    nP = 15;            % Number of plasmids
    Ptot = n2nM(nP);   % nM, Total concentration of plasmids
    pnortot= 24;
   
    %% Create container for the species
    model.timesMap = containers.Map();
    model.speciesMap = containers.Map();
    model.histogramsMap = containers.Map();
    
    %% Input, initial condition and intgrator options
    model.c0 = [0,0,0];
    model.odeOptions = odeset('RelTol', 1e-1);
    model.inputs = {'PnorV3'};
    
    %% Define symbolic variables for species and parameters
    syms PnorV3 mRNA GFP
    syms kmrna kl dmrna k_GFP d_GFP
    model.specieNames = [PnorV3 mRNA GFP];
    model.paramNames = [kmrna kl dmrna k_GFP d_GFP];
    
    %% Stoichiometric and rate matrices
    % Promoter | mRNA | GFP
    
    model.N = [
         0,   1,   0;    % Promoter-> mRNA 
         0,  -1,   0;    % mRNA ->  
         0,  -1,   1;    % mRNA -> mRNA + GFP
         0,   0,  -1;    % GFP ->
      ]';
  
  %% Reaction rates
   model.rates = [
       (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; % Promoter -> mRNA
       dmrna*mRNA;                          % mRNA ->
       k_GFP*mRNA;                          % mRNA -> mRNA + GFP
       d_GFP*GFP;                           % DBxb1 ->
    ];

  %% Reaction propensities
   model.a = [
       (kmrna*kl)+(kmrna*(1-kl)*PnorV3) ; % Promoter -> mRNA
       dmrna*mRNA;                             % mRNA ->
       k_GFP*mRNA;                             % mRNA -> mRNA + GFP
       d_GFP*GFP;                              % DBxb1 ->
    ];
end
