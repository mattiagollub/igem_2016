clear;
%% Some variables
Mno=30.01;%g�mol?1
Mdeta=103.2;%g�mol?1
Mo2= 16;%g�mol?1
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
tf   = 600;
% INnput DETA concentration at T0
cc = [20 100 200 500];

% Input: NO activity
points = 100;
    AHL_stimulus = 30 * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_AHL_ON = ceil((tf-t0)*0.1/tf*points);
    t_AHL_OFF = floor((tf-t0)*0.2/tf*points);
    t_AHL = (0:points)/points*(tf-t0) + t0;
    AHL = zeros(size(t_AHL));
    
    
for i =1:length(cc)
ccdeta = [0, cc(i)*(10^(-6)), 0]    
% Setup model
modelDETA = createDETAMAmodel();
TDETA = modelDETA.timesMap;
SDETA = modelDETA.speciesMap;
TDETA('AHL') = t_AHL;
SDETA('AHL') = AHL;
modelDETA.paramsSet = 'andgate_eth_igem_2014';
modelDETA.c0 = ccdeta;
deterministicSimulation([t0, tf], modelDETA);

model = createDETANOMAmodel();
T = model.timesMap;
S = model.speciesMap;

T('DETA') = TDETA('DETA');
S('DETA') = SDETA('DETA');
model.paramsSet = 'andgate_eth_igem_2014';

%% Run simulation
deterministicSimulation([t0, tf], model);

%% NOR part
modelNO = createAndGateNOpartMAmodel();
modelNO.paramsSet = 'andgate_eth_igem_2014';

modelGFP = createGFPexpressionFromAPromoter ();
modelGFP.paramsSet = 'andgate_eth_igem_2014';
modelGFP.inputs = {'PnorV3'};

modeltest = linkModels({'NO'}, modelNO, modelGFP);


TNO = modeltest.timesMap;
SNO = modeltest.speciesMap;

TNO('NO') = T('NO');
SNO('NO') = S('NO')*10^9;

%% Run simulation
deterministicSimulation([t0, tf], modeltest);
%% PLot simulation
% Pout_flipped input profile
figure(1)

subplot(1, 2, 1);
hold on
plot(T('NO'), S('NO'), 'linewidth', 2);
grid on;
title('Input: NO created from DETA injection')
xlabel('Time [min]');
ylabel('concentration of NO (M)');
drawnow;
%legend('NO');

% Pout_free input profile
subplot(1, 2, 2);
hold on
plot(TNO('GFP'), SNO('GFP'), 'linewidth', 2);
grid on;
title('GFP concentration')
xlabel('Time [min]');
ylabel('GFP concentrtion (nM)');
drawnow;
%legend('GFP');
end
% Use appropriate colors for GFP and mNectarine




