function p = ESTIM_DETER_PARAM(options, experiments, model)
% ESTIMATEDETERMINISTICPARAMS Infer parameters from experimental data
%   Infer the parameters of a mass action model from a set of
%   experimental data.
%
%       p = estimateDeterministicParams(options, experiments, model)
%
%   Returns:
%       p:              containers.Map containing the values of the
%                       estimated parameters.
%
%   Parameters:
%       options:        Structure containing the options about the 
%                       parameters to be estimated.
%       experiments:    Structure containing teh experimental data.
%       model:          The model using the parameters.
%
    %% Make variables global
    global E_options
    global E_experiments
    global E_model
    E_options = options;
    E_experiments = experiments;
    E_model = model;
    
    %% Specify problem setings
    nthreads = 2;               % number of threads
    n_iter = 10;                % number of cooperative iterations
    is_parallel = true;         % parallel (true) or sequential (false)
    maxtime_per_iteration =10;  % time limit for each iteration
    par_struct = get_CeSS_options(nthreads, ...
        length(options.startValues), maxtime_per_iteration);
    
    for i = 1:nthreads
        par_struct(i).opts.maxtime = maxtime_per_iteration;
        par_struct(i).opts.maxeval = 100;
        par_struct(i).opts.local.solver = 0; % Don't use local solver

        par_struct(i).problem.f = ['PARAMETER_ESTIMATION_AHL_PART_PB'];
        par_struct(i).problem.x_L = options.lowerBounds;
        par_struct(i).problem.x_U = options.upperBounds;
    end
    
    paramsSet = model.paramsSet;
    
    %% Run optimization
    % Run CeSS:
    tic
    results = CeSS(par_struct,n_iter,is_parallel)
    save(['results.mat'], 'results');
    toc
    
    %% Return a map containing the parameters
    if (ischar(paramsSet))
        P = loadParameters(paramsSet);
    else
        P = paramsSet;
    end
    
    % Insert estimated parameters
    for p=1:length(results.xbest)
        P(options.targetParams{p}) = results.xbest(p);
    end
    
    % Perform substitutions if necessary
    p = options.subsFunc(P);
    
end