%function andGateMAmodelNOpartPlot()
% AND GATE NO PART Example simulation of the switch. 
%   Deterministically simulates the concentration of the and gate species
%   using an example input.
% 
%% Simulation settings
clear;
% Common constants
nRuns = 500;
V = 1e-15;              % Cell volume in �m
Na = 6.022140857e23;    % Avogadro constant
% Time range for the experiment
t0   = 0;
ts   = 1;
tf   = 60*10;
tspan = t0:ts:tf;
lineWidth = 1.5;
% Input: NO activity
points = 100;
    NO_stimulus = 10000 * 1e9 / (Na * V);    % Assume ~500 mRNAs
    t_NO_ON = ceil((tf-t0)*0.1/tf*points);
    t_NO_OFF = floor((tf-t0)*0.2/tf*points);
    t_NO = (0:points)/points*(tf-t0) + t0;
    NO = zeros(size(t_NO));
    NO(1, t_NO_ON:t_NO_OFF) = NO_stimulus;
%     
% Input : AHL Activity
%     stimulus = 100;   % Assume all promoters active
%     [t_NO, NO] = makeStimulusProfile(0.0, 1.0, stimulus, 100);
%     t_NO = t_NO * (tf - t0) + t0;
%% Setup model
modelNO = createAndGateNOpartMAmodel();
modelNO.paramsSet = 'andgate_eth_igem_2014';

modelGFP = createGFPexpressionFromAPromoter ();
modelGFP.paramsSet = 'andgate_eth_igem_2014';
modelGFP.inputs = {'PnorV3'};

model = linkModels({'NO'},[], modelNO, modelGFP);
% onlu for determination of NorR concnetration at a certain production rate
% model.paramsSet('knorProd')=0.1;
% model.paramSet('dnor')=0.1;

T = model.timesMap;
S = model.speciesMap;

T('NO') = t_NO;
S('NO') = NO;
% Histogram plotting
    histogramsX = 2;
    histogramsY = 2;
    nHistograms = histogramsX * histogramsY;


%% Plot results
% mRNAinv input profile
    figure;
    clf;
    subplot(2, 2, 1);
    stairs(t_NO, NO, 'r','linewidth',2); % Stairstep graph of the input
    grid on;
    title('Input: NO')
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NO');

drawFunctions = containers.Map( ...
        {'NoR', 'DNoR_NO', 'NoR_NO', 'DNoR', 'mRNA', 'GFP', 'PnorV0', 'PnorV1', 'PnorV2', 'PnorV3'}, ...
        { ...
            @(t, x) drawInSubplot(t, x, 2, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 3, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 2, 5, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 1, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 3, 2, lineWidth, 0.3), ...
            @(t, x) drawInSubplot(t, x, 4, 1, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 2, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 3, lineWidth, 0.2), ...
            @(t, x) drawInSubplot(t, x, 4, 4, lineWidth, 0.2), ...
        });
%% Run simulation
deterministicSimulation([t0, tf], model);
%stochasticSimulationNRM(tspan, model, nRuns);%, drawFunctions)
 %% Plot NOR system
    ax = subplot(2, 2, 2);
    hold on;
    ax.ColorOrderIndex = 1;
    p = plot(T('NoR'), S('NoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    hold on
    p = plot(T('DNoR_NO'), S('DNoR_NO'), 'linewidth', lineWidth);
    hold on
    p = plot(T('NoR_NO'), S('NoR_NO'), 'linewidth', lineWidth);
    hold on
    p = plot(T('DNoR'), S('DNoR'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('NOR system');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('NoR', 'DNoR_NO','NoR_NO','DNoR');

    %% Plot PnorV behavior
    ax = subplot(2, 2, 3);
    ax.ColorOrderIndex = 1;
    
    p = plot(T('mRNA'), S('mRNA'), 'linewidth', lineWidth);
    hold on
    p = plot(T('GFP'), S('GFP'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('mRNA','GFP');

    
    %% Plot DEsaR behavior
    ax = subplot(2, 2, 4);
    hold on;
    ax.ColorOrderIndex = 2;
    p = plot(T('PnorV0'), S('PnorV0'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV1'), S('PnorV1'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV2'), S('PnorV2'), 'linewidth', lineWidth);
    hold on
    p = plot(T('PnorV3'), S('PnorV3'), 'linewidth', lineWidth);
    hold on
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('PnorV behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    legend('PnorV0','PnorV1', 'PnorV2','PnorV3');
    
    %% Plot DEsaR_AHL behavior
%     ax = subplot(3, 2, 5);
%     hold on;
%     ax.ColorOrderIndex = 3;
%     p = plot(T('Pnor0'), S('Pnor0'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('Pnor1'), S('Pnor1'), 'linewidth', lineWidth);
%     %p.Color(4) = 1 / nRuns;
%     hold on
%     p = plot(T('Pnor2'), S('Pnor2'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('PnorV0'), S('PnorV0'), 'linewidth', lineWidth);
%     grid on;
%     title('DesaR_AHL behavior');
%     xlabel('Time [min]');
%     ylabel('Concentration [nMol]');
%     legend('Pnor0', 'Pnor1', 'Pnor2', 'PnorV0');
    %% Plot sensitivities for Pout_flipped only
% figure(3)
% nParams = length(model.paramNames);
% nSpecies = length(model.specieNames);
% nInputs = length(model.inputs);
% hold on
% specie = 'mRNA';
% names = cell(size(model.paramNames));
% for p=1:nParams
%     names{p} = char(model.paramNames(p));
%     name = strcat('S_', specie, '_', names{p});
%     plot(T(name), S(name), 'linewidth', 2);
% end
% grid on;
% title('Sensitivity of the parameters w.r.t. mRNAinv');
% xlabel('Time [min]');
% ylabel('Sensitivity');
% l = legend(names{:});
% set(l, 'Interpreter', 'none') 

%% Print matrix with maximum sensitivities
% S = zeros(nParams, nSpecies);
% for s=nInputs+1:nSpecies
% 	for p=1:nParams
%         name = strcat('S_', char(model.specieNames(s)), ...
%                       '_',  char(model.paramNames(p)));
%         S(p, s) = max(model.speciesMap(name));
%     end
% end
% S = S / max(abs(S(:)));
% display(S);
figure;
Dnorr=interp1(T('DNoR'),S('DNoR'),T('NoR'));
    p=plot(T('NoR'),S('NoR')+Dnorr);
%     p = plot(T('mRNA'), S('mRNA'), 'linewidth', lineWidth);
%     hold on
%     p = plot(T('GFP'), S('GFP'), 'linewidth', lineWidth);
    %p.Color(4) = 1 / nRuns;
    grid on;
    title('output behavior');
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    %legend('mRNA','GFP');
    legend('Total concentrationof NorR specie');


%% Plot histograms
    figure;
    clf;
    histsPnorV3 = model.histogramsMap('PnorV3');
    PnorV3Dist = fitdist(histsPnorV3(end, :)','Normal');
    histogram(histsPnorV3(end, :));
    hold on
    x_values = 0:1:30;
    y = pdf(PnorV3Dist,x_values);
    f = fit(x_values.',500*y','gauss1');
    plot(f,x_values,500*y)
    title(sprintf('PnorV3 distribution at t=%d min', tspan(end)));
    xlabel('Number of molecules');
    ylabel('Number of cells');
  
    
    figure;
    y=std(histsPnorV3');
    hold on
    tnew=1:10:length(T('PnorV3'));
    snew=S('PnorV3');
    s=snew(tnew);
    ynew=y(tnew);
    errorbar(tnew,s,ynew);
    title(sprintf('mean(PnorV3) behavior for the and gate modular model, input NO = 100nM'));
    xlabel('Time [min]');
    ylabel('Concentration [nMol]');
    
    

%end

% function drawWithErrorBarInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = errorbar(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end

% function drawInSubplot(t, x, p, c, lineWidth, alpha)
%     ax = subplot(2, 2, p);
%     hold on;
%     ax.ColorOrderIndex = c;
%     p = plot(t, x, 'linewidth', lineWidth);
%     p.Color(4) = alpha;
% end